#第一天
- 详细介绍一些课程要求
- Eclipse环境
- 如何在Eclipse环境下运行Karel
- Public void run(){run 所有执行内容，函数体} run的方法
- Tab增强程序可读性
- For循环
- While循环
- If条件语句

#第二天
- 变量
- 数据类型
- 运算符
- String类型
- 程序设计风格和文档
- BUG和GUI

#第三天
- pre-condition, post-condition，在注释中说明一个函数使用的前后条件是什么
- 选择语句 if switch
- common errors:
> - infinite loop: 死循环
> - off-by-one bug(OBOB):　缺一个问题
> > 解决方法：
> > >   while(frontIsClear()){
> > >       Prefom some operation;
> > >       Move();
> > >   }
> > > Perform the same operation for the final corner
- Decomposition: 分解成subproblem, 直到karel可执行
> 原则：　一个方法解决一个问题，一个方法有1到15代码，命名合适
- Top-down design和stepwise refinement
- Algorithm
- 方法命名：驼峰式

#第四天
- 源代码，目标代码，编译，数值语言
- 方法
- jvm虚拟机
- 面向对象
> - 类：对程序行为的封装
> - 对象：类的一个实例
> - 图形世界的对象：
> > - GLable
> > - GRect
> > - GOval
> > - GLine

#第五天
- 变量命名原则，一定初始化
- 构造函数
> - new GLabel(text, x, y)
> - new GRect(x, y, width, height)
> - new GOval(x, y, width, height)
> - new GLine(x1, y1, x2, y2)

#第六天
- 四则运算
- 强制类型划算
- 定义常数
- private修饰符，static变量

#第七天
- while循环
- 函数返回值，数学函数库
- 字符中和I/O

#第八天
- information hidden: 软件工程需要隐藏细节，黑盒原理
- client和implementer
- 实例变量，局部变量

#第九天
- 创建类，生命对象，构造函数，类变量
- 抽象类
- 编写类的基本套路
- Shadowing隐蔽现象
- 用计数器类，声明对象进调用构造函数，引用指向对象

#第十天
- 继承和多态
- 子类只能调用到父类的public方法和属性
- stacking-order图形叠加的顺序
- GObject
- intercade

#第十一天
- 事件驱动
- 图形
- GPploygon:多边形画法addedge, addvertex
- GCompound
- 监听鼠标：Responding to mouse events

#第十二天
- 枚举，字符，字符串
- applet和多媒体
- 递归算法

#第十三天
- 字符串处理， countUpperCase()中用到 str.length(),charAt()
- str.substring()
- str.indexOf()
- str.length()
- tokenizer
- encryption:凯撒密码，平移3个字母
- 范型

#第十四天
- 内存
- 堆
- 指针
- java集合框架

#第十五天
参数传递

#第十六天
- 数组
- 自增
- 线性表，栈，队列

#第十七天
- 多维数组
- 二叉查找树

#第十八天
- debug
- 调试方法
> - println在调用方法前，打印出变量值
> - unit test

#第十九天
- interface
- map: key, value
> map存储不分次序
- iterator

#第二十天
- GUI
- 容器，布局
- 菜单，工具栏和对话框

#第二十一天
- Text & Graphics
- MVC和swing


#第二十二天
- 服务器域名
- 内容随着窗口的大小进行调整后显示
- 组件：可以是容器，画布可以影响调整结果

#第二十三天
- 排序
- 线性排序和冒泡排序

#第二十四天
- 组织数据相关的设计思路
- 名词->classes
- 动词->methods
- 唯一ID标识，用于查找，或需要通过key来组织的结构

#第二十五天
- 讲解并行处理
- 多线程
- 网络

#第二十六天
- 标准java库
- jar打包
- applet

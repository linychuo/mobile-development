title: introdution mobile development
author: ivan
---
# Introduction mobile development
##### by ivan 2013.11
---
# What is mobile development?
- Low-power handheld devices, such as personal digital assistants, enterprise digital assistants or mobile phones
- Pre-installed, mobile software distribution platforms(play, app store), web app
---
# Syllabus
- Java basics and oop
- Android native development
- Html
- CSS
- JavaScript
- Cross platform mobile app development
- IOS
---
# Java basics & OOP
- Java basics
- OOP
- JDBC
---
# Android native deveopment
- Fundamental of android
- Data access and storage
- Network programming
- Advanced programming
- Game development framework

---
# Html
- BootStrap

---
# CSS
- Less
- Sass
- Stylus
---
# Javascript
- jQuery
- CoffeeScript
- JavaScript module pattern
---
# Cross platform mobile app development
- Html5
- PhoneGap
- Titanium
---
# IOS
- Objective-C
- XCode
- Distribution
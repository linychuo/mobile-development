##java基础和面向对象
- 参考斯坦福大学的***编程方法学***课程来讲解[http://www.stanford.edu/class/cs106a/](http://www.stanford.edu/class/cs106a/)
- 通过编程方法学的课程来穿插的讲解java的基础知识，包括面向对象的设计

##android开发
- 最终实现原生的大芝麻android客户端
- 在教学过程中，会在课程中讲解java的网络编程,io,多线程等知识以及应用
- 流行的android第三方包
- 课程期间也会做一些小的练习，比如计算器,任务管理等等
- 实现用手机来遥控slides

##html css javascript
- 讲解现有的开源的框架来了解html css和javascript代码的组织
- 剖析[jquery](https://github.com/jquery/jquery)的源码设计和组织
- 剖析[bootstrap](http://getbootstrap.com/)里的html设计，css的组织，javascript的组织
- 介绍[coffeescript](http://coffeescript.org/)以及使用，同样会以开源的项目来讲解
- 介绍[less](http://lesscss.org/),[sass](http://sass-lang.com/),[stylus](http://learnboost.github.io/stylus/)

##html5开发手机应用
- html5 vs html的区别
- 介绍phonegap以及使用
- 介绍titanium以及使用

##ios开发
- 环境的搭建以及简单objective-c语法讲解
- 流行的开源包的介绍以及使用
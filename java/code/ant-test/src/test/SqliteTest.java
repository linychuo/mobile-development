package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by ivan on 14-8-5.
 */
public class SqliteTest {

  public static void main(String[] args) {
    try {
      Class.forName("org.sqlite.JDBC");
      Connection connection = DriverManager.getConnection("jdbc:sqlite:a.db");
      Statement stmt = null;
      ResultSet rs = null;
      try {
        stmt = connection.createStatement();
        stmt.executeUpdate(
            "create table if not exists user(id INT, fullname TEXT, age INT, primary key (id))");
        stmt.executeUpdate("insert into user values(1, 'ivan', 20)");
        stmt.executeUpdate("insert into user values(2, '李永超', 30)");

        rs = stmt.executeQuery("select *from user");
        while (rs.next()) {
          System.out.println(
              "id = " + rs.getInt("id") + ", fullname = " + rs.getString("fullname") + ", age = "
              + rs.getInt("age"));
        }
      } finally {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (connection != null) {
          connection.close();
        }
      }
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}

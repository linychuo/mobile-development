ATM atm = new ATM()
menus = new ArrayList<Menu>()
input = new Scanner(System.in)
def userId = -1

def printMenus() {
    println "+++++++++++++++++++++++++++++"
    println "Main menu"
    menus.eachWithIndex() { obj, i ->
        println "[${i + 1}] : ${obj.label}"
    }
    println "+++++++++++++++++++++++++++++"
}

def capture(msg, closure) {
    result = -1
    hasError = true

    print msg
    while (hasError) {
        try {
            closure()
        } catch (e) {
            hasError = true
            input.nextLine()
            print "illegal input, please again: "
        }
    }

    result
}

def captureInt(msg, closure) {
    capture(msg, {
        result = input.nextInt()
        if (closure(result)) {
            hasError = false
        } else {
            hasError = true
            print "illegal input, please again: "
        }
    })
}

def captureDouble(msg, closure) {
    capture(msg, {
        result = input.nextDouble()
        if (closure(result)) {
            hasError = false
        } else {
            hasError = true
            print "illegal input, please again: "
        }
    })
}

menus.add(new Menu("check balance", {
    try {
        def balance = atm.queryBalance(userId)
        println "<<<<<<<<<<<<<<<<<<<<<The balance is ${balance}>>>>>>>>>>>"
        return false
    } catch (e) {
        print e
        return true
    }
}))
menus.add(new Menu("withdraw", {
    amount = captureDouble("Enter an amount to withdraw: ", { result > 0 })
    try {
        result = atm.withdraw(userId, amount)
        if (result) {
            println "<<<<<<<<<<<<<<withdraw successfully!>>>>>>>>>>"
        } else {
            println "<<<<<<<<<<<<withdraw failed!>>>>>>>>>>>>"
        }
        return false
    } catch (e) {
        print e
        return true
    }
}))
menus.add(new Menu("deposit", {
    amount = captureDouble("Enter an amount to deposit: ", { result > 0 })
    try {
        atm.deposit(userId, amount)
        return false
    } catch (e) {
        print e
        return true
    }
}))
menus.add(new Menu("print transactions", {
    try {
        transList = atm.queryTrans(userId)
        println "================================================================================"
        transList.each {
            println it
        }
        println "================================================================================"
        return false
    } catch (e) {
        print e
        return true
    }
}))
menus.add(new Menu("exit", {
    return true
}))

while (true) {
    userId = captureInt("Enter an id: ", { it >= 0 })
    while (true) {
        printMenus()
        menuId = captureInt("Enter a choice: ", { it > 0 && it <= menus.size() })
        if (menus.get(menuId - 1).click()) {
            break
        }
    }
}


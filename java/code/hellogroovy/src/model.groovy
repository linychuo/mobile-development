import groovy.transform.InheritConstructors
import groovy.transform.ToString

import java.text.SimpleDateFormat

@ToString
class Account {

    final int id
    final String name
    double balance
    final List<Transaction> transactions

    Account(int id, String name) {
        this.id = id
        this.name = name
        this.balance = 100
        this.transactions = new ArrayList<>()
    }
}

class Transaction {

    static df = new SimpleDateFormat("yyyy年MM月dd日HH:mm:ss");
    enum Type {

        W("取了"), D("存了")

        String desc

        Type(String desc) {
            this.desc = desc
        }

        String toString() {
            this.desc
        }
    }
    final Date date = new Date()
    Type type
    double balance
    double amount
    String description

    String toString() {
        return "你在${df.format(date)}${type}${amount}，你的账户剩余 ${balance}  \t[${description}]";
    }
}

class Menu {

    final String label
    final Closure closure

    Menu(String label, closure) {
        this.label = label
        this.closure = closure
    }

    def click() {
        return closure()
    }
}

@InheritConstructors
class AccountNotFound extends Exception {}

class ATM {

    final Account[] database

    ATM() {
        database = new Account[10]
        for (int i = 0; i < database.length; i++) {
            database[i] = new Account(i, "NO." + (i + 1))
        }
    }

    Account query(int id) {
        def account = database.find { it.id == id }
        if (account) {
            account
        } else {
            throw new AccountNotFound("account not found")
        }
    }

    boolean withdraw(int id, double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount must be positive!")
        }

        Account account = query(id)
        if (amount > account.balance) {
            return false
        } else {
            def newBalance = account.balance - amount
            account.balance = newBalance

            def trans = new Transaction(type: Transaction.Type.W, amount: amount,
                                        balance: account.balance, description: "withdraw" + amount)
            account.transactions.add(trans)
        }
    }

    def deposit(int id, double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount must be positive!")
        }

        Account account = query(id)
        account.setBalance(account.balance + amount)
        def trans =
                new Transaction(type: Transaction.Type.D, amount: amount,
                                balance: account.getBalance(),
                                description: "deposit " + amount)
        account.transactions.add(trans);
    }

    double queryBalance(int id) {
        Account account = query(id)
        account.balance
    }

    def queryTrans(int id) {
        Account account = query(id)
        account.transactions
    }
}

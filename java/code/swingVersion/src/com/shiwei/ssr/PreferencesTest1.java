package com.shiwei.ssr;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;

import javax.swing.*;


public class PreferencesTest1 {

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        PreferencesFrame1 frame = new PreferencesFrame1();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
      }
    });
  }
}


class PreferencesFrame1 extends JFrame {

  private static final int DEFAULT_WIDTH = 300;
  private static final int DEFAULT_HEIGHT = 200;

  public PreferencesFrame1() {
    // get position, size, title from preferences

    Preferences root = Preferences.userRoot();
    final Preferences node = root.node("com/ivan");
    int left = node.getInt("left", 0);
    int top = node.getInt("top", 0);
    int width = node.getInt("width", DEFAULT_WIDTH);
    int height = node.getInt("height", DEFAULT_HEIGHT);
    setBounds(left, top, width, height);

    // if no title given, ask user

    String title = node.get("title", "");
    if (title.equals("")) {
      title = JOptionPane.showInputDialog("Please supply a frame title:");
    }
    if (title == null) {
      title = "";
    }
    setTitle(title);

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent event) {
        node.putInt("left", getX());
        node.putInt("top", getY());
        node.putInt("width", getWidth());
        node.putInt("height", getHeight());
        node.put("title", getTitle());
        System.exit(0);
      }
    });
  }
}

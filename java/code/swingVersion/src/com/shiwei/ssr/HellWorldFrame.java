package com.shiwei.ssr;

import java.awt.*;

public class HellWorldFrame extends BaseJFrame {

  public HellWorldFrame() {
    super();
    int left = getIntPrefs("left");
    int top = getIntPrefs("top");
    int width = getIntPrefs("width");
    int height = getIntPrefs("height");
    setBounds(left, top, width, height);
    setTitle("helloworld");
    putPrefs("title", "helloworld");
    addWindowListener(this);
  }

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        HellWorldFrame frame = new HellWorldFrame();
        frame.setVisible(true);
      }
    });
  }
}

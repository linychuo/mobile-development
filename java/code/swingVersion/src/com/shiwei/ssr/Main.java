package com.shiwei.ssr;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;


public class Main {

  public static void main(String[] args) {
    // URL url = Main.class.getResource("a.gif");
    // Image image = new ImageIcon(url).getImage();
    try (InputStream is = Main.class.getResourceAsStream("a.txt")) {
      try (Scanner scan = new Scanner(is)) {
        while (scan.hasNext()) {
          System.out.println(scan.next());
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

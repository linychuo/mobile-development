package com.shiwei.ssr;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleTest {

  public static void main(String[] args) {
    // Locale locale = new Locale("zh", "CN");
    Locale locale = Locale.getDefault();
    System.out.println(locale.getDisplayCountry());
    // for (Locale test : Locale.getAvailableLocales()) {
    // System.out.println(test.getDisplayCountry() + ", " + test.getLanguage());
    // }
    // System.out.println(locale.getCountry() + ", " + locale.getDisplayName());
    // Properties prop = System.getProperties();
    // for (Entry<Object, Object> entry : prop.entrySet()) {
    // System.out.println(entry.getKey() + " = " + entry.getValue());
    // }
    ResourceBundle bundle = ResourceBundle.getBundle("App", locale);
    System.out.println(bundle.getString("name"));
  }
}

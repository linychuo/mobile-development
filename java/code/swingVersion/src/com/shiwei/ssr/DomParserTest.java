package com.shiwei.ssr;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class DomParserTest {

  public static void main(String[] args) throws ParserConfigurationException, SAXException,
                                                IOException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();

    InputStream ins = DomParserTest.class.getResourceAsStream("test.xml");
    Document doc = builder.parse(ins);

    Element root = doc.getDocumentElement();
    // System.out.println(root.getTagName() + ", " + root.getNodeName() + ", "
    // + root.getAttribute("name"));

    NodeList children = root.getChildNodes();
    // System.out.println(children.getLength());
    for (int i = 0; i < children.getLength(); i++) {
      Node child = children.item(i);
      if (child.getNodeType() == Node.ELEMENT_NODE) {
        // System.out.println(child.getNodeType());
        System.out.println(child.getNodeName());
        NodeList subChildren = child.getChildNodes();
        for (int j = 0; j < subChildren.getLength(); j++) {
          Node subChild = subChildren.item(j);
          if (subChild.getNodeType() == Node.ELEMENT_NODE) {
            System.out.println("\t" + subChild.getNodeName() + " = " + subChild.getTextContent());
          }
        }
      }
    }
  }
}

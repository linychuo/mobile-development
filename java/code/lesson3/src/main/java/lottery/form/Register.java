package lottery.form;

import com.linychuo.metrofx.control.Form;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import lottery.Main;


/**
 * Created by ivan on 14-8-13.
 */
public class Register extends Form {

  private Main main;

  public Register(Main main) {
    this.main = main;
  }

  @Override
  public Pane build() {
    GridPane gridPane = new GridPane();
    gridPane.setVgap(20);
    gridPane.setHgap(20);
    gridPane.setPadding(new Insets(10));
    gridPane.setAlignment(Pos.CENTER);

    Label titleLbl = new Label("注册");
    titleLbl.setId("title");
    GridPane.setHalignment(titleLbl, HPos.LEFT);
    gridPane.add(titleLbl, 0, 0);

    Hyperlink loginLnk = new Hyperlink("登录");
    loginLnk.setOnAction(e -> {
      main.gotoLogin();
    });
    GridPane.setHalignment(loginLnk, HPos.RIGHT);
    gridPane.add(loginLnk, 1, 0);

    Label loginLbl = new Label("用户名");
    gridPane.add(loginLbl, 0, 1);

    TextField loginTxt = new TextField();
    validate(loginTxt);
    gridPane.add(loginTxt, 1, 1);

    Label pwdLbl = new Label("密码");
    gridPane.add(pwdLbl, 0, 2);

    PasswordField pwdTxt = new PasswordField();
    validate(pwdTxt);
    gridPane.add(pwdTxt, 1, 2);

    Label pwdConfirmLbl = new Label("确认密码");
    gridPane.add(pwdConfirmLbl, 0, 3);

    PasswordField pwdConfirmTxt = new PasswordField();
    validate(pwdConfirmTxt);
    gridPane.add(pwdConfirmTxt, 1, 3);

    HBox hBox = new HBox();
    hBox.setAlignment(Pos.BOTTOM_RIGHT);
    hBox.setSpacing(10);
    Button loginBtn = new Button("确定");
    Button resetBtn = new Button("清空");
    hBox.getChildren().addAll(loginBtn, resetBtn);
    gridPane.add(hBox, 1, 4, 1, 1);

    ColumnConstraints column1 = new ColumnConstraints();
    column1.setHalignment(HPos.RIGHT);
    gridPane.getColumnConstraints().add(column1);

    gridPane.getStylesheets().add(Main.class.getResource("lottery.css").toExternalForm());
    return gridPane;
  }
}

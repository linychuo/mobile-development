package lottery.form;

import com.linychuo.metrofx.control.Form;
import com.linychuo.metrofx.control.dialog.AlertDialog;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import lottery.Main;

/**
 * Created by ivan on 14-8-13.
 */
public class Login extends Form {

  private Main main;

  public Login(Main main) {
    this.main = main;
  }

  @Override
  public Pane build() {
    GridPane gridPane = new GridPane();
    gridPane.setVgap(20);
    gridPane.setHgap(20);
    gridPane.setPadding(new Insets(10));
    gridPane.setAlignment(Pos.CENTER);

    Label titleLbl = new Label("登录");
    titleLbl.setId("title");
    GridPane.setHalignment(titleLbl, HPos.LEFT);
    gridPane.add(titleLbl, 0, 0, 2, 1);

    Label loginLbl = new Label("用户名");
    gridPane.add(loginLbl, 0, 1);

    TextField loginTxt = new TextField();
    validate(loginTxt);
    gridPane.add(loginTxt, 1, 1);

    Label pwdLbl = new Label("密码");
    gridPane.add(pwdLbl, 0, 2);

    PasswordField pwdTxt = new PasswordField();
    validate(pwdTxt);
    gridPane.add(pwdTxt, 1, 2);

    HBox hBox = new HBox();
    hBox.setSpacing(10);
    hBox.setAlignment(Pos.BOTTOM_RIGHT);
    Button loginBtn = new Button("登录");
    loginBtn.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        AlertDialog dialog = new AlertDialog.Builder().setTitle("系统提示").setMsg("不能为空!").build();
        dialog.show();
      }
    });
    loginBtn.requestFocus();

    Button regBtn = new Button("注册");
    regBtn.setOnAction(e -> {
      main.gotoRegister();
    });

    hBox.getChildren().addAll(loginBtn, regBtn);
    gridPane.add(hBox, 1, 3, 1, 1);

    ColumnConstraints column1 = new ColumnConstraints();
    column1.setHalignment(HPos.RIGHT);
    gridPane.getColumnConstraints().add(column1);

    gridPane.getStylesheets().add(Main.class.getResource("lottery.css").toExternalForm());

    return gridPane;
  }
}

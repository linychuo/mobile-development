package lottery;

import com.linychuo.metrofx.control.Window;

import javafx.application.Application;
import javafx.stage.Stage;
import lottery.form.Login;
import lottery.form.Register;

/**
 * Created by ivan on 14-8-13.
 */
public class Main extends Application {

  private Login login;
  private Register register;
  private Window window;

  @Override
  public void init() throws Exception {
    login = new Login(this);
    register = new Register(this);
  }

  public void gotoRegister() {
    window.show(register);
  }

  public void gotoLogin() {
    window.show(login);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = new Window(primaryStage, "抽奖系统", 500, 300);
    window.show(login);
  }

  public static void main(String[] args) {
    launch(args);
  }
}

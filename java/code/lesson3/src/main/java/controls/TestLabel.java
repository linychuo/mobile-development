package controls;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-13.
 */
public class TestLabel extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    Label label3 = new Label("A label that needs to be wrapped");
    label3.setPrefWidth(40);
    label3.setWrapText(true);
    label3.setFont(new Font("Arial", 18));
//    label3.setRotate(158);
//    label3.setTranslateY(50);

    label3.setOnMouseEntered(e -> {
      label3.setScaleX(1.5);
      label3.setScaleY(1.5);
    });

    label3.setOnMouseExited(e -> {
      label3.setScaleX(1);
      label3.setScaleY(1);
    });

    Scene scene = new Scene(label3, 200, 100);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Test label");
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

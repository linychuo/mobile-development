package controls;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-13.
 */
public class TestRadioButton extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    final ToggleGroup group = new ToggleGroup();

    //A radio button with an empty string for its label
    RadioButton rb1 = new RadioButton();
    //Setting a text label
    rb1.setText("Home");
    rb1.setToggleGroup(group);
    rb1.setSelected(true);
    //A radio button with the specified label
    RadioButton rb2 = new RadioButton("Calendar");
    rb2.setToggleGroup(group);

    Image imageOk = new Image(getClass().getResourceAsStream("ok.png"));
    RadioButton rb3 = new RadioButton("Agree");
    rb3.setGraphic(new ImageView(imageOk));
    rb3.setToggleGroup(group);

    group.selectedToggleProperty().addListener(
        (ObservableValue<? extends Toggle> ov, Toggle oldToggle,
         Toggle newToggle) -> {
          if (group.getSelectedToggle() != null) {
            System.out.println(oldToggle + "," + newToggle);
          }
        });

    ToggleButton tb1 = new ToggleButton("toggle");

    VBox vBox = new VBox();
    vBox.setPadding(new Insets(10));
    vBox.setSpacing(10);
    vBox.getChildren().addAll(rb1, rb2, rb3, tb1);

    Scene scene = new Scene(vBox, 300, 150);
//    scene.setRoot();
    primaryStage.setScene(scene);
    primaryStage.setTitle("Test radio button");
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

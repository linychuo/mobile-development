package controls;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-13.
 */
public class TestButton extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    Button button1 = new Button();
//A button with the specified text caption.
    Button button2 = new Button("Accept");
//A button with the specified text caption and icon.
    Image imageOk = new Image(getClass().getResourceAsStream("ok.png"));
    Button button3 = new Button("Accept", new ImageView(imageOk));
    button3.setStyle("-fx-border-color:red;");
    button1.setGraphic(new ImageView(imageOk));

    button2.setOnAction(e -> {
      System.out.println("Accepted");
    });

    DropShadow shadow = new DropShadow();
    //Adding the shadow when the mouse cursor is on
    button3.addEventFilter(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("================");
      }
    });
    button3.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
      System.out.println(e.getSource() + ", " + e.getTarget());
      button3.setEffect(shadow);
    });

    //Removing the shadow when the mouse cursor is off
    button3.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
      button3.setEffect(null);
    });

    HBox hBox = new HBox();
    hBox.setPadding(new Insets(10));
    hBox.setSpacing(20);
    hBox.getChildren().addAll(button1, button2, button3);

    Scene scene = new Scene(hBox, 300, 100);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Test BUTTON");
    primaryStage.show();

  }

  public static void main(String[] args) {
    launch(args);
  }
}

package controls;


import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-13.
 */
public class TextCheckBox extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    //A checkbox without a caption
    CheckBox cb1 = new CheckBox();
    //A checkbox with a string caption
    CheckBox cb2 = new CheckBox("Second");

    cb1.setText("First");
    cb1.setSelected(true);
    cb1.selectedProperty().addListener(
        (ObservableValue<? extends Boolean> ov,
         Boolean old_val, Boolean new_val) -> {
          System.out.println(old_val + ", " + new_val);
        });

    cb2.selectedProperty().addListener(
        (ObservableValue<? extends Boolean> ov,
         Boolean old_val, Boolean new_val) -> {
          System.out.println(old_val + ", " + new_val);
        });

    ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(
        "First", "Second", new Separator(), "Third")
    );

    cb.setTooltip(new Tooltip("Select the language"));
    cb.getSelectionModel().selectedIndexProperty().addListener((ov, oldValue, newValue) -> {
      System.out.println(oldValue + ", " + newValue);
    });

    cb.getSelectionModel().selectedItemProperty()
        .addListener((ObservableValue ov, Object oldValue, Object newValue) -> {
          System.out.println(oldValue + ", " + newValue);
        });
//    cb.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
//      @Override
//      public void changed(ObservableValue<? extends Number> observable, Number oldValue,
//                          Number newValue) {
//
//      }
//    });

    TextField a = new TextField(":ddddd");
    a.setPromptText("请在这里输入....");
//    a.setDisable(true);

    ListView<String> list = new ListView<>();
    ObservableList<String> items = FXCollections.observableArrayList(
        "Single", "Double", "Suite", "Family App");
    list.setItems(items);

    VBox vBox = new VBox();
    vBox.setPadding(new Insets(10));
    vBox.setSpacing(10);
    vBox.getChildren().addAll(cb1, cb2, cb, a, list);

    Scene scene = new Scene(vBox, 300, 500);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Test radio button");
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

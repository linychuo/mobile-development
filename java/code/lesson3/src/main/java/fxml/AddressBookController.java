package fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * Created by ivan on 14-8-19.
 */
public class AddressBookController {

  @FXML
  public void clickMe(ActionEvent event) {
    System.out.println("============");
  }

  @FXML
  public void clickMeAgain(ActionEvent event) {
    System.out.println("++++++++++++++++++++");
  }
}

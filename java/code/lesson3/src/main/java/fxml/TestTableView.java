package fxml;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-19.
 */
public class TestTableView extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    VBox vbox = new VBox();
    vbox.setSpacing(5);
    vbox.setPadding(new Insets(10, 10, 0, 10));
    Label label = new Label("Address Book");
    label.setFont(new Font("Arial", 20));

    List<Person> personList = new ArrayList<>();
    personList.add(new Person("a", "aa", "aa@qq.com"));
    personList.add(new Person("b", "bb", "bb@qq.com"));
    personList.add(new Person("c", "cc", "cc@qq.com"));
    personList.add(new Person("d", "dd", "dd@qq.com"));
    personList.add(new Person("e", "ee", "ee@qq.com"));

    ObservableList<Person> list = FXCollections.observableList(personList);
    TableView<Person> tableView = new TableView<Person>();

    TableColumn firstNameCol = new TableColumn("First Name");
    firstNameCol.setMinWidth(100);
    firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));

    TableColumn lastNameCol = new TableColumn("Last Name");
    lastNameCol.setMinWidth(100);
    lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));

    TableColumn emailCol = new TableColumn("Email");
    emailCol.setMinWidth(100);
    emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));

    tableView.setItems(list);
    tableView.getColumns().addAll(firstNameCol, lastNameCol, emailCol);

    HBox hBox = new HBox();
    hBox.setSpacing(10);
    final TextField addFirstName = new TextField();
    addFirstName.setPromptText("First Name");
    addFirstName.setMaxWidth(firstNameCol.getWidth());

    final TextField addLastName = new TextField();
    addLastName.setMaxWidth(lastNameCol.getWidth());
    addLastName.setPromptText("Last Name");

    final TextField addEmail = new TextField();
    addEmail.setMaxWidth(emailCol.getWidth());
    addEmail.setPromptText("Email");

    final Button addButton = new Button("Add");
    addButton.setOnAction((ActionEvent e) -> {
      list.add(new Person(
          addFirstName.getText().trim(),
          addLastName.getText().trim(),
          addEmail.getText().trim()
      ));
      addFirstName.clear();
      addLastName.clear();
      addEmail.clear();
    });
    hBox.getChildren().addAll(addFirstName, addLastName, addEmail, addButton);

    vbox.getChildren().addAll(label, tableView, hBox);

    Scene scene = new Scene(vbox, 800, 500);
    primaryStage.setTitle("Table View Sample");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

package fxml;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by ivan on 14-8-19.
 */
public class OtherPerson {

  private final SimpleStringProperty firstName;
  private final SimpleStringProperty lastName;
  private final SimpleStringProperty email;

  public OtherPerson(String fName, String lName, String email) {
    this.firstName = new SimpleStringProperty(fName);
    this.lastName = new SimpleStringProperty(lName);
    this.email = new SimpleStringProperty(email);
  }

  public String getFirstName() {
    return firstName.get();
  }

  public void setFirstName(String fName) {
    firstName.set(fName);
  }

  public String getLastName() {
    return lastName.get();
  }

  public void setLastName(String fName) {
    lastName.set(fName);
  }

  public String getEmail() {
    return email.get();
  }

  public void setEmail(String fName) {
    email.set(fName);
  }
}

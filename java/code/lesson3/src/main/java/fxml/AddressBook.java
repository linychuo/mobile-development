package fxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-19.
 */
public class AddressBook extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent p = FXMLLoader.load(getClass().getResource("addressbook.fxml"));

    Scene scene = new Scene(p, 800, 600);
    primaryStage.setTitle("Address book");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

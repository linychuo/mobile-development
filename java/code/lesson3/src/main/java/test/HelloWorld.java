package test;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-12.
 */
public class HelloWorld extends Application {

  @Override
  public void start(Stage primaryStage) {
//    Button btn = new Button("Say 'hello world'");
////    btn.setText("Say 'Hello World'");
//    btn.setOnAction(new EventHandler<ActionEvent>() {
//
//      @Override
//      public void handle(ActionEvent event) {
//        System.out.println("Hello World!");
//      }
//    });
//    btn.setPrefSize(100, 20);
//
//    Rectangle helpIcon = new Rectangle(50, 50);
//    helpIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
//                                        new Stop[]{
//                                            new Stop(0, Color.web("#4977A3")),
//                                            new Stop(0.5, Color.web("#B0C6DA")),
//                                            new Stop(1, Color.web("#9CB6CF")),}));
//    helpIcon.setStroke(Color.web("#D0E6FA"));
//    helpIcon.setArcHeight(3.5);
//    helpIcon.setArcWidth(3.5);
//
//    Text helpText = new Text("?");
//    helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
////    helpText.setFill(Color.WHITE);
//    helpText.setStroke(Color.RED);
//
////    StackPane root = new StackPane();
////    root.getChildren().addAll(helpIcon, btn);
//    VBox root = new VBox();
//    root.getChildren().addAll(btn, helpIcon, helpText);

    HBox root = new HBox();

    addStackPane(root);
    Scene scene = new Scene(root, 300, 250);

    primaryStage.setTitle("Hello World!");
    primaryStage.setScene(scene);
    primaryStage.show();
  }


  public void addStackPane(HBox hb) {
    StackPane stack = new StackPane();
    Rectangle helpIcon = new Rectangle(30.0, 25.0);
    helpIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
                                        new Stop[]{
                                            new Stop(0, Color.web("#4977A3")),
                                            new Stop(0.5, Color.web("#B0C6DA")),
                                            new Stop(1, Color.web("#9CB6CF")),}));
    helpIcon.setStroke(Color.web("#D0E6FA"));
    helpIcon.setArcHeight(3.5);
    helpIcon.setArcWidth(3.5);

    Text helpText = new Text("?");
    helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
    helpText.setFill(Color.WHITE);
    helpText.setStroke(Color.web("#7080A0"));

    stack.getChildren().addAll(helpIcon, helpText);
    stack.setAlignment(Pos.CENTER_RIGHT);     // Right-justify nodes in stack
    StackPane.setMargin(helpText, new Insets(0, 10, 0, 0)); // Center "?"

    hb.getChildren().add(stack);            // Add stack pane to HBox object
    HBox.setHgrow(stack, Priority.ALWAYS);    // Give stack any extra space
  }

  public static void main(String[] args) {
    launch(args);
  }
}

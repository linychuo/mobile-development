package test;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Created by ivan on 14-8-22.
 */
public class TestEvent extends Application {

  private double initX;
  private double initY;

  @Override
  public void start(Stage primaryStage) throws Exception {
//    TextField f = new TextField();
//    f.setOnKeyPressed(new EventHandler<KeyEvent>() {
//      @Override
//      public void handle(KeyEvent event) {
//        System.out.println("1. " + event.getCharacter());
//      }
//    });
//    f.setOnKeyReleased(new EventHandler<KeyEvent>() {
//      @Override
//      public void handle(KeyEvent event) {
//        System.out.println("2. " + event.getCharacter());
//      }
//    });
//    f.setOnKeyTyped(new EventHandler<KeyEvent>() {
//      @Override
//      public void handle(KeyEvent event) {
//        System.out.println("3. " + event.getCode().isArrowKey());
//      }
//    });

    VBox vBox = new VBox();
//    vBox.setOnMouseEntered(new EventHandler<MouseEvent>() {
//      @Override
//      public void handle(MouseEvent event) {
//        System.out.println("xxxxxxxxxxxxxx");
//        event.consume();
//      }
//    });
    vBox.addEventFilter(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxx");
        event.consume();
      }
    });

    Button b = new Button("点我");
    b.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        System.out.println("2222222222");
      }
    });
    b.setOnMouseClicked(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        System.out.println("ddddddddddddd" + event.getButton());
      }
    });

    b.setOnMousePressed(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        initX = event.getSceneX();
        initY = event.getSceneY();
      }
    });

    b.setOnMouseReleased(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        initX = event.getSceneX();
        initY = event.getSceneY();
      }
    });

    b.addEventFilter(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("1111111111111111111111112");
//        event.consume();
      }
    });

//    b.setOnMouseEntered(new EventHandler<MouseEvent>() {
//      @Override
//      public void handle(MouseEvent event) {
//        b.setStyle("-fx-background-color:red");
//        System.out.println("_+_+_+_+_+_");
//      }
//    });
    b.setOnMouseExited(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        b.setStyle("-fx-background-color:green");
      }
    });

    b.setOnMouseMoved(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
//        System.out.println(event.getX() + "," + event.getY());
      }
    });

    b.setOnMouseDragged(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("ddddddddddadsafdsafdsafdsa");
        b.setTranslateX(event.getSceneX() - initX);
        b.setTranslateY(event.getSceneY() - initY);
      }
    });

    b.setOnMouseDragEntered(new EventHandler<MouseDragEvent>() {
      @Override
      public void handle(MouseDragEvent event) {
        System.out.println("11111111111111111");
      }
    });

    b.setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {
      @Override
      public void handle(MouseDragEvent event) {
        System.out.println("--------------------");
        b.setTranslateX(event.getX());
        b.setTranslateY(event.getY());
      }
    });

    b.setOnMouseDragExited(new EventHandler<MouseDragEvent>() {
      @Override
      public void handle(MouseDragEvent event) {
        System.out.println("===============");
      }
    });

    b.setOnMouseDragOver(new EventHandler<MouseDragEvent>() {
      @Override
      public void handle(MouseDragEvent event) {
        System.out.println("++++++++++++++++++");
      }
    });
    primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
      @Override
      public void handle(WindowEvent event) {
        System.out.println("window close");
      }
    });

    vBox.getChildren().add(b);
    Scene scene = new Scene(vBox, 300, 200);
    primaryStage.setScene(scene);
    primaryStage.show();


  }
}

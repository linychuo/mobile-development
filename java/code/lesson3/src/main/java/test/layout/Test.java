package test.layout;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import test.layout.LoginLayout;
import test.layout.MainLayout;
import test.layout.RegLayout;

/**
 * Created by ivan on 14-8-18.
 */
public class Test extends Application {

  private Scene scene;

  public void gotoLogin() {
    LoginLayout login = new LoginLayout(this);
    scene.setRoot(login.build());
  }

  public void gotoReg() {
    RegLayout reg = new RegLayout(this);
    scene.setRoot(reg.build());
  }


  public void gotoMain() {
    MainLayout reg = new MainLayout(this);
    scene.setRoot(reg.build());
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    MainLayout main = new MainLayout(this);
    scene = new Scene(main.build(), 500, 300);

    primaryStage.setTitle("抽奖系统");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}

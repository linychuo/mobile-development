package test.layout;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * Created by ivan on 14-8-18.
 */
public class RegLayout {

  private Test test;

  public RegLayout(Test test) {
    this.test = test;
  }


  public Pane build() {
    VBox content = new VBox(10);
    Label label = new Label("注册");
    label.setFont(Font.font(20));

    TextField loginNameTxt = new TextField();
    loginNameTxt.setPromptText("请输入用户名");

    PasswordField pwdTxt = new PasswordField();
    pwdTxt.setPromptText("请输入密码");

    PasswordField pwdConfirmTxt = new PasswordField();
    pwdConfirmTxt.setPromptText("请输入密码");

    Button loginBtn = new Button("去登录");
    loginBtn.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        test.gotoLogin();
      }
    });

    Hyperlink mainLnk = new Hyperlink("回到首页");
    mainLnk.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        test.gotoMain();
      }
    });

    content.getChildren().addAll(label, loginNameTxt, pwdTxt, pwdConfirmTxt, loginBtn, mainLnk);

    return content;
  }
}

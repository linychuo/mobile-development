package test;


import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Created by ivan on 14-8-20.
 */
public class ListViewDemo extends Application {

  ListView<Person> list = new ListView<>();
  ObservableList<Person>
      data =
      FXCollections.observableArrayList(new Person("李永超", "1.png"), new Person("蒋梦娟", "2.png"),
                                        new Person("胡影影", "3.png"), new Person("宋贺磊", "4.png"),
                                        new Person("张毅", "5.png"), new Person("张伟民", "6.png"),
                                        new Person("刘晓来", "7.png"));
  final Label label = new Label();

  @Override
  public void start(Stage primaryStage) throws Exception {
    label.setMinHeight(30);

    list.setItems(data);
    list.setCellFactory(new Callback<ListView<Person>, ListCell<Person>>() {
      @Override
      public ListCell<Person> call(ListView<Person> param) {
        return new CustomCell();
      }
    });

    list.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Person>() {
      @Override
      public void changed(ObservableValue<? extends Person> observable, Person oldValue,
                          Person newValue) {
        label.setText(newValue.getName());
      }
    });

    VBox box = new VBox();
    box.getChildren().addAll(list, label);
    VBox.setVgrow(list, Priority.ALWAYS);

    Scene scene = new Scene(box, 200, 300);
    scene.getStylesheets().addAll(getClass().getResource("test.css").toExternalForm());
    primaryStage.setScene(scene);
    primaryStage.setTitle("ListViewSample");
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }

  private static class CustomCell extends ListCell<Person> {

    @Override
    protected void updateItem(Person item, boolean empty) {
      super.updateItem(item, empty);
      if (item != null) {
        HBox main = new HBox();
        main.setPadding(new Insets(5));
        main.setSpacing(10);

        ImageView avatar = new ImageView(new Image(ListViewDemo.class.getResourceAsStream(
            item.getAvatar())));
        Text nameTxt = new Text(item.getName());
        main.getChildren().addAll(avatar, nameTxt);
        setGraphic(main);
      } else {
        setGraphic(null);
      }
    }

  }
}

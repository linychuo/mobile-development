package test.scene;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * Created by ivan on 14-8-18.
 */
public class MainScene {

  private Test test;

  public MainScene(Test test) {
    this.test = test;
  }

  public Scene build() {
    VBox content = new VBox(10);
    Label label = new Label("抽奖系统");
    label.setFont(Font.font(20));

    Button loginBtn = new Button("登录");
    loginBtn.setPrefSize(100, 80);
    loginBtn.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        //切换到登录界面
//        test.gotoLogin();
      }
    });

    Button regBtn = new Button("注册");
    regBtn.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        test.gotoReg();
      }
    });
    regBtn.setPrefSize(100, 80);

    content.getChildren().addAll(label, loginBtn, regBtn);
    return new Scene(content, 700, 400);
  }
}

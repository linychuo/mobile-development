package test.scene;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-18.
 */
public class Test extends Application {

  private Stage stage;

  public void gotoReg() {
    RegScene scene = new RegScene();
    stage.setScene(scene.build());
    stage.setTitle("这是注册页面");
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    stage = primaryStage;
    MainScene scene = new MainScene(this);
    stage.setScene(scene.build());
    stage.setTitle("这是首页");
    stage.show();
  }
}

package test.fxml.resources;

/**
 * Created by ivan on 14-8-25.
 */
public interface R {

  String mainScreenID = "main";
  String mainScreenFile = "main.fxml";

  String loginScreenID = "login";
  String loginScreenFile = "login.fxml";

  String registerScreenID = "register";
  String registerScreenFile = "register.fxml";
}

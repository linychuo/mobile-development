package test.fxml;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import test.fxml.base.DisplayBody;
import test.fxml.resources.R;

/**
 * Created by ivan on 14-8-20.
 */
public class Test extends Application {

  private DisplayBody mainContainer;

  @Override
  public void init() throws Exception {
    mainContainer = new DisplayBody();
    mainContainer.loadScreen(R.mainScreenID, R.mainScreenFile);
    mainContainer.loadScreen(R.loginScreenID, R.loginScreenFile);
    mainContainer.loadScreen(R.registerScreenID, R.registerScreenFile);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    mainContainer.setScreen(R.mainScreenID);
    Scene scene = new Scene(mainContainer, 400, 200);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

package test.fxml.base;

import java.util.HashMap;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import test.fxml.resources.R;

/**
 * Created by ivan on 14-8-25.
 */
public class DisplayBody extends StackPane {

  private Object param;

  private HashMap<String, Node> screens = new HashMap<>();

  public DisplayBody() {
    super();
  }

  public boolean loadScreen(String name, String resource) {
    try {
      FXMLLoader myLoader = new FXMLLoader(R.class.getResource(resource));
      Parent loadScreen = (Parent) myLoader.load();
      ControlledScreen myScreenController = ((ControlledScreen) myLoader.getController());
      myScreenController.setScreenParent(this);
      screens.put(name, loadScreen);
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  public boolean setScreen(final String name) {
    if (screens.get(name) != null) {
      if (!getChildren().isEmpty()) {
        getChildren().remove(0);
        getChildren().add(0, screens.get(name));
      } else {
        getChildren().add(screens.get(name));
      }
      return true;
    } else {
      System.out.println("screen hasn't been loaded!!! \n");
      return false;
    }
  }


  public Object getParam() {
    return param;
  }

  public void setParam(Object param) {
    this.param = param;
  }
}

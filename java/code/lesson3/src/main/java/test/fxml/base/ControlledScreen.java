package test.fxml.base;

/**
 * Created by ivan on 14-8-25.
 */
public abstract class ControlledScreen {

  protected DisplayBody myController;

  protected void setScreenParent(DisplayBody screenPage) {
    myController = screenPage;
  }
}

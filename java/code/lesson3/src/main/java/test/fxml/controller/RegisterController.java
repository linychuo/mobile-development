package test.fxml.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import test.fxml.base.ControlledScreen;
import test.fxml.resources.R;

/**
 * Created by ivan on 14-8-25.
 */
public class RegisterController extends ControlledScreen implements Initializable {

  @Override
  public void initialize(URL location, ResourceBundle resources) {
  }

  @FXML
  public void gotoLogin(ActionEvent event) {
    myController.setScreen(R.loginScreenID);
  }

}

package sample.networking;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by ivan on 14-8-21.
 */
public class Client {

  public static void main(String[] args) throws Exception {
    Socket socket = new Socket("127.0.0.1", 8189);
    Scanner in = new Scanner(socket.getInputStream());
    PrintWriter writer = new PrintWriter(socket.getOutputStream());
    // send request data
    writer.print("dd");
    writer.flush();
    socket.shutdownOutput();
// now socket is half-closed
// read response data
    while (in.hasNextLine()) {
      String line = in.nextLine();
      System.out.println(line);
    }
    socket.close();
  }
}

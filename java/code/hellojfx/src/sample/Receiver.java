package sample;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by ivan on 14-7-30.
 */
public class Receiver {

  public static void main(String[] args) throws IOException {
    byte[] buffer = new byte[65507];
    DatagramSocket ds = new DatagramSocket(9394);
    ds.setSoTimeout(5000);
    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

    while (true) {
      System.out.println("-------------");
      ds.receive(packet);
      System.out.println(packet.getLength());
      String s = new String(packet.getData(), 0, packet.getLength());
//      System.out.println(packet.getAddress() + ":" + packet.getPort() + "    →    " + s);
      System.out.println(s);
    }
  }
}

package sample.second;

import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-7-29.
 */
public class Login extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    Parent
        root =
        FXMLLoader.load(getClass().getResource("main.fxml"), ResourceBundle.getBundle("main"));
    Scene scene = new Scene(root, 300, 275);
//    scene.getStylesheets().addAll(().getResource("main.css").toExternalForm());
    stage.setTitle("FXML Welcome");
    stage.setScene(scene);
    stage.show();
  }


  public static void main(String[] args) {
    launch(args);
  }
}

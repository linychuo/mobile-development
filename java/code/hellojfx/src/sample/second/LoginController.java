package sample.second;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * Created by ivan on 14-7-29.
 */
public class LoginController implements Initializable {


  @FXML
  private Text actiontarget;
  @FXML
  private TextField uname_txt;


  @FXML
  protected void handleSubmitButtonAction(ActionEvent event) {
    actiontarget.setText("Sign in button pressed");
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    //链接数据库
    //设置初始化值
    System.out.println(location + ", " + resources);
    uname_txt.setText(resources.getString("name_label"));
  }
}

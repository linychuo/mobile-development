package sample.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-7-31.
 */
public class FlowPaneTest extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    FlowPane flow = new FlowPane();
    flow.setPadding(new Insets(5, 0, 5, 0));
    flow.setVgap(4);
    flow.setHgap(4);
    flow.setPrefWrapLength(170); // preferred width allows for two columns
    flow.setStyle("-fx-background-color: DAE6F3;");

    ImageView pages[] = new ImageView[8];
    for (int i = 0; i < 8; i++) {
      pages[i] = new ImageView(
          new Image(FlowPaneTest.class.getResourceAsStream(
              "chart_" + (i + 1) + ".png")));
      flow.getChildren().add(pages[i]);
    }

    Scene scene = new Scene(flow);
    stage.setTitle("FlowPane");
    stage.setScene(scene);
    stage.show();
  }
}

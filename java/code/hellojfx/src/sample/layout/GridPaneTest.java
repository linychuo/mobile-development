package sample.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-7-31.
 */
public class GridPaneTest extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    GridPane grid = new GridPane();
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(10));

    Text one = new Text("one 一");
    one.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(one, 0, 0);

    Text two = new Text("two 二");
    two.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(two, 0, 1);

    Text three = new Text("three 三");
    three.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(three, 0, 2);

    Text four = new Text("four 四");
    four.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(four, 1, 0);

    Text five = new Text("five 五");
    five.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(five, 1, 1);

    Text six = new Text("six 六");
    six.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(six, 1, 2);

    Text seven = new Text("seven 七");
    seven.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(seven, 2, 0);

    Text eight = new Text("eight 八");
    eight.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(eight, 2, 1);

    Text nine = new Text("nine 九");
    nine.setFont(Font.font("Arial", FontWeight.BOLD, 20));
    grid.add(nine, 2, 2);

    grid.setGridLinesVisible(true);
    Scene scene = new Scene(grid);
    stage.setTitle("GridPane");
    stage.setScene(scene);
    stage.show();
  }
}

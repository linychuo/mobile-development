package sample.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-13.
 */
public class AnchorPaneTest extends Application {

  public AnchorPane addAnchorPane() {
    Button buttonSave = new Button("Save");
    Button buttonCancel = new Button("Cancel");

    HBox hb = new HBox();
    hb.setPadding(new Insets(0, 10, 10, 10));
    hb.setSpacing(10);
    hb.getChildren().addAll(buttonSave, buttonCancel);

    AnchorPane anchorpane = new AnchorPane();
    anchorpane.getChildren().addAll(hb);   // Add grid from Example 1-5
    AnchorPane.setBottomAnchor(hb, 8.0);
    AnchorPane.setRightAnchor(hb, 8.0);

    return anchorpane;
  }

  @Override
  public void start(Stage primaryStage) throws Exception {

    Scene scene = new Scene(addAnchorPane(), 200, 80);
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}

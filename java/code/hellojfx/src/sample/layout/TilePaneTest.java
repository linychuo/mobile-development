package sample.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-7-31.
 */
public class TilePaneTest extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    TilePane tile = new TilePane();
    tile.setPadding(new Insets(5, 0, 5, 0));
    tile.setVgap(4);
    tile.setHgap(4);
    tile.setPrefColumns(2);
    tile.setStyle("-fx-background-color: DAE6F3;");

    ImageView pages[] = new ImageView[8];
    for (int i = 0; i < 8; i++) {
      pages[i] = new ImageView(
          new Image(TilePaneTest.class.getResourceAsStream(
              "chart_" + (i + 1) + ".png")));
      tile.getChildren().add(pages[i]);
    }

    Scene scene = new Scene(tile);
    stage.setTitle("TilePane");
    stage.setScene(scene);
    stage.show();
  }
}

package sample;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by ivan on 14-7-29.
 */
public class Ping {

  public static void main(String[] args) throws IOException {
    Socket socket = new Socket("127.0.0.1", 8080);
    DataInputStream di = new DataInputStream(socket.getInputStream());
    DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

    dout.writeInt(110);
    System.out.println(di.readInt());
  }

}

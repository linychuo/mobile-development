package sample.controls;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-7-31.
 */
public class Main extends Application {

  @Override
  public void start(Stage stage) throws Exception {
    VBox vbox = new VBox();
    MenuBar menuBar = new MenuBar();

    Menu menuFile = new Menu("File");
    Menu menuEdit = new Menu("Edit");
    Menu menuView = new Menu("View");
    menuBar.getMenus().addAll(menuFile, menuEdit, menuView);
    vbox.getChildren().addAll(menuBar);

    ListView<String> list = new ListView<String>();
    ObservableList<String> items = FXCollections.observableArrayList(
        "Single", "Double", "Suite", "Family App");
    list.setItems(items);
    vbox.getChildren().addAll(list);

    Scene scene = new Scene(vbox, 200, 400, Color.TRANSPARENT);

    stage.setTitle("Yoyo");
    stage.setScene(scene);
    stage.show();
  }
}

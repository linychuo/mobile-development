package sample;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by ivan on 14-7-29.
 */
public class Broadcast {

  public static void main(String[] args) throws IOException {

    DatagramSocket ds = new DatagramSocket();
    while (true) {
      String msg = "李永超：大家好，下午好";
      Object d = new Object();
      DatagramPacket dp = new DatagramPacket(msg.getBytes(), msg.getBytes().length,
                                             InetAddress.getByName("10.10.52.60"), 8080);
      ds.send(dp);
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
//    ds.close();
  }
}

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by ivan on 14-7-29.
 */
public class PingServer {

  public static void main(String[] args) throws IOException {
    ServerSocket server = new ServerSocket(8080);
    Socket socket = server.accept();
    DataInputStream di = new DataInputStream(socket.getInputStream());
    DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

    System.out.println(di.readInt());
    dout.writeInt(111);
  }
}

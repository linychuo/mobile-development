package model;

import util.RandomUtil;

/**
 * Created by ivan on 14-8-7.
 */
public class User {

  private String loginName;
  private String password;
  private String cardNo;

  public User(String loginName, String password) {
    this.loginName = loginName;
    this.password = password;
    this.cardNo = RandomUtil.buildCardNumber();
  }

  public User(String loginName, String password, String cardNo) {
    this.loginName = loginName;
    this.password = password;
    this.cardNo = cardNo;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public String getLoginName() {
    return loginName;
  }

  public String getCardNo() {
    return cardNo;
  }
}

import java.util.Arrays;
import java.util.Set;

import model.User;
import store.DataStore;
import util.RandomUtil;

/**
 * Created by ivan on 14-8-7.
 */
public class LotterySystem {

  private DataStore<User> ums;

  public LotterySystem(DataStore ums) {
    this.ums = ums;
  }

  public User getUser(String loginName, String password) {
    return ums.query(loginName, password);
  }

  public boolean register(User user) {
    if (ums.query(user.getLoginName(), user.getPassword()) == null) {
      ums.save(user);
      return true;
    }
    return false;
  }

  public boolean draw(User user) {
    //记录抽奖的日志
    Set<String> ids = RandomUtil.buildCardNumbers();
    System.out.println(Arrays.toString(ids.toArray()));
    return ids.contains(user.getCardNo());
  }
}

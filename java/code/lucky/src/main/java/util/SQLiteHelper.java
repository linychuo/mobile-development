package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import store.RowProcess;

/**
 * Created by ivan on 14-8-7.
 */
public class SQLiteHelper {

  public void execute(String sql, Object[] params) throws SQLException {
    try (Connection connection = getConnection()) {
      try (PreparedStatement stmt = connection.prepareStatement(sql)) {
        fillParams(stmt, params);
        stmt.executeUpdate();
      }
    } catch (ClassNotFoundException e) {
      //ignore
    } catch (SQLException e) {
      throw e;
    }

  }

  public <T> T load(String sql, Object[] params, RowProcess<T> processor) throws SQLException {
    try (Connection connection = getConnection()) {
      try (PreparedStatement stmt = connection.prepareStatement(sql)) {
        fillParams(stmt, params);
        try (ResultSet rs = stmt.executeQuery()) {
          if (rs.next()) {
            return processor.handler(rs);
          }
        }
      }
    } catch (ClassNotFoundException e) {
      //ignore
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
      throw e;
    }
    return null;
  }

  private void fillParams(PreparedStatement stmt, Object[] params) throws SQLException {
    if (params != null) {
      for (int i = 0; i < params.length; i++) {
        stmt.setObject(i + 1, params[i]);
      }
    }
  }

  private Connection getConnection() throws ClassNotFoundException, SQLException {
    Class.forName("org.sqlite.JDBC");
    return DriverManager.getConnection("jdbc:sqlite:lucky.db");
  }
}

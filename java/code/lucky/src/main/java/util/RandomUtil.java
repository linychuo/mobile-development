package util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by ivan on 14-8-7.
 */
public class RandomUtil {

  private static Random random = new Random();

  private RandomUtil() {

  }

  public static int rand() {
    return random.nextInt(10);
  }

  public static String buildCardNumber() {
    return "" + rand() + rand() + rand() + rand();
  }

  public static Set<String> buildCardNumbers() {
    Set<String> ids = new HashSet<String>();
    for (int j = 0; j < 5; j++) {
      ids.add(buildCardNumber());
    }
    return ids;
  }
}

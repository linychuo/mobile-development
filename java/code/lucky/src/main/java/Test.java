import model.User;
import store.impl.UserSQLiteStore;

/**
 * Created by ivan on 14-8-7.
 */
public class Test {

  public static void main(String[] args) {
    LotterySystem system = new LotterySystem(new UserSQLiteStore());
//    model.User user = new model.User("test", "test");
//    boolean result = system.register(user);
//    System.out.println(result);

//    model.User u = system.getUser("test", "' or '1 = 1");
    User u = system.getUser("test", "test");
    if (u != null) {
      System.out.println("登录成功");
      boolean result = system.draw(u);
      System.out.println(u.getCardNo() + ", " + result);
    } else {
      System.out.println("登录失败");
    }
  }
}

package store;

/**
 * Created by ivan on 14-8-7.
 */
public interface DataStore<T> {

  public T query(Object... params);

  public void save(T t);

}

package store;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ivan on 3/6/14.
 */
public interface RowProcess<T> {

  T handler(ResultSet rs) throws SQLException;

}

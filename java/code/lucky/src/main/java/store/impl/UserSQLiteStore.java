package store.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;
import store.DataStore;
import store.RowProcess;
import util.SQLiteHelper;

/**
 * Created by ivan on 14-8-7.
 */
public class UserSQLiteStore implements DataStore<User> {

  private SQLiteHelper helper;

  public UserSQLiteStore() {
    this.helper = new SQLiteHelper();
    try {
      this.helper.execute(
          "create table  if not exists user (login_name text, password text, card_no text, primary key(login_name, card_no))",
          null);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public User query(Object... params) {
    try {
      String sql = "select *from user where login_name = ? and password = ?";
      RowProcess<User> ru = new RowProcess<User>() {
        @Override
        public User handler(ResultSet rs) throws SQLException {
          return new User(rs.getString("login_name"),
                          rs.getString("password"),
                          rs.getString("card_no"));
        }
      };

      return helper.load(sql, params, ru);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  public void save(User user) {
    String sql = "insert into user values(?, ?, ?)";
    try {
      helper.execute(sql, new Object[]{user.getLoginName(), user.getPassword(), user.getCardNo()});
    } catch (SQLException e) {
      //
    }
  }
}

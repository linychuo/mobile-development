package store.impl;

import java.util.ArrayList;
import java.util.List;

import model.User;
import store.DataStore;

/**
 * Created by ivan on 14-8-7.
 */
public class UserMemoryStore implements DataStore<User> {

  private List<User> db = new ArrayList<User>();

  @Override
  public User query(Object... params) {
    for (User item : db) {
      if (item.getLoginName().equals(params[0]) && item.getPassword().equals(params[1])) {
        return item;
      }
    }
    return null;
  }

  public void save(User user) {
    db.add(user);
  }
}

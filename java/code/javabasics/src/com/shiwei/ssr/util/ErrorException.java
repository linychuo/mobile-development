package com.shiwei.ssr.util;

public class ErrorException extends RuntimeException {

  private static final long serialVersionUID = -2582528272190574007L;

  public ErrorException(String msg) {
    super(msg);
  }

  public ErrorException(Exception ex) {
    super(ex.getClass().getName() + ": " + ex.getMessage());
  }
}

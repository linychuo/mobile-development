package com.shiwei.ssr.util;

public class CancelledException extends RuntimeException {

  private static final long serialVersionUID = -5082975039726824833L;

  public CancelledException() {
    /* Empty */
  }

}

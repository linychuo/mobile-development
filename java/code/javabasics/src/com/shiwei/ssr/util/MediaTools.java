package com.shiwei.ssr.util;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class MediaTools {

  private MediaTools() {
    /* Empty */
  }

  public static Image loadImage(Image image) {
    MediaTracker tracker = new MediaTracker(createEmptyContainer());
    tracker.addImage(image, 0);
    try {
      tracker.waitForID(0);
    } catch (InterruptedException ex) {
      throw new ErrorException("Image loading process interrupted");
    }
    return image;
  }

  public static Image createImage(int[][] array) {
    int height = array.length;
    int width = array[0].length;
    int[] pixels = new int[width * height];
    for (int i = 0; i < height; i++) {
      System.arraycopy(array[i], 0, pixels, i * width, width);
    }
    return createImage(pixels, width, height);
  }

  public static Image createImage(int[] pixels, int width, int height) {
    Image image =
        Toolkit.getDefaultToolkit().createImage(
            new MemoryImageSource(width, height, pixels, 0, width));
    loadImage(image);
    return image;
  }

  public static Image createImage(InputStream in) {
    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      for (int ch = in.read(); ch != -1; ch = in.read()) {
        out.write(ch);
      }
      Image image = Toolkit.getDefaultToolkit().createImage(out.toByteArray());
      loadImage(image);
      return image;
    } catch (Exception ex) {
      throw new ErrorException("Exception: " + ex);
    }
  }

  public static Image createImage(String[] hexData) {
    return createImage(new HexInputStream(hexData));
  }

  public static Container createEmptyContainer() {
    return new EmptyContainer();
  }

  private static class EmptyContainer extends Container {
    private static final long serialVersionUID = -8861122984227452897L;

    public void update(Graphics g) {
      paint(g);
    }
  }

  private static class HexInputStream extends InputStream {
    private String[] hex;
    private int arrayIndex;
    private int charIndex;

    public HexInputStream(String[] hexData) {
      hex = hexData;
      arrayIndex = 0;
      charIndex = 0;
    }

    public int read() {
      if (arrayIndex >= hex.length) return -1;
      if (charIndex >= hex[arrayIndex].length()) {
        arrayIndex++;
        charIndex = 0;
        return read();
      }
      int data = Character.digit(hex[arrayIndex].charAt(charIndex++), 16) << 4;
      data |= Character.digit(hex[arrayIndex].charAt(charIndex++), 16);
      return data;
    }

  }
}

package com.shiwei.ssr.util;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.shiwei.ssr.Program;

public class Utils {
  private static boolean swingChecked;
  private static boolean isSwingAvailable;

  private Utils() {

  }

  public static <K, V> HashMap<K, V> newHashMap() {
    return new HashMap<K, V>();
  }

  public static <E> ArrayList<E> newArrayList() {
    return new ArrayList<E>();
  }

  public static Frame getEnclosingFrame(Object obj) {
    if (obj instanceof Program) obj = ((Program) obj).getContentPane();
    while (obj != null && !(obj instanceof Frame)) {
      obj = ((Frame) obj).getParent();
    }
    return (Frame) obj;
  }

  public static boolean isSwingAvailable() {
    if (!swingChecked) {
      swingChecked = true;
      isSwingAvailable = false;
      if (compareVersion("1.2") >= 0) {
        try {
          isSwingAvailable = Class.forName("javax.swing.JComponent") != null;
        } catch (Exception ex) {
          /* Empty */
        }
      }
    }
    return isSwingAvailable;
  }

  public static int compareVersion(String version) {
    return compareVersion(System.getProperty("java.version"), version);
  }

  public static int compareVersion(String v1, String v2) {
    StringTokenizer t1 = new StringTokenizer(v1, ".");
    StringTokenizer t2 = new StringTokenizer(v2, ".");
    while (t1.hasMoreTokens() && t2.hasMoreTokens()) {
      int n1 = Integer.parseInt(t1.nextToken());
      int n2 = Integer.parseInt(t2.nextToken());
      if (n1 != n2) return (n1 < n2) ? -1 : +1;
    }
    if (t1.hasMoreTokens()) return +1;
    if (t2.hasMoreTokens()) return -1;
    return 0;
  }
}

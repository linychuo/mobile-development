package com.shiwei.ssr.util;

public class CharacterQueue {

  private StringBuilder sb;
  private boolean isWaiting;

  public CharacterQueue() {
    sb = new StringBuilder();
  }

  public char dequeue() {
    synchronized (this) {
      while (sb.length() == 0) {
        try {
          isWaiting = true;
          wait();
          isWaiting = false;
        } catch (InterruptedException ex) {
          /* Empty */
        }
      }
      char ch = sb.charAt(0);
      sb.deleteCharAt(0);
      return ch;
    }
  }

  public void enqueue(char c) {
    synchronized (this) {
      sb.append(c);
      notifyAll();
    }
  }

  public void enqueue(String s) {
    synchronized (this) {
      sb.append(s);
      notifyAll();
    }
  }

  public boolean isWaiting() {
    return isWaiting;
  }
}

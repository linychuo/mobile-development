package com.shiwei.ssr;

import com.shiwei.ssr.io.IOConsole;

public abstract class ConsoleProgram extends Program {

  public ConsoleProgram() {
    super();
    add(getConsole(), CENTER);
    showMe();
  }

  public ConsoleProgram(String title) {
    super(title);
    add(getConsole(), CENTER);
    showMe();
  }

  @Override
  protected IOConsole createConsole() {
    return new IOConsole();
  }
}

package com.shiwei.ssr;

public class Add2Dialog extends DialogProgram {
  public void run() {
    println("This program adds two numbers.");
    int n1 = readInt("Enter n1: ");
    int n2 = readInt("Enter n2: ");
    int total = n1 + n2;
    println("The total is " + total + ".");
  }

  public static void main(String[] args) {
    Add2Dialog test = new Add2Dialog();
    test.run();
  }
}

package com.shiwei.ssr.io;

import java.awt.Color;
import java.awt.Component;
import java.io.IOException;


public class SystemConsoleModel implements ConsoleModel {
  private IOConsole console;
  private final StringBuilder text;

  public SystemConsoleModel() {
    this.text = new StringBuilder();
  }

  @Override
  public IOConsole getConsole() {
    return console;
  }

  @Override
  public void setConsole(IOConsole console) {
    this.console = console;
  }

  @Override
  public void print(String str, int style) {
    System.out.print(str);
    text.append(str);
  }

  @Override
  public String readLine() {
    System.out.flush();
    String line = "";
    int ch;
    try {
      while (true) {
        ch = System.in.read();
        if (ch == -1 || ch == '\n') break;
        line += (char) ch;
      }
    } catch (IOException ex) {
      /* Empty */
    }
    return line;
  }

  @Override
  public void clear() {

  }

  @Override
  public String getText() {
    return text.toString();
  }

  @Override
  public String getText(int start, int end) {
    return text.substring(start, end);
  }

  @Override
  public int getLength() {
    return text.length();
  }

  @Override
  public void setInputColor(Color color) {

  }

  @Override
  public void setInputStyle(int style) {

  }

  @Override
  public void setErrorStyle(int style) {

  }

  @Override
  public void setErrorColor(Color color) {

  }

  @Override
  public Component getConsolePane() {
    return null;
  }

  @Override
  public Component getTextPane() {
    return null;
  }

  @Override
  public void requestFocus() {

  }

}

package com.shiwei.ssr.io;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import com.shiwei.ssr.util.CharacterQueue;
import com.shiwei.ssr.util.ErrorException;

public class StandardConsoleModel implements KeyListener, ConsoleModel {
  private CharacterQueue buffer;
  private SimpleAttributeSet outputAttributes;
  private SimpleAttributeSet inputAttributes;
  private SimpleAttributeSet errorAttributes;
  private JScrollPane scrollPane;
  private JTextPane textPane;
  private Document document;
  private int base;
  private IOConsole console;
  private Object inputLock;
  private Object outputLock;

  public StandardConsoleModel() {
    scrollPane =
        new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    textPane = new JTextPane();
    textPane.addKeyListener(this);
    scrollPane.setViewportView(textPane);
    document = textPane.getDocument();
    outputAttributes = new SimpleAttributeSet();
    inputAttributes = new SimpleAttributeSet();
    errorAttributes = new SimpleAttributeSet();
    buffer = new CharacterQueue();
    inputLock = new Object();
    outputLock = new Object();
    base = 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#getConsole()
   */
  @Override
  public IOConsole getConsole() {
    return console;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#setConsole(com.shiwei.ssr.IOConsole)
   */
  @Override
  public void setConsole(IOConsole console) {
    this.console = console;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#print(java.lang.String, int)
   */
  @Override
  public void print(String str, int style) {
    synchronized (outputLock) {
      insert(str, base, style);
      base += str.length();
      setCaretPosition(base);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#readLine()
   */
  @Override
  public String readLine() {
    synchronized (inputLock) {
      base = getLength();
      int dot;
      char ch;
      setCaretPosition(base);
      while ((ch = buffer.dequeue()) != '\n' && ch != '\r') {
        if (getCaretPosition() < base) {
          setCaretPosition(getLength());
        }
        dot = getSelectionStart();
        switch (ch) {
          case '\b':
          case '\177':
            if (dot == getSelectionEnd()) {
              if (dot > base) {
                delete(dot - 1, dot);
                dot--;
              }
            } else {
              dot = deleteSelection();
            }
            break;
          default:
            if (dot != getSelectionEnd()) {
              dot = deleteSelection();
            }
            insert("" + ch, dot, INPUT_STYLE);
            dot++;
        }
        if (dot != -1) {
          select(dot, dot);
          setCaretPosition(dot);
        }
      }
      int len = getLength() - base;
      String line = getText(base, base + len);
      insert("\n", base + len, OUTPUT_STYLE);
      base += len + 1;
      return line;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#clear()
   */
  @Override
  public void clear() {
    textPane.setText("");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#getText()
   */
  @Override
  public String getText() {
    return textPane.getText();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#getText(int, int)
   */
  @Override
  public String getText(int start, int end) {
    try {
      return document.getText(start, end - start);
    } catch (BadLocationException ex) {
      throw new ErrorException(ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#getLength()
   */
  @Override
  public int getLength() {
    return document.getLength();
  }

  public void setInputStyle(int style) {
    if (getLength() != 0) {
      throw new ErrorException("Console styles and colors cannot be changed after I/O has started.");
    }
    inputAttributes.addAttribute(StyleConstants.Bold, new Boolean((style & Font.BOLD) != 0));
    inputAttributes.addAttribute(StyleConstants.Italic, new Boolean((style & Font.ITALIC) != 0));
  }

  public void setInputColor(Color color) {
    if (getLength() != 0) {
      throw new ErrorException("Console styles and colors cannot be changed after I/O has started.");
    }
    inputAttributes.addAttribute(StyleConstants.Foreground, color);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#setErrorStyle(int)
   */
  @Override
  public void setErrorStyle(int style) {
    if (getLength() != 0) {
      throw new ErrorException("Console styles and colors cannot be changed after I/O has started.");
    }
    errorAttributes.addAttribute(StyleConstants.Bold, new Boolean((style & Font.BOLD) != 0));
    errorAttributes.addAttribute(StyleConstants.Italic, new Boolean((style & Font.ITALIC) != 0));
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#setErrorColor(java.awt.Color)
   */
  @Override
  public void setErrorColor(Color color) {
    if (getLength() != 0) {
      throw new ErrorException("Console styles and colors cannot be changed after I/O has started.");
    }
    errorAttributes.addAttribute(StyleConstants.Foreground, color);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#getConsolePane()
   */
  @Override
  public Component getConsolePane() {
    return scrollPane;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#getTextPane()
   */
  @Override
  public Component getTextPane() {
    return textPane;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.shiwei.ssr.IConsoleModel#requestFocus()
   */
  @Override
  public void requestFocus() {
    if (textPane != null) textPane.requestFocus();
  }

  private void insert(String str, int dot, int style) {
    try {
      SimpleAttributeSet attributes = outputAttributes;
      switch (style) {
        case INPUT_STYLE:
          attributes = inputAttributes;
          break;
        case ERROR_STYLE:
          attributes = errorAttributes;
          break;
      }
      document.insertString(dot, str, attributes);
    } catch (BadLocationException ex) {
      /* Empty */
    }
  }

  private void setCaretPosition(int pos) {
    textPane.setCaretPosition(pos);
  }

  private int getCaretPosition() {
    return textPane.getCaretPosition();
  }

  private int getSelectionStart() {
    return textPane.getSelectionStart();
  }

  private int getSelectionEnd() {
    return textPane.getSelectionEnd();
  }

  private void delete(int p1, int p2) {
    try {
      document.remove(p1, p2 - p1);
    } catch (BadLocationException ex) {
      throw new ErrorException(ex);
    }
  }

  private int deleteSelection() {
    int start = Math.max(base, getSelectionStart());
    int end = getSelectionEnd();
    if (end <= base) return getLength();
    delete(start, end);
    return start;
  }

  private void select(int p1, int p2) {
    textPane.select(p1, p2);
  }

  @Override
  public void keyTyped(KeyEvent e) {
    buffer.enqueue(e.getKeyChar());
    e.consume();
  }

  @Override
  public void keyPressed(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_LEFT:
        buffer.enqueue('\002');
        break;
      case KeyEvent.VK_RIGHT:
        buffer.enqueue('\006');
        break;
    }
    e.consume();
  }

  @Override
  public void keyReleased(KeyEvent e) {
    e.consume();
  }

}

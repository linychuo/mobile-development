package com.shiwei.ssr.io;

import java.awt.Color;
import java.awt.Component;


public interface ConsoleModel {

  public static final int OUTPUT_STYLE = 0;
  public static final int INPUT_STYLE = 1;
  public static final int ERROR_STYLE = 2;

  IOConsole getConsole();

  void setConsole(IOConsole console);

  void print(String str, int style);

  String readLine();

  void clear();

  String getText();

  String getText(int start, int end);

  int getLength();

  void setInputColor(Color color);

  void setInputStyle(int style);

  void setErrorStyle(int style);

  void setErrorColor(Color color);

  Component getConsolePane();

  Component getTextPane();

  void requestFocus();

}

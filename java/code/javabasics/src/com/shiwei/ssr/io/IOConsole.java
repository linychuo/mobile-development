package com.shiwei.ssr.io;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

import com.shiwei.ssr.util.ErrorException;

public class IOConsole extends Container implements IOModel {

  private static final long serialVersionUID = 3854689806510368917L;
  public static final IOConsole SYSTEM_CONSOLE = new SystemConsole();
  public static final String LINE_SEPARATOR = System.getProperty("line.separator");

  private ConsoleModel consoleModel;
  private BufferedReader reader;
  private PrintWriter writer;
  private boolean exceptionOnError;

  public IOConsole() {
    consoleModel = createConsoleModel();
    consoleModel.setConsole(this);

    setBackground(Color.white);
    setInputColor(Color.blue);
    setInputStyle(Font.BOLD);
    setErrorColor(Color.red);
    setErrorStyle(Font.BOLD);

    setLayout(new BorderLayout());
    Component consolePane = consoleModel.getConsolePane();
    if (consolePane != null) {
      setLayout(new BorderLayout());
      add(consolePane, BorderLayout.CENTER);
    }
    exceptionOnError = false;
  }

  public void setInputColor(Color color) {
    consoleModel.setInputColor(color);
  }

  public void setInputStyle(int style) {
    consoleModel.setInputStyle(style);
  }

  public void setErrorColor(Color color) {
    consoleModel.setErrorColor(color);
  }

  public void setErrorStyle(int style) {
    consoleModel.setErrorStyle(style);
  }

  public void setBackground(Color color) {
    Component textPane = consoleModel.getTextPane();
    if (textPane != null) textPane.setBackground(color);
    super.setBackground(color);
  }

  public void setForeground(Color color) {
    Component textPane = consoleModel.getTextPane();
    if (textPane != null) textPane.setForeground(color);
    super.setForeground(color);
  }

  protected ConsoleModel createConsoleModel() {
    return new StandardConsoleModel();
  }

  public void clear() {
    consoleModel.clear();
  }

  @Override
  public void print(String value) {
    getWriter().print(value);
  }

  @Override
  public void print(boolean x) {
    print("" + x);
  }

  @Override
  public void print(char c) {
    print("" + c);
  }

  @Override
  public void print(double x) {
    print("" + x);
  }

  @Override
  public void print(float x) {
    print("" + x);
  }

  @Override
  public void print(int x) {
    print("" + x);
  }

  @Override
  public void print(long x) {
    print("" + x);
  }

  @Override
  public void print(Object o) {
    print("" + o);
  }

  @Override
  public void println() {
    getWriter().println();
  }

  @Override
  public void println(String value) {
    getWriter().println(value);
  }

  @Override
  public void println(boolean x) {
    println("" + x);
  }

  @Override
  public void println(char c) {
    println("" + c);
  }

  @Override
  public void println(double x) {
    println("" + x);
  }

  @Override
  public void println(float x) {
    println("" + x);

  }

  @Override
  public void println(int x) {
    println("" + x);
  }

  @Override
  public void println(long x) {
    println("" + x);
  }

  @Override
  public void println(Object o) {
    println("" + o);
  }

  @Override
  public void showErrorMsg(String msg) {
    consoleModel.print(msg, ConsoleModel.ERROR_STYLE);
    consoleModel.print("\n", ConsoleModel.ERROR_STYLE);
  }

  @Override
  public final String readLine() {
    return readLine(null);
  }

  @Override
  public String readLine(String prompt) {
    if (prompt != null) print(prompt);
    consoleModel.requestFocus();
    try {
      String str = getReader().readLine();
      return str;
    } catch (IOException ex) {
      throw new ErrorException(ex);
    }
  }

  @Override
  public int readInt() {
    return readInt(null);
  }

  @Override
  public int readInt(String prompt) {
    String msg = null;
    while (true) {
      String line = readLine(prompt);
      try {
        int n = Integer.parseInt(line);
        if (n >= Integer.MIN_VALUE && n <= Integer.MAX_VALUE) return n;
        msg = "Value is outside the range [" + Integer.MIN_VALUE + ":" + Integer.MAX_VALUE + "]";
      } catch (NumberFormatException ex) {
        msg = "Illegal numeric format";
      }
      showErrorMsg(msg);
      if (prompt == null) prompt = "Retry: ";
    }
  }

  @Override
  public double readDouble() {
    return readDouble(null);
  }

  @Override
  public double readDouble(String prompt) {
    String msg = null;
    while (true) {
      String line = readLine(prompt);
      try {
        double d = Double.valueOf(line).doubleValue();
        if (d >= Double.NEGATIVE_INFINITY && d <= Double.POSITIVE_INFINITY) return d;
        msg =
            "Value is outside the range [" + Double.NEGATIVE_INFINITY + ":"
                + Double.POSITIVE_INFINITY + "]";
      } catch (NumberFormatException ex) {
        msg = "Illegal numeric format";
      }
      showErrorMsg(msg);
      if (prompt == null) prompt = "Retry: ";
    }
  }

  @Override
  public boolean readBoolean() {
    return readBoolean(null);
  }

  @Override
  public boolean readBoolean(String prompt) {
    while (true) {
      String line = readLine(prompt);
      if (line == null) {
        throw new ErrorException("End of file encountered");
      } else if (line.equalsIgnoreCase("true")) {
        return true;
      } else if (line.equalsIgnoreCase("false")) {
        return false;
      } else {
        if (exceptionOnError) {
          throw new ErrorException("Illegal boolean format");
        }
        showErrorMsg("Illegal boolean format");
        if (prompt == null) prompt = "Retry: ";
      }
    }
  }

  public BufferedReader getReader() {
    if (reader == null) {
      reader = new BufferedReader(new ConsoleReader(consoleModel));
    }
    return reader;
  }

  public void setReader(BufferedReader reader) {
    this.reader = reader;
  }

  public PrintWriter getWriter() {
    if (writer == null) {
      writer = new PrintWriter(new ConsoleWriter(consoleModel));
    }
    return writer;
  }

  public void setWriter(PrintWriter writer) {
    this.writer = writer;
  }

  public boolean isExceptionOnError() {
    return exceptionOnError;
  }

  public void setExceptionOnError(boolean exceptionOnError) {
    this.exceptionOnError = exceptionOnError;
  }

  private static class ConsoleReader extends Reader {
    private ConsoleModel consoleModel;
    private String buffer;

    public ConsoleReader(ConsoleModel cp) {
      buffer = null;
      consoleModel = cp;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
      if (len == 0) return 0;
      if (buffer == null) {
        buffer = consoleModel.readLine();
        if (buffer == null) return -1;
        buffer += "\n";
      }
      if (len < buffer.length()) {
        buffer.getChars(0, len, cbuf, off);
        buffer = buffer.substring(len);
      } else {
        len = buffer.length();
        buffer.getChars(0, len, cbuf, off);
        buffer = null;
      }
      return len;
    }

    @Override
    public void close() throws IOException {

    }
  }

  private static class ConsoleWriter extends Writer {

    private ConsoleModel consoleModel;

    public ConsoleWriter(ConsoleModel cp) {
      consoleModel = cp;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
      String str = new String(cbuf, off, len);
      int start = 0;
      int eol;
      while ((eol = str.indexOf(IOConsole.LINE_SEPARATOR, start)) != -1) {
        consoleModel.print(str.substring(start, eol), ConsoleModel.OUTPUT_STYLE);
        consoleModel.print("\n", ConsoleModel.OUTPUT_STYLE);
        start = eol + IOConsole.LINE_SEPARATOR.length();
      }
      consoleModel.print(str.substring(start), ConsoleModel.OUTPUT_STYLE);
    }

    @Override
    public void flush() throws IOException {

    }

    @Override
    public void close() throws IOException {

    }
  }

  private static class SystemConsole extends IOConsole {
    private static final long serialVersionUID = -1878565995194990583L;

    @Override
    protected ConsoleModel createConsoleModel() {
      return new SystemConsoleModel();
    }
  }
}

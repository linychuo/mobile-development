package com.shiwei.ssr.io;

import java.awt.Component;
import java.lang.reflect.Constructor;

import javax.swing.JPanel;

import com.shiwei.ssr.dialog.AWTDialogModel;
import com.shiwei.ssr.dialog.DialogModel;
import com.shiwei.ssr.util.CancelledException;
import com.shiwei.ssr.util.ErrorException;
import com.shiwei.ssr.util.Utils;

public class IODialog implements IOModel {
  private Component myComponent;
  private DialogModel model;
  private boolean exceptionOnError;
  private boolean allowCancel;
  private IOConsole myConsole;
  private StringBuilder outputLine;

  public IODialog() {
    this(null);
  }

  public IODialog(Component owner) {
    this.myComponent = owner;
    model = createModel();
    outputLine = new StringBuilder();
    exceptionOnError = false;
    allowCancel = false;
  }

  @Override
  public void print(String value) {
    outputLine.append(value);
  }

  @Override
  public final void print(boolean x) {
    print("" + x);
  }

  @Override
  public final void print(char c) {
    print("" + c);
  }

  @Override
  public final void print(double x) {
    print("" + x);
  }

  @Override
  public final void print(float x) {
    print("" + x);
  }

  @Override
  public final void print(int x) {
    print("" + x);
  }

  @Override
  public final void print(long x) {
    print("" + x);
  }

  @Override
  public final void print(Object o) {
    print("" + o);
  }

  @Override
  public void println() {
    model.popupMessage(outputLine.toString());
    outputLine.delete(0, outputLine.length());
  }

  @Override
  public void println(String value) {
    print(value);
    println();
  }

  @Override
  public final void println(boolean x) {
    println("" + x);

  }

  @Override
  public final void println(char c) {
    println("" + c);
  }

  @Override
  public final void println(double x) {
    println("" + x);
  }

  @Override
  public final void println(float x) {
    println("" + x);
  }

  @Override
  public final void println(int x) {
    println("" + x);
  }

  @Override
  public final void println(long x) {
    println("" + x);
  }

  @Override
  public final void println(Object o) {
    println("" + o);
  }

  @Override
  public void showErrorMsg(String msg) {
    model.popupErrorMessage(msg);
  }

  @Override
  public final String readLine() {
    return readLine(null);
  }

  @Override
  public String readLine(String prompt) {
    prompt = (prompt == null) ? outputLine.toString() : outputLine.append(prompt).toString();
    outputLine.delete(0, outputLine.length());
    String line;
    while ((line = model.popupLineInputDialog(prompt, allowCancel)) == null) {
      if (allowCancel) throw new CancelledException();
    }
    return line;
  }

  @Override
  public final int readInt() {
    return readInt(null);
  }

  @Override
  public int readInt(String prompt) {
    while (true) {
      String line = readLine(prompt);
      try {
        int n = Integer.parseInt(line);
        if (n < Integer.MIN_VALUE || n > Integer.MAX_VALUE) {
          signalError("Value is outside the range [" + Integer.MIN_VALUE + ":" + Integer.MAX_VALUE
              + "]");
        }
        return n;
      } catch (NumberFormatException ex) {
        signalError("Illegal integer format");
      }
    }
  }

  @Override
  public final double readDouble() {
    return readDouble(null);
  }

  @Override
  public double readDouble(String prompt) {
    while (true) {
      String line = readLine(prompt);
      try {
        double d = Double.valueOf(line).doubleValue();
        if (d < Double.NEGATIVE_INFINITY || d > Double.POSITIVE_INFINITY) {
          signalError("Value is outside the range [" + Double.NEGATIVE_INFINITY + ":"
              + Double.POSITIVE_INFINITY + "]");
        }
        return d;
      } catch (NumberFormatException ex) {
        signalError("Illegal numeric format");
      }
    }
  }

  @Override
  public final boolean readBoolean() {
    return readBoolean(null);
  }

  @Override
  public boolean readBoolean(String prompt) {
    prompt = (prompt == null) ? outputLine.toString() : outputLine.append(prompt).toString();
    outputLine.delete(0, outputLine.length());
    Boolean choice;
    while ((choice = model.popupBooleanInputDialog(prompt, "true", "false", allowCancel)) == null) {
      if (allowCancel) throw new CancelledException();
    }
    return choice.booleanValue();
  }

  public void setExceptionOnError(boolean flag) {
    exceptionOnError = flag;
  }

  public boolean getExceptionOnError() {
    return exceptionOnError;
  }

  public void setAllowCancel(boolean flag) {
    allowCancel = flag;
  }

  public boolean getAllowCancel() {
    return allowCancel;
  }

  public void setAssociatedConsole(IOConsole console) {
    myConsole = console;
  }

  public IOConsole getAssociatedConsole() {
    return myConsole;
  }

  protected DialogModel createModel() {
    String className = new JPanel().getClass().getName();
    if (className.startsWith("javax.swing.") && Utils.isSwingAvailable()) {
      try {
        Class<?> swingDialogModelClass = Class.forName("com.shiwei.ssr.dialog.SwingDialogModel");
        Class<?>[] types = {Class.forName("java.awt.Component")};
        Object[] args = {myComponent};
        Constructor<?> con = swingDialogModelClass.getConstructor(types);
        return (DialogModel) con.newInstance(args);
      } catch (Exception ex) {
        return new AWTDialogModel(myComponent);
      }
    } else {
      return new AWTDialogModel(myComponent);
    }
  }

  private void signalError(String msg) {
    if (exceptionOnError) throw new ErrorException(msg);
    model.popupErrorMessage(msg);
  }
}

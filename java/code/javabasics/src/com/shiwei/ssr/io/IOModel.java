package com.shiwei.ssr.io;

public interface IOModel {

  void print(String value);

  void print(boolean x);

  void print(char c);

  void print(double x);

  void print(float x);

  void print(int x);

  void print(long x);

  void print(Object o);

  void println();

  void println(String value);

  void println(boolean x);

  void println(char c);

  void println(double x);

  void println(float x);

  void println(int x);

  void println(long x);

  void println(Object o);

  void showErrorMsg(String msg);

  String readLine();

  String readLine(String prompt);

  int readInt();

  int readInt(String prompt);

  double readDouble();

  double readDouble(String prompt);

  boolean readBoolean();

  boolean readBoolean(String prompt);

}

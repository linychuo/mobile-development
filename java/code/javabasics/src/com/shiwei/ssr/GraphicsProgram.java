package com.shiwei.ssr;

import com.shiwei.ssr.graphics.GCanvas;
import com.shiwei.ssr.graphics.GContainer;
import com.shiwei.ssr.graphics.GObject;
import com.shiwei.ssr.graphics.GPoint;

public abstract class GraphicsProgram extends Program implements GContainer {
  private GCanvas gc;

  public GraphicsProgram(String title) {
    super(title);
    gc = createGCanvas();
    add(gc, CENTER);
  }

  protected GCanvas createGCanvas() {
    return new GCanvas();
  }

  @Override
  public void add(GObject gobj) {
    gc.add(gobj);
  }

  @Override
  public void add(GObject gobj, double x, double y) {
    gc.add(gobj, x, y);
  }

  @Override
  public void add(GObject gobj, GPoint pt) {
    gc.add(gobj, pt);
  }

  @Override
  public void remove(GObject gobj) {
    gc.remove(gobj);
  }

  @Override
  public void removeAll() {
    gc.removeAll();
  }

  public void repaint() {
    gc.repaint();
  }
}

package com.shiwei.ssr.dialog;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.ActionEvent;

public class AWTMessageDialog extends AWTDialog {
  private static final long serialVersionUID = 1564033619264290513L;
  private Button okButton;

  public AWTMessageDialog(Frame frame, String title, Image icon, String msg) {
    super(frame, title, icon, false);
    setMessage(msg);
  }

  @Override
  public void initButtonPanel(Panel buttonPanel, boolean allowCancel) {
    okButton = new Button("OK");
    okButton.addActionListener(this);
    buttonPanel.add(okButton);
  }

  @Override
  public void initDataPanel(Panel dataPanel) {
    /* Empty */
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == okButton) {
      setVisible(false);
    }
  }

}

package com.shiwei.ssr.dialog;

import java.awt.Button;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.ActionEvent;

public class AWTBooleanInputDialog extends AWTDialog {
  private static final long serialVersionUID = 7473828646645641206L;

  private Button trueButton, falseButton, cancelButton;
  private Boolean input;

  public AWTBooleanInputDialog(Frame frame, String title, Image icon, String trueLabel,
      String falseLabel, boolean allowCancel) {
    super(frame, title, icon, allowCancel);
    setMessage(title);
    trueButton.setLabel(trueLabel);
    falseButton.setLabel(falseLabel);
  }

  @Override
  public void initButtonPanel(Panel buttonPanel, boolean allowCancel) {
    trueButton = new Button("True");
    trueButton.addActionListener(this);
    buttonPanel.add(trueButton);
    falseButton = new Button("False");
    falseButton.addActionListener(this);
    buttonPanel.add(falseButton);
    if (allowCancel) {
      cancelButton = new Button("Cancel");
      cancelButton.addActionListener(this);
      buttonPanel.add(cancelButton);
    }
  }

  @Override
  public void initDataPanel(Panel dataPanel) {

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Component source = (Component) e.getSource();
    if (source == trueButton) {
      input = Boolean.TRUE;
      setVisible(false);
    } else if (source == falseButton) {
      input = Boolean.FALSE;
      setVisible(false);
    } else if (source == cancelButton) {
      input = null;
      setVisible(false);
    }
  }

  public Boolean getInput() {
    return input;
  }
}

package com.shiwei.ssr.dialog;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;

public class AWTLineInputDialog extends AWTDialog {
  private static final long serialVersionUID = -4907655209955311686L;

  private Button cancelButton, okButton;
  private TextField textLine;
  private String input;

  public AWTLineInputDialog(Frame frame, String title, Image icon, boolean allowCancel) {
    super(frame, "Input", icon, allowCancel);
    setMessage(title);
  }

  @Override
  public void initButtonPanel(Panel buttonPanel, boolean allowCancel) {
    okButton = new Button("OK");
    okButton.addActionListener(this);
    buttonPanel.add(okButton);
    if (allowCancel) {
      cancelButton = new Button("Cancel");
      cancelButton.addActionListener(this);
      buttonPanel.add(cancelButton);
    }
  }

  @Override
  public void initDataPanel(Panel dataPanel) {
    textLine = new TextField();
    textLine.addActionListener(this);
    dataPanel.add(textLine, BorderLayout.SOUTH);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Component source = (Component) e.getSource();
    if (source == okButton || source == textLine) {
      input = textLine.getText();
      setVisible(false);
    } else if (source == cancelButton) {
      input = null;
      setVisible(false);
    }
  }

  public String getInput() {
    return input;
  }

  public void setVisible(boolean flag) {
    super.setVisible(flag);
    if (flag) textLine.requestFocus();
  }

}

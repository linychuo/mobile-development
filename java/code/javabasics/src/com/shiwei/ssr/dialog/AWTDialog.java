package com.shiwei.ssr.dialog;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

public abstract class AWTDialog extends Dialog implements ActionListener {
  private static final long serialVersionUID = 834421933870340468L;
  public static final int WIDTH = 260;
  public static final int HEIGHT = 100;

  private AWTMessageCanvas messageArea;

  public AWTDialog(Frame frame, String title, Image icon, boolean allowCancel) {
    super(frame, title, true);
    setLayout(new BorderLayout());
    Panel topPanel = new Panel();
    Panel buttonPanel = new Panel();
    Panel dataPanel = new Panel();
    topPanel.setLayout(new BorderLayout());
    buttonPanel.setLayout(new FlowLayout());
    dataPanel.setLayout(new BorderLayout());
    messageArea = new AWTMessageCanvas();
    dataPanel.add(messageArea, BorderLayout.CENTER);
    initButtonPanel(buttonPanel, allowCancel);
    initDataPanel(dataPanel);
    topPanel.add(new AWTIconCanvas(icon), BorderLayout.WEST);
    topPanel.add(dataPanel, BorderLayout.CENTER);
    add(topPanel, BorderLayout.CENTER);
    add(buttonPanel, BorderLayout.SOUTH);
    Rectangle bounds = frame.getBounds();
    int cx = bounds.x + bounds.width / 2;
    int cy = bounds.y + bounds.height / 2;
    setBounds(cx - WIDTH / 2, cy - HEIGHT / 2, WIDTH, HEIGHT);
    validate();
  }

  public abstract void initButtonPanel(Panel buttonPanel, boolean allowCancel);

  public abstract void initDataPanel(Panel dataPanel);

  public abstract void actionPerformed(ActionEvent e);

  public void setMessage(String msg) {
    messageArea.setMessage(msg);
  }

  private static class AWTMessageCanvas extends Canvas {
    private static final long serialVersionUID = 1543745186666274817L;
    public static final int MARGIN = 8;
    public static final Font MESSAGE_FONT = new Font("Dialog", Font.PLAIN, 12);

    private String message;

    public AWTMessageCanvas() {
      setFont(MESSAGE_FONT);
    }

    public void setMessage(String msg) {
      message = msg;
    }

    @Override
    public void paint(Graphics g) {
      FontMetrics fm = g.getFontMetrics();
      int x = MARGIN;
      int y = MARGIN + fm.getAscent();
      int limit = getSize().width - MARGIN;
      StringTokenizer tokenizer = new StringTokenizer(message, " ", true);
      while (tokenizer.hasMoreTokens()) {
        String token = tokenizer.nextToken();
        int width = fm.stringWidth(token);
        if (x + width > limit) {
          x = MARGIN;
          y += fm.getHeight();
          if (token.equals(" ")) continue;
        }
        g.drawString(token, x, y);
        x += width;
      }
    }
  }

  private static class AWTIconCanvas extends Canvas {
    private static final long serialVersionUID = 5407563420122416473L;
    private Image icon;

    public AWTIconCanvas(Image icon) {
      this.icon = icon;
    }

    public Dimension getMinimumSize() {
      return new Dimension(48, 48);
    }

    public Dimension getPreferredSize() {
      return getMinimumSize();
    }

    @Override
    public void paint(Graphics g) {
      g.drawImage(icon, 8, 8, this);
    }
  }

}

package com.shiwei.ssr.dialog;

public interface DialogModel {

  void popupMessage(String msg);

  void popupErrorMessage(String msg);

  String popupLineInputDialog(String prompt, boolean allowCancel);

  Boolean popupBooleanInputDialog(String prompt, String trueLabel, String falseLabel,
      boolean allowCancel);
}

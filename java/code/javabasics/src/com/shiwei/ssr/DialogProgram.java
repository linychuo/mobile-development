package com.shiwei.ssr;

import com.shiwei.ssr.io.IODialog;

public class DialogProgram extends Program {

  public DialogProgram() {
    super();
    IODialog dialog = getDialog();
    setInputModel(dialog);
    setOutputModel(dialog);
  }

  public DialogProgram(String title) {
    super(title);
    IODialog dialog = getDialog();
    setInputModel(dialog);
    setOutputModel(dialog);
  }
}

package com.shiwei.ssr;

public class Test extends ConsoleProgram {

  public Test(String title) {
    super(title);
  }

  public void run() {
    int numDice = readInt("Number of dice:");
    println(numDice);
  }

  public static void main(String[] args) {
    Test test = new Test("hello");
    test.run();
  }

}

package com.shiwei.ssr.graphics;

import java.awt.*;

public class GDimension {

  private double myWidth;
  private double myHeight;

  public GDimension() {
    this(0, 0);
  }

  public GDimension(double width, double height) {
    myWidth = width;
    myHeight = height;
  }

  public GDimension(GDimension size) {
    this(size.myWidth, size.myHeight);
  }

  public GDimension(Dimension size) {
    this(size.width, size.height);
  }

  public double getWidth() {
    return myWidth;
  }

  public double getHeight() {
    return myHeight;
  }

  public void setSize(double width, double height) {
    myWidth = width;
    myHeight = height;
  }

  public void setSize(GDimension size) {
    setSize(size.myWidth, size.myHeight);
  }

  public GDimension getSize() {
    return new GDimension(myWidth, myHeight);
  }

  public Dimension toDimension() {
    return new Dimension((int) Math.round(myWidth), (int) Math.round(myHeight));
  }

  public int hashCode() {
    return new Float((float) myWidth).hashCode() ^ new Float((float) myHeight).hashCode();
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof GDimension)) return false;
    GDimension dim = (GDimension) obj;
    return ((float) myWidth == (float) dim.myWidth) && ((float) myHeight == (float) dim.myHeight);
  }

  public String toString() {
    return "(" + (float) myWidth + "x" + (float) myHeight + ")";
  }

}

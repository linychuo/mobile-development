package com.shiwei.ssr.graphics;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

public abstract class GObject implements Cloneable {
  private double xc, yc;
  private GContainer myParent;
  private Color color;
  private boolean isVisible;

  public abstract void paint(Graphics g);

  public abstract GRectangle getBounds();

  public void setLocation(double x, double y) {
    xc = x;
    yc = y;
    repaint();
  }

  public final void setLocation(GPoint pt) {
    setLocation(pt.getX(), pt.getY());
  }

  public GPoint getLocation() {
    return new GPoint(xc, yc);
  }

  public double getX() {
    return xc;
  }

  public double getY() {
    return yc;
  }

  public void move(double dx, double dy) {
    setLocation(xc + dx, yc + dy);
  }

  public GDimension getSize() {
    GRectangle bounds = getBounds();
    return new GDimension(bounds.getWidth(), bounds.getHeight());
  }

  public double getWidth() {
    return getBounds().getWidth();
  }

  public double getHeight() {
    return getBounds().getHeight();
  }

  public boolean contains(double x, double y) {
    return getBounds().contains(GMath.round(x), GMath.round(y));
  }

  public final boolean contains(GPoint pt) {
    return contains(pt.getX(), pt.getY());
  }

  public void setColor(Color c) {
    color = c;
    repaint();
  }

  public Color getColor() {
    GObject obj = this;
    while (obj.color == null) {
      GContainer parent = obj.getParent();
      if (parent instanceof GObject) {
        obj = (GObject) parent;
      } else if (parent instanceof Component) {
        return ((Component) parent).getForeground();
      } else {
        return Color.black;
      }
    }
    return obj.color;
  }

  public void setVisible(boolean visible) {
    isVisible = visible;
    repaint();
  }

  public boolean isVisible() {
    return isVisible;
  }

  public GContainer getParent() {
    return myParent;
  }

  public void setParent(GContainer parent) {
    myParent = parent;
  }

  protected void repaint() {
    GContainer parent = getParent();
    while (parent instanceof GObject) {
      parent = ((GObject) parent).getParent();
    }
    if (parent instanceof GCanvas) {
      ((GCanvas) parent).conditionalRepaint();
    }
  }

  protected void paintObject(Graphics g) {
    if (!isVisible()) return;
    Color oldColor = g.getColor();
    if (color != null) g.setColor(color);
    paint(g);
    if (color != null) g.setColor(oldColor);
  }
}

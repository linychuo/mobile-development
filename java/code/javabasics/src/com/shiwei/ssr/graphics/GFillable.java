package com.shiwei.ssr.graphics;

import java.awt.Color;

public interface GFillable {
  public void setFilled(boolean fill);

  public boolean isFilled();

  public void setFillColor(Color c);

  public Color getFillColor();
}

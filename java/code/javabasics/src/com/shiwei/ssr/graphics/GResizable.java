package com.shiwei.ssr.graphics;


public interface GResizable {
  public void setSize(double width, double height);

  public void setSize(GDimension size);

  public void setBounds(double x, double y, double width, double height);

  public void setBounds(GRectangle bounds);
}

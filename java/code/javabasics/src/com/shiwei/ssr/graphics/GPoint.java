package com.shiwei.ssr.graphics;

import java.awt.Point;

public class GPoint {
  private double xc;
  private double yc;

  public GPoint() {
    this(0, 0);
  }

  public GPoint(double x, double y) {
    xc = x;
    yc = y;
  }

  public GPoint(GPoint p) {
    this(p.xc, p.yc);
  }

  public GPoint(Point p) {
    this(p.x, p.y);
  }

  public double getX() {
    return xc;
  }

  public double getY() {
    return yc;
  }

  public void setLocation(double x, double y) {
    xc = x;
    yc = y;
  }

  public void setLocation(GPoint p) {
    setLocation(p.xc, p.yc);
  }

  public GPoint getLocation() {
    return new GPoint(xc, yc);
  }

  public void translate(double dx, double dy) {
    xc += dx;
    yc += dy;
  }

  public Point toPoint() {
    return new Point((int) Math.round(xc), (int) Math.round(yc));
  }

  public int hashCode() {
    return new Float((float) xc).hashCode() ^ new Float((float) yc).hashCode();
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof GPoint)) return false;
    GPoint pt = (GPoint) obj;
    return ((float) xc == (float) pt.xc) && ((float) yc == (float) pt.yc);
  }

  public String toString() {
    return "(" + (float) xc + ", " + (float) yc + ")";
  }

}

package com.shiwei.ssr.graphics;

public interface GContainer {
  public void add(GObject gobj);

  public void add(GObject gobj, double x, double y);

  public void add(GObject gobj, GPoint pt);

  public void remove(GObject gobj);

  public void removeAll();

}

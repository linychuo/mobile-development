package com.shiwei.ssr.graphics;

public class GMath {

  private GMath() {
    /* Empty */
  }

  public static int round(double x) {
    return (int) Math.round(x);
  }

  public static double sinDegrees(double angle) {
    return Math.sin(toRadians(angle));
  }

  public static double cosDegrees(double angle) {
    return Math.cos(toRadians(angle));
  }

  public static double tanDegrees(double angle) {
    return sinDegrees(angle) / cosDegrees(angle);
  }

  public static double toDegrees(double radians) {
    return radians * 180 / Math.PI;
  }

  public static double toRadians(double degrees) {
    return degrees * Math.PI / 180;
  }

  public static double distance(double x, double y) {
    return Math.sqrt(x * x + y * y);
  }

  public static double distance(double x0, double y0, double x1, double y1) {
    return distance(x1 - x0, y1 - y0);
  }

  public static double angle(double x, double y) {
    if (x == 0 && y == 0) return 0;
    return toDegrees(Math.atan2(-y, x));
  }

  public static double angle(double x0, double y0, double x1, double y1) {
    return angle(x1 - x0, y1 - y0);
  }
}

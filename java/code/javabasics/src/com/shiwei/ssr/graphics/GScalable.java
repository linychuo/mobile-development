package com.shiwei.ssr.graphics;

public interface GScalable {
  public void scale(double sx, double sy);

  public void scale(double sf);
}

package com.shiwei.ssr.graphics;

import java.awt.Rectangle;

public class GRectangle {
  private double xc;
  private double yc;
  private double myWidth;
  private double myHeight;

  public GRectangle() {
    this(0, 0, 0, 0);
  }

  public GRectangle(double x, double y, double width, double height) {
    xc = x;
    yc = y;
    myWidth = width;
    myHeight = height;
  }

  public GRectangle(double width, double height) {
    this(0, 0, width, height);
  }

  public GRectangle(GPoint pt, GDimension size) {
    this(pt.getX(), pt.getY(), size.getWidth(), size.getHeight());
  }

  public GRectangle(GPoint pt) {
    this(pt.getX(), pt.getY(), 0, 0);
  }

  public GRectangle(GDimension size) {
    this(0, 0, size.getWidth(), size.getHeight());
  }

  public GRectangle(GRectangle r) {
    this(r.xc, r.yc, r.myWidth, r.myHeight);
  }

  public double getX() {
    return xc;
  }

  public double getY() {
    return yc;
  }

  public double getWidth() {
    return myWidth;
  }

  public double getHeight() {
    return myHeight;
  }

  public void setBounds(double x, double y, double width, double height) {
    xc = x;
    yc = y;
    myWidth = width;
    myHeight = height;
  }

  public void setBounds(GPoint pt, GDimension size) {
    setBounds(pt.getX(), pt.getY(), size.getWidth(), size.getHeight());
  }

  public void setBounds(GRectangle bounds) {
    setBounds(bounds.xc, bounds.yc, bounds.myWidth, bounds.myHeight);
  }

  public GRectangle getBounds() {
    return new GRectangle(this);
  }

  public void setLocation(double x, double y) {
    xc = x;
    yc = y;
  }

  public void setLocation(GPoint pt) {
    setLocation(pt.getX(), pt.getY());
  }

  public GPoint getLocation() {
    return new GPoint(xc, yc);
  }

  public void translate(double dx, double dy) {
    xc += dx;
    yc += dy;
  }

  public void setSize(double width, double height) {
    myWidth = width;
    myHeight = height;
  }

  public void setSize(GDimension size) {
    setSize(size.getWidth(), size.getHeight());
  }

  public GDimension getSize() {
    return new GDimension(myWidth, myHeight);
  }

  public void grow(double dx, double dy) {
    xc -= dx;
    yc -= dy;
    myWidth += 2 * dx;
    myHeight += 2 * dy;
  }

  public boolean isEmpty() {
    return myWidth <= 0 || myHeight <= 0;
  }

  public boolean contains(double x, double y) {
    return x >= xc && y >= yc && x < xc + myWidth && y < yc + myHeight;
  }

  public boolean contains(GPoint pt) {
    return contains(pt.getX(), pt.getY());
  }

  public boolean intersects(GRectangle r) {
    if (xc > r.xc + r.myWidth) return false;
    if (yc > r.yc + r.myHeight) return false;
    if (r.xc > xc + myWidth) return false;
    if (r.yc > yc + myHeight) return false;
    return true;
  }

  public GRectangle intersection(GRectangle r) {
    double x1 = Math.max(xc, r.xc);
    double y1 = Math.max(yc, r.yc);
    double x2 = Math.min(xc + myWidth, r.xc + r.myWidth);
    double y2 = Math.min(yc + myHeight, r.yc + r.myHeight);
    return new GRectangle(x1, y1, x2 - x1, y2 - y1);
  }

  public GRectangle union(GRectangle r) {
    if (isEmpty()) return new GRectangle(r);
    if (r.isEmpty()) return new GRectangle(this);
    double x1 = Math.min(xc, r.xc);
    double y1 = Math.min(yc, r.yc);
    double x2 = Math.max(xc + myWidth, r.xc + r.myWidth);
    double y2 = Math.max(yc + myHeight, r.yc + r.myHeight);
    return new GRectangle(x1, y1, x2 - x1, y2 - y1);
  }

  public void add(GRectangle r) {
    if (r.isEmpty()) return;
    if (isEmpty()) {
      setBounds(r);
      return;
    }
    double x2 = Math.max(xc + myWidth, r.xc + r.myWidth);
    double y2 = Math.max(yc + myHeight, r.yc + r.myHeight);
    xc = Math.min(r.xc, xc);
    yc = Math.min(r.yc, yc);
    myWidth = x2 - xc;
    myHeight = y2 - yc;
  }

  public void add(double x, double y) {
    if (isEmpty()) {
      setBounds(x, y, 0, 0);
      return;
    }
    double x2 = Math.max(x + myWidth, x);
    double y2 = Math.max(y + myHeight, y);
    xc = Math.min(x, xc);
    yc = Math.min(y, yc);
    myWidth = x2 - xc;
    myHeight = y2 - yc;
  }

  public Rectangle toRectangle() {
    return new Rectangle((int) Math.round(xc), (int) Math.round(yc), (int) Math.round(myWidth),
        (int) Math.round(myHeight));
  }

  public int hashCode() {
    return new Float((float) xc).hashCode() ^ new Float((float) yc).hashCode()
        ^ new Float((float) myWidth).hashCode() ^ new Float((float) myHeight).hashCode();
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof GRectangle)) return false;
    GRectangle r = (GRectangle) obj;
    if ((float) xc != (float) r.xc) return false;
    if ((float) yc != (float) r.yc) return false;
    if ((float) myWidth != (float) r.myWidth) return false;
    if ((float) myHeight != (float) r.myHeight) return false;
    return true;
  }

  public String toString() {
    return "[" + (float) xc + ", " + (float) yc + ", " + (float) myWidth + "x" + (float) myHeight
        + "]";
  }

}

package com.shiwei.ssr.graphics;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;

import com.shiwei.ssr.util.Utils;

public class GCanvas extends Container implements GContainer {
  private static final long serialVersionUID = 434074069096907148L;
  private boolean autoRepaint;
  private List<GObject> contents;
  private boolean opaque;
  private Image offscreenImage;

  public GCanvas() {
    contents = Utils.newArrayList();
    setBackground(Color.white);
    setOpaque(true);
    // setAutoRepaintFlag(true);
    setLayout(null);
  }

  protected void conditionalRepaint() {
    if (autoRepaint) repaint();
  }

  @Override
  public void add(GObject gobj) {
    synchronized (contents) {
      if (gobj.getParent() != null) gobj.getParent().remove(gobj);
      gobj.setParent(this);
      contents.add(gobj);
    }
    conditionalRepaint();

  }

  @Override
  public void add(GObject gobj, double x, double y) {
    add(gobj);
    gobj.setLocation(x, y);
  }

  @Override
  public void add(GObject gobj, GPoint pt) {
    add(gobj, pt.getX(), pt.getY());
  }

  @Override
  public void remove(GObject gobj) {
    synchronized (contents) {
      contents.remove(gobj);
      gobj.setParent(null);
    }
    conditionalRepaint();
  }

  @Override
  public void removeAll() {
    synchronized (contents) {
      contents.clear();
    }
    super.removeAll();
    repaint();
  }

  public void setOpaque(boolean flag) {
    opaque = flag;
    conditionalRepaint();
  }

  public boolean isOpaque() {
    return opaque;
  }

  public int getWidth() {
    return getSize().width;
  }

  public int getHeight() {
    return getSize().height;
  }

  public void setAutoRepaintFlag(boolean state) {
    autoRepaint = state;
  }

  public boolean getAutoRepaintFlag() {
    return autoRepaint;
  }

  @Override
  public void paint(Graphics g) {
    Graphics g0 = g;
    if (isOpaque()) {
      if (offscreenImage == null) initOffscreenImage();
      if (offscreenImage != null) g = offscreenImage.getGraphics();
      Dimension size = getSize();
      g.setColor(getBackground());
      g.fillRect(0, 0, size.width, size.height);
      g.setColor(getForeground());
    }
    synchronized (contents) {
      int nElements = contents.size();
      for (int i = 0; i < nElements; i++) {
        ((GObject) contents.get(i)).paintObject(g);
      }
    }
    if (isOpaque() && offscreenImage != null) {
      g0.drawImage(offscreenImage, 0, 0, this);
    }
    super.paint(g);
  }

  @Override
  public void update(Graphics g) {
    paint(g);
  }

  protected void initOffscreenImage() {
    synchronized (contents) {
      Dimension size = getSize();
      if (size.width <= 0 || size.height <= 0) return;
      offscreenImage = createImage(size.width, size.height);
    }
  }

}

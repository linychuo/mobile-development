package com.shiwei.ssr;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.shiwei.ssr.io.IOConsole;
import com.shiwei.ssr.io.IODialog;
import com.shiwei.ssr.io.IOModel;

public abstract class Program implements IOModel {
  private static final int DEFAULT_X = 16;
  private static final int DEFAULT_Y = 40;
  private static final int DEFAULT_WIDTH = 300;
  private static final int DEFAULT_HEIGHT = 180;

  public static final String NORTH = BorderLayout.NORTH;
  public static final String SOUTH = BorderLayout.SOUTH;
  public static final String EAST = BorderLayout.EAST;
  public static final String WEST = BorderLayout.WEST;
  public static final String CENTER = BorderLayout.CENTER;

  private JFrame frame;
  private IOConsole myConsole;
  private IOModel outputModel;
  private IOModel inputModel;
  private String frameTitle;
  private IODialog myDialog;

  public Program() {
    frameTitle = getClass().getName();
    frameTitle = frameTitle.substring(frameTitle.lastIndexOf(".") + 1);
    defaultSetting();
  }

  public Program(String title) {
    defaultSetting();
  }

  private void defaultSetting() {
    frame = new JFrame(frameTitle);
    Rectangle programBounds = getDefaultBounds();
    Insets insets = frame.getInsets();
    int frameWidth = programBounds.width + insets.left + insets.right;
    int frameHeight = programBounds.height + insets.top + insets.bottom;
    frame.setBounds(programBounds.x, programBounds.y, frameWidth, frameHeight);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(false);
    setConsole(createConsole());
    myDialog = createDialogIO();
    myDialog.setAssociatedConsole(myConsole);
  }

  private Rectangle getDefaultBounds() {
    Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
    int width = DEFAULT_WIDTH;
    int height = DEFAULT_HEIGHT;
    int x = (width >= size.width) ? 0 : DEFAULT_X;
    int y = (height >= size.height) ? 0 : DEFAULT_Y;
    return new Rectangle(x, y, width, height);
  }

  public void add(Component component, String direction) {
    frame.add(component, direction);
  }

  public void setBounds(int x, int y, int width, int height) {
    frame.setBounds(x, y, width, height);
  }

  public void setBounds(Rectangle r) {
    frame.setBounds(r);
  }

  public Component getContentPane() {
    return frame.getContentPane();
  }

  public void showMe() {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

  public IOConsole getConsole() {
    return myConsole;
  }

  public void setConsole(IOConsole console) {
    this.myConsole = console;
  }

  protected IOConsole createConsole() {
    return IOConsole.SYSTEM_CONSOLE;
  }

  @Override
  public void print(String value) {
    getOutputModel().print(value);
  }

  @Override
  public final void print(boolean x) {
    print("" + x);
  }

  @Override
  public final void print(char c) {
    print("" + c);
  }

  @Override
  public final void print(double x) {
    print("" + x);
  }

  @Override
  public final void print(float x) {
    print("" + x);
  }

  @Override
  public final void print(int x) {
    print("" + x);
  }

  @Override
  public final void print(long x) {
    print("" + x);
  }

  @Override
  public final void print(Object o) {
    print("" + o);
  }

  @Override
  public void println() {
    getOutputModel().println();
  }

  @Override
  public void println(String value) {
    getOutputModel().println(value);
  }

  @Override
  public final void println(boolean x) {
    println("" + x);
  }

  @Override
  public final void println(char c) {
    println("" + c);
  }

  @Override
  public final void println(double x) {
    println("" + x);
  }

  @Override
  public final void println(float x) {
    println("" + x);
  }

  @Override
  public final void println(int x) {
    println("" + x);
  }

  @Override
  public final void println(long x) {
    println("" + x);
  }

  @Override
  public final void println(Object o) {
    println("" + o);
  }

  @Override
  public void showErrorMsg(String msg) {
    getOutputModel().showErrorMsg(msg);
  }

  @Override
  public final String readLine() {
    return readLine(null);
  }

  @Override
  public String readLine(String prompt) {
    return getOutputModel().readLine(prompt);
  }

  @Override
  public final int readInt() {
    return readInt(null);
  }

  @Override
  public int readInt(String prompt) {
    return getOutputModel().readInt(prompt);
  }

  @Override
  public final double readDouble() {
    return readDouble(null);
  }

  @Override
  public double readDouble(String prompt) {
    return getOutputModel().readDouble(prompt);
  }

  @Override
  public final boolean readBoolean() {
    return readBoolean(null);
  }

  @Override
  public boolean readBoolean(String prompt) {
    return readBoolean(prompt);
  }

  public IOModel getOutputModel() {
    return (outputModel == null) ? myConsole : outputModel;
  }

  public void setOutputModel(IOModel outputModel) {
    this.outputModel = outputModel;
  }

  public IOModel getInputModel() {
    return (inputModel == null) ? myConsole : inputModel;
  }

  public void setInputModel(IOModel inputModel) {
    this.inputModel = inputModel;
  }

  public void setDialog(IODialog dialog) {
    myDialog = dialog;
  }

  public IODialog getDialog() {
    return myDialog;
  }

  protected IODialog createDialogIO() {
    return new IODialog(getContentPane());
  }
}

package com.shiwei.ssr.exception;

public class NoSpaceException extends RuntimeException {

  private static final long serialVersionUID = -3570604426849572554L;

  public NoSpaceException() {
    super("no space for save new book!");
  }
}

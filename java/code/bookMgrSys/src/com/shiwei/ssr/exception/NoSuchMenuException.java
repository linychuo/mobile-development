package com.shiwei.ssr.exception;

public class NoSuchMenuException extends Exception {

  private static final long serialVersionUID = -2244751268583084101L;

  public NoSuchMenuException() {
    super("no such menu item!");
  }
}

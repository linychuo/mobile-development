package com.shiwei.ssr;

import com.mysql.jdbc.Driver;
import com.shiwei.ssr.exception.NoSuchMenuException;
import com.shiwei.ssr.model.ConsoleMenu;

import java.sql.SQLException;
import java.util.Scanner;

public class TestMenu {

  public static void main(String[] args) throws SQLException {
    Driver d = new Driver();
    ConsoleMenuMgr.add(new ConsoleMenu("新增图书", new ActionLinstener() {
      @Override
      public void performed() {
        System.out.println("新增一本图书");
      }
    }));

    ConsoleMenuMgr.add(new ConsoleMenu("查看图书", new ActionLinstener() {
      @Override
      public void performed() {
        System.out.println("查看一本图书");
      }
    }));

    ConsoleMenuMgr.add(new ConsoleMenu("退出", new ActionLinstener() {
      @Override
      public void performed() {
        System.exit(0);
      }
    }));

    printScreen();
    Scanner input = new Scanner(System.in);
    while (input.hasNextInt()) {
      int select = input.nextInt();
      try {
        ConsoleMenuMgr.invoke(select);
      } catch (NoSuchMenuException ex) {
        printScreen();
      }
    }
  }

  private static void printScreen() {
    System.out.println("欢   迎   使   用   图   书   管   理   系   统");
    System.out.println(ConsoleMenuMgr.showMenu());
    System.out.print("请选择：");
  }
}

package com.shiwei.ssr;

import com.shiwei.ssr.exception.NoSuchMenuException;
import com.shiwei.ssr.model.ConsoleMenu;

import java.util.ArrayList;
import java.util.List;

public class ConsoleMenuMgr {

  private static final List<ConsoleMenu> MENUS = new ArrayList<ConsoleMenu>(6);

  private ConsoleMenuMgr() {
  }

  // public static ConsoleMenuMgr getInstance() {
  // return ConsoleMenuMgrHolder.instance;
  // }

  public static void add(ConsoleMenu menu) {
    MENUS.add(menu);
  }

  public static void remove(ConsoleMenu menu) {
    MENUS.remove(menu);
  }

  public static void remove(int idx) {
    MENUS.remove(idx - 1);
  }

  public static void invoke(int idx) throws NoSuchMenuException {
    if (idx <= 0 || idx > MENUS.size()) {
      throw new NoSuchMenuException();
    }
    MENUS.get(idx - 1).click();
  }

  public static String showMenu() {
    StringBuilder sb = new StringBuilder();
    sb.append("--------------------------------------------\n");
    for (int i = 0; i < MENUS.size(); i++) {
      sb.append((i + 1));
      sb.append(". | ");
      sb.append(MENUS.get(i).getLabel());
      sb.append("\n");
    }
    sb.append("--------------------------------------------\n");
    return sb.toString();
  }
  // private static class ConsoleMenuMgrHolder {
  // public static final ConsoleMenuMgr instance = new ConsoleMenuMgr();
  // }
}

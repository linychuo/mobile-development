package com.shiwei.ssr.db;

import com.shiwei.ssr.dbtool.RowProcessor;
import com.shiwei.ssr.model.Book;

import java.sql.ResultSet;
import java.util.List;

public class BookDBDataStore implements DataStore<Book> {

  @Override
  public void save(Book obj) {

  }

  @Override
  public List<Book> queryAll() {
    return null;
  }

  private static class BookRowProcessor implements RowProcessor<Book> {

    @Override
    public Book handle(ResultSet rs) {
      Book book = new Book();
      return book;
    }

  }

}

package com.shiwei.ssr.db;

import com.shiwei.ssr.exception.NoSpaceException;
import com.shiwei.ssr.model.Book;

import java.util.List;

public class BookMemoryDataStore implements DataStore<Book> {

  private Book[] books;

  public BookMemoryDataStore(int initialCapacity) {
    if (initialCapacity < 0) {
      throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
    }
    this.books = new Book[initialCapacity];
  }

  public BookMemoryDataStore() {
    this.books = new Book[6];
  }


  @Override
  public void save(Book obj) {
    if (isFull()) {
      throw new NoSpaceException();
    }
    books[actualSize() - 1] = obj;
  }


  public boolean isFull() {
    return actualSize() == size();
  }

  public int actualSize() {
    int counter = 0;
    for (Book book : books) {
      if (book != null) {
        counter++;
      }
    }
    return counter;
  }

  public int size() {
    return books.length;
  }

  @Override
  public List<Book> queryAll() {
    // TODO Auto-generated method stub
    return null;
  }
}

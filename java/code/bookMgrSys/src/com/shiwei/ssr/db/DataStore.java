package com.shiwei.ssr.db;

import java.util.List;

public interface DataStore<T> {

  void save(T obj);

  List<T> queryAll();
}

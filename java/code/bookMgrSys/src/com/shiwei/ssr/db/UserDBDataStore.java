package com.shiwei.ssr.db;

import com.shiwei.ssr.dbtool.DBUtil;
import com.shiwei.ssr.dbtool.GeneralConnectionBroker;
import com.shiwei.ssr.dbtool.RowProcessor;
import com.shiwei.ssr.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserDBDataStore implements DataStore<User> {

  private DBUtil dbUtil;

  public UserDBDataStore() {
    dbUtil = new DBUtil(new GeneralConnectionBroker());
  }

  @Override
  public void save(User obj) {
    dbUtil.execute("insert into user values(?, ?)", new Object[]{obj.getId(), obj.getName()});
  }

  @Override
  public List<User> queryAll() {
    return dbUtil.queryAll("select * from user", null, new UserRowProcessor());
  }


  public User queryById(int id) {
    return dbUtil
        .query("select *from user where id = ?", new Object[]{id}, new UserRowProcessor());
  }

  private static class UserRowProcessor implements RowProcessor<User> {

    @Override
    public User handle(ResultSet rs) throws SQLException {
      User user = new User();

      user.setId(rs.getInt("id"));
      user.setName(rs.getString("name"));
      return user;
    }
  }

  public static void main(String[] args) {
    UserDBDataStore test = new UserDBDataStore();
    // User user = new User();
    // user.setId(99999);
    // user.setName("999999999999-99");
    // test.save(user);
    // User result = test.queryById(99999);
    // System.out.println(result);

    // User user1 = new User();
    // user1.setId(2);
    // user1.setName("22222");
    // test.save(user1);
    //
    // User user2 = new User();
    // user2.setId(3);
    // user2.setName("33333");
    // test.save(user2);
    //
    // User user3 = new User();
    // user3.setId(4);
    // user3.setName("4444");
    // test.save(user3);
    //
    List<User> list = test.queryAll();
    for (int i = 0; i < list.size(); i++) {
      System.out.println((i + 1) + " : " + list.get(i));
    }

  }
}

package com.shiwei.ssr.dbtool;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionBroker {

  public Connection getConnection() throws SQLException;

  public void closeConnection() throws SQLException;

}

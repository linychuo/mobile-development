package com.shiwei.ssr.dbtool;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowProcessor<T> {

  T handle(ResultSet rs) throws SQLException;
}

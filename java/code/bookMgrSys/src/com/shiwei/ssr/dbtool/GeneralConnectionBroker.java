package com.shiwei.ssr.dbtool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GeneralConnectionBroker implements ConnectionBroker {

  private Connection conn;

  static {
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  @Override
  public Connection getConnection() throws SQLException {
    conn =
        DriverManager
            .getConnection("jdbc:mysql://localhost/test?user=root&password=abc123&charset=utf8");
    return conn;
  }

  @Override
  public void closeConnection() throws SQLException {
    if (conn != null) {
      conn.close();
    }
  }
}

package com.shiwei.ssr.dbtool;

import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;


public class DBUtil {

  private ConnectionBroker connBroker;

  public DBUtil(ConnectionBroker connectionBroker) {
    this.connBroker = connectionBroker;
  }

  public void execute(String sql, Object[] params) {
    Connection conn = null;
    PreparedStatement pstmt = null;
    try {
      try {
        conn = connBroker.getConnection();
        pstmt = conn.prepareStatement(sql);
        fillStatement(pstmt, params);
        pstmt.executeUpdate();
      } finally {
        connBroker.closeConnection();
        if (pstmt != null) {
          pstmt.close();
        }
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
  }

  public <T> T query(String sql, Object[] params, RowProcessor<T> rowProcessor) {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    T obj = null;
    try {
      try {
        conn = connBroker.getConnection();
        pstmt = conn.prepareStatement(sql);
        fillStatement(pstmt, params);
        rs = pstmt.executeQuery();
        if (rs.next()) {
          obj = rowProcessor.handle(rs);
        }
      } finally {
        connBroker.closeConnection();
        if (pstmt != null) {
          pstmt.close();
        }
        if (rs != null) {
          rs.close();
        }
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return obj;
  }

  public <T> List<T> queryAll(String sql, Object[] params, RowProcessor<T> rowProcessor) {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    List<T> obj = new ArrayList<>();
    try {
      try {
        conn = connBroker.getConnection();
        pstmt = conn.prepareStatement(sql);
        fillStatement(pstmt, params);
        rs = pstmt.executeQuery();
        while (rs.next()) {
          obj.add(rowProcessor.handle(rs));
        }
      } finally {
        connBroker.closeConnection();
        if (pstmt != null) {
          pstmt.close();
        }
        if (rs != null) {
          rs.close();
        }
      }
    } catch (SQLException ex) {
      ex.printStackTrace();
    }
    return obj;
  }


  private void fillStatement(PreparedStatement pstmt, Object[] params) throws SQLException {
    ParameterMetaData pmd = pstmt.getParameterMetaData();
    int pstmtCount = pmd.getParameterCount();
    int paramsCount = params == null ? 0 : params.length;

    if (pstmtCount != paramsCount) {
      throw new SQLException("Wrong number of parameters: expected " + pstmtCount + ", was given "
                             + paramsCount);
    }

    if (params == null) {
      return;
    }

    for (int i = 0; i < params.length; i++) {
      if (params[i] != null) {
        pstmt.setObject(i + 1, params[i]);
      } else {
        pstmt.setNull(i + 1, Types.VARCHAR);
      }
    }
  }

}

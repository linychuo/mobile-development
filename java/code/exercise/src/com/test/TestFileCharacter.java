package com.test;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TestFileCharacter {
  public static void main(String[] args) throws IOException {
    FileWriter output = new FileWriter("temp1.dat");
    for (int i = 1; i <= 10; i++)
      output.write(i);

    // Close the output stream
    output.close();


    // Create an input stream for the file
    FileReader input = new FileReader("temp1.dat");
    // Read values from the file

    int value;
    while ((value = input.read()) != -1)
      System.out.print(value);
    // Close the out
  }
}

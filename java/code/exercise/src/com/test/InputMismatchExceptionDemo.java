package com.test;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputMismatchExceptionDemo {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    boolean continueInput = true;

    do {
      try {
        System.out.print("Enter a integer: ");
        System.out.println("The number entered is : [" + in.nextInt() + "]");
        continueInput = false;

      } catch (InputMismatchException e) {
        // System.out.println("Try again.");
        in.nextLine();
      }
    } while (continueInput);

    in.close();
  }
}

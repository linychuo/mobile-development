package com.test;

import java.io.File;
import java.util.Scanner;

public class TestBlock {
  public static void main(String[] args) {
    new Block() {
      public void body() throws Exception {
        Scanner in = new Scanner(new File("ququx"));
        while (in.hasNext())
          System.out.println(in.next());
      }
    }.toThread().start();
  }
}

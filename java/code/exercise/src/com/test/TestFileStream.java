package com.test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestFileStream {

  public static void main(String[] args) throws IOException {
    FileOutputStream output = new FileOutputStream("temp.dat");
    for (int i = 1; i <= 10; i++)
      output.write(i);
    output.write('\n');
    output.write(65);
    output.write('h');
    output.write('e');
    output.write('l');
    output.write('l');
    output.write('o');
    output.write((byte)1000);

    // Close the output stream
    output.close();


    // Create an input stream for the file
    FileInputStream input = new FileInputStream("temp.dat");
    // Read values from the file

    int value;
    while ((value = input.read()) != -1)
      System.out.print(value);
    // Close the out
  }
}

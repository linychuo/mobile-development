package com.test;

import com.shiwei.ssr.Employee;
import com.shiwei.ssr.Manager;

public class TestInheritanceOfGeneric {


  public static void main(String[] args) {
    Manager ceo = new Manager("ceo", 100_000, 2000, 1, 1);
    Manager cfo = new Manager("cfo", 50_000, 2000, 1, 1);

    Pair<Manager> managerBuddies = new Pair<>(ceo, cfo);
    // Pair employeeBuddiesOfRaw = managerBuddies;
    // Pair<Employee> employeeBuddies = managerBuddies;
    Pair<? extends Employee> wildcardEmployeeBuddies = managerBuddies;
    // wildcardEmployeeBuddies.setFirst(new Employee("rwar", 100, 2000, 1, 1));
    Pair<? super Manager> wildcardEmployeeBuddiesOther = managerBuddies;

    Manager[] managerBuddiesOther = {ceo, cfo};
    Employee[] employeeBuddiesOther = managerBuddiesOther;
    // Employee[] employeeBuddiesOther = new Employee[2];
    employeeBuddiesOther[0] = new Employee("cto", 20_000, 2000, 1, 1);
    // printBuddies(managerBuddies);
  }

  public static void printBuddies(Pair<Employee> p) {
    Employee first = p.getFirst();
    Employee second = p.getSecond();
    System.out.println(first.getName() + " and " + second.getName() + "are buddies.");
  }
}

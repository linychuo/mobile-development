package com.test;

import java.awt.Component;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JSlider;

public class GenericTest {

  @SafeVarargs
  public static <T> T getMiddle(T... a) {
    return a[a.length / 2];
  }

  public static <T extends Comparable<T>> T min(T[] a) {
    if (a == null || a.length == 0) return null;
    T smallest = a[0];
    for (int i = 1; i < a.length; i++)
      if (smallest.compareTo(a[i]) > 0) smallest = a[i];
    return smallest;
  }

  public static <T extends Throwable> void doWork(T t) throws T {
    try {
      System.out.println("0000");
    } catch (Throwable realCause) {
      t.initCause(realCause);
      throw t;
    }
  }

  @SuppressWarnings("unchecked")
  public static <T extends Throwable> void throwAs(Throwable e) throws T {
    throw (T) e;
  }

  public static void swap(Pair<?> p) {
    Object o = p.getFirst();
    // p.setFirst(p.getSecond());
    // p.setSecond(o);
  }

  public static void main(String[] args) {
    Integer[] aaa = {3,4,5};
    System.out.println(min(aaa));
    String middle = GenericTest.<String>getMiddle("aa", "bb", "cc");
    // GenericTest.getMiddle("aa", "bb", "cc");
    // GenericTest.getMiddle(1, "2");
    System.out.println(middle);

    JSlider slider = new JSlider();
    Dictionary<Integer, Component> labelTable = new Hashtable<>();
    labelTable.put(0, new JLabel(new ImageIcon("nine.gif")));
    labelTable.put(20, new JLabel(new ImageIcon("ten.gif")));

    slider.setLabelTable(labelTable);
    @SuppressWarnings("unchecked")
    Dictionary<Integer, Component> returnLableTable = slider.getLabelTable();
    System.out.println(returnLableTable);

    Pair<String> a = new Pair<>("a", "b");
    if (a.getClass() == Pair.class) {
      System.out.println("------------------");
    }

    @SuppressWarnings("unchecked")
    Pair<String>[] table = (Pair<String>[]) new Pair<?>[10];
    table[0] = new Pair<String>("a", "b");
    System.out.println(table[0].getFirst());
  }
}

package com.test.collections;

public abstract class Syntax {

  public String process(String content) {
    StringBuilder sb = new StringBuilder();
    //

    return sb.toString();
  }

  private String processKeywords(String keywords) {
    return build(keywords, keywordsColor());
  }

  private String processComments(String comment) {
    return build(comment, commentsColor());
  }

  private String processLiterals(String literals) {
    return build(literals, literalsColor());
  }

  private String build(String content, String color) {
    return "<span style=\"color:" + color + "\">" + content + "</span>";
  }

  protected String keywordsColor() {
    return "bold navy";
  }

  protected String commentsColor() {
    return "green";
  }

  protected String literalsColor() {
    return "blue";
  }

  protected abstract String[] getKeywords();

  protected String getCommentPattern() {
    return "";
  }

  protected String getLiteralsPattern() {
    return "\".*\"";
  }
}

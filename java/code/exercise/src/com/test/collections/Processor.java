package com.test.collections;

public interface Processor {

  String process(String str);
}

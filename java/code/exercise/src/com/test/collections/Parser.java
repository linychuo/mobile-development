package com.test.collections;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * fdsa fdsa
 * 
 * @author ivan
 * 
 */
public class Parser {

	public static final String KEYWORDS_PATTERN;
	public static final String SINGLE_COMMENTS_PATTERN = "^\\s*//.*$";
	public static final String[] MULTI_COMMENTS_PATTERN = { "/\\*\\*$",
			"^\\s*\\*.*$" };
	public static final String LITERALS_PATTERN = "\"[^,]*\"|'([^,]*)'";
	public static final String cls_NAME = "com.test.java";

	private static final String[] KEYWORDS = { "abstract", "continue", "for",
			"new", "switch", "assert", "default", "goto", "package",
			"synchronized", "boolean", "do", "if", "private", "this", "break",
			"double", "implements", "protected", "throw", "byte", "else",
			"import", "public", "throws", "case", "enum", "instanceof",
			"return", "transient", "catch", "extends", "int", "short", "try",
			"char", "final", "interface", "static", "void", "class", "finally",
			"long", "strictfp", "volatile", "const", "float", "native",
			"super", "while" };

	private static final List<Processor> processors = new ArrayList<>(3);

	static {
		StringBuilder sb = new StringBuilder();
		sb.append("\\b(?:");
		for (int i = 0; i < KEYWORDS.length; i++) {
			sb.append(KEYWORDS[i]);
			if (i < KEYWORDS.length - 1)
				sb.append("|");
		}
		sb.append(")\\b");
		KEYWORDS_PATTERN = sb.toString();
	}

	private Parser() {

	}

	private static String readFile(String filePath) {
		StringBuilder sb = new StringBuilder();
		try {
			Scanner scanner = new Scanner(new File(filePath));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				sb.append("<li>");
				if (line.isEmpty()) {
					sb.append("&nbsp;");
				} else {
					// < &lt; > &gt;
					if (!isCommentsOfLine(line)) {
						line = line.replaceAll("<", "&lt;");
						line = line.replaceAll(">", "&gt;");
					}

					for (Processor item : processors) {
						line = item.process(line);
					}
					// line = line.replaceAll("\\s", "&nbsp;&nbsp;");
					sb.append(line);
				}

				sb.append("</li>");

			}
		} catch (FileNotFoundException e) {

		}
		return sb.toString();
	}

	public static void buildHtml(String javaFileFullName,
			String htmlFileFullname) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><title>test</title></head><body><pre>");
		String result = readFile(javaFileFullName);
		sb.append("<ol>");
		sb.append(result);
		sb.append("</ol>");
		sb.append("</pre></body></html>");
		System.out.println(sb.toString());

		// Writer writer = new FileWriter(htmlFileFullname);
		// writer.write(sb.toString());
		// writer.close();
	}

	private static boolean isCommentsOfLine(String line) {
		boolean result = line.matches(SINGLE_COMMENTS_PATTERN);
		if (!result) {
			for (String pattern : MULTI_COMMENTS_PATTERN) {
				result = line.matches(pattern);
				if (result)
					return result;
			}
		} else {
			return true;
		}
		return false;
	}

	private static boolean isQuoteOfLine(String line) {
		Pattern p = Pattern.compile(LITERALS_PATTERN);
		Matcher m = p.matcher(line);
		return m.find();
	}

	public static void main(String[] args) throws IOException {

		processors.add(new Processor() {
			@Override
			public String process(String str) {
				if (!isCommentsOfLine(str)) {
					str = str.replaceAll(LITERALS_PATTERN,
							"<span style=\"color:blue;\">$0</span>");
				}
				return str;
			}
		});

		processors.add(new Processor() {
			@Override
			public String process(String str) {
				str = str.replaceAll(SINGLE_COMMENTS_PATTERN,
						"<span style=\"color:green;\">$0</span>");
				return processMultiComments(str);
			}

			private String processMultiComments(String str) {
				for (String pattern : MULTI_COMMENTS_PATTERN) {
					str = str.replaceAll(pattern,
							"<span style=\"color:green;\">$0</span>");
				}
				return str;
			}
		});

		processors.add(new Processor() {
			@Override
			public String process(String str) {
				if (!isCommentsOfLine(str) && !isQuoteOfLine(str)) {
					return str
							.replaceAll(KEYWORDS_PATTERN,
									"<span style=\"font-weight:bold;color:navy;\">$0</span>");
				}
				return str;
			}
		});

		// // String javaFileFullName = args[0];
		// String htmlFileName = args[1];
		char d = 'd';
		String javaFileFullName = "/home/ivan/mobile-development/java/code/exercise/src/com/test/collections/Parser.java";
		// System.out.println(readFile(javaFileFullName));
		buildHtml(javaFileFullName, "");
	}
}

package com.lyc;

public interface DataStore<T> {
  String DRIVER_ARRAY = "DRIVER_ARRAY";

  abstract void insert(T obj);

  abstract void remove(T obj);

  abstract T query(T obj);

  abstract String getDriver();
}

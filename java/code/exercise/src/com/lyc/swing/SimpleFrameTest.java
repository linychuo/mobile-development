package com.lyc.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class SimpleFrameTest {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GraphicsEnvironment env = GraphicsEnvironment
						.getLocalGraphicsEnvironment();
				GraphicsDevice vc = env.getDefaultScreenDevice();
				JFrame window = new JFrame();
				JPanel comp = new JPanel();
				comp.setBackground(Color.RED);
				window.add(comp);
				window.setUndecorated(true);
				window.setResizable(false);
				vc.setFullScreenWindow(window);
				// SimpleFrame frame = new SimpleFrame();
				// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				// frame.setVisible(true);
				// frame.setUndecorated(true);
				// Toolkit tk = Toolkit.getDefaultToolkit();
				// System.out.println(tk.getScreenSize().width);
				// System.out.println(tk.getScreenSize().height);
				// GraphicsEnvironment ge = GraphicsEnvironment
				// .getLocalGraphicsEnvironment();
				// GraphicsDevice[] gs = ge.getScreenDevices();
				// System.out.println(gs.length);
			}
		});
	}
}

class SimpleFrame extends JFrame {

	private static final long serialVersionUID = -896098185246746303L;

	private static final int DEFAULT_WIDTH = 300;
	private static final int DEFAULT_HEIGHT = 200;

	public SimpleFrame() {
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		Image icon = new ImageIcon(
				"/home/ivan/mobile-development/java/code/exercise/src/com/lyc/swing/eclipse.gif")
				.getImage();
		setIconImage(icon);
	}

	@Override
	public void paintComponents(Graphics g) {
		super.paintComponents(g);
		g.drawString("aaaa", 200, 100);
		update(g);
	}

}
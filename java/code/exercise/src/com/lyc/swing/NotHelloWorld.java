package com.lyc.swing;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class NotHelloWorld {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame jf = new NotHelloWorldFrame();
				jf.setTitle("NotHelloWorld");
				jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				jf.setVisible(true);
			}
		});
	}
}

class NotHelloWorldFrame extends JFrame {

	private static final long serialVersionUID = 7848117194869522381L;

	public NotHelloWorldFrame() {
		add(new NotHelloWorldComponent());
		pack();
	}
}

class NotHelloWorldComponent extends JPanel {
	private static final long serialVersionUID = -1478998769942355477L;
	public static final int MESSAGE_X = 75;
	public static final int MESSAGE_Y = 100;

	private static final int DEFAULT_WIDTH = 300;
	private static final int DEFAULT_HEIGHT = 200;

	@Override
	protected void paintComponent(Graphics g) {
		// Font sansbold14 = new Font("SansSerif", Font.BOLD, 14);
		// g.setFont(sansbold14);
		// g.setColor(Color.RED);
		g.drawString("Not a hello, world program", MESSAGE_X, MESSAGE_Y);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
}
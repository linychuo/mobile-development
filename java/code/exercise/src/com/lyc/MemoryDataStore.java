package com.lyc;


public class MemoryDataStore implements DataStore<Book> {

  private Book[] books;

  public MemoryDataStore(int initialCapacity) {
    if (initialCapacity < 0)
      throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
    this.books = new Book[initialCapacity];
  }

  public MemoryDataStore() {
    this.books = new Book[6];
  }

  @Override
  public String getDriver() {
    return DRIVER_ARRAY;
  }

  @Override
  public void insert(Book obj) {
    if (isFull()) throw new NoSpaceException();
    books[actualSize() - 1] = obj;
  }

  @Override
  public void remove(Book obj) {

  }

  @Override
  public Book query(Book obj) {
    return null;
  }


  public boolean isFull() {
    return actualSize() == size();
  }

  public int actualSize() {
    int counter = 0;
    for (Book book : books) {
      if (book != null) counter++;
    }
    return counter;
  }

  public int size() {
    return books.length;
  }
}

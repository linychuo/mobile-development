package com.lyc;

public class ConsoleMenu {

  private String label;
  private ActionLinstener listener;

  public ConsoleMenu(String label, ActionLinstener linstener) {
    this.label = label;
    this.listener = linstener;
  }

  public String getLabel() {
    return label;
  }

  public void setListener(ActionLinstener listener) {
    this.listener = listener;
  }

  public void click() {
    this.listener.performed();
  }
}

package com.lyc;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TestMenu {

  public static void main(String[] args) {
    ConsoleMenuMgr.add(new ConsoleMenu("新增图书", new ActionLinstener() {
      @Override
      public void performed() {
        System.out.println("新增一本图书");
      }
    }));

    ConsoleMenuMgr.add(new ConsoleMenu("查看图书", new ActionLinstener() {
      @Override
      public void performed() {
        System.out.println("查看一本图书");
      }
    }));

    ConsoleMenuMgr.add(new ConsoleMenu("退出", new ActionLinstener() {
      @Override
      public void performed() {
        System.exit(0);
      }
    }));

    printScreen();

    Scanner input = new Scanner(System.in);
    boolean appearError = false;
    do {
      try {
        int select = input.nextInt();
        ConsoleMenuMgr.invoke(select);
        appearError = false;
      } catch (NoSuchMenuException e) {
        printScreen();
      } catch (InputMismatchException e) {
        input.nextLine();
        System.out.print("输入错误，请重新输入:");
        appearError = true;
      }
    } while (appearError);

//    while (input.hasNextInt()) {
//      int select = input.nextInt();
//      try {
//        ConsoleMenuMgr.invoke(select);
//      } catch (NoSuchMenuException ex) {
//        printScreen();
//      }
//    }
  }

  private static void printScreen() {
    System.out.println("欢   迎   使   用   图   书   管   理   系   统");
    System.out.println(ConsoleMenuMgr.showMenu());
    System.out.print("请选择：");
  }
}

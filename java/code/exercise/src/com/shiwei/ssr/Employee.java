package com.shiwei.ssr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;


public class Employee extends Person implements Cloneable {

  private double salary;
  private Date hireDay;

  public Employee(String name, double s, int year, int month, int day) {
    super(name);
    this.salary = s;
    GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
    // GregorianCalendar uses 0 for January
    hireDay = calendar.getTime();
  }

  public void setHireDay(Date hireDay) {
    this.hireDay = hireDay;
  }

  public double getSalary() {
    return salary;
  }

  public Date getHireDay() {
    return hireDay;
  }

  public void raiseSalary(double byPercent) {
    double raise = salary * byPercent / 100;
    salary += raise;
  }

  @Override
  public String getDescription() {
    return String.format("an employee with a salary of $%.2f", salary);
  }

  @Override
  public boolean equals(Object otherObject) {
    // a quick test to see if the objects are identical
    if (this == otherObject) {
      return true;
    }
    // must return false if the explicit parameter is null
    if (otherObject == null) {
      return false;
    }
    // if the classes don't match, they can't be equal

    if (getClass() != otherObject.getClass()) {
      return false;
    }
    // now we know otherObject is a non-null Employee
    Employee other = (Employee) otherObject;
    // test whether the fields have identical values
    // return Objects.equals(this.getName(), other.getName()) && salary == other.salary
    // && Objects.equals(hireDay, other.getHireDay());
    return getName().equals(other.getName()) && salary == other.salary
           && hireDay.equals(other.hireDay);
  }

//  @Override
//  public Employee clone() throws CloneNotSupportedException {
//    Employee cloned = (Employee) super.clone();
//    cloned.hireDay = (Date) hireDay.clone();
//    return cloned;
//  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName(), salary, hireDay);
  }

  @Override
  public String toString() {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return getName() + ", " + getSalary() + ", " + df.format(getHireDay());
  }
}

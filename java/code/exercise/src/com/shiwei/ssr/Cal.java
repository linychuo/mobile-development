package com.shiwei.ssr;

public class Cal {
  public static void main(String[] args) {
    double radius = 5.5f;
    double length = 12f;
    double area = radius * radius * Math.PI;
    System.out.print("area = " + area + ", ");
    System.out.println((int) (area * 10000) / 10000.0);
    double volume = area * length;
    System.out.print("volume = " + volume + ", ");
    System.out.printf("%.1f, ", volume);
    System.out.println(Math.round(volume * 10) / 10.0);
  }
}

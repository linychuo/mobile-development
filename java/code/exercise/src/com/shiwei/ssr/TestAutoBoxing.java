package com.shiwei.ssr;

public class TestAutoBoxing {
  public static void main(String[] args) {
    String a = "a";
    String b = new String("a");
    System.out.println(a.hashCode() + ", " + b.hashCode());
    System.out.println(a == b);

    Integer c = 127;
    Integer d = 127;
    System.out.println(c.hashCode() + "," + d.hashCode());
    System.out.println(c == d);

    Character e = new Character('a');
    Character f = new Character('a');
    System.out.println(e.hashCode() + ", " + f.hashCode());
    System.out.println(e == f);

    Boolean g = new Boolean(false);
    Boolean h = new Boolean(false);
    System.out.println(g.hashCode() + ", " + h.hashCode());
    System.out.println(g == h);

    Byte i = new Byte((byte) 1);
    Byte j = new Byte((byte) 1);
    System.out.println(i.hashCode() + ", " + j.hashCode());
    System.out.println(i == j);

  }
}

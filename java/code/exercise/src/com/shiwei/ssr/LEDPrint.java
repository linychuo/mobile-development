package com.shiwei.ssr;

public class LEDPrint {

  private static final char[][] A = { {' ', ' ', ' ', 'A', ' ', ' ', ' '},
      {' ', ' ', 'A', ' ', 'A', ' ', ' '}, {' ', 'A', 'A', 'A', 'A', 'A', ' '},
      {'A', ' ', ' ', ' ', ' ', ' ', 'A'}};

  private static final char[][] E = { {'E', 'E', 'E', 'E', 'E', 'E', 'E'},
      {'E', ' ', ' ', ' ', ' ', ' ', ' '}, {'E', 'E', 'E', 'E', 'E', 'E', 'E'},
      {'E', ' ', ' ', ' ', ' ', ' ', ' '}, {'E', 'E', 'E', 'E', 'E', 'E', 'E'}};

  private static final char[][] H = { {'H', ' ', ' ', ' ', ' ', ' ', 'H'},
      {'H', ' ', ' ', ' ', ' ', ' ', 'H'}, {'H', 'H', 'H', 'H', 'H', 'H', 'H'},
      {'H', ' ', ' ', ' ', ' ', ' ', 'H'}, {'H', ' ', ' ', ' ', ' ', ' ', 'H'}};

  private static final char[][] J = { {' ', ' ', ' ', ' ', ' ', ' ', 'J'},
      {' ', ' ', ' ', ' ', ' ', ' ', 'J'}, {'J', ' ', ' ', ' ', ' ', ' ', 'J'},
      {' ', ' ', 'J', ' ', 'J', ' ', ' '}};

  private static final char[][] L = { {'L', ' ', ' ', ' ', ' ', ' ', ' '},
      {'L', ' ', ' ', ' ', ' ', ' ', ' '}, {'L', ' ', ' ', ' ', ' ', ' ', ' '},
      {'L', ' ', ' ', ' ', ' ', ' ', ' '}, {'L', 'L', 'L', 'L', 'L', 'L', 'L'}};

  private static final char[][] O = { {'O', 'O', 'O', 'O', 'O', 'O', 'O'},
      {'O', ' ', ' ', ' ', ' ', ' ', 'O'}, {'O', ' ', ' ', ' ', ' ', ' ', 'O'},
      {'O', ' ', ' ', ' ', ' ', ' ', 'O'}, {'O', 'O', 'O', 'O', 'O', 'O', 'O'}};

  private static final char[][] V = { {'V', ' ', ' ', ' ', ' ', ' ', 'V'},
      {' ', 'V', ' ', ' ', ' ', 'V', ' '}, {' ', ' ', 'V', ' ', 'V', ' ', ' '},
      {' ', ' ', ' ', 'V', ' ', ' ', ' '}};



  private static void print(char[][] charArray) {
    for (int i = 0; i < charArray.length; i++) {
      for (int j = 0; j < charArray[i].length; j++) {
//        System.out.print("[" + charArray[i][j] + "]");
        System.out.print(charArray[i][j]);
      }
      System.out.println();
    }
  }

  private static void fill(int begin, char[][] charArray, char[][] led) {
    for (int i = 0; i < charArray.length; i++) {
      for (int j = 0; j < charArray[i].length; j++) {
        led[i][j + begin] = charArray[i][j];
      }
    }
  }

  private static void init(char[][] led) {
    for (int i = 0; i < led.length; i++) {
      for (int j = 0; j < led[i].length; j++) {
        led[i][j] = ' ';
      }
    }
  }

  private static void printJAVA() {
    char[][] led = new char[5][28];
    init(led);


    fill(0, J, led);
    fill(7, A, led);
    fill(14, V, led);
    fill(21, A, led);
    print(led);
  }


  private static void printHELLO() {
    char[][] led = new char[5][39];
    init(led);


    fill(0, H, led);
    fill(8, E, led);
    fill(16, L, led);
    fill(24, L, led);
    fill(32, O, led);
    print(led);
  }

  public static void main(String[] args) {
    printHELLO();
    System.out.println();
    printJAVA();
  }


}

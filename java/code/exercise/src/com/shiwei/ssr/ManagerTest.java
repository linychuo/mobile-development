package com.shiwei.ssr;

public class ManagerTest {
  public static void main(String[] args) {
    // construct a Manager object
//    Manager boss = new Manager("Carl Cracker", 80000, 1987, 12, 15);
//    boss.setBonus(5000);
//
//    Employee[] staff = new Employee[3];
//
//    // fill the staff array with Manager and Employee objects
//    staff[0] = boss;
//    staff[1] = new Employee("Harry Hacker", 50000, 1989, 10, 1);
//    staff[2] = new Employee("Tommy Tester", 40000, 1990, 3, 15);
//
//    // print out information about all Employee objects
//    for (Employee e : staff)
//      System.out.println("name=" + e.getName() + ",salary=" + e.getSalary());


    System.out.println("------------");
    Employee e1 = new Employee("ivan", 20, 2000, 1, 1);
    Employee m1 = new Manager("joe", 30, 2000, 1, 1);

    System.out.println(e1.getSalary());
    System.out.println(m1.getSalary());
//    System.out.println(m1.equals(e1));
//    System.out.println(e1.hashCode());
//    System.out.println(m1.hashCode());


    System.out.println("++++++++++++++");
    Employee e2 = new Employee("lisi", 22, 2010, 1, 1);
    Employee e3 = new Employee("lisi", 22, 2010, 1, 1);
    System.out.println(e2.equals(e3));
    System.out.println(e2.hashCode());
    System.out.println(e3.hashCode());
  }
}

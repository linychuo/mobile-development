package com.shiwei.ssr;

public class TestCipher {
  private static final String LOWER_CASE_KEY = "qwertyuiopasdfghjklzxcvbnm";
  private static final String UPPER_CASE_KEY = "QWERTYUIOPASDFGHJKLZXCVBNM";

  public static void main(String[] args) {
    String str = "helLo, wOlD";
    System.out.println(encodingString(str));
  }

  private static String encodingString(String str) {
    char[] charOfStr = str.toCharArray();
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < charOfStr.length; i++) {
      result.append(encodingOneCharacter(charOfStr[i]));
    }
    return result.toString();
  }

  private static char encodingOneCharacter(char ch) {
    // ch可能不是字符，需要判断
    if (isLetter(ch)) {
      if (isUpperCaseLetter(ch)) {
        ch = UPPER_CASE_KEY.charAt(ch - 'A');
      } else {
        ch = LOWER_CASE_KEY.charAt(ch - 'a');
      }
    }
    return ch;
  }

  private static boolean isLetter(char ch) {
    // a:97, z:122 A:65 Z:90
    return isLowerCaseLetter(ch) || isUpperCaseLetter(ch);
  }

  private static boolean isLowerCaseLetter(char ch) {
    return ch >= 97 && ch <= 122;
  }

  private static boolean isUpperCaseLetter(char ch) {
    return ch >= 65 && ch <= 90;
  }
}

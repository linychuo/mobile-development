package com.shiwei.ssr;

import java.util.Objects;

public class Manager extends Employee {

  private double bonus;

  public Manager(String name, double s, int year, int month, int day) {
    super(name, s, year, month, day);
    bonus = 10;
  }

  @Override
  public double getSalary() {
    double baseSalary = super.getSalary();
    return baseSalary + bonus;
  }

  public void setBonus(double b) {
    bonus = b;
  }

  @Override
  public boolean equals(Object otherObject) {
    if (!super.equals(otherObject)) {
      return false;
    }
    Manager other = (Manager) otherObject;
    return bonus == other.bonus;
  }

  @Override
  public int hashCode() {
    return super.hashCode() + Objects.hash(bonus);
  }

  public double getBonus() {
    return bonus;
  }


}

package com.shiwei.ssr;

public class LetterSubstitutionCipher {

  private static final String LOWER_CASE_KEY = "qwertyuiopasdfghjklzxcvbnm";
  private static final String UPPER_CASE_KEY = "QWERTYUIOPASDFGHJKLZXCVBNM";

  private static String encrypt(String str) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < str.length(); i++) {
      char ch = encryptCharacter(str.charAt(i));
      result.append(ch);
    }
    return result.toString();
  }


  private static char encryptCharacter(char ch) {
    if (Character.isLetter(ch)) {
      if (Character.isLowerCase(ch)) {
        ch = LOWER_CASE_KEY.charAt(ch - 'a');
      } else {
        ch = UPPER_CASE_KEY.charAt(ch - 'A');
      }
    }
    return ch;
  }



  private static String decrypt(String str) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < str.length(); i++) {
      char ch = decryptCharacter(str.charAt(i));
      result.append(ch);
    }
    return result.toString();
  }


  private static char decryptCharacter(char ch) {
    if (Character.isLowerCase(ch)) {
      int index = LOWER_CASE_KEY.indexOf(ch);
      if (index != -1) {
        return (char) ('a' + index);
      }
    } else {
      int index = UPPER_CASE_KEY.indexOf(ch);
      if (index != -1) {
        return (char) ('A' + index);

      }
    }
    return ch;
  }

  public static void main(String[] args) {

    String plainText = "hEllo3SD23 wOrld";
    String cipherText = encrypt(plainText);
    String decryptionText = decrypt(cipherText);

    System.out.println(cipherText);
    System.out.println(decryptionText);
  }
}

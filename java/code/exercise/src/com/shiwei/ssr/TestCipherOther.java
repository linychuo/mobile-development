package com.shiwei.ssr;

public class TestCipherOther {
  private static final char[] LOWER_CASE_KEY = {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
      'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'};
  private static final char[] LOWER_CASE_KEY_F = {'a', 'b', 'c', 'd', 'e', 'f', 'j', 'h', 'i', 'j',
      'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

  public static void main(String[] args) {
    String str = "hello world, hi";
    System.out.println(encoding(str));
    System.out.println(decoding(encoding(str)));
  }

  private static String encoding(String str) {
    char[] charStr = str.toCharArray();
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < charStr.length; i++) {
      result.append(encodeingOneCharacter(charStr[i]));
    }
    return result.toString();
  }


  private static String decoding(String str) {
    char[] charStr = str.toCharArray();
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < charStr.length; i++) {
      result.append(decodingOneCharacter(charStr[i]));
    }
    return result.toString();
  }


  private static char encodeingOneCharacter(char ch) {
    for (int i = 0; i < 26; i++) {
      if (ch == LOWER_CASE_KEY_F[i]) {
        return LOWER_CASE_KEY[i];
      }
    }
    return ch;
  }


  private static char decodingOneCharacter(char ch) {
    for (int i = 0; i < 26; i++) {
      if (ch == LOWER_CASE_KEY[i]) {
        return LOWER_CASE_KEY_F[i];
      }
    }
    return ch;
  }
}

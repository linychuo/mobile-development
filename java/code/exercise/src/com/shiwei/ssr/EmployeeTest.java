package com.shiwei.ssr;

import java.util.Date;


/**
 * This program tests the Employee class
 *
 * @author ivan
 * @version 1.1 2013-12-8
 */
public class EmployeeTest {

  public static void main(String[] args) throws CloneNotSupportedException {
    // // fill the staff array with three Employee objects
    // Employee[] staff = new Employee[3];
    // staff[0] = new Employee("Carl Cracker", 7500, 1987, 12, 15);
    // staff[1] = new Employee("Harry Hacker", 5000, 1989, 10, 1);
    // staff[2] = new Employee("Tony Tester", 4000, 1990, 3, 15);
    //
    // // raise everyone's salary by 5%
    // for (Employee e : staff) {
    // e.raiseSalary(5);
    // }
    //
    //
    // // print out information about all Employee objects
    // for (Employee e : staff) {
    // System.out.println("name = " + e.getName() + ", salary = " + e.getSalary() + ", hireDay = "
    // + e.getHireDay() + ", " + e.getClass());
    // }

    Employee e = new Employee("Carl Cracker", 7500, 1987, 12, 15);
    System.out.println("1. " + e);
    Employee e1 = (Employee) e.clone();
    System.out.println("2. " + e1);
    e1.setHireDay(new Date());
    System.out.println("3. " + e1);
    System.out.println("----------------------");
    System.out.println("4. " + e);
    System.out.println(e.getHireDay() == e1.getHireDay());


  }
}

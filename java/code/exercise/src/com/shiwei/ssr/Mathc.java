package com.shiwei.ssr;

import java.util.Arrays;

public class Mathc {
	private static final char[] OPERATORS = { '+', '-', '*' };
	private static final int[][] STROAGE = new int[10][7];

	public static void main(String[] args) {
		// 1.生成单个两位数
		// 2.随机挑选操作符
		// 3.组合成一个2个数的运算式子
		// int i = 0;
		// while (i < 10) {
		// int[] subject =
		// {buildDoubleDigit(), buildOperator(), buildDoubleDigit(),
		// buildOperator(),
		// buildDoubleDigit(), buildOperator(), buildDoubleDigit()};
		// System.out.println(toString(subject) + "," + calc(subject));
		// if (validate(subject)) {
		// STROAGE[i] = subject;
		// i++;
		// }
		// }
		// System.out.println("-----------------------");
		// print(STROAGE);
		int[] a = { 2, '+', 0, '+', 4, '*', 2, '*', 1 };
		System.out.println(calc(a));
	}

	public static boolean validate(int[] args) {
		return !validateIsExisting(args) && validateIsValid(args);
	}

	public static boolean validateIsExisting(int[] args) {
		for (int i = 0; i < STROAGE.length; i++) {
			if (Arrays.equals(args, STROAGE[i])) {
				return true;
			}
		}
		return false;
	}

	public static boolean validateIsValid(int[] args) {
		return calc(args) > 0;
	}

	public static int calc(int[] args) {
		int[] newArrays = Arrays.copyOf(args, args.length);
		calcMulti(newArrays);
		int[] subject = removeZeroOfArray(newArrays);
		return calcOther(subject);
	}

	public static int calcOther(int[] args) {
		int result = args[0];
		for (int i = 1; i < args.length; i = i + 2) {
			switch (args[i]) {
			case '+':
				// 43
				if (i < args.length - 1) {
					result += args[i + 1];
					System.out.println("aaa  = " + result);
				}
				break;
			case '-':
				// 45
				// if (result - args[i + 1] < 0) {
				// int temp = result;
				// result = args[i + 1];
				// args[i + 1] = temp;
				// }
				if (i < args.length - 1) {
					if (result - args[i + 1] < 0) {
						// 如果中间结果产生负数，返回－１标明产生的题无效
						return -1;
					}
					result -= args[i + 1];
					System.out.println("sss = " + result);
				}
				break;
			}
		}
		return result;
	}

	public static int calcMulti(int[] args) {
		int latestMultiIdx = 0;
		int multiIdx = 0;
		int fromIndex = 0;
		int result = 1;

		while (true) {
			multiIdx = searchMulti(args, fromIndex + 1);
			System.out.println("multiIdx = " + multiIdx + ", latestMultiIdx = "
					+ latestMultiIdx);
			if (multiIdx == -1) {
				break;
			} else {
				args[multiIdx] = 0;
				if (multiIdx - latestMultiIdx == 2) {
					result *= args[multiIdx + 1];
					args[multiIdx + 1] = result;
					args[multiIdx - 1] = 0;
				} else {
					result = args[multiIdx - 1] * args[multiIdx + 1];
					args[multiIdx + 1] = result;
					args[multiIdx - 1] = 0;
				}
				fromIndex = multiIdx;
				latestMultiIdx = multiIdx;
			}
		}
		System.out.println(Arrays.toString(args));
		return result;
	}

	public static int[] removeZeroOfArray(int[] ary) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ary.length; i++) {
			if (ary[i] != 0) {
				sb.append(ary[i]);
				sb.append(",");
			}
		}
		String[] ret = sb.toString().split(",");
		int[] result = new int[ret.length];
		for (int i = 0; i < ret.length; i++) {
			result[i] = Integer.parseInt(ret[i]);
		}
		return result;
	}

	public static int searchMulti(int[] args, int fromIndex) {
		for (int i = fromIndex; i < args.length; i++) {
			if (i % 2 != 0 && args[i] == 42) {
				return i;
			}
		}
		return -1;
	}

	public static String toString(int[] ary) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ary.length; i++) {
			if (i % 2 != 0) {
				sb.append(" " + (char) ary[i] + " ");
			} else {
				sb.append(ary[i]);
			}
		}
		return sb.toString();
	}

	public static void print(int[][] args) {
		for (int i = 0; i < args.length; i++) {
			System.out.println(toString(args[i]));
		}
	}

	public static int buildDoubleDigit() {
		return (int) (Math.random() * 10 + 1);
	}

	public static char buildOperator() {
		return OPERATORS[(int) (Math.random() * 3)];
	}
}

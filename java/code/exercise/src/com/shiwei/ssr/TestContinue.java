package com.shiwei.ssr;

import java.util.Scanner;

public class TestContinue {

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = 0;
    int sum = 0;
    int goal = 10;
    while (sum < goal) {
      System.out.print("Enter a number: ");
      n = in.nextInt();
      if (n < 0) continue;
      sum += n; // not executed if n < 0
    }
  }
}

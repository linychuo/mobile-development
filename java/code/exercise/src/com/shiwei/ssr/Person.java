package com.shiwei.ssr;

public abstract class Person {

  private String name;
  protected String a;

  public Person(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  protected void hell() {
    System.out.println("hello");
  }

  public abstract String getDescription();
}

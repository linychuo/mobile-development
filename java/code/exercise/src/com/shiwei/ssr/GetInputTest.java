package com.shiwei.ssr;

import java.util.Scanner;


public class GetInputTest {


  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    System.out.print("Please enter the age : ");
    String ageStr = input.next();
    int age = convertToPositiveInteger(ageStr);
    while (age == 0 || age > 120) {
      System.out.println("Age needs to be between 1 and 120 !");
      System.out.print("Please enter the age again: ");
      ageStr = input.next();
      age = convertToPositiveInteger(ageStr);
    }
    System.out.println("age is : " + age);
  }

  public static int convertToPositiveInteger(String str) {
    if (str.matches("\\d+")) {
      try {
        return Integer.parseInt(str);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return 0;
  }

  public static int convertToNegativeInteger(String str) {
    if (str.matches("-\\d+")) {
      try {
        return Integer.parseInt(str);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return 0;
  }
}

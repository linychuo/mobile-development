package com.shiwei.ssr;

import java.util.Scanner;

public class Test {
  public static void main(String\u005B\u005D args) {
    int a = 1_00_0_0;
    System.out.println(a);
    double b = 0x1.0p-3;
    System.out.println(b);
    double c = -1.0 / 0.0;
    System.out.println(c);
    System.out.println(2.0 - 1.1);
    char d = '\u0022';
    System.out.println(d);
    System.out.println(2 & 2);
    System.out.println(~1);
    System.out.println(5 << 3);
    System.out.println(8 >> 3);
    System.out.println(12 >>> 1);

    Scanner input = new Scanner(System.in);
    input.useDelimiter("\b");
    while (input.hasNext()) {
      String aa = input.next();
      System.out.println(aa);
    }

  }
}

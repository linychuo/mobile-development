package com.shiwei.ssr;

public interface ClassInInterface {
  int id = 1;
  void howdy();

  class Test implements ClassInInterface {
    public void howdy() {
      System.out.println(id);
    }

    public static void main(String[] args) {
      new Test().howdy();
    }
  }
}

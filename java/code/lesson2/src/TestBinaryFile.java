import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by ivan on 14-7-30.
 */
public class TestBinaryFile {

  public static void main(String[] args) throws IOException {
    File f = new File(args[0]);
    System.out.println(new Date(f.lastModified()));
    System.out.println(f.length());

//    DataOutputStream dou = new DataOutputStream(new FileOutputStream(f));
//    dou.writeChars("hello");
//    dou.close();

    FileOutputStream fos = new FileOutputStream(f);
    fos.write(97);
    fos.close();

//    FileInputStream input = new FileInputStream(f);
//    int value;
//
//    while ((value = input.read()) != -1) {
//      System.out.print(value + " ");
//    }
//    System.out.println();
//    // Close the output stream
//    input.close();

    DataInputStream dis = new DataInputStream(new FileInputStream(f));
    int value;

    while ((value = dis.read()) != -1) {
      char ch = (char) value;
      System.out.println(ch);
    }
    dis.close();
  }
}

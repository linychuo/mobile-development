package custom;

import java.util.Arrays;

/**
 * Created by ivan on 14-7-24.
 */
public class ArrayList<E> {

  E[] values;
  private int index;

  public ArrayList() {
    values = (E[]) new Object[5];
    index = 0;
  }

  public void add(E o) {
    if (index >= values.length) {
      E[] newValues = Arrays.copyOf(values, values.length + 5);
      values = newValues;
    }
    values[index] = o;
    index++;
  }

  public E get(int idx) {
    if (idx > index) {
      return null;
    }
    return values[idx];
  }

  public void clear() {
    for (int i = 0; i < values.length; i++) {
      values[i] = null;
    }
    index = 0;
  }

  public boolean contains(E o) {
    return indexOf(o) >= 0;
  }

  public int indexOf(E o) {
    if (o != null) {
      for (int i = 0; i < values.length; i++) {
        if (o.equals(values[i])) {
          return i;
        }
      }
    }
    return -1;
  }

  public int lastIndexOf(E o) {
//    for (int i = values.length - 1; i >= 0; i--) {
//      if (o.equals(values[i])) {
//        return i;
//      }
//    }
//    return -1;

    int result = -1;
    for (int i = 0; i < values.length; i++) {
      if (o.equals(values[i])) {
        result = i;
      }
    }
    return result;
  }

  public boolean isEmpty() {
    return index == 0;
  }

  public int size() {
    return index;
  }

  public boolean remove(E o) {
    if (o == null) {
      return false;
    }
    for (int i = 0; i < values.length; i++) {
      if (o.equals(values[i])) {
        remove(i);
      }
    }
    return true;
  }

  public boolean remove(int idx) {
    if (values[idx] == null) {
      return false;
    }

    int i = idx;
    for (; i < index - 1; i++) {
      values[i] = values[i + 1];
    }
    values[i] = null;
    return true;
  }
}

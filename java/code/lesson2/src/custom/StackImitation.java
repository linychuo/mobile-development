package custom;

import java.util.Arrays;

/**
 * Created by ivan on 14-7-28.
 */
public class StackImitation<E> {

  E[] value;
  private int id;//数组下标

  //无参构造函数，初始化value数组的长度和id
  public StackImitation() {
    value = (E[]) new Object[5];
    id = 0;
  }

  //入栈
  public void push(E newStack) {
    if (id >= value.length) {
      E[]
          newValue =
          Arrays.copyOf(value, value.length + 5);//建一个Object类型的数组newValue,长度是value的长度加5，值是复制value的值
      value = newValue;//将newValue的引用复制给value
    }
    value[id] = newStack;
    id++;
  }

  //查看栈顶元素
  public E peek() {
    return value[id - 1];
  }

  //查看指定元素位置
  public int search(E j) {
    for (int i = 0; i <= id; i++) {
      if (value[i].equals(j)) {
        return i;
      }
    }
    return -1;
  }

  //查看栈
  public int length() {
    return id;
  }

  //清空栈
  public void clear() {
    for (int i = value.length - 1; i >= 0; i--) {
      value[i] = null;
    }
    id = 0;
  }

  //判断是否为空
  public boolean isEmpty() {
    if (id == 0) {
      return true;
    }
    return false;
  }

  //移除栈顶元素
  public E pop() {
    E i = value[id - 1];
    value[id - 1] = null;
    id -= 1;
    return i;
  }

  public void print() {
    for (int i = id - 1; i >= 0; i--) {
      System.out.print(value[i] + "\t");
    }
    System.out.println();
  }
}

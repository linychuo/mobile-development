package custom;

/**
 * Created by ivan on 14-7-29.
 */
public class Student implements Comparable<Student> {

  private int id;

  public Student(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  @Override
  public int compareTo(Student o) {
    if (this.id == o.getId()) {
      return 0;
    } else if (this.id > o.getId()) {
      return 1;
    } else {
      return -1;
    }
  }

  @Override
  public String toString() {
    return "id = " + id;
  }
}

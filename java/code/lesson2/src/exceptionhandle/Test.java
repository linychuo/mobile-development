package exceptionhandle;

/**
 * Created by ivan on 14-7-25.
 */
public class Test {

  public static void main(String[] args) {
    System.out.println("result = " + fun(4, 2));
  }

  private static int fun(int num1, int num2) {
    int a = 2;
    try {
      a = 4;
      int c = num1 / num2;
      a = 5;
      return a;
    } catch (ArithmeticException e) {
      System.out.println("=====");
      a = 6;
    } finally {
      a = 7;
      System.out.println("-------------");
    }

    return a;
  }


  private static int fun1(int num1, int num2) {
    return num1 / num2;
  }
}

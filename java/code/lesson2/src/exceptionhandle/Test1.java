package exceptionhandle;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by ivan on 14-7-25.
 */
public class Test1 {


  public static void main(String[] args) throws IOException {
//    try {
      InputStream is = null;
      Scanner scanner = null;
      try {
        is = new FileInputStream("/home/ivan/t.py");
        scanner = new Scanner(is);
        while (scanner.hasNextLine()) {
          System.out.println(scanner.nextLine());
        }
      } finally {
        System.out.println("33333333333");
        if (is != null) {
          is.close();
        }
        if (scanner != null) {
          scanner.close();
        }
      }
//    } catch (FileNotFoundException e) {
//      System.out.println("11111111111111");
//    } catch (IOException e) {
//      System.out.println("222222222222222");
//    }

    print();
  }


  public static void print() {
    try (InputStream is = new FileInputStream("/home/ivan/t.py")) {
      try (Scanner scanner = new Scanner(is)) {
        while (scanner.hasNextLine()) {
          System.out.println(scanner.nextLine());
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

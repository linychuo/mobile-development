import custom.ArrayList;
import custom.Student;

/**
 * Created by ivan on 14-7-28.
 */
public class ArrayAlg {

  @SafeVarargs
  public static <T> T getMiddle(T... a) {
    return a[a.length / 2];
  }

  public static <T extends Comparable<T>> T max(T a, T b) {
    return a.compareTo(b) > 0 ? a : b;
  }

  public static <T extends Comparable<T>> T min(T[] a) {
    if (a == null || a.length == 0) {
      return null;
    }
    T smallest = a[0];
    for (int i = 1; i < a.length; i++) {
      if (smallest.compareTo(a[i]) > 0) {
        smallest = a[i];
      }
    }
    return smallest;
  }

  public static void main(String[] args) {
    ArrayList d = new ArrayList();
    ArrayList<int[]> a = new ArrayList<>();
    a.add(new int[]{1});
    System.out.println(ArrayAlg.<Integer>max(new Integer(6), 4));
    System.out.println(ArrayAlg.<Double>max(2.13, new Double(1.02)));
    System.out.println(ArrayAlg.min(new Integer[]{1, 2, 3}));
    System.out.println(ArrayAlg.<String>max("a", "b"));
    System.out.println(ArrayAlg.min(new Student[]{new Student(4), new Student(2)}));
  }
}

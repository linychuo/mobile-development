/**
 * Created by ivan on 14-7-18.
 */
public class Manager extends Employee {

  private double bonus;

  public double getBonus() {
    return bonus;
  }

  public Manager(String name, double salary, int year, int month, int day, double bonus) {
    super(name, salary, year, month, day);
    this.bonus = bonus;
  }

  @Override
  public double getSalary() {
    double baseSalary = super.getSalary();
    return (float) baseSalary + bonus;
  }

}

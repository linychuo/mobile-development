package thread;

/**
 * Created by ivan on 14-7-31.
 */
public class TaskTest {

  public static void main(String[] args) throws InterruptedException {
    Thread thread1 = new Thread(new PrintChar('a', 100));
    Thread thread2 = new Thread(new PrintChar('b', 100));
    Thread thread3 = new Thread(new PrintNum(100));

    thread1.start();
//    thread1.join();
    thread2.start();
//    thread2.join();
    thread3.start();
  }
}

class PrintChar implements Runnable {

  private char charToPrint;
  private int times;

  public PrintChar(char c, int t) {
    charToPrint = c;
    times = t;
  }

  @Override
  public void run() {
//    try {
    for (int i = 0; i < times; i++) {
      System.out.println(charToPrint);
//      Thread.yield();
    }
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
  }
}


class PrintNum implements Runnable {

  private int lastNum;

  public PrintNum(int n) {
    lastNum = n;
  }

  @Override
  public void run() {
    for (int i = 0; i <= lastNum; i++) {
      System.out.printf(" " + i);
    }
  }
}
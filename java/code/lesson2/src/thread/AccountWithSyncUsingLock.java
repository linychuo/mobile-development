package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by ivan on 14-7-31.
 */
public class AccountWithSyncUsingLock {

  private static Account account = new Account();

  public static void main(String[] args) {
    ExecutorService executor = Executors.newCachedThreadPool();

    for (int i = 0; i < 100; i++) {
      executor.execute(new AddAPennyTask());
    }

    executor.shutdown();

    while (!executor.isTerminated()) {

    }

    System.out.println(account.getBalance());

  }


  private static class AddAPennyTask implements Runnable {

    @Override
    public void run() {
      account.deposit(1);
    }
  }

  private static class Account {

    private static Lock lock = new ReentrantLock();
    private int balance = 0;

    public int getBalance() {
      return balance;
    }

    public synchronized void deposit(int amount) {
      lock.lock();
      try {
        int newBalance = balance + amount;
        balance = newBalance;
      } finally {
        lock.unlock();
      }
    }
  }
}

package thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ivan on 14-7-31.
 */
public class ArrayListMultiThread {

  //  private static List<String> str = Collections.synchronizedList(new ArrayList<String>());
  private static List<String> str = new ArrayList<String>();

  public static void main(String[] args) throws InterruptedException {
    ExecutorService executor = Executors.newFixedThreadPool(3);

    executor.execute(new AddTask("task1 "));
    executor.execute(new AddTask("task2 "));
    executor.execute(new AddTask("task3 "));

    executor.shutdown();

    System.out.println(str.size());
    for (int i = 0; i < str.size(); i++) {
      System.out.println(str.get(i));
    }
  }

  private static class AddTask implements Runnable {

    private String name;

    public AddTask(String name) {
      this.name = name;
    }

    @Override
    public void run() {
      for (int i = 0; i < 10; i++) {
        str.add(name + i);
      }
    }
  }
}

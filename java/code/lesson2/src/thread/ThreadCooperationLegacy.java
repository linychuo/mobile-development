package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ivan on 14-7-31.
 */
public class ThreadCooperationLegacy {

  private static Account account = new Account();

  public static void main(String[] args) {
    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.execute(new DepositTask());
    executor.execute(new WithdrawTask());
    executor.shutdown();
  }

  private static class DepositTask implements Runnable {

    @Override
    public void run() {
      try {
        for (int i = 0; i < 10; i++) {
          account.deposit((int) (Math.random() * 10) + 1);
          Thread.sleep(400);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }


  private static class WithdrawTask implements Runnable {

    @Override
    public void run() {
      try {
        for (int i = 0; i < 10; i++) {
          account.withdraw((int) (Math.random() * 10) + 1);
          Thread.sleep(400);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private static class Account {

    private int balance = 0;

    public int getBalance() {
      return balance;
    }

    public synchronized void withdraw(int amount) {
      try {
        while (balance < amount) {
          System.out.println("\t\t\tWait for a deposit");
          wait();
        }

        balance -= amount;
        System.out.println("\t\t\tWithdraw " + amount +
                           "\t\t" + getBalance());
      } catch (InterruptedException ex) {
        ex.printStackTrace();
      }
    }


    public synchronized void deposit(int amount) {
      balance += amount;
      System.out.println("Deposit " + amount +
                         "\t\t\t\t\t" + getBalance());
      notifyAll();
    }
  }
}

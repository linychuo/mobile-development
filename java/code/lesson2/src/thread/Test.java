package thread;

import java.util.Date;

/**
 * Created by ivan on 14-7-31.
 */
public class Test {

  public static void main(String[] args) {
    new Thread(new PrintTime()).start();
  }
}

class PrintTime implements Runnable {

  @Override
  public void run() {
    try {
      while (true) {
        System.out.println(new Date());
        Thread.sleep(1000);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}

import java.util.Arrays;

/**
 * Created by ivan on 14-7-22.
 */
public class EnumTest {

  private static enum SIZE {SMALL, LARGE}

  public class Size {
    public static final int SMALL = 1;
    public static final int LARGE = 2;
  }

  public class Other extends Size{

  }
//  public enum SIZE {
//    SMALL("S"), MEDIUM("M"), LARGE("L"), EXTRA_LARGE("XL");
//
//    private String abbreviation;
//
//    private SIZE(String abbreviation) {
//      this.abbreviation = abbreviation;
//    }
//
//    public String getAbbreviation() {
//      return abbreviation;
//    }
//  }

  public static void main(String[] args) {
    System.out.println(Arrays.toString(SIZE.values()));
//    SIZE a = SIZE.LARGE;
    int size = Size.LARGE;
    switch (size) {
      case Size.SMALL:
        System.out.println("========");
        break;
      case Size.LARGE:
        System.out.println("==-=-=-=-=-=-=-=-");
        break;
    }
  }

}

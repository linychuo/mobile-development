package test2;

import test2.menu.CheckBalanceMenu;
import test2.menu.Context;
import test2.menu.DepositMenu;
import test2.menu.ExitMenu;
import test2.menu.PrintTransMenu;
import test2.menu.WithDrawMenu;

/**
 * Created by ivan on 14-7-21.
 */
public class Test {

  public static void main(String[] args) {
    Context ctx = new Context();

    Screen screen = new Screen();
    screen.addMenu(new CheckBalanceMenu());
    screen.addMenu(new WithDrawMenu());
    screen.addMenu(new PrintTransMenu());
    screen.addMenu(new DepositMenu());
    screen.addMenu(new ExitMenu());

    while (true) {
      Condition d = new ACondition();
      int userId = Util.captureInt("Enter an id: ", d);
      ctx.setUserId(userId);

      while (true) {
        screen.displayMenu();
        boolean isGoHead = screen.clickMenu(ctx);
        if (isGoHead) {
          break;
        }
      }
    }

  }

}

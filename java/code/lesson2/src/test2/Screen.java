package test2;

import java.util.ArrayList;
import java.util.List;

import test2.menu.Context;
import test2.menu.Menu;

/**
 * Created by ivan on 14-7-21.
 */
public class Screen {

  private List<Menu> menus = new ArrayList<Menu>();

  public void displayMenu() {
    System.out.println("Main menu");
    for (Menu menu : menus) {
      System.out.println(menu.getId() + ": " + menu.getLabel());
    }
  }

  public boolean clickMenu(Context ctx) {
    int menuId = Util.captureInt("Enter a choice: ", new Condition() {
      @Override
      public boolean check(int result) {
        return result > 0 && result <= menus.size();
      }
    });
    Menu selectedMenu = null;

    for (Menu menu : menus) {
      if (menu.getId() == menuId) {
        selectedMenu = menu;
        break;
      }
    }

    return selectedMenu.process(ctx);
  }

  public void addMenu(Menu menu) {
    menu.setId(menus.size() + 1);
    menus.add(menu);
  }

}

package test2.menu;

import test2.AccountNotFoundException;

/**
 * Created by ivan on 14-7-21.
 */
public class CheckBalanceMenu extends Menu {

  public CheckBalanceMenu() {
    super("check balance");
  }

  @Override
  public boolean process(Context context) {
    try {
      double balance = context.getAtm().queryBalance(context.getUserId());
      System.out.println("The balance is " + balance);
      return false;
    } catch (AccountNotFoundException e) {
      System.out.print("account not found! ");
    }
    return true;
  }
}

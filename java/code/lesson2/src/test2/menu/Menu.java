package test2.menu;

/**
 * Created by ivan on 14-7-21.
 */
public abstract class Menu {

  private int id;
  private String label;

  public Menu(String label) {
    this.label = label;
  }

  public abstract boolean process(Context context);

  public String getLabel() {
    return label;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}

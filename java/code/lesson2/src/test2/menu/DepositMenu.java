package test2.menu;

import test2.AccountNotFoundException;
import test2.Condition;
import test2.Util;

/**
 * Created by ivan on 14-7-21.
 */
public class DepositMenu extends Menu {

  public DepositMenu() {
    super("deposit");
  }

  @Override
  public boolean process(Context context) {
    double amount = Util.captureDouble("Enter an amount to deposit: ", new Condition() {
      @Override
      public boolean check(double result) {
        return result < 0;
      }
    });
    try {
      context.getAtm().deposit(context.getUserId(), amount);
      return false;
    } catch (AccountNotFoundException e) {
      System.out.print("account not found! ");
    }
    return true;
  }
}

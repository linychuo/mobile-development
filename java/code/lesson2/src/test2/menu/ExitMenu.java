package test2.menu;

/**
 * Created by ivan on 14-7-21.
 */
public class ExitMenu extends Menu {

  public ExitMenu() {
    super("exit");
  }

  @Override
  public boolean process(Context context) {
    return true;
  }
}

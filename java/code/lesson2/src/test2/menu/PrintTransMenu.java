package test2.menu;

import java.util.List;

import test2.AccountNotFoundException;
import test2.Transaction;

/**
 * Created by ivan on 14-7-22.
 */
public class PrintTransMenu extends Menu {

  public PrintTransMenu() {
    super("list transactions");
  }

  @Override
  public boolean process(Context context) {
    try {
      List<Transaction> transactionList = context.getAtm().queryTransByUserId(context.getUserId());
      System.out
          .println("=========================================================================");
      for (Transaction transaction : transactionList) {
        System.out.println("|\t" + transaction + "\t|");
      }
      System.out
          .println("=========================================================================");
      return false;
    } catch (AccountNotFoundException e) {
      System.out.print("account not found! ");
    }
    return true;
  }
}

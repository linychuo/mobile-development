package test2.menu;

import test2.ATM;

/**
 * Created by ivan on 14-7-21.
 */
public class Context {

  private final ATM atm;
  private int userId;

  public Context() {
    this.atm = new ATM();
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getUserId() {
    return userId;
  }

  public ATM getAtm() {
    return atm;
  }
}

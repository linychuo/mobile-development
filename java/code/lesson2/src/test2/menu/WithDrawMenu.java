package test2.menu;

import test2.AccountNotFoundException;
import test2.Condition;
import test2.Util;

/**
 * Created by ivan on 14-7-21.
 */
public class WithDrawMenu extends Menu {

  public WithDrawMenu() {
    super("withdraw");
  }

  @Override
  public boolean process(Context context) {
    double amount = Util.captureDouble("Enter an amount to withdraw: ", new Condition() {
      @Override
      public boolean check(double result) {
        return result < 0;
      }
    });
    try {
      boolean result = context.getAtm().withdraw(context.getUserId(), amount);
      if (result) {
        System.out.println("withdraw successfully!");
      } else {
        System.out.println("withdraw failed!");
      }

      return false;
    } catch (AccountNotFoundException e) {
      System.out.print("account not found! ");
    }
    return true;
  }
}

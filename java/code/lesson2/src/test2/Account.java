package test2;

import java.util.ArrayList;

/**
 * Created by ivan on 14-7-21.
 */
public class Account {

  private int id;
  private double balance;
  private String name;
  private ArrayList<Transaction> transactions;

  public Account(int id, String name) {
    this.id = id;
    this.name = name;
    this.balance = 100;
    transactions = new ArrayList<Transaction>();
  }

  public Account(int id, String name, double balance) {
    this(id, name);
    this.balance = balance;
  }

  public int getId() {
    return id;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public void addTransaction(Transaction transaction) {
    this.transactions.add(transaction);
  }

  public ArrayList<Transaction> getTransactions() {
    return transactions;
  }
}

package test2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by ivan on 14-7-21.
 */
public class Util {

  private static Scanner input = new Scanner(System.in);
  private static DateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

  private Util() {

  }

  public static int captureInt(String promptMsg, Condition condition) {
    int result = -1;
    boolean appearError = false;

    System.out.print(promptMsg);
    do {
      try {
        result = input.nextInt();
        if (condition.check(result)) {
          appearError = false;
        } else {
          appearError = true;
          System.out.print("illegal input, please again: ");
        }
      } catch (InputMismatchException ex) {
        input.nextLine();
        appearError = true;
        System.out.print("illegal input, please again: ");
      }
    } while (appearError);
    return result;
  }


  public static double captureDouble(String promptMsg, Condition condition) {
    double result = 0;
    boolean appearError = false;
    System.out.print(promptMsg);
    do {
      try {
        result = input.nextDouble();
        if (condition.check(result)) {
          appearError = true;
          System.out.print("illegal input, please again: ");
        } else {
          appearError = false;
        }
      } catch (InputMismatchException ex) {
        input.nextLine();
        appearError = true;
        System.out.print("illegal input, please again: ");
      }
    } while (appearError);
    return result;
  }


  public static String format(Date date) {
    return df.format(date);
  }

}

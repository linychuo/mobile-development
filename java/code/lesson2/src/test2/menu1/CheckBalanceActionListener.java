package test2.menu1;

import test2.AccountNotFoundException;
import test2.menu.Context;

/**
 * Created by ivan on 14-7-23.
 */
public class CheckBalanceActionListener implements ActionListener {

  @Override
  public boolean process(Context context) {
    try {
      double balance = context.getAtm().queryBalance(context.getUserId());
      System.out.println("The balance is " + balance);
      return false;
    } catch (AccountNotFoundException e) {
      System.out.print("account not found! ");
    }
    return true;
  }
}

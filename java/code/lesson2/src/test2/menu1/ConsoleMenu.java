package test2.menu1;

import test2.menu.Context;

/**
 * Created by ivan on 14-7-23.
 */
public class ConsoleMenu {

  private int id;
  private String label;
  private ActionListener actionListener;

  public ConsoleMenu(String label, ActionListener actionListener) {
    this.label = label;
    this.actionListener = actionListener;
  }

  public String getLabel() {
    return label;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public boolean click(Context context) {
    return actionListener.process(context);
  }
}

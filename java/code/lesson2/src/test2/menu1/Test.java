package test2.menu1;

import test2.Condition;
import test2.Util;
import test2.menu.Context;

/**
 * Created by ivan on 14-7-23.
 */
public class Test {

  public static void main(String[] args) {
    Context ctx = new Context();
    Screen screen = new Screen();

    screen.addMenu(new ConsoleMenu("check balance", new CheckBalanceActionListener()));
    screen.addMenu(new ConsoleMenu("deposit", new DepositActionListener()));
    screen.addMenu(new ConsoleMenu("exit", new ActionListener() {
      @Override
      public boolean process(Context context) {
        return true;
      }
    }));

    while (true) {
      int userId = Util.captureInt("Enter an id: ", new Condition() {
        @Override
        public boolean check(int result) {
          return result >= 0;
        }
      });
      ctx.setUserId(userId);

      while (true) {
        screen.displayMenu();
        boolean isGoHead = screen.clickMenu(ctx);
        if (isGoHead) {
          break;
        }
      }
    }
  }
}

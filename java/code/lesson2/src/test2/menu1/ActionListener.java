package test2.menu1;

import test2.menu.Context;

/**
 * Created by ivan on 14-7-23.
 */
public interface ActionListener {

  boolean process(Context context);
}

package test2.menu1;

import java.util.ArrayList;
import java.util.List;

import test2.Condition;
import test2.Util;
import test2.menu.Context;

/**
 * Created by ivan on 14-7-23.
 */
public class Screen {

  private List<ConsoleMenu> menus = new ArrayList<ConsoleMenu>();

  public void displayMenu() {
    System.out.println("Main menu");
    for (ConsoleMenu menu : menus) {
      System.out.println(menu.getId() + ": " + menu.getLabel());
    }
  }

  public void addMenu(ConsoleMenu menu) {
    menu.setId(menus.size() + 1);
    this.menus.add(menu);
  }


  public boolean clickMenu(Context ctx) {
    int menuId = Util.captureInt("Enter a choice: ", new Condition() {
      @Override
      public boolean check(int result) {
        return result > 0 && result <= menus.size();
      }
    });

    ConsoleMenu selectedMenu = null;

    for (ConsoleMenu menu : menus) {
      if (menu.getId() == menuId) {
        selectedMenu = menu;
        break;
      }
    }

    return selectedMenu.click(ctx);
  }
}

package test2.menu1;

import test2.AccountNotFoundException;
import test2.Condition;
import test2.Util;
import test2.menu.Context;

/**
 * Created by ivan on 14-7-23.
 */
public class DepositActionListener implements ActionListener {

  @Override
  public boolean process(Context context) {
    double amount = Util.captureDouble("Enter an amount to deposit: ", new Condition() {
      @Override
      public boolean check(double result) {
        return result < 0;
      }
    });
    try {
      context.getAtm().deposit(context.getUserId(), amount);
      return false;
    } catch (AccountNotFoundException e) {
      System.out.print("account not found! ");
    }
    return true;
  }
}

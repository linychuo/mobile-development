package test2;

/**
 * Created by ivan on 14-7-23.
 */
public class ACondition extends Condition {

  @Override
  public boolean check(int result) {
    return result >= 0;
  }

}

package test2;

/**
 * Created by ivan on 14-7-21.
 */
public abstract class Condition {

  public boolean check(int result) {
    return false;
  }

  public boolean check(double result) {
    return false;
  }
}

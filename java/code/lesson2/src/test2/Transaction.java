package test2;

import java.util.Date;

/**
 * Created by ivan on 14-7-22.
 */
public class Transaction {

  enum Type {W, D}

  private Date date;//操作的时间
  private Type type;

  private double amount;
  private double balance;
  private String description;

  public Transaction(Type type, double amount, double balance,
                     String description) {
    this.type = type;
    this.amount = amount;
    this.balance = balance;
    this.description = description;
    this.date = new Date();
  }


  @Override
  public String toString() {
    return "你在" + Util.format(date) + translate() + amount + "，你的账户剩余 " + balance + " ["
           + description + "]";
  }

  private String translate() {
    return type == Type.W ? "取了" : "存了";
  }
}

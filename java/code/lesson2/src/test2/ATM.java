package test2;

import java.util.ArrayList;

/**
 * Created by ivan on 14-7-21.
 */
public class ATM {

  private Account[] database;

  public ATM() {
    database = new Account[10];
    for (int i = 0; i < database.length; i++) {
      database[i] = new Account(i, "NO." + (i + 1));
    }
  }

  private Account query(int id) throws AccountNotFoundException {
    for (Account account : database) {
      if (account.getId() == id) {
        return account;
      }
    }

    throw new AccountNotFoundException();
  }

  /**
   * 取钱
   *
   * @return true false
   */
  public boolean withdraw(int id, double amount) throws AccountNotFoundException {
    //判断amount的有效性
    //根据id来获取一个Account
    //判断用户取的额度是否超过该用户的余额
    if (amount < 0) {
      throw new IllegalArgumentException("amount must be positive!");
    }

    Account account = query(id);
    if (amount > account.getBalance()) {
      return false;
    } else {
      double newBalance = account.getBalance() - amount;
      account.setBalance(newBalance);
      Transaction
          transaction =
          new Transaction(Transaction.Type.W, amount, account.getBalance(), "withdraw " + amount);
      account.addTransaction(transaction);
      return true;
    }
  }


  /**
   * 存钱
   */
  public void deposit(int id, double amount) throws AccountNotFoundException {
    if (amount < 0) {
      throw new IllegalArgumentException("amount must be positive!");
    }

    Account account = query(id);
    account.setBalance(account.getBalance() + amount);
    Transaction
        transaction =
        new Transaction(Transaction.Type.D, amount, account.getBalance(), "deposit " + amount);
    account.addTransaction(transaction);
  }

  /**
   * 查询余额
   */
  public double queryBalance(int id) throws AccountNotFoundException {
    Account account = query(id);
    return account.getBalance();
  }


  public ArrayList<Transaction> queryTransByUserId(int id) throws AccountNotFoundException {
    Account account = query(id);
    return account.getTransactions();
  }
}

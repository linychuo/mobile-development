package test2;

/**
 * Created by ivan on 14-7-21.
 */
public class AccountNotFoundException extends Exception {

  public AccountNotFoundException() {
    super("account not found");
  }
}

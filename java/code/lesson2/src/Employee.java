import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by ivan on 14-7-18.
 */
public class Employee {

  private String name;
  private Date hireDay;
  private double salary;

  public Employee(String name, double salary) {
    this.name = name;
    this.salary = salary;
  }

  public Employee(String name, double salary, int year, int month, int day) {
    this(name, salary);
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.set(year, month - 1, day);
    this.hireDay = calendar.getTime();
  }

  public String getName() {
    return name;
  }

  public Date getHireDay() {
    return hireDay;
  }

  public double getSalary() {
    return salary;
  }

  public void raiseSalary(double byPercent) {
    salary += salary * byPercent / 100;
  }

  @Override
  public String toString() {
    DateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
    return name + ", " + getSalary() + ", " + df.format(hireDay);
  }

}

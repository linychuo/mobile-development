/**
 * Created by ivan on 14-7-18.
 */
public class Test {

  public static void main(String[] args) {
    Employee employee = new Employee("张三", 5, 2014, 24, 56);
    System.out.println(employee.getSalary());

    Employee employee1 = new Manager("李四", 100, 2014, 1, 1, 10);
    System.out.println(employee1.getSalary());
//    manager.raiseSalary(2);
//    System.out.println(manager);

    max(1, 2);
    max(1.0, 2.0);
  }

  public static int max(int a, int b) {
    return a > b ? a : b;
  }

  public static double max(double a, double b) {
    return a > b ? a : b;
  }
}

package test1;

/**
 * Created by ivan on 14-7-21.
 */
public class Point {

  private final int x;
  private final int y;

  public Point() {
    this.x = 0;
    this.y = 0;
  }

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }


  public int getY() {
    return y;
  }

  public int getX() {
    return x;
  }

  @Override
  public String toString() {
    return "x = " + x + ", y = " + y;
  }
}

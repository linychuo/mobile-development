package test1;

/**
 * Created by ivan on 14-7-21.
 */
public class LinearEquation {

  private final Line line1;
  private final Line line2;

  public LinearEquation(Line line1, Line line2) {
    if (line1 == null) {
//      throw new RuntimeException("line1 must not be null!");
      line1 = new Line(new Point(0, 0), new Point(0, 0));
    }

    if (line2 == null) {
//      throw new RuntimeException("line2 must not be null!");
      line2 = new Line(new Point(0, 0), new Point(0, 0));
    }
    this.line1 = line1;
    this.line2 = line2;
  }

  private int getE() {
    return -line1.getStart().getY() * (line1.getStart().getX() - line1.getEnd().getX())
           + line1.getA() * line1
        .getStart().getX();
  }

  private int getF() {
    return -line2.getStart().getY() * (line2.getStart().getX() - line2.getEnd().getX())
           + line2.getA() * line2
        .getStart().getX();
  }


  private int getSolvable() {
    return line1.getA() * line2.getB() - line1.getB() * line2.getA();
  }

  public boolean isSolvable() {
    return getSolvable() != 0;
  }

  public Point getIntersecting() {
    int x = (getE() * line2.getB() - line1.getB() * getF()) / getSolvable();
    int y = (line1.getA() * getF() - getE() * line2.getA()) / getSolvable();
    return new Point(x, y);
  }

//  public static Point getIntersecting(Line line1, Line line2) {
//    int a = line1.getA();
//    int b = line1.getB();
//    int c = line2.getA();
//    int d = line2.getB();
//
//    int
//        e =
//        -line1.getStart().getY() * (line1.getStart().getX() - line1.getEnd().getX()) + a * line1
//            .getStart().getX();
//
//    int
//        f =
//        -line2.getStart().getY() * (line2.getStart().getX() - line2.getEnd().getX()) + c * line2
//            .getStart().getX();
//
//    boolean result = a * d - b * c != 0;
//    if (result) {
//      int x = (e * d - b * f) / (a * d - b * c);
//      int y = (a * f - e * c) / (a * d - b * c);
//      return new Point(x, y);
//    }
//
//    return null;
//  }
}

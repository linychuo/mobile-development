package test1;

/**
 * Created by ivan on 14-7-21.
 */
public class Test {

  public static void main(String[] args) {
    Line line1 = new Line(new Point(1, 4), new Point(4, 4));
    Line line2 = new Line(new Point(2, 1), new Point(2, 5));
    LinearEquation linearEquation = new LinearEquation(line1, line2);
    if (linearEquation.isSolvable()) {
      System.out.println(linearEquation.getIntersecting());
    }

    Line line3 = new Line(new Point(1, 1), new Point(4, 1));
    Line line4 = new Line(new Point(1, 2), new Point(4, 2));
    LinearEquation linearEquation1 = new LinearEquation(line3, null);
    if (linearEquation1.isSolvable()) {
      System.out.println(linearEquation1.getIntersecting());
    } else {
      System.out.println("==========");
    }
  }
}

package clone;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by ivan on 14-7-24.
 */
public class Course implements Cloneable {

  private String name;
  private int grade;
  private Date date;

  public Course(String name, int grade) {
    this.name = name;
    this.grade = grade;
    Calendar calendar = new GregorianCalendar(2014, 7, 1);//2014-7-1
    this.date = calendar.getTime();
  }

  @Override
  public String toString() {
    return "名称: " + name + ", 学分: " + grade + ", 时间: " + date.toLocaleString();
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setGrade(int grade) {
    this.grade = grade;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public Course clone() throws CloneNotSupportedException {
    return (Course) super.clone();
  }
}

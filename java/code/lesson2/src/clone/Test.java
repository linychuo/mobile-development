package clone;

import java.util.Date;

/**
 * Created by ivan on 14-7-24.
 */
public class Test {

  public static void main(String[] args) throws CloneNotSupportedException {

    Student o = new Student(1);
    o.setCourse(new Course("语文", 2));
    System.out.println(o);

    Student a = o.clone();
    System.out.println(a);

    a.getCourse().setName("英语");
    a.getCourse().setGrade(10);
    a.getCourse().setDate(new Date());
    System.out.println(a);

    System.out.println(o);
//
//    System.out.println(o);

//    Course course = new Course("语文", 2);
//    System.out.println(course);
//
//    Course course1 = course.clone();
//    course1.setName("英语");
//    System.out.println(course);
//
//    course.setName("数学");
//    System.out.println(course1);
  }
}

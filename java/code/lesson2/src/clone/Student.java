package clone;

/**
 * Created by ivan on 14-7-23.
 */
public class Student implements Cloneable {

  private int id;
  private Course course;

  public Student(int id) {
    this.id = id;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  @Override
  public Student clone() throws CloneNotSupportedException {
    Student cloned = (Student) super.clone();
    cloned.course = course.clone();
    return cloned;
  }

//  @Override
//  protected Student clone() throws CloneNotSupportedException {
//    return (Student) super.clone();
//  }

  @Override
  public String toString() {
    return id + ", " + course;
  }
}



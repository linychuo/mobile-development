import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by ivan on 14-7-30.
 */
public class TestFileClass {

  public static void main(String[] args) throws FileNotFoundException {
    File f = new File(args[0]);
    System.out.println(new Date(f.lastModified()));
    System.out.println(f.length());

//    PrintWriter pw = new PrintWriter(f);
//    pw.print("hello");
//    pw.print("world\n");
//    pw.println("你好");
//    pw.println("世界");
//    pw.close();

    Scanner input = new Scanner(f);
    while (input.hasNext()) {
      System.out.println(input.next());
    }
    input.close();
  }
}

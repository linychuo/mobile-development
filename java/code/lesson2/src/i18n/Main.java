package i18n;

import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-1.
 */
public class Main extends Application {

  private ResourceBundle resources;

  @Override
  public void init() throws Exception {
    super.init();
    resources = ResourceBundle.getBundle("main");
  }

  @Override
  public void start(Stage stage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource("main.fxml"), resources);
    Scene scene = new Scene(root, 400, 150);

    stage.setTitle("main");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

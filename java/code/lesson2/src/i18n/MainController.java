package i18n;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;

/**
 * Created by ivan on 14-8-4.
 */
public class MainController implements Initializable {

  @FXML
  private ChoiceBox<String> localeChoice;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    localeChoice.getSelectionModel().selectedItemProperty()
        .addListener(new ChangeListener<String>() {
          @Override
          public void changed(ObservableValue<? extends String> observable, String oldValue,
                              String newValue) {
            switch (newValue) {
              case "English":
                reload(location, Locale.ENGLISH);
                break;
              case "Chinese":
                reload(location, Locale.CHINA);
                break;
            }
          }
        });
  }

  private void reload(URL location, Locale locale) {
    try {
      Parent root = FXMLLoader.load(location, ResourceBundle.getBundle("main", locale));
      localeChoice.getScene().setRoot(root);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

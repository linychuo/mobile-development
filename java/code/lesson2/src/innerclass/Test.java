package innerclass;

/**
 * Created by ivan on 14-7-24.
 */
public class Test {

  public static void main(String[] args) {
    A a = new A();
    A.B b = a.new B();
  }
}

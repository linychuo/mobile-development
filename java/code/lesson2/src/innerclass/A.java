package innerclass;

/**
 * Created by ivan on 14-7-24.
 */
public class A {

  private static int data = 1;


  public class B {

    public void hello() {
      System.out.println(data);
    }
  }
}

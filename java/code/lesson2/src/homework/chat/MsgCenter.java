package homework.chat;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by ivan on 14-8-8.
 */
public class MsgCenter {

  private Stack<String> msgStack = new Stack<>();
  private final List<DataOutputStream> clients;

  public MsgCenter() {
    this.clients = new ArrayList<>();
  }

  public void add(DataOutputStream client) {
    clients.add(client);
  }

  public synchronized void put(String msg) {
    msgStack.push(msg);
    notifyAll();
  }

  public synchronized void broadcast() {
    try {
      while (msgStack.isEmpty()) {
        wait();
      }

      String msg = msgStack.pop();
      System.out.println(msg);
      for (DataOutputStream item : clients) {
        item.writeUTF(msg);
      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

package homework.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-1.
 */
public class Client extends Application {

  private DataInputStream inputFromServer;
  private DataOutputStream outputToServer;

  private ListView<String> msgListView;
  private String name;
  private String host;
  private int port;

  @Override
  public void start(Stage stage) throws Exception {
    Scene scene = new Scene(buildLayout());
    stage.setScene(scene);
    stage.setTitle("多人聊天");
    stage.show();
  }

  private VBox buildLayout() {
    TextField hostInput = new TextField("localhost");
    hostInput.setPrefColumnCount(18);
    TextField portInput = new TextField("9090");
    portInput.setPrefColumnCount(4);
    Button btnSetting = new Button("设置服务器");
    HBox hboxSetting = new HBox();
    hboxSetting.setSpacing(10);
    hboxSetting.getChildren().addAll(hostInput, portInput, btnSetting);

    final TextField nameInput = new TextField();
    nameInput.setPrefColumnCount(27);
    nameInput.setPromptText("姓名");
    nameInput.setEditable(false);
    Button btnCreate = new Button("连接");
    btnCreate.setDisable(true);
    HBox hbox = new HBox();
    hbox.setSpacing(10);
    hbox.getChildren().addAll(nameInput, btnCreate);

    final TextField contentInput = new TextField();
    contentInput.setPrefColumnCount(27);
    contentInput.setPromptText("聊天内容");
    Button btnSend = new Button("发送");
    btnSend.setDisable(true);
    HBox hbox1 = new HBox();
    hbox1.setSpacing(10);
    hbox1.getChildren().addAll(contentInput, btnSend);

    msgListView = new ListView<String>();
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(10));
    vbox.setSpacing(10);
    vbox.getChildren().addAll(hboxSetting, hbox, hbox1, msgListView);

    btnSetting.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        try {
          host = hostInput.getText().trim();
          port = Integer.parseInt(portInput.getText().trim());
          btnCreate.setDisable(false);
          nameInput.setEditable(true);
          btnSetting.setDisable(true);
          hostInput.setEditable(false);
          portInput.setEditable(false);
        } catch (NumberFormatException e) {
        }
      }
    });

    btnCreate.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        name = nameInput.getText().trim();
        if (!name.equals("")) {
          connectToServer();
          sendMsg(name + " 已连接");
          nameInput.setEditable(false);
          btnCreate.setDisable(true);
          btnSend.setDisable(false);
        }
      }
    });

    btnSend.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        String content = contentInput.getText().trim();
        if (!content.equals("")) {
          sendMsg(name + ": " + content);
          contentInput.clear();
        }
      }
    });

    return vbox;
  }

  private void connectToServer() {
    try {
      Socket socket = new Socket(host, port);
      inputFromServer = new DataInputStream(socket.getInputStream());
      outputToServer = new DataOutputStream(socket.getOutputStream());
      new Thread(new Receiver()).start();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void sendMsg(String msg) {
    try {
      outputToServer.writeUTF(msg);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private class Receiver implements Runnable {

    @Override
    public void run() {
      while (true) {
        try {
          String s = inputFromServer.readUTF();
          Platform.runLater(new Runnable() {
            @Override
            public void run() {
              msgListView.getItems().add(s);
            }
          });
        } catch (IOException e) {
          e.printStackTrace();
        }

      }
    }
  }

  public static void main(String[] args) {
    launch(args);
  }
}

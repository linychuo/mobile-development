package homework.chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by ivan on 14-8-1.
 */
public class Server {

  private final ServerSocket server;
  private final MsgCenter center;

  public Server() throws IOException {
    this.server = new ServerSocket(9090);
    center = new MsgCenter();
    new Thread(new Sender(center)).start();
  }

  public void run() {
    try {
      while (true) {
        Socket socket = server.accept();
        Handler handler = new Handler(socket, center);
        center.add(handler.outputToClient);
        new Thread(handler).start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private static class Sender implements Runnable {

    private MsgCenter center;

    public Sender(MsgCenter center) {
      this.center = center;
    }

    @Override
    public void run() {
      while (true) {
        center.broadcast();
      }
    }
  }

  private static class Handler implements Runnable {

    private DataInputStream inputFromClient;
    private DataOutputStream outputToClient;
    private MsgCenter center;

    public Handler(Socket socket, MsgCenter center) throws IOException {
      inputFromClient = new DataInputStream(socket.getInputStream());
      outputToClient = new DataOutputStream(socket.getOutputStream());
      this.center = center;
    }

    @Override
    public void run() {
      try {
        while (true) {
          String msg = inputFromClient.readUTF();
          center.put(msg);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }


  public static void main(String[] args) throws IOException {
    new Server().run();
  }

}

package homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by ivan on 14-7-31.
 */
public class TestWordOccurrence {

  public static void main(String[] args) {
    String text = "Good morning. Have a good class. " +
                  "Have a good visit. Have fun!";
    String[] words = text.split("[ \n\t\r\\.,;:!?(){]");
    List<WordOccurrence> result = new ArrayList<>();
//    TreeSet<WordOccurrence> result = new TreeSet<>();
    for (int i = 0; i < words.length; i++) {
      String key = words[i].toLowerCase();
      if (key.length() > 0) {
        //在list里查找是否已经存在和当前key一样的WordOccurrence对象，如果有，获取这个对象的实例，对count增加，
        // 否则创建一个新的WordOccurrence实例，添加到list中
        WordOccurrence wordOccurrence = search(key, result);
        if (wordOccurrence != null) {
          wordOccurrence.setCount(wordOccurrence.getCount() + 1);
        } else {
          wordOccurrence = new WordOccurrence(key, 1);
          result.add(wordOccurrence);
        }
      }
    }

    Collections.sort(result);
    for (WordOccurrence item : result) {
      System.out.println(item);
    }
  }

  private static WordOccurrence search(String key, List<WordOccurrence> result) {
    for (WordOccurrence item : result) {
      if (item.getWord().equals(key)) {
        return item;
      }
    }
    return null;
  }


  private static WordOccurrence search(String key, Set<WordOccurrence> result) {
    for (WordOccurrence item : result) {
      if (item.getWord().equals(key)) {
        return item;
      }
    }
    return null;
  }


}

package homework;

/**
 * Created by ivan on 14-7-31.
 */
public class WordOccurrence implements Comparable<WordOccurrence> {

  private String word;
  private int count;

  public WordOccurrence(String word, int count) {
    this.word = word;
    this.count = count;
  }

  public String getWord() {
    return word;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  @Override
  public int compareTo(WordOccurrence o) {
    return this.count - o.getCount();
  }

  @Override
  public String toString() {
    return word + " = " + count;
  }
}

package homework;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by ivan on 14-7-30.
 */
public class Main {

  private static final String[] KEYWORDS =
      {"abstract", "double", "int", "super", "assert",
       "else", "interface", "switch",
       "boolean", "enum", "long", "synchronized",
       "break", "extends", "native", "this",
       "byte", "for", "new", "throw",
       "case", "final", "package", "throws",
       "catch", "finally", "private", "transient",
       "char", "float", "protected", "try",
       "class", "goto", "public", "void",
       "const", "if", "return", "volatile",
       "continue", "implements", "short", "while",
       "default", "import", "static", "do", "instanceof",
       "strictfp*"
      };

  public static void main(String[] args) {
    //1.获取用户输入的文件名,判断这个文件是否存在,然后根据后缀判断是否是java源文件
    //2.循环读取文件,每读一行,进行关键字的统计,累加
    //3.输入结果

    if (args.length > 0) {
      String filePath = args[0];
      File f = new File(filePath);
      if (f.exists()) {
        String[] splitFilePath = f.getName().split("\\.");
        if (splitFilePath[1].equals("java")) {
          if (f.canRead()) {
            compute(f);
//            log("你输入的文件" + f.getName() + "有" + count + "个关键字.");
          } else {
            log("你输入的文件不能读取,请修改权限后重新执行.");
          }
        } else {
          log("你输入的文件不是java源文件,请重新执行.");
        }
      } else {
        log("你输入的文件名有问题,请重新执行.");
      }
    } else {
      log("java homework.Main [文件路径名]");
    }
  }

  private static void log(String msg) {
    System.out.println(msg);
    System.exit(0);
  }

  private static void compute(File f) {
//    int count = 0;
    Map<String, Integer> result = new HashMap<>();
    try (Scanner fileRead = new Scanner(f)) {
      while (fileRead.hasNext()) {
        String line = fileRead.nextLine();
        String[] splitLine = line.split("[ \n\t\r.,;:!?(){]");
        for (String t : KEYWORDS) {
          int cc = Collections.frequency(Arrays.asList(splitLine), t);
          if (cc > 0) {
            if (result.containsKey(t)) {
              int a = result.get(t).intValue();
              a += cc;
              result.put(t, a);
            } else {
              result.put(t, cc);
            }
          }
        }
      }
    } catch (FileNotFoundException e) {
      //
    }

    System.out.println(result);

//    return count;
  }

}

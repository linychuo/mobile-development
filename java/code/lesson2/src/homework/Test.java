package homework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ivan on 14-7-30.
 */
public class Test {

  public static void main(String[] args) {
    String line = "public static static void main(String[] args) {";
    Pattern p = Pattern.compile("(public)|(static)|(void)");
    Matcher matcher = p.matcher(line);
    System.out.println(matcher.groupCount());
  }
}

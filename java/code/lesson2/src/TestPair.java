/**
 * Created by ivan on 14-7-28.
 */
public class TestPair {

  public static void main(String[] args) {
    Employee d = new Employee("aa", 200, 1923, 1, 1);
    Employee d1 = new Employee("bb", 300, 1924, 1, 1);

    Manager m = new Manager("mm", 300, 1900, 1, 1, 20);
    Manager m1 = new Manager("nn", 400, 1901, 1, 1, 10);

    Pair<Employee> pe = new Pair<>(d, d1);
    Pair<Manager> pm = new Pair<>(m, m1);
  }
}

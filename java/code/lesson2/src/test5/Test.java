package test5;

/**
 * Created by ivan on 14-7-22.
 */
public class Test {

  public static void main(String[] args) {
    A a = new A(3);
  }
}

class A extends B {

  public A(int t) {
    System.out.println("A's constructor is invoked");
  }
}

class B extends C {

  public B() {
    System.out.println("B's constructor is invoked");
  }
}


class C {

  public C() {
    System.out.println("=========");
  }
}
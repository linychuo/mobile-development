package test3;

/**
 * Created by ivan on 14-7-22.
 */
public class Test {

  public static void main(String[] args) {
//    Person student = new Student();
//    student.setName("小明");
//    student.hello();
//    Person teacher = new Teacher();
//    teacher.setName("李老师");
//    teacher.hello();
//
//    Student student1 = new Student();
//    student1.setName("小明");
//    System.out.println(student == student1);
//    System.out.println(student.equals(student1));

    System.out.println(max(1, 3, 4, 3, 5));
    System.out.println(max(1, 3));
    System.out.println(max(1, 3, 4, 3, 5, 43, 21, 54));

    System.out.println(maxOther(new double[]{2, 34, 3, 431}));
    System.out.println(maxOther(new double[]{2, 34, 3243, 32, 432, 432, 43}));
  }

  public static double max(double... values) {
    double largest = Double.MIN_VALUE;

    for (double v : values) {
      if (v > largest) {
        largest = v;
      }
    }
    return largest;
  }

  public static double maxOther(double[] values) {
    double largest = Double.MIN_VALUE;

    for (double v : values) {
      if (v > largest) {
        largest = v;
      }
    }
    return largest;
  }


  public static void hello(int d, double... values) {

  }
}

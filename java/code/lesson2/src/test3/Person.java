package test3;

/**
 * Created by ivan on 14-7-22.
 */
public abstract class Person {

  private String name;
  private String id;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    if (name == null) {
      throw new IllegalArgumentException();
    }
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public abstract void hello();
}

package test3;

/**
 * Created by ivan on 14-7-22.
 */
public class Student extends Person {

  @Override
  public void hello() {
    System.out.println(getName() + ": I'm a student!");
  }

  @Override
  public boolean equals(Object otherObject) {
    if (this == otherObject) {
      return true;
    }

    if (otherObject == null) {
      return false;
    }

    if (!(otherObject instanceof Student)) {
      return false;
    }

    Student student = (Student) otherObject;
    return this.getName().equals(student.getName());
  }
}

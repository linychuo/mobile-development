package test3;

/**
 * Created by ivan on 14-7-22.
 */
public class Teacher extends Person {

  @Override
  public void hello() {
    System.out.println(getName() + ": I'm a teacher!");
  }
}

package test3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ivan on 14-7-22.
 */
public class ArrayListTest {

  public static void main(String[] args) {
    ArrayList d = new ArrayList();
    d.add(1);
    d.add("hello");
    for (Object o : d) {
      if (o instanceof Integer) {
        System.out.println(((int) o) + 1);
      }

      if (o instanceof String) {
        System.out.println("[[[[" + o + "]]]]");
      }
    }

    ArrayList<Integer> a = new ArrayList<Integer>();
    a.add(1);// new Integer(1)
    a.add(3);

    for (double e : a) {
      System.out.println(e);
    }

    for (int i = 0; i < a.size(); i++) {
      System.out.println(a.get(i));
    }

    System.out.println(a.contains(3));
    System.out.println(a.indexOf(3));
    System.out.println(a.indexOf(5));
    Integer[] e = a.toArray(new Integer[]{});
    System.out.println(Arrays.toString(e));

    List<Integer> f = Arrays.asList(1, 3);
    System.out.println(f.size());
  }
}

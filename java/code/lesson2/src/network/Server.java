package network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by ivan on 14-8-1.
 */
public class Server {

  public static void main(String[] args) {
    try {
      ServerSocket serverSocket = new ServerSocket(8080);
      System.out.println("1111111");
      Socket socket = serverSocket.accept();
      System.out.println("222222222");

      DataInputStream inputFromClient = new DataInputStream(socket.getInputStream());
      DataOutputStream outputToClient = new DataOutputStream(socket.getOutputStream());
      while (true) {
        double radius = inputFromClient.readDouble();
        double area = radius * radius * 3.14;
        outputToClient.writeDouble(area);

        System.out.println("Radius received from client: " + radius);
        System.out.println("Area found: " + area);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}

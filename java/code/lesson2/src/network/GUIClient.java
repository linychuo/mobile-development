package network;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by ivan on 14-8-1.
 */
public class GUIClient extends Application {

  @Override
  public void start(Stage stage) throws Exception {

    Socket socket = new Socket("localhost", 8081);
    final DataInputStream inputFromServer = new DataInputStream(socket.getInputStream());
    final DataOutputStream outputToServer = new DataOutputStream(socket.getOutputStream());

    HBox hBox = new HBox();
    final TextField textField = new TextField();
    textField.setPrefColumnCount(20);
    textField.setPromptText("enter radius");
    Button btn = new Button("Submit");
    hBox.setSpacing(10);
    hBox.getChildren().addAll(textField, btn);

    VBox vBox = new VBox();
    vBox.setPadding(new Insets(10));
    vBox.setSpacing(10);

    final ListView<String> list = new ListView<String>();
    vBox.getChildren().addAll(hBox, list);

    btn.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        String inputRadius = textField.getText();
        try {
          double radius = Double.parseDouble(inputRadius);
          outputToServer.writeDouble(radius);
          outputToServer.flush();

          double area = inputFromServer.readDouble();
          list.getItems().addAll("Radius is " + radius + ", area is " + area);
          textField.clear();
        } catch (NumberFormatException e) {
          textField.clear();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });

    Scene scene = new Scene(vBox);
    stage.setTitle("Client");
    stage.setScene(scene);
    stage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}

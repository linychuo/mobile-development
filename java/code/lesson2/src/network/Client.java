package network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by ivan on 14-8-1.
 */
public class Client {

  private static final Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    try {
      Socket socket = new Socket("localhost", 8080);
      DataInputStream inputFromServer = new DataInputStream(socket.getInputStream());
      DataOutputStream outputToServer = new DataOutputStream(socket.getOutputStream());

      String inputStr = "";
      do {
        double radius = captureDouble();
        outputToServer.writeDouble(radius);
        outputToServer.flush();

        double area = inputFromServer.readDouble();
        System.out.println("Radius is " + radius);
        System.out.println("Area received from the server is " + area);

        System.out.print("continue?");
        inputStr = input.next();
      } while (!inputStr.equalsIgnoreCase("n"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static double captureDouble() {
    double result = 0;
    boolean appearError = false;
    System.out.print("Please input radius: ");
    do {
      try {
        result = input.nextDouble();
        if (result < 0) {
          appearError = true;
          System.out.print("illegal input, please again: ");
        } else {
          appearError = false;
        }
      } catch (InputMismatchException ex) {
        input.nextLine();
        appearError = true;
        System.out.print("illegal input, please again: ");
      }
    } while (appearError);
    return result;
  }
}

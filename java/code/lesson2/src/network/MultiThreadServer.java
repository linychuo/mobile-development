package network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * Created by ivan on 14-8-1.
 */
public class MultiThreadServer {

  public static void main(String[] args) {
    try {
      ServerSocket serverSocket = new ServerSocket(8081);
      System.out.println("MultiThreadServer started at " + new Date());

      int clientNo = 1;
      while (true) {
        Socket socket = serverSocket.accept();
        System.out.println("Starting thread for client " + clientNo + " at " + new Date());
        InetAddress inetAddress = socket.getInetAddress();
        System.out.println("Client " + clientNo + "'s host name is "
                           + inetAddress.getHostName());
        System.out.println("Client " + clientNo + "'s IP Address is "
                           + inetAddress.getHostAddress());
        new Thread(new HandleAClient(socket)).start();
        clientNo++;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static class HandleAClient implements Runnable {

    private Socket socket;

    public HandleAClient(Socket socket) {
      this.socket = socket;
    }

    @Override
    public void run() {
      try {
        DataInputStream inputFromClient = new DataInputStream(socket.getInputStream());
        DataOutputStream outputToClient = new DataOutputStream(socket.getOutputStream());
        //ObjectOutputStream ObjectInputStream

        while (true) {
          double radius = inputFromClient.readDouble();
          double area = radius * radius * 3.14;
          outputToClient.writeDouble(area);


          System.out.println("Radius received from client: " + radius);
          System.out.println("Area found: " + area);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}

title: Introduction JavaFX
author: ivan
---
# Introduction JavaFX
##### by ivan 2014.7
---
# What is JavaFX?
JavaFX is a set of graphics and media packages the enables developers to design, create, test, debug, and deploy rich client applications that operate consistently across diverse platforms

---
# History
[http://en.wikipedia.org/wiki/JavaFX](http://en.wikipedia.org/wiki/JavaFX)

---
# Key Features
- Java APIS
- FXML and Scene Builder
- WebView
- Swing interoperability
- Built-in UI controls and CSS
- Multitouch Support
- Hardware-accelerated graphics pipline
- High-performance media engine
- Self-contained application deployment model

---
# Build development envi.

---
# Sample application "Hello world"
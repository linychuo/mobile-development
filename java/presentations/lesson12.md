title: XML & Internationalization
author: ivan
---
# XML & Internationalization
##### by ivan 2014.2
---
# Working with Files
- java.nio.Paths
- java.nio.Files
---
# Introducing XML
A property file contains a set of name/value pairs, such as

    fontname=Times Roman
    fontsize=12
    windowsize=400 200
    color=0 50 100
    
--
An XML file for describing a program configuration might look like this
    
    <configuration>
        <font>
            <name>Times Roman</name>
            <size>12</size>
        </font>
        <window>
            <width>400</width>
            <height>200</height>
        </window>
        <color>
            <red>0</red>
            <green>50</green>
            <blue>100</blue>
        </color>
    </configuration>
    
--
## The structure of an xml document
An xml doucment should start with a header such as
    
    <?xml version="1.0">
or

    <?xml version="1.0" encoding="utf-8">
    
--
## Parsing ans xml document
The java library supplies two kinds of xml parsers
    - Tree parsers, such as the Document Object Model(DOM) parser, that read an XML document into a tree structure
    - Streaming parsers, such as the simple API for XML(SAX) parser, that generate events as they read an XML document
    
--
## Tree parsers
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    
    File f = ...;
    Document doc = builder.parser(f);
    
    Element root = doc.getDocumentElement();
    
    NodeList children = root.getChildNodes();
    for (int i = 0; i < children.getLength(); i++){
        Node child = children.item(i);
        . . .
    }
    
--
## Streaming Parsers
    
    package com.shiwei.ssr;

    import java.io.InputStream;
    import java.net.URL;
    
    import javax.xml.parsers.SAXParser;
    import javax.xml.parsers.SAXParserFactory;
    
    import org.xml.sax.Attributes;
    import org.xml.sax.helpers.DefaultHandler;
    
    public class SAXTest {
      public static void main(String[] args) throws Exception {
        String url;
        if (args.length == 0) {
          url = "http://www.w3c.org";
          System.out.println("Using " + url);
        } else
          url = args[0];
    
        DefaultHandler handler = new DefaultHandler() {
          public void startElement(String namespaceURI, String lname, String qname, Attributes attrs) {
            if (lname.equals("a") && attrs != null) {
              for (int i = 0; i < attrs.getLength(); i++) {
                String aname = attrs.getLocalName(i);
                if (aname.equals("href")) System.out.println(attrs.getValue(i));
              }
            }
          }
        };
    
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        SAXParser saxParser = factory.newSAXParser();
        InputStream in = new URL(url).openStream();
        saxParser.parse(in, handler);
      }
    }

    
--
## StAX Parser
    
    package com.shiwei.ssr;

    import java.io.InputStream;
    import java.net.URL;
    
    import javax.xml.stream.XMLInputFactory;
    import javax.xml.stream.XMLStreamConstants;
    import javax.xml.stream.XMLStreamReader;
    
    public class StAXTest {
      public static void main(String[] args) throws Exception {
        String urlString;
        if (args.length == 0) {
          urlString = "http://www.w3c.org";
          System.out.println("Using " + urlString);
        } else
          urlString = args[0];
        URL url = new URL(urlString);
        InputStream in = url.openStream();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser = factory.createXMLStreamReader(in);
        while (parser.hasNext()) {
          int event = parser.next();
          if (event == XMLStreamConstants.START_ELEMENT) {
            if (parser.getLocalName().equals("a")) {
              String href = parser.getAttributeValue(null, "href");
              if (href != null) System.out.println(href);
            }
          }
        }
      }
    }
    

---
# Resource Bundles
- java.util.Locale
- java.util.ResourceBundle
    
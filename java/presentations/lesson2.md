title: Fundamental programming structures in java 1
author: ivan
---
# Fundamental programming structures in java 1
##### by ivan 2013.11
---
# Objectives
- [Data types](#/2)
- [Variables](#/3)
- [Operators](#/4)
- [Strings](#/5)
- [Input and Output](#/6)
---
# Data Types
Java is a **strongly** typed language. There are **eight** primitive types in java. Four of them are integer types. two are floating-point number types. one is the character type char. and one is a boolean type for truth types.

--
## Integer types
The integer types are for numbers without fractional parts. Negative values are allowed.

![integer types](./integerTypes.jpg)
--
## Floating-Point types
The floating-point types denote numbers with fractional parts.

![floating-point types](./floating-pointTypes.jpg)
--
## The *char* type
- Used to describe individual characters
- Escape sequences for special characters
![escape characters](./escapeCharacters.jpg)
--
## Conversions between Numeric types
![conversions numeric](./conversionsNumeric.jpg)
--
## The *boolean* types
- It has two values, _false_ and _true_
- It is used for evaluating logical conditions

--
## Enumerated types
Sometimes, a variable should only hold a restricted set of values

    enum Size { SMALL, MEDIUM, LARGE, EXTRA_LARGE };
    Size s = Size.MEDIUM;
---
# Variables
- A variable name must begin with a letter and must be a sequence of *letters* or *digits*
- A letter is defined as 'A'-'Z', 'a'-'z', '_', '$', or any unicode characters
- Digits are '0'-'9' and any unicode characters

--
## Initializing variables
    int vacationDays;
    System.out.println(vacationDays); //ERROR: variable not initialized
    
--
## Constants
In java, you use the keyword *final* to denote a constant.

    public class Constant{
        public static final double CM_PER_INCH = 2.54;
        public static void main(String[] args){
            double paperWidth = 8.5;
            double paperHeight = 11;
            System.out.println("Paper size in centimeters: " 
                + paperWidth * CM_PER_INCH + " by " 
                + paperHeight * CM_PER_INCH);
        }
    }

---
# Operators
- Increment and decrement
- Relational
- Bitwise
- Shift
- Ternary

--
## Increment and Decrement
    int m = 7;
    int n = 7;
    int a = 2 * ++m; // now a is 16, m is 8
    int b = 2 * n++; // now b is 14, n is 8
* * *   
We recommned against using ++ inside expressions because this often leads to confusing code and annoying bugs
* * *
--
## Relational operators
- Less than(<), greater than(>), less than or equal to (<=), greater than or equal to(>=), equivalent(==) and not equivalent(!=)
- Equivalence and non equivalence work with all primitives, but the other comparisons won't work with type boolean
- Each of the logical operators AND(&&), OR(||) and NOT(!) produces a _boolean_ value of true or false based on the logical relationship of its arguments

    
    public class Equivalence {
        public static void main(String[] args) {
            Integer n1 = new Integer(47);
            Integer n2 = new Integer(47);
            System.out.println(n1 == n2);
            System.out.println(n1 != n2);
        }
    }
--
## Bitwise operators
- Bitwise AND(&), bitwise OR(|), bitwise EXCLUSIVE OR, or XOR(^), bitwise NOT(~)
- Because bits are “small”, there is only one character in the bitwise operators

--
## Shift operators
- Left-shift(<<), right-shift(>>), unsigned right shift(>>>)
- If you shift a _char_, _byte_, or _short_, it will be promoted to int before the shift takes place, and the result will be an int

--
## Ternary if-else operators

    boolean-exp ? value0 : value1

--
## Mathematical functions and Constants
    double x = 4;
    double y = Math.sqrt(x);
    System.out.println(y);// prints 2.0
    double y = Math.pow(x, a);
    Math.PI
    Math.E
    
    /** Return the trigonometric sine of an angle in radians */
    public static double sin(double radians)
    
    /** Return the trigonometric cosine of an angle in radians */
    public static double cos(double radians)
    
    /** Return the trigonometric tangent of an angle in radians */
    public static double tan(double radians)
    
    /** Convert the angle in degrees to an angle in radians */
    public static double toRadians(double degree)
    
    /** Convert the angle in radians to an angle in degrees */
    public static double toDegrees(double radians)
    
    /** Return the angle in radians for the inverse of sin */
    public static double asin(double a)
    
    /** Return the angle in radians for the inverse of cos */
    public static double acos(double a)
    
    /** Return the angle in radians for the inverse of tan */
    public static double atan(double a)
    
--
## Parentheses and operator hierarchy
![operator precedence](./operatorPrecedence.jpg)    
---
# Strings
- Substrings
- Concatenation
- Strings are immutable
- Empty and Null strings

--
## Strings are immutalbe
- You cannot change the individual characters in a java string
- The compiler can arrange that strings are shared
- Don't change strings, just compare them
- equals & ==

--
## Building Strings
- StringBuilder
- StringBuffer, slightly less efficient
---
# Reading Input
- System.out & System.in
- java.util.Scanner
- java.io.Console

--
## InputTest.java
    import java.util.*;
    /**
     * This program demonstrates console input.
     * @version 1.0 2013-11-27
     * @author ivan
     */
     public class InputTest{
        public static void main(String[] args){
            Scanner in = new Scanner(System.in);
            
            //get first input
            System.out.println("what is your name?");
            String name = in.nextLine();
            
            //get second input
            System.out.println("how old are you?");
            int age = in.nextInt();
            
            //display output on console
            System.out.println("Hello, " + name + ". Next year, you'll be" 
            + (age + 1));
        }
     }

--
## Formatting output
- System.out.printf
- String.format

* * *
    double x = 10000.0 / 3.0;
    System.out.print(x);
    System.out.printf("%8.2f", x);
    System.out.printf("%1$s %2$tB %2$te, %2$tY", "Due date:", new Date());
    
    String name = "ivan";
    int age = 27;
    String message = String.format("Hello, %s. Next year, you'll be %d", name, age);
    

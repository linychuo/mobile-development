title: Graphics progamming
author: ivan
---
# Graphics progamming
##### by ivan 2013.12
---
# Objectives
- Swing
- Frame
---
# Introduction swing
- AWT
- Swing
    - Swing has a rich and convenient set of user interface elements
    - Swing has few dependencies on the underlying platform; it is therefore less prone to platform-specific bugs
    - Swing gives a consistent user experience across platforms
    
---
# Creating a Frame
    package com.lyc.swing;

    import javax.swing.JFrame;
    import javax.swing.SwingUtilities;
    
    public class SimpleFrameTest {
        	public static void main(String[] args) {
        		SwingUtilities.invokeLater(new Runnable() {
        			@Override
        			public void run() {
        				SimpleFrame frame = new SimpleFrame();
        				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        				frame.setVisible(true);
        			}
        		});
        	}
    }
    
    class SimpleFrame extends JFrame {
    
        	private static final long serialVersionUID = -896098185246746303L;
        
        	private static final int DEFAULT_WIDTH = 300;
        	private static final int DEFAULT_HEIGHT = 200;
        
        	public SimpleFrame() {
        		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        	}
    }
    
--
## Positioning a frame
- The *setLocation* and *setBounds* methods for setting the position of the frame
- The *setIconImage* method, which tells the windowing system which icon to display in the title bar, task switcher window, and so on
- The *setTitle* method for changing the text in the title bar
- The *setResizable* method, which takes a *boolean* to determine if a frame will be resizeable by the user

--
![frame and component](./frameAndComponent.png)

--
## Frame properties
Many methods of component classes come in getter/setter pairs, such as following methods of the *Frame* class:

    public String getTitle()
    public void setTitle(String title)
    
--
## Determining a good frame size
- If you don't explicitly size a frame, all frames will default to 0 by 0 pixels
- Toolkit

--
## Tips
- If your frame contains only standard components such as buttons and text fields, you can simply call the *pack* method to set the frame size. The frame will be set to the smallest size that contains all components. It is quite common to set the main frame of a program to the maximum size. As of Java SE 1.4, you can simply maximize a frame by calling

    frame.setExtendedState(Frame.MAXIMIZED_BOTH);
    
--
- It is also a good idea to remember how the user positions and sizes the frame of your application and restore those bounds when you start the application again
- If you write an application that takes advantage of multiple display screens, use the *GraphicsEnvironment* and *GraphicsDevice* classes to find the dimensions of the display screens
- The *GraphicsDevice* class also lets you execute your application in full-screen mode

---
# Displaying information in a component
You could draw the message string directly onto a frame, but that is not considered good programming practice. In Java, frames are really designed to be containers for components, such as a menu bar and other user interface elements. You normally draw on another component which you add to the frame

--
## Structure of JFrame
![internalStructureOfJFrame](./internalStructureOfJFrame.png)

--
## NotHelloWorld.java
    package com.lyc.swing;

    import java.awt.Dimension;
    import java.awt.Graphics;
    
    import javax.swing.JComponent;
    import javax.swing.JFrame;
    import javax.swing.SwingUtilities;
    
    public class NotHelloWorld {
        	public static void main(String[] args) {
        		SwingUtilities.invokeLater(new Runnable() {
        
        			@Override
        			public void run() {
        				JFrame jf = new NotHelloWorldFrame();
        				jf.setTitle("NotHelloWorld");
        				jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        				jf.setVisible(true);
        			}
        		});
        	}
    }
    
    class NotHelloWorldFrame extends JFrame {
    
        	private static final long serialVersionUID = 7848117194869522381L;
        
        	public NotHelloWorldFrame() {
        		add(new NotHelloWorldComponent());
        		pack();
        	}
    }
    
    class NotHelloWorldComponent extends JComponent {
        	private static final long serialVersionUID = -1478998769942355477L;
        	public static final int MESSAGE_X = 75;
        	public static final int MESSAGE_Y = 100;
        
        	private static final int DEFAULT_WIDTH = 300;
        	private static final int DEFAULT_HEIGHT = 200;
        
        	@Override
        	protected void paintComponent(Graphics g) {
        		g.drawString("Not a hello, world program", MESSAGE_X, MESSAGE_Y);
        	}
        
        	@Override
        	public Dimension getPreferredSize() {
        		return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        	}
    }

---
# Using Color
- setColor
- setBackground
- setForeground

---
# Using special Fonts for Text

    package com.lyc.swing;

    import java.awt.GraphicsEnvironment;
    
    public class LIstFonts {
        	public static void main(String[] args) {
        		String[] fontNames = GraphicsEnvironment.getLocalGraphicsEnvironment()
        				.getAvailableFontFamilyNames();
        		for (String fontName : fontNames) {
        			System.out.println(fontName);
        		}
        	}
    }
    
    Font sansbold14 = new Font("SansSerif", Font.BOLD, 14);
title: Inheritance
author: ivan
---
# Inheritance
##### by ivan 2013.12
---
#Objectives
- Superclasses and Subclasses
- Polymorphism

---
# Classes, Superclasses and Subclasses
- The keyword *extends* indicates that you are making a new class that derives from an existing class
- The existing class is called the superclass, base class or parent class
- The new class is called the subclass, derived class or child class

        class Manager extends Employee{
            private double bonus;
            ......
            public void setBonus(double b){
                bonus = b;
            }
        }
        

--
## Override the method *getSalary*

        public double getSalary(){
            return salary + bonus;
        }
        
--
## Override again
        
        public double getSalary(){
            double baseSalary = getSalary();
            return baseSalary + bonus;
        }
        
--
## Super
- *super* is not reference to an object
- *super* is a special keyword that directs the compiler to invoke the superclass method

        public double getSalary(){
            double baseSalary = super.getSalary();
            return baseSalary + bonus;
        }
        

        public Manager(String name, double salary, int yer, int month, int day){
            super(name, salary, year, month, day);
            bonus = 0;
        }
        
--
## *this* vs *super*
- *this* keyword has two meanings:
    - to denote a reference to the implicti parameter
    - to calll another constructor of the same class
- *super* keyword has tow meanings:
    - to invoke a superclass method
    - to invoke a superclass constructor
- The constructor calls can only occur as the first statement in another constructor

--
## Manager.java
    package com.shiwei.ssr;

    public class Manager extends Employee {
    
      private double bonus;
    
      public Manager(String name, double s, int year, int month, int day) {
        super(name, s, year, month, day);
        bonus = 0;
    
      }
    
      public double getSalary() {
        double baseSalary = super.getSalary();
        return baseSalary + bonus;
      }
    
      public void setBonus(double b) {
        bonus = b;
      }
    }
    
    
--
## ManagerTest.java
    
    package com.shiwei.ssr;

    public class ManagerTest {
      public static void main(String[] args) {
        // construct a Manager object
        Manager boss = new Manager("Carl Cracker", 80000, 1987, 12, 15);
        boss.setBonus(5000);
    
        Employee[] staff = new Employee[3];
    
        // fill the staff array with Manager and Employee objects
        staff[0] = boss;
        staff[1] = new Employee("Harry Hacker", 50000, 1989, 10, 1);
        staff[2] = new Employee("Tommy Tester", 40000, 1990, 3, 15);
    
        // print out information about all Employee objects
        for (Employee e : staff)
          System.out.println("name=" + e.getName() + ",salary=" + e.getSalary());
      }
    }
    
--
## Employee inheritance hierarchy
![employee inheritance hierarchy](./employeeInheritanceHierarchy.png)

---
# Polymorphism
- Whether or not inheritance is the right design for your data:

  - The "is-a" rule states that every object of the subclass in an object of the superclass
  - You can use a subclass object whenever the program expects a superclass object 


      Employee e;
      e = new Employee(...);
      e = new Manager(...);
--
## Dynamic binding
- It is important to understand what happens when a method call is applied to an object, here are the details:

    - The compiler looks at the declared type of the object and the method name
    - Next, the compiler determines the types of the parameters that are supplied in the method call
    - If the method is *private*, *static*, *final*, or a constructor, then the compiler knows exactly which method to call. This is called *static binding*. Otherwise, the method to be called depends on the actual type of the implicity parameter, and dynamic binding must be used at runtime
    - When the program runs and uses dynamic binding to call a method, then the virtual machine must call the version of the method that is appropriate for the actual type of the object to which *x* refers

--
## Method signature
- Recall that the name and parameter type list for a method is called the method’s signature, The return type is not part of the signature
- If you define a method in a subclass that has the same signature as a superclass emthod, then you override that method
- When you override a method, you need to keep the return type compatible
***
Dynamic binding has a very important property: It makes programs *extensible* without the need for modifying existing code

--
## Preventing inheritance: Final classes and methods
- Classes that cannot be extended are called *final* class
- There is only one good reason to make a method or class final: to make sure
that its semantics cannot be changed in a subclass

--
## Casting
instanceof
- You can cast only within an inheritance hierarchy
- Use *instanceof* to check before casting from a superclass to a subclass

The test

    if(x instanceof C)
    
does not generate an exception if x is null, It simply returns *false*, That
makes sense: *null* refers to no object, so it certainly doesn’t refer to an
object of type C

--
## Abstract classes
![Inheritance diagram for person and its subclasses](./inheritanceDiagramForPersonAndItsSubclasses.png)

--
## Protected access
As you know, fields in a class are best tagged as *private*, and methods are usually tagged as *public*

1. Visible to the class only ( private )
2. Visible to the world ( public )
3. Visible to the package and all subclasses ( protected )
4. Visible to the package—the (unfortunate) default. No modifiers are needed
---
# Object: The cosmic superclass
The Object class is the ultimate ancestor—every class in Java extends Object

--
## The *equals* method
- The *equals* method in the *Object* class tests whether one object is considered equal to another
- The equals method, as implemented in the Object class, determines whether two object references are identical

--
## Equality testing and inheritance
1. It is reflexive: For any non-null reference x , x.equals(x) should return true
2. It is symmetric : For any references x and y , x.equals(y) should return true
if and only if y.equals(x) returns true
3. It is transitive: For any references x , y , and z , if x.equals(y) returns true
and y.equals(z) returns true , then x.equals(z) should return true
4. It is consistent : If the objects to which x and y refer haven’t changed, then
repeated calls to x.equals(y) return the same value
5. For any non-null reference x , x.equals(null) should return false

--
## Writing the prefect *equals* method
- Name the explicit parameter otherObject —later, you will need to cast it toanother variable that you should call other
- Test whether this happens to be identical to otherObject:

        if (this == otherObject) return true;
    
    This statement is just an optimization. In practice, this is a common case. It is much cheaper to check for identity than to compare the fields

- Test whether otherObject is null and return false if it is. This test is required
    
        if (otherObject == null) return false;
    
--
- Compare the classes of this and otherObject . If the semantics of equals
can change in subclasses, use the getClass test:

        if (getClass() != otherObject.getClass()) return false;
    
    If the same semantics holds for all subclasses, you can use an instanceof test:
    
        if (!(otherObject instanceof ClassName)) return false;
    
- Cast otherObject to a variable of your class type:

        ClassName other = (ClassName) otherObject
    
--
- Now compare the fields, as required by your notion of equality. Use == for
primitive type fields, Objects.equals for object fields. Return true if all fields
match, false otherwise

        return field1 == other.field1
            && Objects.equals(field2, other.field2)
            && . . .;

    If you redefine equals in a subclass, include a call to super.equals(other)

--
## The *hasCode* method
If you redefine the equals method, you will also need to redefine the hashCode
method for objects that users might insert into a hash table

--
## The *toString* method
- The toString method is a great tool for logging
- We strongly recommend that you add a toString method to each class that you write. You, as well as other programmers who use your classes, will be grateful for the logging support.

---
# Generic Array Lists
Before Java SE 5.0, there were no generic classes. Instead, there was a single ArrayList class, a “one size fits all” collection that holds elements of type Object. You can still use ArrayList without a <...> suffix. It is considered a “raw” type, with the type parameter erased

---
# Object Wrappers and Autoboxing
- All primitive types have class counterparts. These kinds of classes are usually called wrappers
- The wrapper classes have obvious names: Integer, Long, Float, Double, Short, Byte, Character, Void, and Boolean
- The wrapper classes are immutable
--
- Another Java SE 5.0 innovation makes it easy to add and get array elements. The call

        list.add(3)
    
    is automatically translated to 
    
        list.add(Integer.valueOf(3));
    
    This conversion is called *autoboxing* 
    
- Conversely, when you assign an *Integer* object to an *int* value, it is automatically unboxed. That is the compiler translates

        int n = list.get(i);
    
    into
    
        int n = list.get(i).intValue();

---
# Methods with a Variable Number of Parameters
Before Java SE 5.0, every java method had a fixed number of parameters. You have already seen such a method: *printf*

        System.out.pirntf("%d", n);
    
    or
    
        System.out.printf("%d %s", n, "widgets");
    
--
## Example.java
You can define your own methods with variable parameters

        public static double max(double... values){
            double largest = Double.MIN_VALUE;
            for (double v : values) if (v > largest) largest = v;
            return largest;
        }
    
    
---
# Enumeration Classes
We have already defined below this enumerated types

    public enum Size { SMALL, MEDIUM, LARGE, EXTRA_LARGE };

- The type defined by this declaration is actually a class, The class has exactly four instances, it is not possible to construct new object
- We never need to use *equals* for values of enumerated types, You could use *==* to compare them

--
## Size.java
    public enum Size{
        SMALL("S"), MEDIUM("M"), LARGE("L"), EXTRA_LARGE("XL");
        
        private String abbreviation;
        private Size(String abbreviation) { this.abbreviation = abbreviation; }
        public String getAbbreviation() { return abbreviation; }
    }
    

- All enumerated types are subclasses of the class *Enum*
- Each enumerated type has a static *values* method that returns an array of all values of the enumeration

--
## EnumTest.java

    package com.shiwei.ssr;

    import java.util.Scanner;
    /**
     * This program demonstrates enumerated types.
     * 
     * @version 1.0
     * @author ivan
     */
    public class EnumTest {
      public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a size: (SMALL, MEDIUM, LARGE, EXTRA_LARGE) ");
        String input = in.next().toUpperCase();
        Size size = Enum.valueOf(Size.class, input);
        System.out.println("size=" + size);
        System.out.println("abbreviation=" + size.getAbbreviation());
        if (size == Size.EXTRA_LARGE) System.out.println("Good job--you paid attention to the _.");
      }
    }
    
    
    enum Size {
      SMALL("S"), MEDIUM("M"), LARGE("L"), EXTRA_LARGE("XL");
    
      private Size(String abbreviation) {
        this.abbreviation = abbreviation;
      }
    
      public String getAbbreviation() {
        return abbreviation;
      }
    
      private String abbreviation;
    }

    
---
# Design hints for inheritance
1. Place common operations and fields in the superclass
2. Don't use protected fields
3. Use inheritance to model the "is-a" relationship
4. Don't use inheritance unless all inerited methods make sense
5. Don't change the expected behavior when you override a method
6. Use polymorphism, not type information
title: Interfaces and Inner Classes
author: ivan
---
# Interfaces and Inner Classes
##### by ivan 2013.12

--- 
# Objectives
- Interfaces
- Inner classes

---
# Interfaces
- An interface is not a class but a set of *requirements* for the classes that want to conform to the interface

        public interface Comparable{
            int compareTo(Object other);
        }
    
- All methods of an interface are automatically *public*, However, when implementing the interface, you must declare te method as *public*
- Interfaces never have instance fields, and the methods are never implemented in the interface

--
## Properties of interfaces
- Interfaces are not classes, you can never use the *new* operator to instantiate an interface, but you can still declare interface variables
- A n interface variable must refer to an object of a class that implements the interface
- You can use *instanceof* to check whether an object implements an interface
- You cannot put instance fields or static methods in an interface, you can supply constants in them
- All fields of an interface are always *public static final*
- Each class can have only one superclass, but it can implement multiple interfaces

--
## Interfaces VS Abstract classes

---
# Object cloning
- References copy
    
        Employee original = new Employee("ivan", 300, 2010, 12, 1);
        Employee copy = original;
        copy.raiseSalary(10); //also changed original

- For every class, you need to decide whether
    1. The default *clone* method is good enough
    2. The default *clone* method can be patched by calling *clone* on the mutalbe subobjects
    3. *clone* should not be attempted
    
- The third option is actually the default. To choose either the first or the second option, a class must
    1. Implement the *Cloneable* interface
    2. Redefine the *clone* method with the *public* access modifier
    
--
## Shallow copy

    class Employee implements Cloneable{
        @Override
        public Employee clone() throws CloneNotSupportedException{
            return (Employee)super.clone();
        }
        ......
    }
    

--
## Deep copy
    
    @Override
    public Employee clone() throws CloneNotSupportedException {
        Employee cloned = (Employee) super.clone();
        //clone mutable fields
        cloned.hireDay = (Date) hireDay.clone();
        return cloned;
    }
    
---
# Interfaces and callbacks
---
# Inner classes
An inner class is a class that is defined inside another class, Why would you want to do that?

- Inner class methods can access the data from the scope in which they are defined, including the data that would otherwise be private
- Inner classes can be hidden from other classes in the same package
- Anonymous inner classes are handy when you want to define callbacks without
writing a lot of code

--
## Use of an inner class to access object state
An inner class method gets to access both its own data fields and those of the outer object creating it

--
## InnerClassTest.java

    package com.shiwei.ssr;

    import java.awt.Toolkit;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    import java.util.Date;
    
    import javax.swing.JOptionPane;
    import javax.swing.Timer;
    
    public class TestInnerClass {
      public static void main(String[] args) {
        TalkingClock clock = new TalkingClock(1000, true);
        clock.start();
    
        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
      }
    }
    
    
    class TalkingClock {
      private int interval;
      private boolean beep;
    
      public TalkingClock(int interval, boolean beep) {
        this.interval = interval;
        this.beep = beep;
      }
    
      public void start() {
        ActionListener listener = new TimerPrinter();
        Timer t = new Timer(interval, listener);
        t.start();
      }
    
      public class TimerPrinter implements ActionListener {
    
        @Override
        public void actionPerformed(ActionEvent e) {
          Date now = new Date();
          System.out.println("the time is " + now);
          if (beep) Toolkit.getDefaultToolkit().beep();
        }
    
      }
    }

--
## Static inner class
- Occasionally, you may want to use an inner class simply to hide one class inside another, but you don’t need the inner class to have a reference to the outer class object. You can suppress the generation of that reference by declaring the inner class *static*
- Inner classes that are declared inside an interface are automatically static and public
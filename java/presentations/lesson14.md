title: Built-in Layout Panes
author: ivan
---
# Built-in Layout Panes
#### by ivan 2014.7

---
# Objectivies
- BorderPane
- HBox
- VBox
- StackPane
- GridPane
- FlowPane
- TilePane
- AnchorPane

---
# BorderPane
![borderpane](./borderpane.png)

--
## code snippet

    BorderPane borderPane = new BorderPane();
    borderPane.setTop(...);
    borderPane.setLeft(...);
    borderPane.setCenter(...);
    borderPane.setRight(...);
    borderPane.setBottom(...);
    
---
# HBox
![hbox](./hbox.png)

--
## code snippet

    package sample.layout;

    import javafx.application.Application;
    import javafx.geometry.Insets;
    import javafx.scene.Scene;
    import javafx.scene.control.Button;
    import javafx.scene.layout.HBox;
    import javafx.stage.Stage;
    
    /**
     * Created by ivan on 14-7-31.
     */
    public class HBoxTest extends Application {
    
      @Override
      public void start(Stage stage) throws Exception {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #336699;");
    
        Button buttonCurrent = new Button("Current");
        buttonCurrent.setPrefSize(100, 20);
    
        Button buttonProjected = new Button("Projected");
        buttonProjected.setPrefSize(100, 20);
        hbox.getChildren().addAll(buttonCurrent, buttonProjected);
    
        Scene scene = new Scene(hbox);
        stage.setTitle("HBox");
        stage.setScene(scene);
        stage.show();
      }
    }
    

---
# VBox
![vbox](./vbox.png)

--
## code snippet

    package sample.layout;

    import javafx.application.Application;
    import javafx.geometry.Insets;
    import javafx.scene.Scene;
    import javafx.scene.control.Hyperlink;
    import javafx.scene.layout.VBox;
    import javafx.scene.text.Font;
    import javafx.scene.text.FontWeight;
    import javafx.scene.text.Text;
    import javafx.stage.Stage;
    
    /**
     * Created by ivan on 14-7-31.
     */
    public class VBoxTest extends Application {
    
      @Override
      public void start(Stage stage) throws Exception {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);
    
        Text title = new Text("Data");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(title);
    
        Hyperlink options[] = new Hyperlink[]{
            new Hyperlink("Sales"),
            new Hyperlink("Marketing"),
            new Hyperlink("Distribution"),
            new Hyperlink("Costs")};
    
        for (int i = 0; i < 4; i++) {
          VBox.setMargin(options[i], new Insets(0, 0, 0, 8));
          vbox.getChildren().add(options[i]);
        }
    
        Scene scene = new Scene(vbox);
        stage.setTitle("VBox");
        stage.setScene(scene);
        stage.show();
      }
    }
    
---
# StackPane
The StackPane layout pane places all of the nodes within a single stack with each new node added on top of the previous node. This layout model provides an easy way to overlay text on a shape or image or to overlap common shapes to create a complex shape

---
# GridPane
The GridPane layout pane enables you to create a flexible grid of rows and columns in which to lay out nodes. Nodes can be placed in any cell in the grid and can span cells as needed. A grid pane is useful for creating forms or any layout that is organized in rows and columns

---
# FlowPane
The nodes within a FlowPane layout pane are laid out consecutively and wrap at the boundary set for the pane. Nodes can flow vertically (in columns) or horizontally (in rows). A vertical flow pane wraps at the height boundary for the pane. A horizontal flow pane wraps at the width boundary for the pane

---
# TilePane
A tile pane is similar to a flow pane. The TilePane layout pane places all of the nodes in a grid in which each cell, or tile, is the same size. Nodes can be laid out horizontally (in rows) or vertically (in columns). Horizontal tiling wraps the tiles at the tile pane's width boundary and vertical tiling wraps them at the height boundary. Use the prefColumns and prefRows properties to establish the preferred size of the tile pane.

Gap properties can be set to manage the spacing between the rows and columns. The padding property can be set to manage the distance between the nodes and the edges of the pane

---
# AnchorPane
The AnchorPane layout pane enables you to anchor nodes to the top, bottom, left side, right side, or center of the pane. As the window is resized, the nodes maintain their position relative to their anchor point. Nodes can be anchored to more than one position and more than one node can be anchored to the same position.

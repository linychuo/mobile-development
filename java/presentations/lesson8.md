title: Error hanlding with Exceptions
author: ivan
---
# Objectives
- Errors
- Exceptions

---
# The classification of exceptions
![exceptionHierarchyInJava](./exceptionHierarchyInJava.png)

--
# Errors
You should not throw an object of this type

--
# Exceptions
- Exceptions that inherit from *RuntimeException* include such problems as 
	- A bad cast
	- An out-of-bounds array access
	- A null pointer access

- Exceptions that do not inherit from *RuntimeException* include
	- Trying to read past the end of a file
	- Trying to open a file that doesn't exist
	- Trying to find a *Class* object for a string that does not denote an existing class

- The java language specification calls any exception that derives from the class *Error* of the class *RuntimeException* an unchecked exception. All other exceptions are called *checked* exceptions

--
## Declaring checked exceptions
- You call a method that throws a checked exception. for example, the *FileInputStream* constructor
- You detect an error and throw a checked exception with the *throw* statement
- You make a programming error, such as a[-1] = 0 that gives rise to an unchecked exception(in this case, an *ArrayIndexOutOfBoundsException*)
- An internal error occurs in the virtual machine or runtime library

--
## How to throw an exception
    throw new EOFException();
    
    String readData(Scanner in) throws EOFException{
        ......
        while(...){
            if(!in.hasNext()){
                if(n < len) throw new EOFException();
            }
            ......
        }
        
        return s;
    }

As you can see, throwing an exception is easy if one of the existing exception classes works for you. in this case:

1. Find an appropriate exception class
2. Make an object fo that class
3. Throw it

--
## Creating exception classes
    
    class FileFormatException extends IOException{
        public FileFormatException(){}
        public FileFormatException(String gripe){
            super(gripe);
        }
    }
    
Now you are ready to throw your very own exception type.
   
---
# Catching exceptions
    try{
        code
        more code
        more code
    }catch(ExceptionType e){
        handler for this type
    }
    
If any code inside the *try* block throws an exception of the class specified in the *catch* clause, then

1. The program skips the remainder of the code in the try block
2. The program executes the handler code inside the catch clause

--
## Catching multiple exceptions
    try{
        code that might throw exceptions        
    }catch(FileNotFoundException e){
        emergency action for missing files
    }catch(UnKnownHostException e){
        emergency action for unkown hosts
    }catch(IOException e){
        emergency action for all other I/O problems
    }
--
## Rethrowing and chaining exceptions
    try{
        access the database
    }catch(SQLException e){
        throw new ServletException("database error: " + e.getMessage());
    }
    
--
## The *finally* clause
    InputStream in = new FileInputStream(...);
    try{
        //1
        code that might throw exceptions
        //2
    }catch(IOException e){
        //3
        show error message
        //4
    }finally{
        //5
        in.close();
    }
    //6
      
--
## Tip    
We strongly suggest that you decouple *try/catch* and *try/finally* blocks. This makes code far less confusing. For example:

    InputStream in = ...;
    try{
        try{
            code that might throw exceptions
        }finally{
            in.close();
        }
    }catch(IOException e){
        show error message
    }
    
The inner try block has a single responsibility: to make sure that the input stream is closed.

The outer try block has a single responsibility: to ensure that erros are reported. Not only is this solution clearer, it is also more functional: Errors in the *finally* clause are reported

--
## The Try-with-Resources statement
Java SE 7 provides a useful shortcut to the code pattern

    open a resource
    try{
        work with the resource
    }finally{
        close the resource
    }
    
provided the resource belongs to a class that implements the *AutoCloseable* interface

    try (Scanner in = new Scanner(new FileInputStream("/home/ivan/words")){
        while (in.hasNext())
            System.out.println(in.next());
    }
    
When the try block exits, then in.close() is called automatically

---
# Tips for using exceptions
1. Exception handling is not supposed to replace a simple test
2. Do not micromanage exceptions
3. Make good use of the exception hierarchy
4. Do not squelch exceptions
5. When you detect an error, "tough love" works better that indulgence
6. Propagating exceptions is not a sign of shame

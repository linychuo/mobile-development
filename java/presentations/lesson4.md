title: Fundamental programming structures in java 3
author: ivan
---
# Fundamental programming structures in java 3
##### by ivan 2013.11
---
# Objectives
- Programming errors
- Methods
---
# Programming errors
- Syntax errors
- Runtime errors
- Logic Errors
- Debugging

--
## Syntax errors
Errors that occur during compilation are called _syntax errors_ or _compile errors_

    // ShowSyntaxErrors.java: The program contains syntax erros
    public class ShowSyntaxErrors{
        public static void main(String[] args){
            i = 30;
            System.out.print(i + 4);
        }
    }
    
--
## Runtime errors
Runtime errors are errors that cause a program to terminate abnormally

    // ShowRuntimeErrors.java: Program contains runtime errors
    public class ShowRuntimeErrors{
        public static void main(String[] args){
            int i = 1 / 0;
        }
    }
    
--
## Logic errors
Logic errors occur when a program does not perform the way it was intended to

    // ShowLogicErrors.java: The program contains a logic error
    public class ShowLogicErrors{
        public static void main(String[] args){
            // Add number1 to number2
            int number1 = 3;
            int number2 = 3;
            number += number1 + number2;
            System.out.println("number2 is " + number2);
        }
    }
    
--
## Debugging
- Syntax errors are eaxy to find and easy to correct, beacuse the compiler gives indications as to where the errors came from and why they are wrong
- Runtime errors are not difficult to find, either, since the reasons and locations of the errors are displayed on the console when program aborts.
- Bugs & debugging
---
# Methods
- Defining & Calling
- Passing parameters by values
- Overloading methods
- The Scope of variables

--
## Defining & Calling
    modifier returnValueType methodName(list of parameters){
        // method body;
    }
    
    public static int max(int num1, int num2){
        int result;
        
        if (num1 > num2)
            result = num1;
        else
            result = num2;
        return result;
    }
    
    System.out.println(max(3, 5));
    
Each time a method is invoked, the system stores parameters and variables in an area of memory known as a stack, which stores elements in last-in, first-out fashion

--
## Pass-by-value
The arguments must match the parameters in order, number, and compatible type, as defined in the method signature.

    public class TestPassByValue{
        public static void main(String[] args){
            int num1 = 1;
            int num2 = 2;
            
            System.out.println("Before invoking the swap method, num1 is " 
            + num1 + " and num2 is " + num2);
            
            swap(num1, num2);
            
            System.out.println("After invoking the swap method, num1 is " 
            + num1 + " and num2 is " + num2);
        }
        
        public static void swap(int n1, int n2){
            System.out.println("\tInside the swap method");
            System.out.println("\t\tBefore swapping n1 is " + n1 + " n2 is " + n2);
            
            int temp = n1;
            n1 = n2;
            n2 = temp;
            
            System.out.println("\t\tAfter swapping n1 is " + n1 + " n2 is " + n2);
        }
    }
    
--
## Overloading methods
Overloaded methods must have different parameter lists. You cannot overload methods based on different modifiers or returnn type, Consider the following code:

    public class AmbiguousOverloading {
        public static void main(String[] args) {
            System.out.println( max(1, 2) );
        }
    
        public static int max(int num1, int num2) {
            if (num1 > num2)
                return num1;
            else
                return num2;
        }
        
        public static double max(double num1, double num2) {
            if (num1 > num2)
                return num1;
            else
                return num2;
        }
    }


--
## The Scope of variables
The scope of a local variable starts from its declaration and continues to the end of the block that contains the variable. A local variable must be declared and assigned a value before it can be used.

---
# Method abstraction and stepwise refinement
- Information hiding
- Divide-and-conquer strategy
- Top-Down Design
- Top-Down or Bottom-Up Implementation

title: Introduction java programming language
author: Ivan
---
# Introduction java programming language
###### by ivan 2013.11
---
# Objectives
- Introduction objects
- Distinguish the terms API, IDE, JDK and JRE
- Building development environment
---
# Introduction objects
- The progress of abstraction
- An object has an interface and provides services
- The hidden implementation and reusing
- Inheritance
- Is-a vs is-like-a relationships
- Polymorphism
- The singly rooted hierarchy
- Parameterized types (generics)
- Object creation & lifetime
- Exception handling: dealing with errors
- Concurrent programming
---
# Introduction java
- Java as a programming platform
- Everything is an object
- Java language specification and API
- JDK and JRE
- IDE
--
## Java as a programming Platform
- Huge library
- Security
- Across OS
- Automatic garbage collection

--
## Everything is an object
- You manipulate objects with references
- You must create all the objects
    
    String s = new String("hello");
    
- Where storage lives: Registers, stack, heap, constant storage, non-ram storage
- Primitive types, High-precision, Arrays
- You never need to destory an object
- Creating new data types: class

--
## Java language specification and API
- The java language specification is a technical definition of the language that includes the syntax and semantics of Java Programming language. The complete Java language specification can be found at [here](http://docs.oracle.com/javase/specs/jls/se7/html/index.html)
- [http://docs.oracle.com/javase/7/docs/api/](http://docs.oracle.com/javase/7/docs/api/)
--
## JDK & JRE
- [Java development kit](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [Java runtime environment](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- ![jdk directory tree](./jdkDirectoryTree.jpg)
--
## IDE
- [![eclipse](./eclipse.gif)](http://www.eclipse.org/downloads/)
- [![netbeas](./netbeans.gif)](https://netbeans.org/downloads/)
---
# Hello, Java
    /**
     * This is the first program in java
     * @version 0.1.0 2013-11-25
     * @author ivan
     */
    public class HelloJava{
        public static void main(String[] args){
            System.out.println("hello, java");
        }
    }

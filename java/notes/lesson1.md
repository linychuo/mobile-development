#介绍什么是对象
##抽象的演变
- 汇编语言是对底层机器的抽象，许多命令式的语言是跟随汇编来的（fortan, basic, c），这些语言依然是从机器的角度来思考，然后用过程化的编程来解决问题
- 函数式编程语言(lisp)
- 面向对象认为每一个对象都是一台小计算机，有自己的状态，操作。但是他们都有自己独特的特征和行为
- Alan Kay汇总了smalltalk的５个基本特征，一个好的面向对象的语言应该具备以下的特征
>1.一切都是对象
>2.程序是由一个个对象构成，它们彼此可以通信
>3.每一个对象都有它们自己的内存空间
>4.每一个对象都有同样的类型
>5.所有对象的都能够接受一样的消息
- Booch offers描述一个对象：一个对象有状态，行为和标识


##每一个对象都有对外的接口，举例子：电灯有开关的接口可供外部调用
##一个对象应该提供服务
##它要隐藏实现的细节
##它可重用已有的实现
##继承
##is-a, is-like-a关系
##多态
##单根结构
##容器：list, map, set
##参数类型化，也是范型
##对象的建立和生命周期，垃圾收集器
##异常处理
##并发编程

#介绍java
#什么叫IDE(集成开发环境)：IDE通常包括程式語言編輯器、自動建立工具、通常還包括除錯器。有些IDE包含編譯器
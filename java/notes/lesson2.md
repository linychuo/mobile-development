#回顾昨天讲的东西和练习中的问题

#数据类型
##integer types
- int是最常使用,但是要表示地球上的总人口的话，就得用long
- byte和short用在一些特殊程序，比如low-level文件处理
- java中的integer类型的范围不依赖机器本身，因为象c或者c++，它们在32位和64位就不一样,而java是固定的
- 定义long型的变量是必须显示加一个L,16进制的数字必须加0x,8进制的前缀是0,因为容易混淆，所以不建议使用8进制
- 从java７开始，你可以用一个二进制来描述一个数字，前缀是ob,比如0b1001就是9,你还可以用下划线来分隔数字，比如1_000_000,下划线主要是为了我们方便阅读，编译器会在编译时remove它
- In C and C++, the sizes of types such as int and long depend on the target　platform. On a 16-bit processor　  such as the 8086, integers are 2 bytes, but on a 32-bit processor like a Pentium or SPARC they are 4-byte quantities. Similarly, long　values are 4-byte on 32-bit processors and 8-byte on 64-bit processors. These　differences make it challenging to write cross-platform programs. In Java, the sizes of　all numeric types are　platform-independent.
Note that Java does not have any unsigned types.
- 思考一下，为什么java中没有sizeof

##floating types
###浮点类型表示带有小数部分的数字
- double类型的精度是float的两倍,大部分程序使用double
- float类型的数据后面必须跟一个F,如果没有的话，编译器会认为它是double,你也可显示的加一个D来表示是double类型
- You can specify floating-point literals in hexadecimal. For example, 0.125 = 2 –3 can be written as 0x1.0p-3 . In hexadecimal notation, you use a p , not an e , to denote the exponent. (An e is a hexadecimal digit.) Note that the mantissa is written in hexadecimal and the exponent in decimal. The base of the exponent is 2, not 10.
- 0x1.0p-3 其中1.0指的是分子,分母是2的-3次方
- 正的无穷大，和负的无穷大,NaN不是一个number
- 浮点数不适合做财务计算的，因为不进位舍入是不能被容忍的，例如System.out.println(2.0 -1.1) prints 0.8999999999999999,它并不是你期望的0.9, Such roundoff errors are caused by the fact that floating-point numbers are represented in the binary number system.


##char type
###char是用来表示单个字符,'A'和"A"是不一样的
- In Java, the char type describes a code unit in the UTF-16 encoding
- 不建议使用char类型，除非你直接来表示utf-16字符,多使用string

##类型之间的转换
- 实线表示没有丢失信息丢失，虚线会丢失精度


#变量
- 定义变量推荐分开定义，例如int i,j最好能够分开定义，方便阅读

#操作符
- 5 << 3　表示5 * 8 = 40, 8 >> 3表示 8 / 8 = 1, 8 >>> 1，表示右移一位，且将移过去的位置补零


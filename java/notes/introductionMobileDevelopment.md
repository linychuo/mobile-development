##开始
- 找一些成功的故事来说明这个行业的前景
- 说说开这个专业的原因，前景和现在所存在的问题，包括以后所能做的工作
- [什么是移动开发?](http://en.wikipedia.org/wiki/Mobile_application_development)

##概述
- 分别讲此课程所讲的内容，重点说明本课程用的操作系统，为什么选ubuntu

##关于java课程
- 为什么要以java为基础来讲，因为这门课的重点是android原生开发，所以要以java语言来做为基础
- java课程所涉及的内容，包括基础和面向对象的设计和编程

##android课程
- android是什么，讲述它的历史和特点，还有将来的发展，目前android的版本历史
- 简要说明Dalvik是什么和jvm之间的区别

##html&css&js
- 为什么要学html,css,javascript?
- html课程涉及的内容
- css能干什么
- javascript基础语法和存在的问题，为什么会出现coffeescript

##跨平台开发
- 跨平台开发的优势和存在的问题，举一些实际的例子来说明
- phonegap能干什么
- 什么是titanium?举大芝麻客户端的例子来说明它的优势和存在的问题

##IOS
- IOS开发所涉及的内容，目前的状况，为什么没有这个专业
- Objective-C的历史，为什么会被选中做为ios开发的语言
- xcode是用来干什么的
- 发布流程中注意的问题（个人和企业），具体流程到正式的课程再讲解

##学生提问
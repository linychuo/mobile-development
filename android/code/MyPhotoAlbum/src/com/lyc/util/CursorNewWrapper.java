package com.lyc.util;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ivan on 3/6/14.
 */
public class CursorNewWrapper extends CursorWrapper {

  private final Cursor cursor;
  private final Map<String, Integer> tableColumnMetadata;

  public CursorNewWrapper(Cursor cursor) {
    super(cursor);
    this.cursor = cursor;
    this.tableColumnMetadata = new HashMap<String, Integer>();
    for (String name : cursor.getColumnNames()) {
      tableColumnMetadata.put(name, cursor.getColumnIndex(name));
    }
  }

  public int getInt(String columnName) {
    return cursor.getInt(tableColumnMetadata.get(columnName));
  }

  public String getString(String columnName) {
    return cursor.getString(tableColumnMetadata.get(columnName));
  }

  public long getLong(String columnName) {
    return cursor.getLong(tableColumnMetadata.get(columnName));
  }

  @Override
  public void close() {
    super.close();
    cursor.close();
  }
}

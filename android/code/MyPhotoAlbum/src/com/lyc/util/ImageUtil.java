package com.lyc.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by ivan on 3/10/14.
 */
public class ImageUtil {

  private static final String TAG = "ImageUtil";

  private ImageUtil() {

  }

  public static Bitmap decodeSampledBitmap(String path, int width, int height) {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(path, localOptions);
    localOptions.inSampleSize = calculateInSampleSize(localOptions,
                                                      width, height);
    localOptions.inJustDecodeBounds = false;
    return BitmapFactory.decodeFile(path, localOptions);
  }

  public static int calculateInSampleSize(BitmapFactory.Options options,
                                          int reqWidth, int reqHeight) {
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;
    if (height > reqHeight || width > reqWidth) {
      final int heightRatio = Math.round((float) height / (float) reqHeight);
      final int widthRatio = Math.round((float) width / (float) reqWidth);
      inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
    }
    return inSampleSize;
  }

}

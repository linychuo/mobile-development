package com.lyc.adapter.bean;

import android.content.ContentUris;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.lyc.util.CursorNewWrapper;
import com.lyc.util.ImageUtil;

import static android.provider.MediaStore.Images.ImageColumns;
import static android.provider.MediaStore.Images.Media;

/**
 * Created by ivan on 3/10/14.
 */
public class Image {

  private static final String TAG = "Image";

  private final long id;
  private final String path;
  private final String mimeType;
  private final long dateTaken;
  private final String title;

  public Image(long id, String path, String mimeType, long dateToken,
               String title) {
    this.id = id;
    this.path = path;
    this.mimeType = mimeType;
    this.dateTaken = dateToken;
    this.title = title;
  }

  public Image(CursorNewWrapper cursor) {
    id = cursor.getLong(ImageColumns._ID);
    path = cursor.getString(ImageColumns.DATA);
    dateTaken =
        cursor.getLong(ImageColumns.DATE_TAKEN) == 0 ? cursor.getLong(ImageColumns.DATE_MODIFIED)
                                                       * 1000
                                                     : cursor.getLong(ImageColumns.DATE_TAKEN);
    title =
        cursor.getString(ImageColumns.TITLE) == null ? path : cursor.getString(ImageColumns.TITLE);
    mimeType = cursor.getString(ImageColumns.MIME_TYPE);
  }

  public Uri getUri() {
    try {
      long existingId = ContentUris.parseId(Media.EXTERNAL_CONTENT_URI);
      if (existingId != id) {
        Log.e(TAG, "id mismatch");
      }
      return Media.EXTERNAL_CONTENT_URI;
    } catch (NumberFormatException ex) {
      return ContentUris.withAppendedId(Media.EXTERNAL_CONTENT_URI, id);
    }
  }

  public long getId() {
    return id;
  }

  public String getPath() {
    return path;
  }

  public String getMimeType() {
    return mimeType;
  }

  public long getDateTaken() {
    return dateTaken;
  }

  public String getTitle() {
    return title;
  }

  public Bitmap getBitmap() {
    return ImageUtil.decodeSampledBitmap(this.path, 80, 80);
  }

  public Bitmap getBitmap(int width, int height) {
    return ImageUtil.decodeSampledBitmap(this.path, width, height);
  }

  @Override
  public String toString() {
    return "Image{" +
           "uri=" + getUri() +
           ", id=" + id +
           ", path='" + path + '\'' +
           ", mimeType='" + mimeType + '\'' +
           ", dateTaken=" + dateTaken +
           ", title='" + title + '\'' +
           '}';
  }
}

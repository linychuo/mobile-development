package com.lyc.adapter.bean;

import com.lyc.util.CursorNewWrapper;

import static android.provider.MediaStore.Images.ImageColumns;

/**
 * Created by ivan on 3/10/14.
 */
public class Album {

  private final String bucketId;
  private final String bucketName;
  private Image cover;
  private int count;

  public Album(String bucketId) {
    this.bucketId = bucketId;
    this.bucketName = null;
  }

  public Album(CursorNewWrapper cursor) {
    this.bucketName = cursor.getString(ImageColumns.BUCKET_DISPLAY_NAME);
    this.bucketId = cursor.getString(ImageColumns.BUCKET_ID);
  }


  public String getBucketId() {
    return bucketId;
  }

  public String getBucketName() {
    return bucketName;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public int getCount() {
    return count;
  }

  public Image getCover() {
    return cover;
  }

  public void setCover(Image cover) {
    this.cover = cover;
  }

}

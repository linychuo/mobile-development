package com.lyc.adapter.bean;

import static android.provider.MediaStore.Images.ImageColumns;
import static android.provider.MediaStore.Images.Media;

/**
 * Created by ivan on 3/11/14.
 */
public final class Constants {

  private Constants() {

  }

  public static final String[] ACCEPTABLE_IMAGE_TYPES = new String[]{
      "image/jpeg", "image/png", "image/gif"};

  public static final String[]
      ALBUM_PROJECTION =
      new String[]{Media.BUCKET_DISPLAY_NAME, Media.BUCKET_ID};
  public static final String[] IMAGE_PROJECTION = new String[]{ImageColumns._ID,
                                                               ImageColumns.DATA,
                                                               ImageColumns.DATE_TAKEN,
                                                               ImageColumns.TITLE,
                                                               ImageColumns.MIME_TYPE,
                                                               ImageColumns.DATE_MODIFIED};

  public static final String WHERE_CLAUSE = "(" + Media.MIME_TYPE
                                            + " in (?, ?, ?))";

  public static final String
      WHERE_CLAUSE_WITH_BUCKET_ID =
      WHERE_CLAUSE + " AND " + ImageColumns.BUCKET_ID + " = ?";


  public static String[] whereWithBucketId(String bucketId) {
    int count = Constants.ACCEPTABLE_IMAGE_TYPES.length;
    String[] result = new String[count + 1];
    System.arraycopy(Constants.ACCEPTABLE_IMAGE_TYPES, 0, result, 0, count);
    result[count] = bucketId;
    return result;
  }

  public static String sortOrder() {
    String ascending = " DESC";
    String dateExpr = "case ifnull(datetaken,0)"
                      + " when 0 then date_modified*1000" + " else datetaken"
                      + " end";
    return dateExpr + ascending + ", _id" + ascending;
  }
}

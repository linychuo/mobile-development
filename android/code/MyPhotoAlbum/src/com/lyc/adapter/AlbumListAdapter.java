package com.lyc.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyc.R;
import com.lyc.adapter.bean.Album;
import com.lyc.adapter.bean.Image;

/**
 * Created by ivan on 3/10/14.
 */
public class AlbumListAdapter extends ArrayAdapter<Album> {

  private static final String TAG = "AlbumListAdapter";
  private Activity ctx;


  public AlbumListAdapter(Activity context) {
    super(context, R.layout.row);
    this.ctx = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    if (rowView == null) {
      LayoutInflater inflater = ctx.getLayoutInflater();
      rowView = inflater.inflate(R.layout.row, null);
      ViewHolder holder = new ViewHolder();
      holder.albumNameTxtView = (TextView) rowView.findViewById(R.id.album_name);
      holder.albumCoverImgView = (ImageView) rowView.findViewById(R.id.album_cover);
      rowView.setTag(holder);
    }

    ViewHolder holder = (ViewHolder) rowView.getTag();
    holder.albumNameTxtView
        .setText(getItem(position).getBucketName() + " (" + getItem(position).getCount() + ")");

    Image image = getItem(position).getCover();
    Log.d(TAG, "image = " + image);
    holder.albumCoverImgView.setImageBitmap(image.getBitmap());

    return rowView;
  }

  private static class ViewHolder {

    TextView albumNameTxtView;
    ImageView albumCoverImgView;
  }
}

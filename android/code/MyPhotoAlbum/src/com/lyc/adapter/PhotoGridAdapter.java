package com.lyc.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.lyc.R;
import com.lyc.adapter.bean.Image;
import com.lyc.util.CursorNewWrapper;

import java.util.Date;

/**
 * Created by ivan on 3/11/14.
 */
public class PhotoGridAdapter extends ResourceCursorAdapter {

  public PhotoGridAdapter(Activity ctx) {
    super(ctx, R.layout.item, null);
  }

  @Override
  public void bindView(View view, Context context, Cursor cursor) {
    CursorNewWrapper c = new CursorNewWrapper(cursor);
    Image image = new Image(c);
    ImageView imgView = (ImageView) view.findViewById(R.id.photo_item_imgView);
    TextView txtView = (TextView) view.findViewById(R.id.photo_title_txtView);
    imgView.setImageBitmap(image.getBitmap(160, 160));
    txtView.setText(getDate(image.getDateTaken()));
  }

  private String getDate(long datetime) {
    return DateFormat.format("yyyy-MM-dd HH:mm:ss", new Date(datetime)).toString();
  }

}

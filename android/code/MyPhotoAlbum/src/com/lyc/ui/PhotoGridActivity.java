package com.lyc.ui;


import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.widget.GridView;

import com.lyc.R;
import com.lyc.adapter.PhotoGridAdapter;
import com.lyc.adapter.bean.Constants;

/**
 * Created by ivan on 3/10/14.
 */
public class PhotoGridActivity extends FragmentActivity
    implements LoaderManager.LoaderCallbacks<Cursor> {

  private static final String TAG = "PhotoGridActivity";
  private PhotoGridAdapter adapter;
  private GridView gridView;
  private String bucketId;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.photo_grid);

    bucketId = getIntent().getStringExtra("album_id");
    gridView = (GridView) findViewById(R.id.photo_grid_view);

    adapter = new PhotoGridAdapter(this);
    gridView.setAdapter(adapter);
    getSupportLoaderManager().initLoader(0, null, this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    getSupportLoaderManager().restartLoader(0, null, this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    adapter = null;
    gridView = null;
    bucketId = null;
    getSupportLoaderManager().destroyLoader(0);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
    return new CursorLoader(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            Constants.IMAGE_PROJECTION,
                            Constants.WHERE_CLAUSE_WITH_BUCKET_ID,
                            Constants.whereWithBucketId(bucketId),
                            Constants.sortOrder());
  }

  @Override
  public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
    adapter.changeCursor(cursor);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> cursorLoader) {
    adapter.changeCursor(null);
  }
}
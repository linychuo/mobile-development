package com.lyc.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lyc.R;
import com.lyc.adapter.AlbumListAdapter;
import com.lyc.adapter.bean.Album;
import com.lyc.adapter.bean.Constants;
import com.lyc.adapter.bean.Image;
import com.lyc.util.CursorNewWrapper;

import static android.provider.MediaStore.Images.Media;

public class AlbumListActivity extends BaseActivity implements AdapterView.OnItemClickListener {

  private static final String TAG = "AlbumListActivity";

  private Handler mHandler;
  private AlbumListAdapter mAdapter;
  private ListView listView;
  private View mNoImagesView;
  private AlertDialog mAlertDialog;

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    mHandler = new Handler();
    listView = (ListView) findViewById(R.id.albums);
    listView.setOnItemClickListener(this);
    mAdapter = new AlbumListAdapter(this);
    listView.setAdapter(mAdapter);
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (getExternalCacheDir() == null) {
      if (mAlertDialog == null) {
        DialogInterface.OnCancelListener onCancel = new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
            finish();
          }
        };
        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
          }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
            .setTitle(R.string.no_external_storage_title)
            .setMessage(R.string.no_external_storage)
            .setNegativeButton(android.R.string.cancel, onClick)
            .setOnCancelListener(onCancel);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
      } else {
        mAlertDialog.show();
      }
    } else {
      mAdapter.clear();
      mHandler.post(new Runnable() {
        @Override
        public void run() {
          retrieveAlbum();
        }
      });
    }
  }

  @Override
  protected void onStop() {
    super.onStop();
    if (mAlertDialog != null) {
      mAlertDialog.dismiss();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    // free up some ram
    mAdapter = null;
    listView = null;
    mHandler = null;
    mNoImagesView = null;
    mAlertDialog = null;
  }

  private void showNoImagesView() {
    if (mNoImagesView == null) {
      ViewGroup root = (ViewGroup) findViewById(R.id.root);
      getLayoutInflater().inflate(R.layout.gallerypicker_no_images, root);
      mNoImagesView = findViewById(R.id.no_images);
    }
    mNoImagesView.setVisibility(View.VISIBLE);
  }

  private void hideNoImagesView() {
    if (mNoImagesView != null) {
      mNoImagesView.setVisibility(View.GONE);
    }
  }

  private void retrieveAlbum() {
    Uri
        uri =
        Media.EXTERNAL_CONTENT_URI.buildUpon()
            .appendQueryParameter("distinct", "true").build();

    CursorNewWrapper cursor = query(uri, Constants.ALBUM_PROJECTION, Constants.WHERE_CLAUSE,
                                    Constants.ACCEPTABLE_IMAGE_TYPES,
                                    Media.DEFAULT_SORT_ORDER);
    if (cursor.getCount() == 0) {
      showNoImagesView();
      return;
    }

    try {
      while (cursor.moveToNext()) {
        Album data = new Album(cursor);
        setCntAndCover(data);
        mAdapter.add(data);
      }
    } finally {
      cursor.close();
    }
  }

  private void setCntAndCover(Album album) {
    CursorNewWrapper cursor = query(Media.EXTERNAL_CONTENT_URI,
                                    Constants.IMAGE_PROJECTION,
                                    Constants.WHERE_CLAUSE_WITH_BUCKET_ID,
                                    Constants.whereWithBucketId(album.getBucketId()),
                                    Constants.sortOrder());
    try {
      album.setCount(cursor.getCount());
      //get first image
      cursor.moveToPosition(0);
      Image cover = new Image(cursor);
      album.setCover(cover);
    } finally {
      cursor.close();
    }

  }

  @Override
  public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    Album album = (Album) adapterView.getItemAtPosition(i);
    Intent intent = new Intent(this, PhotoGridActivity.class);
    intent.putExtra("album_id", album.getBucketId());
    startActivity(intent);
  }
}

package com.lyc.ui;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;

import com.lyc.util.CursorNewWrapper;

import static android.provider.MediaStore.Images.Media;

/**
 * Created by ivan on 3/11/14.
 */
public abstract class BaseActivity extends Activity {

  protected CursorNewWrapper query(Uri uri,
                                   String[] projection,
                                   String selection,
                                   String[] selectionArgs,
                                   String sortOrder) {
    Cursor cursor = Media.query(getContentResolver(), uri, projection, selection, selectionArgs,
                                sortOrder);
    return new CursorNewWrapper(cursor);
  }

}

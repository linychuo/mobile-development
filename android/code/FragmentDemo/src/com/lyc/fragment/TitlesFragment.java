package com.lyc.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by ivan on 3/16/14.
 */
public class TitlesFragment extends ListFragment {

  private static final String TAG = "TitlesFragment";

  private MyActivity myActivity;
  private int curCheckPosition = 0;

  @Override
  public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
    super.onInflate(activity, attrs, savedInstanceState);
    if (savedInstanceState != null) {
      curCheckPosition = savedInstanceState.getInt("curChoice", 0);
    }
    Log.d(TAG, "-----onInflate----");
  }

  @Override
  public void onAttach(Activity activity) {
    Log.d(TAG, "--------onAttach-------");
    super.onAttach(activity);
    this.myActivity = (MyActivity) activity;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "-------Fragment onCreate----------");
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    setListAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                                            getResources().getStringArray(R.array.titles)));

    ListView lv = getListView();
    lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    lv.setSelection(curCheckPosition);

    myActivity.showDetail(curCheckPosition);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt("curChoice", curCheckPosition);
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    myActivity.showDetail(position);
    curCheckPosition = position;
  }

  @Override
  public void onDetach() {
    Log.d(TAG, "---------onDetach----------");
    super.onDetach();
    myActivity = null;
  }
}

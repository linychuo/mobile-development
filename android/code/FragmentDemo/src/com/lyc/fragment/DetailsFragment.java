package com.lyc.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ivan on 3/16/14.
 */
public class DetailsFragment extends Fragment {

  private int index = 0;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.index = getArguments().getInt("index", 0);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.details, container, false);
    TextView text1 = (TextView) view.findViewById(R.id.text1);
    String[] details = getResources().getStringArray(R.array.details);
    text1.setText(details[index]);
    return view;
  }


  public static DetailsFragment newInstance(int index) {
    DetailsFragment df = new DetailsFragment();
    Bundle args = new Bundle();
    args.putInt("index", index);
    df.setArguments(args);
    return df;
  }

  public static DetailsFragment newInstance(Bundle bundle) {
    int index = bundle.getInt("index", 0);
    return newInstance(index);
  }

  public int getShowIndex() {
    return index;
  }
}

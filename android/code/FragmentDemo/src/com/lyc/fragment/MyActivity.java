package com.lyc.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class MyActivity extends FragmentActivity {

  private static final String TAG = "MyActivity";

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d(TAG, "--------onCreate---------");
    setContentView(R.layout.main);
  }

  public boolean isMultiPane() {
    return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
  }

  public void showDetail(int index) {
    if (isMultiPane()) {
      DetailsFragment details =
          (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.details);
//      Bundle args = new Bundle();
//      args.putInt("index", index);
//      details.setArguments(args);
//      String[] detailArray = getResources().getStringArray(R.array.details);
//      TextView detailView = (TextView) details.getView().findViewById(R.id.text1);
//      detailView.setText(detailArray[index]);
      if (details == null || details.getShowIndex() != index) {
        Log.d(TAG, "------------" + index);
        details = DetailsFragment.newInstance(index);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//        ft.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_down);
        ft.replace(R.id.details, details);
        ft.commit();
      }
    } else {
      Intent intent = new Intent();
      intent.setClass(this, DetailActivity.class);
      intent.putExtra("index", index);
      startActivity(intent);
    }
  }
}

package com.lyc.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by ivan on 3/16/14.
 */
public class DetailActivity extends FragmentActivity {


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
      finish();
      return;
    }

    if (getIntent() != null) {
      DetailsFragment detailsFragment = DetailsFragment.newInstance(getIntent().getExtras());
      getSupportFragmentManager().beginTransaction().add(android.R.id.content, detailsFragment)
          .commit();
    }
  }
}

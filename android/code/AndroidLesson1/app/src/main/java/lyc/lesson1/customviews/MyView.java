package lyc.lesson1.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import lyc.app.R;

/**
 * Created by ivan on 14-9-18.
 */
public class MyView extends View {

  private Paint linePaint;

  public MyView(Context context) {
    super(context);
    init();
  }

  public MyView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public MyView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  private void init() {
    linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    linePaint.setColor(getResources().getColor(R.color.notepad_lines));
  }

  @Override
  protected void onDraw(Canvas canvas) {
    canvas.drawCircle(getMeasuredWidth() / 2, getMeasuredHeight() / 2, getMeasuredWidth() / 2,
                      linePaint);
    super.onDraw(canvas);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int width = MeasureSpec.getSize(widthMeasureSpec);
    int height = MeasureSpec.getSize(heightMeasureSpec);
    Log.d("MyView", "width = " + width);
    Log.d("MyView", "height = " + height);
    if (width > height) {
      setMeasuredDimension(height, height);
    } else {
      setMeasuredDimension(width, width);
    }
  }
}

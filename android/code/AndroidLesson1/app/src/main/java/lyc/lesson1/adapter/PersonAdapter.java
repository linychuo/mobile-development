package lyc.lesson1.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import lyc.app.R;
import lyc.lesson1.model.Person;

/**
 * Created by ivan on 14-9-17.
 */
public class PersonAdapter extends ArrayAdapter<Person> {

  private Context context;
  private float lastX;

  public PersonAdapter(Activity activity, Person[] data) {
    super(activity, -1, -1, data);
    this.context = activity;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    Log.d("PersonAdapter", "" + position + ", " + convertView + ", " + parent);
    if (rowView == null) {
      Log.d("PersonAdapter", "<><><><><><><>");
      LayoutInflater inflater = (LayoutInflater) context
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      rowView = inflater.inflate(R.layout.simple_list_item_custom, parent, false);
      final ViewHolder viewHolder = new ViewHolder();
      viewHolder.contentView = (RelativeLayout) rowView.findViewById(R.id.content_view);
      viewHolder.btnDelete = (Button) rowView.findViewById(R.id.btnDelete);
      viewHolder.firstNameTextView = (TextView) rowView.findViewById(R.id.firstName_txtView);
      viewHolder.lastNameTextView = (TextView) rowView.findViewById(R.id.lastName_txtView);

      viewHolder.contentView.setOnTouchListener(new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
          switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
              lastX = event.getX();
              return true;
            case MotionEvent.ACTION_UP:
              float currentX = event.getX();
              Log.d("PersonAdapter", "lastX = " + lastX + ", currentX = " + currentX);
              if (currentX < lastX) {
                Log.d("PersonAdapter", "=========");
//                viewHolder.contentView.animate().translationX(-30);
                viewHolder.contentView.startAnimation(new TranslateAnimation(0, -30, 0, 0));
                viewHolder.btnDelete.setVisibility(View.VISIBLE);
              } else {
                viewHolder.contentView.startAnimation(new TranslateAnimation(-30, 0, 0, 0));
                viewHolder.btnDelete.setVisibility(View.GONE);
              }
              return false;
            case MotionEvent.ACTION_MOVE:
              break;
          }
          return false;
        }
      });

      rowView.setTag(viewHolder);
    }

    ViewHolder holder = (ViewHolder) rowView.getTag();
    holder.firstNameTextView.setText(getItem(position).getFirstName());
    holder.lastNameTextView.setText(getItem(position).getLastName());
    return rowView;
  }

  private static class ViewHolder {

    RelativeLayout contentView;
    TextView firstNameTextView;
    TextView lastNameTextView;
    Button btnDelete;
  }
}

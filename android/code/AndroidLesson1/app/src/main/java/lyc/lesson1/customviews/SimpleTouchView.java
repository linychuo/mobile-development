package lyc.lesson1.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ivan on 3/3/14.
 */
public class SimpleTouchView extends View {

  private Paint paint;
  private Path path;

  public SimpleTouchView(Context context) {
    super(context);
    init();
  }

  public SimpleTouchView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public SimpleTouchView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    canvas.drawPath(this.path, this.paint);
    Log.d("SimpleTouchView", "----------" + path.isEmpty());
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    float eventX = event.getX();
    float eventY = event.getY();

    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        path.moveTo(eventX, eventY);
        break;
      case MotionEvent.ACTION_MOVE:
        path.lineTo(eventX, eventY);
//        path.addCircle(eventX, eventY, 10, Path.Direction.CW);
        break;
      case MotionEvent.ACTION_UP:
        break;
    }

    invalidate();
    return false;
  }

  private void init() {
    paint = new Paint();
    paint.setAntiAlias(true);
    paint.setStrokeWidth(6f);
    paint.setColor(Color.WHITE);
    paint.setStyle(Paint.Style.STROKE);
    paint.setStrokeJoin(Paint.Join.ROUND);
    path = new Path();
  }
}

package lyc.lesson1.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import lyc.app.R;
import lyc.lesson1.model.Course;

/**
 * Created by ivan on 14-9-23.
 */
public class CourseAdapter extends ArrayAdapter<Course> {

  private Context context;

  public CourseAdapter(Activity activity, Course[] data) {
    super(activity, -1, -1, data);
    this.context = activity;
  }


  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    if (rowView == null) {
      LayoutInflater inflater = (LayoutInflater) context
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      rowView = inflater.inflate(R.layout.course_item_layout, parent, false);
      ViewHolder viewHolder = new ViewHolder();
      viewHolder.nameTxtView = (TextView) rowView.findViewById(R.id.name_txtView);
      viewHolder.creditTxtView = (TextView) rowView.findViewById(R.id.credit_txtView);
      rowView.setTag(viewHolder);
    }

    ViewHolder holder = (ViewHolder) rowView.getTag();
    holder.nameTxtView.setText(getItem(position).getName());
    holder.creditTxtView.setText(String.valueOf(getItem(position).getCredit()));
    return rowView;
  }


  private static class ViewHolder {

    TextView nameTxtView;
    TextView creditTxtView;
  }
}

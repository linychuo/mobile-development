package lyc.lesson1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import lyc.app.R;
import lyc.lesson1.adapter.PersonAdapter;
import lyc.lesson1.model.Person;

/**
 * Created by ivan on 14-9-16.
 */
public class ListViewTestActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.listview_test);

    final Person[]
        data =
        {new Person("jacky", "chan"), new Person("andy", "lau"), new Person("steve", "chou"),
         new Person("jacky", "chan"), new Person("andy", "lau"), new Person("steve", "chou"),
         new Person("jacky", "chan"), new Person("andy", "lau"), new Person("steve", "chou"),
         new Person("jacky", "chan"), new Person("andy", "lau"), new Person("steve", "chou")};
    PersonAdapter adapter = new PersonAdapter(this, data);

    ListView myListView = (ListView) findViewById(R.id.my_listview);
    myListView.setAdapter(adapter);

    myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("ListViewTestActivity", "" + adapterView);
        Log.d("ListViewTestActivity", "" + view);
        Log.d("ListViewTestActivity", "" + data[i]);
        Log.d("ListViewTestActivity", "" + l);
      }
    });
  }
}

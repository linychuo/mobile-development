package lyc.lesson1;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import lyc.app.R;
import lyc.lesson1.util.BitmapUtil;


public class ViewFlipperTestActivity extends Activity {

  private static final String TAG = "ViewFlipperTestActivity";

  private ViewFlipper viewFlipper;
  private float lastX;
  private static final int[] imgs = {R.drawable.img1, R.drawable.img2,
                                     R.drawable.img3, R.drawable.img4, R.drawable.img5};
  private List<Bitmap> bitmaps = new ArrayList<Bitmap>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_view_flipper_test);
    viewFlipper = (ViewFlipper) findViewById(R.id.viewflipper);

    for (int imgId : imgs) {
      ImageView imageView = new ImageView(this);
      Bitmap bitmap = BitmapUtil.decodeSampledBitmapFromResource(getResources(), imgId, 480, 800);
      imageView.setImageBitmap(bitmap);
      bitmaps.add(bitmap);
//      imageView.setImageResource(imgId);
//      imageView.setScaleType(ImageView.ScaleType.FIT_XY);
      viewFlipper
          .addView(imageView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                         ViewGroup.LayoutParams.MATCH_PARENT));
      viewFlipper.setAutoStart(true);
      viewFlipper.setFlipInterval(3000);
    }
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    viewFlipper.stopFlipping();
    viewFlipper.setAutoStart(false);
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        lastX = event.getX();
        break;
      case MotionEvent.ACTION_UP:
        float currentX = event.getX();
        if (lastX < currentX) {
          //left to right
          if (viewFlipper.getDisplayedChild() == 0) {
            break;
          }

          viewFlipper.setInAnimation(this, R.anim.in_from_left);
          viewFlipper.setOutAnimation(this, R.anim.out_to_right);
          viewFlipper.showNext();
        }

        if (lastX > currentX) {
          //right to left
          if (viewFlipper.getDisplayedChild() == 1) {
            break;
          }
          viewFlipper.setInAnimation(this, R.anim.in_from_right);
          viewFlipper.setOutAnimation(this, R.anim.out_to_left);
          viewFlipper.showPrevious();
        }
        break;
    }
    return false;
  }

  @Override
  protected void onDestroy() {
    Log.d(TAG, ",,,,,,,,,,,,onDestroy");
    for (Bitmap item : bitmaps) {
      if (!item.isRecycled()) {
        item.recycle();
      }
    }
    bitmaps.clear();
    bitmaps = null;
    super.onDestroy();
  }
}

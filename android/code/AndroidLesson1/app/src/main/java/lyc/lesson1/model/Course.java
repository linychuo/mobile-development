package lyc.lesson1.model;

/**
 * Created by ivan on 14-9-23.
 */
public class Course {

  private String name;
  private int credit;

  public Course(String name, int credit) {
    this.name = name;
    this.credit = credit;
  }

  public String getName() {
    return name;
  }

  public int getCredit() {
    return credit;
  }
}

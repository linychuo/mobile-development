package lyc.lesson1;

import android.app.Application;
import android.content.res.Configuration;
import android.util.Log;

/**
 * Created by ivan on 14-9-13.
 */
public class MyApplication extends Application {

  private static MyApplication singleton;

  public static MyApplication getInstance() {
    return singleton;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    singleton = this;
    Log.d("MyApplication", "=====onCreate=====");
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    Log.d("MyApplication",
          "=========onConfigurationChanged=======" + newConfig.keyboardHidden);
  }
}



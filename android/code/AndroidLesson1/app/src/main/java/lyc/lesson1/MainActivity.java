package lyc.lesson1;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import lyc.app.R;


public class MainActivity extends ActionBarActivity {

  private static final String TAG = "MainActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "========== MainActivity onCreate========");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Button button = (Button) findViewById(R.id.btnOther);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(TAG, "===fdsafds=af=dsafdsa");
//        Intent intent = new Intent(MainActivity.this, OtherActivity.class);
        Intent intent = new Intent("lyc.app.OTHER_ACTIVITY");
        intent.putExtra("name", "liyongchao");
        startActivityForResult(intent, 11);

        Intent intent1 = new Intent("lyc.app2.ACTION_TEST_BROADCAST");
        intent1.putExtra("message", "你好");
//        sendBroadcast(intent1, "lyc.app.ACTION_BROADCAST_PERMISSION");
        sendOrderedBroadcast(intent1, "lyc.app.ACTION_BROADCAST_PERMISSION");
      }
    });

    Button btnListView = (Button) findViewById(R.id.btnListView);
    btnListView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, ListViewTestActivity.class);
        startActivity(intent);
      }
    });

    Button btnViewFlipper = (Button) findViewById(R.id.btnViewFlipper);
    btnViewFlipper.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, ViewFlipperTestActivity.class);
        startActivity(intent);
      }
    });

    Button btnToast = (Button) findViewById(R.id.btnToast);
    btnToast.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Toast.makeText(MainActivity.this, R.string.app_name, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this, ShowCustomViewActivity.class);
        startActivity(intent);
      }
    });

    Button btnLong = (Button) findViewById(R.id.btnLong);
    btnLong.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View view) {
        Log.d(TAG, "<><><><><><><><><>");
        return false;
      }
    });

    btnLong.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(TAG, "111111111111111");
      }
    });

    btnLong.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.d(TAG, "" + motionEvent.getAction());
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
          return true;
        }
        return false;
      }
    });

    Button btnOpenDialog = (Button) findViewById(R.id.btnOpenDialog);
    final LayoutInflater inflater = getLayoutInflater();
    btnOpenDialog.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//        builder.setCustomTitle(inflater.inflate(R.layout.layout_signin, null));
        builder.setTitle("Hello");
//        .setItems(R.array.courses, new DialogInterface.OnClickListener() {
//          @Override
//          public void onClick(DialogInterface dialogInterface, int i) {
//            String[] courses = getResources().getStringArray(R.array.courses);
//            Log.d("MainActivity", "You select course is " + courses[i]);
//          }
//        });

//        final Course[]
//            courses =
//            {new Course("语文", 3), new Course("数学", 5), new Course("哲学", 4), new Course("音乐", 2)};
//        builder.setAdapter(new CourseAdapter(MainActivity.this, courses),
//                           new DialogInterface.OnClickListener() {
//                             @Override
//                             public void onClick(DialogInterface dialogInterface, int i) {
//                               Log.d("MainActivity",
//                                     "You select course is [" + courses[i].getName() + ", "
//                                     + courses[i].getCredit() + "]");
//                             }
//                           });

        final View loginView = inflater.inflate(R.layout.layout_signin, null);
        builder.setView(loginView);
//        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//          @Override
//          public void onClick(DialogInterface dialogInterface, int i) {
//            TextView userName = (TextView) loginView.findViewById(R.id.username);
//            TextView password = (TextView) loginView.findViewById(R.id.password);
//            Log.d(TAG,
//                  "yes------------" + userName.getText() + ", " + password.getText());
//          }
//        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
//          @Override
//          public void onClick(DialogInterface dialogInterface, int i) {
//            Log.d(TAG, "no-------------");
//          }
//        });

        AlertDialog dialog = builder.create();
        dialog.show();
      }
    });

    Button btnIntent = (Button) findViewById(R.id.btnIntent);
    btnIntent.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent =
            new Intent(Intent.ACTION_VIEW, Uri.parse("http://0755-22334422"));
        startActivity(intent);
      }
    });

    Button btnSimpleTouchView = (Button) findViewById(R.id.btnSimpleTouchView);
    btnSimpleTouchView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, SimpleTouchActivity.class);
        startActivity(intent);
      }
    });
    Log.d(TAG, getString(R.string.app_name));
  }


  @Override
  protected void onPause() {
    super.onPause();
    Log.d(TAG, "-------onPause---------");
  }

  @Override
  protected void onRestart() {
    super.onRestart();
    Log.d(TAG, "-------onRestart---------");
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d(TAG, "-------onResume---------");
  }

  @Override
  protected void onStop() {
    super.onStop();
    Log.d(TAG, "-------onStop---------");
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.d(TAG, "-------onActivityResult [" + requestCode + "]---------");
    Log.d(TAG, "-------onActivityResult [" + resultCode + "]---------");
    if (resultCode != RESULT_CANCELED) {
      Log.d(TAG,
            "-------onActivityResult [" + data.getStringExtra("name") + "]---------");
    }
    super.onActivityResult(requestCode, resultCode, data);
  }
}

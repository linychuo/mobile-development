package lyc.lesson1.model;

/**
 * Created by ivan on 14-9-17.
 */
public class Person {

  private String firstName;
  private String lastName;


  public Person(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }
}

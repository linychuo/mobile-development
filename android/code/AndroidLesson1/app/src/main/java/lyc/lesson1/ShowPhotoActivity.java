package lyc.lesson1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import lyc.app.R;

/**
 * Created by ivan on 14-9-23.
 */
public class ShowPhotoActivity extends Activity {

  private static final String TAG = "ShowPhotoActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_showphoto);

    ImageView sharedImageView = (ImageView) findViewById(R.id.sharedImage);

    Intent intent = getIntent();
    if (intent != null) {
      Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
      Log.d(TAG, imageUri.getEncodedPath());
      sharedImageView.setImageURI(imageUri);
    }
  }
}

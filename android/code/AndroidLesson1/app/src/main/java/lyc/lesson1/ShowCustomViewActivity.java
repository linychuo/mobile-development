package lyc.lesson1;

import android.app.Activity;
import android.os.Bundle;

import lyc.app.R;

/**
 * Created by ivan on 14-9-18.
 */
public class ShowCustomViewActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.myview_test);
  }
}

package lyc.lesson1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.AlarmClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

import lyc.app.R;

/**
 * Created by ivan on 14-9-11.
 */
public class OtherActivity extends Activity {

  private static final String TAG = "OtherActivity";
  private Chronometer chronometer;
  private String name;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "========== OtherActivity onCreate========");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_other);

    Intent intent = getIntent();
    if (intent != null) {
      name = intent.getStringExtra("name");
      Log.d(TAG, name);
    }

    Button btnStart = (Button) findViewById(R.id.btnStart);
    Button btnStop = (Button) findViewById(R.id.btnStop);
    Button btnReset = (Button) findViewById(R.id.btnReset);
    Button btnSetAlarm = (Button) findViewById(R.id.btnSetAlarm);

    chronometer = (Chronometer) findViewById(R.id.chronometer);
    chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
      @Override
      public void onChronometerTick(Chronometer chronometer) {

      }
    });

    btnStart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(TAG, "111111111111");
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
      }
    });

    btnStop.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(TAG, "22222222222");
        chronometer.stop();
      }
    });

    btnReset.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(TAG, "3333333333");
        chronometer.setBase(SystemClock.elapsedRealtime());
        Intent d = new Intent();
        d.putExtra("name", "my name is " + name);
        setResult(111, d);
        finish();
      }
    });

    btnSetAlarm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
            .putExtra(AlarmClock.EXTRA_MESSAGE, "hello")
            .putExtra(AlarmClock.EXTRA_HOUR, 14)
            .putExtra(AlarmClock.EXTRA_MINUTES, 20);
        if (intent.resolveActivity(getPackageManager()) != null) {
          startActivity(intent);
        }
      }
    });
  }

  @Override
  public void onBackPressed() {
    Log.d(TAG, "---------onBackPressed-----------");
    Intent d = new Intent();
    d.putExtra("name", "my name is " + name);
    setResult(111, d);
    super.onBackPressed();
  }
}

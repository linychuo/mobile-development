package lyc.lesson1;

import android.app.Activity;
import android.os.Bundle;

import lyc.lesson1.customviews.SimpleTouchView;

/**
 * Created by ivan on 14-9-24.
 */
public class SimpleTouchActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(new SimpleTouchView(this));
  }
}

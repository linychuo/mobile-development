package com.shiwei.ssr;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by ivan on 3/7/14.
 */
public class MainActivity extends BaseActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    Button normalBtn = (Button) findViewById(R.id.normal_btn);
    normalBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        switchOther(NormalActivity.class);
      }
    });

    Button otherBtn = (Button) findViewById(R.id.other_btn);
    otherBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        switchOther(OtherActivity.class);
      }
    });

    Button asyncBtn = (Button) findViewById(R.id.async_btn);
    asyncBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        switchOther(SSActivity.class);
      }
    });
  }
}

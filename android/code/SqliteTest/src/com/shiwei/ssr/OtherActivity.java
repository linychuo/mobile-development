package com.shiwei.ssr;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.shiwei.ssr.adapter.TodoCursorAdapter;
import com.shiwei.ssr.db.TodoDBMetaData;
import com.shiwei.ssr.provider.TodoContentProvider;

/**
 * Created by ivan on 3/7/14.
 */
public class OtherActivity extends Activity {

  private static final String TAG = "OtherActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.normal);

    ListView taskListView = (ListView) findViewById(R.id.taskListView);
    Cursor
        cursor =
        managedQuery(TodoContentProvider.CONTENT_URI, null,
                     TodoDBMetaData.TodoColumns.STATUS + " = ?", new String[]{String.valueOf(
            Todo.TodoStatus.UNFINISHED.ordinal())},
                     TodoDBMetaData.TodoColumns.DEFAULT_ORDER);

    SimpleCursorAdapter
        adapter =
        new TodoCursorAdapter(this, R.layout.todo_list_item, cursor, new String[]{
            TodoDBMetaData.TodoColumns.TASK, TodoDBMetaData.TodoColumns.CREATED},
                              new int[]{R.id.taskTxtView, R.id.createdTxtView});
    taskListView.setAdapter(adapter);

    taskListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(final AdapterView<?> adapterView, final View view,
                                     final int position,
                                     long l) {
        Cursor c = (Cursor) adapterView.getItemAtPosition(position);
        Log.d(TAG,
              "task = " + c.getString(c.getColumnIndex(TodoDBMetaData.TodoColumns.TASK)) + ", id = "
              + c
                  .getInt(c.getColumnIndex(
                      TodoDBMetaData.TodoColumns.ID)));
        return false;
      }
    });
    cursor.close();
  }

}

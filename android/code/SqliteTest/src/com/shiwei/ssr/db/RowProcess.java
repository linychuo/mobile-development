package com.shiwei.ssr.db;

/**
 * Created by ivan on 3/6/14.
 */
public interface RowProcess<T> {

  T handler(CursorNewWrapper cursor);

}

package com.shiwei.ssr.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ivan on 3/5/14.
 */
public class TodoDBHelper extends SQLiteOpenHelper {

  public static final String DB_NAME = "todo.db";
  public static final int DB_VERSION = 2;
  private static final String TAG = "TodoDBHelper";
  public static TodoDBHelper instance = null;


  private TodoDBHelper(Context context) {
    super(context, DB_NAME, null, DB_VERSION);
  }

  public static synchronized TodoDBHelper getInstance(Context context) {
    if (instance == null) {
      Log.d(TAG, "--------only one------------");
      instance = new TodoDBHelper(context);
    }
    return instance;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    Log.d(TAG, "------------- create table ----------");
    db.execSQL("create table " + TodoDBMetaData.Tables.TODO + " ("
               + TodoDBMetaData.TodoColumns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
               + TodoDBMetaData.TodoColumns.TASK + " TEXT NOT NULL,"
               + TodoDBMetaData.TodoColumns.STATUS + " INTEGER NOT NULL,"
               + TodoDBMetaData.TodoColumns.CREATED + " INTEGER NOT NULL,"
               + TodoDBMetaData.TodoColumns.MODIFIED + " INTEGER NOT NULL"
               + ");");
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    Log.d(TAG, "upgrading from version " + oldVersion + " to  " + newVersion);
    db.execSQL("DROP TABLE IF EXISTS " + TodoDBMetaData.Tables.TODO);
    onCreate(db);
  }

  public SQLiteDatabase getDB(boolean writable) {
    return writable ? getWritableDatabase() : getReadableDatabase();
  }


  public int insert(String tableName, ContentValues values) {
    SQLiteDatabase sd = null;
    int rowId = 0;
    try {
      sd = getDB(true);
      rowId = (int) sd.insert(tableName, null, values);
    } finally {
      if (sd != null) {
        sd.close();
      }
    }
    return rowId;
  }

  public int update(String tableName, ContentValues values, String where) {
    SQLiteDatabase sd = null;
    int result = 0;
    try {
      sd = getDB(true);
      result = sd.update(tableName, values, where, null);
    } finally {
      if (sd != null) {
        sd.close();
      }
    }
    return result;
  }

  public <T> List<T> queryAll(String sql, String[] selectionArgs, RowProcess<T> process) {
    SQLiteDatabase sd = null;
    Cursor cursor = null;
    List<T> result = new ArrayList<>();
    try {
      sd = getDB(false);
      cursor = sd.rawQuery(sql, selectionArgs);
      CursorNewWrapper cursorNewWrapper = new CursorNewWrapper(cursor);
      while (cursor.moveToNext()) {
        result.add(process.handler(cursorNewWrapper));
      }
    } finally {
      if (cursor != null) {
        cursor.close();
      }
      if (sd != null) {
        sd.close();
      }
    }
    return result;
  }
}

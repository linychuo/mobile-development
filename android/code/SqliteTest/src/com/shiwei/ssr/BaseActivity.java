package com.shiwei.ssr;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by ivan on 3/7/14.
 */
public abstract class BaseActivity extends Activity {

  protected void switchOther(Class<?> other) {
    Intent intent = new Intent(this, other);
    startActivity(intent);
  }
}

package com.shiwei.ssr.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.shiwei.ssr.R;
import com.shiwei.ssr.db.TodoDBMetaData;

import java.util.Date;

/**
 * Created by ivan on 3/7/14.
 */
public class TodoCursorAdapter extends SimpleCursorAdapter {

  public TodoCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
    super(context, layout, c, from, to);
  }

  private String getDate(long datetime) {
    return DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date(datetime)).toString();
  }

  @Override
  public void bindView(View view, Context context, Cursor cursor) {
    super.bindView(view, context, cursor);
    TextView createdView = (TextView) view.findViewById(R.id.createdTxtView);

    createdView.setText(getDate(cursor.getLong(
        cursor.getColumnIndex(TodoDBMetaData.TodoColumns.CREATED))));
  }

}

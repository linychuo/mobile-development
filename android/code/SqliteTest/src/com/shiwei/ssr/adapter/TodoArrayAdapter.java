package com.shiwei.ssr.adapter;

import android.app.Activity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shiwei.ssr.R;
import com.shiwei.ssr.Todo;

import java.util.Date;
import java.util.List;

/**
 * Created by ivan on 3/6/14.
 */
public class TodoArrayAdapter extends ArrayAdapter<Todo> {

  private Activity ctx;

  private static class ViewHolder {

    TextView taskTxtView;
    TextView createdView;
  }

  public TodoArrayAdapter(Activity context,
                          List<Todo> objects) {
    super(context, R.layout.todo_list_item, objects);
    this.ctx = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    if (rowView == null) {
      LayoutInflater inflater = ctx.getLayoutInflater();
      rowView = inflater.inflate(R.layout.todo_list_item, null);
      ViewHolder holder = new ViewHolder();
      holder.taskTxtView = (TextView) rowView.findViewById(R.id.taskTxtView);
      holder.createdView = (TextView) rowView.findViewById(R.id.createdTxtView);
      rowView.setTag(holder);
    }

    ViewHolder holder = (ViewHolder) rowView.getTag();
    Todo item = getItem(position);
    holder.taskTxtView.setText(item.getTask());
    holder.createdView.setText(getDate(item.getCreated()));
    return rowView;
  }

  private String getDate(long datetime) {
    return DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date(datetime)).toString();
  }
}

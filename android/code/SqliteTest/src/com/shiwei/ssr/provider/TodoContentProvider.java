package com.shiwei.ssr.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.shiwei.ssr.db.TodoDBHelper;
import com.shiwei.ssr.db.TodoDBMetaData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ivan on 3/5/14.
 */
public class TodoContentProvider extends ContentProvider {

  private static final String TAG = "TodoContentProvider";
  private static final int INCOMING_TODO_COLLECTION_URI_INDICATOR = 1;
  private static final int INCOMING_TODO_URI_INDICATOR = 2;

  public static final String AUTHORITY = "com.shiwei.ssr.provider.TodoContentProvider";
  public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/todo");
  public static final String CONTENT_TYPE = "vnd.android.cursor.dir/com.shiwei.ssr.todo";
  public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/com.shiwei.ssr.todo";

  private static Map<String, String> todoProjectionMap;
  private static final UriMatcher uriMatcher;
  private TodoDBHelper dbHelper;

  static {
    todoProjectionMap = new HashMap<>();
    todoProjectionMap.put(TodoDBMetaData.TodoColumns.ID, TodoDBMetaData.TodoColumns.ID);
    todoProjectionMap
        .put(TodoDBMetaData.TodoColumns.TASK, TodoDBMetaData.TodoColumns.TASK);
    todoProjectionMap
        .put(TodoDBMetaData.TodoColumns.STATUS, TodoDBMetaData.TodoColumns.STATUS);
    todoProjectionMap
        .put(TodoDBMetaData.TodoColumns.CREATED, TodoDBMetaData.TodoColumns.CREATED);
    todoProjectionMap
        .put(TodoDBMetaData.TodoColumns.MODIFIED, TodoDBMetaData.TodoColumns.MODIFIED);

    uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    uriMatcher.addURI(AUTHORITY, "todo", INCOMING_TODO_COLLECTION_URI_INDICATOR);
    uriMatcher.addURI(AUTHORITY, "todo/#", INCOMING_TODO_URI_INDICATOR);
  }

  @Override
  public boolean onCreate() {
    dbHelper = TodoDBHelper.getInstance(getContext());
    return true;
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                      String sortOrder) {
    SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
    switch (uriMatcher.match(uri)) {
      case INCOMING_TODO_COLLECTION_URI_INDICATOR:
        qb.setTables(TodoDBMetaData.Tables.TODO);
        qb.setProjectionMap(todoProjectionMap);
        break;
      case INCOMING_TODO_URI_INDICATOR:
        qb.setTables(TodoDBMetaData.Tables.TODO);
        qb.setProjectionMap(todoProjectionMap);
        qb.appendWhere(TodoDBMetaData.TodoColumns.ID + " = " + uri.getPathSegments().get(1));
        break;
      default:
        throw new IllegalArgumentException("UnKnow URI " + uri);
    }

    String orderBy = TodoDBMetaData.TodoColumns.DEFAULT_ORDER;
    if (!TextUtils.isEmpty(sortOrder)) {
      orderBy = sortOrder;
    }

    SQLiteDatabase db = dbHelper.getDB(false);
    Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);
    int count = cursor.getCount();
    cursor.setNotificationUri(getContext().getContentResolver(), uri);
    return cursor;
  }

  @Override
  public String getType(Uri uri) {
    switch (uriMatcher.match(uri)) {
      case INCOMING_TODO_URI_INDICATOR:
        return CONTENT_ITEM_TYPE;
      case INCOMING_TODO_COLLECTION_URI_INDICATOR:
        return CONTENT_TYPE;
      default:
        throw new IllegalArgumentException("UnKnow URI " + uri);
    }
  }

  @Override
  public Uri insert(Uri uri, ContentValues values) {
    if (uriMatcher.match(uri) != INCOMING_TODO_COLLECTION_URI_INDICATOR) {
      throw new IllegalArgumentException("UnKnow URI " + uri);
    }
    if (values == null) {
      throw new IllegalArgumentException("Failed insert rwo because not data were provided " + uri);
    }

    if (!values.containsKey(TodoDBMetaData.TodoColumns.TASK)) {
      throw new IllegalArgumentException("Failed to insert row because TODO task is needed " + uri);
    }

    ContentValues contentValues = new ContentValues(values);
    if (!contentValues.containsKey(TodoDBMetaData.TodoColumns.CREATED)) {
      contentValues.put(TodoDBMetaData.TodoColumns.CREATED, System.currentTimeMillis());
    }

    if (!contentValues.containsKey(TodoDBMetaData.TodoColumns.MODIFIED)) {
      contentValues.put(TodoDBMetaData.TodoColumns.MODIFIED, System.currentTimeMillis());
    }

    SQLiteDatabase db = dbHelper.getDB(true);
    long
        rowId =
        db.insert(TodoDBMetaData.Tables.TODO, TodoDBMetaData.TodoColumns.TASK, contentValues);
    if (rowId > 0) {
      Uri insertedTodoUri = ContentUris.withAppendedId(CONTENT_URI, rowId);
      getContext().getContentResolver().notifyChange(insertedTodoUri, null);
      return insertedTodoUri;
    }
    return null;
  }

  @Override
  public int delete(Uri uri, String where, String[] whereArgs) {
    SQLiteDatabase db = dbHelper.getDB(true);
    int count;
    switch (uriMatcher.match(uri)) {
      case INCOMING_TODO_COLLECTION_URI_INDICATOR:
        count = db.delete(TodoDBMetaData.Tables.TODO, where, whereArgs);
        break;
      case INCOMING_TODO_URI_INDICATOR:
        String rowId = uri.getPathSegments().get(1);
        where = !TextUtils.isEmpty(where) ? " AND (" + where + ")" : "";
        count =
            db.delete(TodoDBMetaData.Tables.TODO,
                      TodoDBMetaData.TodoColumns.ID + "=" + rowId + where, whereArgs);
        break;
      default:
        count = 0;
        break;
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return count;
  }

  @Override
  public int update(Uri uri, ContentValues contentValues, String where, String[] whereArgs) {
    SQLiteDatabase db = dbHelper.getDB(true);
    int count;
    switch (uriMatcher.match(uri)) {
      case INCOMING_TODO_COLLECTION_URI_INDICATOR:
        count = db.update(TodoDBMetaData.Tables.TODO, contentValues, where, whereArgs);
        break;
      case INCOMING_TODO_URI_INDICATOR:
        String rowId = uri.getPathSegments().get(1);
        where = !TextUtils.isEmpty(where) ? " AND (" + where + ")" : "";
        count =
            db.update(TodoDBMetaData.Tables.TODO, contentValues,
                      TodoDBMetaData.TodoColumns.ID + " = " + rowId + where, whereArgs);
        break;
      default:
        count = 0;
        break;
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return count;
  }
}

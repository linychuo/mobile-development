package com.shiwei.ssr;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

/**
 * Created by ivan on 3/26/14.
 */
public class ListViewItemClickDialog extends DialogFragment {

  public static ListViewItemClickDialog newInstance(int position) {
    ListViewItemClickDialog dialog = new ListViewItemClickDialog();
    Bundle args = new Bundle();
    args.putInt("position", position);
    dialog.setArguments(args);
    return dialog;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    Log.d("dddd", "------------");
    final int position = getArguments().getInt("position");
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setMessage(R.string.alert_msg);
    builder.setNegativeButton(R.string.cancel_btn, null);
    builder.setPositiveButton(R.string.ok_btn, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ((DialogListener) getActivity()).onItemClick(position);
      }
    });
    return builder.create();
  }

  public interface DialogListener {

    void onItemClick(int position);
  }
}

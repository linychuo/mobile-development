package com.shiwei.ssr;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

import com.shiwei.ssr.adapter.TodoCursorAdapter;
import com.shiwei.ssr.db.TodoDBMetaData;
import com.shiwei.ssr.provider.TodoContentProvider;

/**
 * Created by ivan on 3/7/14.
 */
public class SSActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {

  private TodoCursorAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.normal);

    ListView taskListView = (ListView) findViewById(R.id.taskListView);
    adapter =
        new TodoCursorAdapter(this, R.layout.todo_list_item, null, new String[]{
            TodoDBMetaData.TodoColumns.TASK, TodoDBMetaData.TodoColumns.CREATED},
                              new int[]{R.id.taskTxtView, R.id.createdTxtView});
    taskListView.setAdapter(adapter);
    getLoaderManager().initLoader(0, null, this);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
    return new CursorLoader(this, TodoContentProvider.CONTENT_URI, null,
                            TodoDBMetaData.TodoColumns.STATUS + " = ?", new String[]{String.valueOf(
        Todo.TodoStatus.UNFINISHED.ordinal())}, TodoDBMetaData.TodoColumns.DEFAULT_ORDER);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
    adapter.swapCursor(cursor);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> cursorLoader) {
    adapter.swapCursor(null);
  }
}

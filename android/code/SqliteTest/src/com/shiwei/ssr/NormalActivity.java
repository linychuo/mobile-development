package com.shiwei.ssr;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.shiwei.ssr.adapter.TodoArrayAdapter;
import com.shiwei.ssr.db.TodoDao;

import java.util.List;

public class NormalActivity extends FragmentActivity
    implements ListViewItemClickDialog.DialogListener {

  private static final String TAG = "NormalActivity";
  private TodoDao dao;
  private List<Todo> todoList;
  private TodoArrayAdapter adapter;
  private ListView taskListView;

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.normal);
    dao = TodoDao.getInstance(this);

    todoList = dao.getAllTodoByStatus(Todo.TodoStatus.UNFINISHED);
    adapter = new TodoArrayAdapter(this, todoList);
    taskListView = (ListView) findViewById(R.id.taskListView);
    taskListView.setAdapter(adapter);
    taskListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(final AdapterView<?> adapterView, final View view,
                                     final int position,
                                     long l) {
        ListViewItemClickDialog
            dialog = ListViewItemClickDialog.newInstance(position);
        dialog.show(getSupportFragmentManager(), "test");
        return false;
      }
    });

    final EditText taskTxt = (EditText) findViewById(R.id.iptTxt);
    taskTxt.setOnKeyListener(new View.OnKeyListener() {
      @Override
      public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        String task = taskTxt.getText().toString();
        if (!TextUtils.isEmpty(task)) {
          if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            if ((keyCode == KeyEvent.KEYCODE_DPAD_CENTER) ||
                (keyCode == KeyEvent.KEYCODE_ENTER)) {
              Log.d(TAG, "task = " + task);
              Todo todo = new Todo();
              todo.setTask(task);
              todo.setStatus(Todo.TodoStatus.UNFINISHED);
              todo.setCreated(System.currentTimeMillis());
              todo.setModified(System.currentTimeMillis());
              int id = dao.saveTodo(todo);
              todo.setId(id);
              todoList.add(0, todo);
              adapter.notifyDataSetChanged();
              taskTxt.setText("");
              taskTxt.requestFocus();
            }
          }
        }

        return false;
      }
    });
  }

  @Override
  protected void onDestroy() {
    if (dao != null) {
      dao.release();
    }
    todoList = null;
    adapter = null;
    super.onDestroy();
  }


  @Override
  public void onItemClick(int position) {
    Todo todo = (Todo) taskListView.getItemAtPosition(position);
    Log.d(TAG, "todo = " + todo);
    todo.setStatus(Todo.TodoStatus.FINISHED);
    todo.setModified(System.currentTimeMillis());
    dao.updateTodo(todo);
    todoList.remove(position);
    adapter.notifyDataSetChanged();
  }
}

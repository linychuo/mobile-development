package com.example.customView;

import android.text.format.DateFormat;

import java.util.Date;

/**
 * Created by ivan on 3/2/14.
 */
public class ToDoItem {
    private String task;
    private Date created;

    ToDoItem(String task, Date created) {
        this.task = task;
        this.created = created;
    }

    ToDoItem(String task) {
        this(task, new Date());
    }

    public String getTask() {
        return task;
    }

    public String getCreated() {
        return DateFormat.format("yyyy-MM-dd", created).toString();
    }

    @Override
    public String toString() {
        return "(" + DateFormat.format("yyyy-MM-dd", created) + ")" + task;
    }
}

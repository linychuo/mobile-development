package com.example.customView;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MyActivity extends Activity {

    private ArrayList<ToDoItem> toDoItems;
    private ArrayAdapter<ToDoItem> adapter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final EditText myEditTxt = (EditText) findViewById(R.id.myEditText);
        final ListView myListView = (ListView) findViewById(R.id.myListView);

        toDoItems = new ArrayList<>();
//        adapter = new ArrayAdapter<>(this, R.layout.todolist_item, toDoItems);
        final MyAdapter adapter1 = new MyAdapter(this, R.layout.compound_layout, toDoItems);

        myListView.setAdapter(adapter1);
        myEditTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (i == keyEvent.KEYCODE_ENTER) {
                        toDoItems.add(0, new ToDoItem(myEditTxt.getText().toString()));
                        myEditTxt.setText("");
                        adapter1.notifyDataSetChanged();
                        myEditTxt.requestFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        myEditTxt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });
    }
}

package com.example.customView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ivan on 3/2/14.
 */
public class MyAdapter extends ArrayAdapter<ToDoItem> {

    private int textViewResourceId;

    public MyAdapter(Context context, int textViewResourceId, List<ToDoItem> objects) {
        super(context, textViewResourceId, objects);
        this.textViewResourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ToDoItem item = getItem(position);

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(this.textViewResourceId, null);
        }

        TextView task = (TextView) rowView.findViewById(R.id.task);
        task.setText(item.getTask());
        TextView created = (TextView) rowView.findViewById(R.id.created);
        created.setText(item.getCreated());

        return rowView;
    }

}

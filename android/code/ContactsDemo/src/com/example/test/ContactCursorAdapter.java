package com.example.test;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by ivan on 3/9/14.
 */
public class ContactCursorAdapter extends SimpleCursorAdapter {

  private static final String TAG = "ContactCursorAdapter";

  public ContactCursorAdapter(Context context, int layout) {
    super(context, layout, null, new String[]{ContactsContract.Contacts.DISPLAY_NAME,
                                              ContactsContract.Contacts.PHOTO_ID}, null);
  }

  @Override
  public void bindView(View view, Context context, Cursor cursor) {
    ImageView avatarImageView = (ImageView) view.findViewById(R.id.avatar);
    TextView nameTextView = (TextView) view.findViewById(R.id.name);

    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
    String
        photoId =
        cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_ID));

    if (photoId != null) {
      Uri uri = Uri.withAppendedPath(ContactsContract.Data.CONTENT_URI, photoId);
      avatarImageView.setImageURI(uri);
    } else {
      avatarImageView.setImageResource(R.drawable.avatar);
    }

    nameTextView.setText(name);
  }

}

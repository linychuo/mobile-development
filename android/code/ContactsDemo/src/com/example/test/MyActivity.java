package com.example.test;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MyActivity extends FragmentActivity
    implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener,
               AbsListView.OnScrollListener {

  private static final String TAG = "MyActivity";

  private SimpleCursorAdapter mAdapter;
  private int currentVisibleItemCount;
  private int hasVisibleItemCount;
  private int currentScrollState;
  private int totalItemCount;

  private ListView listview;

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "-----onCreate---------");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    listview = (ListView) findViewById(R.id.contact_list);
    listview.setOnItemClickListener(this);
    listview.setOnScrollListener(this);

    mAdapter =
        new ContactCursorAdapter(this, R.layout.row);
    listview.setAdapter(mAdapter);
    getSupportLoaderManager().initLoader(0, null, this);
  }

  @Override
  protected void onResume() {
    Log.d(TAG, "-----onResume---------");
    super.onResume();
    getSupportLoaderManager().restartLoader(0, null, this);
  }

  @Override
  protected void onDestroy() {
    Log.d(TAG, "-----------onDestroy-------------------");
    super.onDestroy();
    this.listview = null;
    this.mAdapter = null;
    getSupportLoaderManager().destroyLoader(0);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
    return new CursorLoader(this, ContactsContract.Contacts.CONTENT_URI,
                            null,
                            ContactsContract.Contacts.HAS_PHONE_NUMBER + " = " + 1,
                            null,
                            ContactsContract.Contacts.SORT_KEY_PRIMARY);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
    mAdapter.changeCursor(cursor);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> cursorLoader) {
    mAdapter.changeCursor(null);
  }

  @Override
  public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    Cursor cursor = (Cursor) adapterView.getItemAtPosition(i);
    Log.d("MyActivity", "id = " + cursor.getInt(cursor.getColumnIndex(ContactsContract.Data._ID)));

//
//    Intent
//        intent =
//        new Intent(Intent.ACTION_PICK,
//                   Uri.parse(ContactsContract.CommonDataKinds.Phone.CONTENT_URI + "/" + cursor
//                       .getInt(cursor.getColumnIndex(ContactsContract.Data._ID))));
//
//    startActivity(intent);
  }

  @Override
  public void onScrollStateChanged(AbsListView absListView, int scrollState) {
    this.currentScrollState = scrollState;
    this.isScrollCompleted();
  }

  @Override
  public void onScroll(AbsListView absListView, int hasVisibleItemCount, int visibleItemCount,
                       int totalItemCount) {
    this.currentVisibleItemCount = visibleItemCount;
    this.hasVisibleItemCount = hasVisibleItemCount;
    this.totalItemCount = totalItemCount;
  }

  private void isScrollCompleted() {
    if (this.currentVisibleItemCount + this.hasVisibleItemCount == this.totalItemCount
        && this.currentScrollState == SCROLL_STATE_IDLE) {
      Toast.makeText(this, "Total item count = " + this.totalItemCount, 2000).show();
    }
  }
}

package com.example.test;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

/**
 * Created by ivan on 3/17/14.
 */
public class LoaderManagerTest extends FragmentActivity
    implements LoaderManager.LoaderCallbacks<List<String>> {

  private static final String TAG = "LoaderManagerTest";
  private ArrayAdapter<String> adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.loader_manager_test);

    ListView resultListView = (ListView) findViewById(R.id.resultListView);
    adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1);
    resultListView.setAdapter(adapter);
    getSupportLoaderManager().initLoader(0, null, this);
  }

  @Override
  public Loader<List<String>> onCreateLoader(int i, Bundle bundle) {
    Log.d(TAG, "------------------");
    return new ListLoader(this);
  }

  @Override
  public void onLoadFinished(Loader<List<String>> stringLoader, List<String> result) {
    Log.d(TAG, "======================");
    adapter.clear();
    for (String s : result) {
      adapter.add(s);
    }
  }

  @Override
  public void onLoaderReset(Loader<List<String>> stringLoader) {
    Log.d(TAG, "??????????????");
    adapter.clear();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    getSupportLoaderManager().destroyLoader(0);
  }
}

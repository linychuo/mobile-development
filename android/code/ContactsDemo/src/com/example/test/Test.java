package com.example.test;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;

/**
 * Created by ivan on 3/17/14.
 */
public class Test extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Cursor cursor = getContentResolver()
        .query(ContactsContract.Contacts.CONTENT_URI, null,
               ContactsContract.Contacts.HAS_PHONE_NUMBER + " = " + 1, null, null);

    try {
      while (cursor.moveToNext()) {
        String name = cursor
            .getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        String
            photoThumbnail =
            cursor.getString(
                cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));

        Cursor cursor1 = getContentResolver()
            .query(ContactsContract.Data.CONTENT_URI, null,
                   ContactsContract.Data.CONTACT_ID + " = " + cursor.getString(
                       cursor.getColumnIndex(ContactsContract.Contacts._ID)) + " AND "
                   + ContactsContract.Data.MIMETYPE + "='"
                   + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'",
                   null,
                   null);
        while (cursor1.moveToNext()) {
          Log.d("Test",
                name + ", " + photoThumbnail + ", " + cursor1
                    .getString(cursor1.getColumnIndex(ContactsContract.Data.DATA1)));
        }
        cursor1.close();
      }
    } finally {
      cursor.close();
    }
  }
}

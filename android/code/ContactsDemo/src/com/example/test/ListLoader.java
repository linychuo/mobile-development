package com.example.test;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ivan on 3/17/14.
 */
public class ListLoader extends AsyncTaskLoader<List<String>> {

//  private List<String> mData;

  public ListLoader(Context context) {
    super(context);
  }

  @Override
  public List<String> loadInBackground() {
    Log.d("ListLoader", "++++++++++++++++++");
    List<String> result = new ArrayList<String>(10);

    try {
      Thread.currentThread().sleep(4000);
    } catch (InterruptedException e) {

    }
    for (int j = 1; j <= 10; j++) {
      result.add(String.valueOf(j));
    }

    return result;
  }

  @Override
  protected void onStartLoading() {
//    if (mData != null) {
//      deliverResult(mData);
//    }
//    if (takeContentChanged() || mData == null || mData.size() == 0) {
//      forceLoad();
//    }
    forceLoad();
  }

  @Override
  protected void onStopLoading() {
    cancelLoad();
  }

  @Override
  protected void onReset() {
    super.onReset();
    onStopLoading();
//    if (mData != null && mData.size() > 0) {
//      mData.clear();
//    }
//    mData = null;
  }

//  @Override
//  public void deliverResult(List<String> data) {
//    if (isReset()) {
//      data.clear();
//      return;
//    }
//
//    List<String> oldData = mData;
//    mData = data;
//
//    if (isStarted()) {
//      super.deliverResult(data);
//    }
//
//    if (oldData != null && oldData != data && oldData.size() > 0) {
//      oldData.clear();
//    }
//  }
}

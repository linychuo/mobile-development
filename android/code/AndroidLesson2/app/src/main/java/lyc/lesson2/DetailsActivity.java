package lyc.lesson2;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;

import lyc.lesson2.fragment.DetailsFragment;

/**
 * Created by ivan on 14-10-20.
 */
public class DetailsActivity extends ActionBarActivity {

  private static final String TAG = "DetailsActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ActionBar actionBar = getSupportActionBar();
    actionBar.setTitle("Second");
    actionBar.setSubtitle("two");
//    actionBar.setHomeButtonEnabled(true);
    actionBar.setDisplayHomeAsUpEnabled(true);

    if (getIntent() != null) {
      DetailsFragment detailsFragment = new DetailsFragment();
      Bundle bundle = new Bundle();
      bundle.putString("desc", getIntent().getStringExtra("desc"));
      detailsFragment.setArguments(bundle);
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.add(android.R.id.content, detailsFragment);
      ft.commit();
    }
  }

//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    switch (item.getItemId()) {
//      case android.R.id.home:
//        Log.d(TAG, "=============");
//        finish();
//        return true;
//      default:
//        return super.onOptionsItemSelected(item);
//    }
//  }
}

package lyc.lesson2.db;

import android.provider.BaseColumns;

/**
 * Created by ivan on 3/6/14.
 */
public interface TodoDBMetaData {

  public interface Tables {

    String TODO = "todo";
  }

  public interface TodoColumns {

    //integer type, auto increment
    String ID = BaseColumns._ID;
    //string type
    String TASK = "task";
    //integer type, task status: 0,unfinished, 1,finished
    String STATUS = "status";
    //integer type
    String CREATED = "created";
    //integer type
    String MODIFIED = "modified";
    //default order by created desc
    String DEFAULT_ORDER = CREATED + " DESC";
  }
}

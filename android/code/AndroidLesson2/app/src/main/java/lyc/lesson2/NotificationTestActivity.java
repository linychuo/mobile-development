package lyc.lesson2;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by ivan on 14-10-16.
 */
public class NotificationTestActivity extends ActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_notification_test);

    ActionBar actionBar = getSupportActionBar();
    actionBar.setSubtitle("mytest");
    actionBar.setTitle("vogella.com");

    Button firstBtn = (Button) findViewById(R.id.firstBtn);
    Button secondBtn = (Button) findViewById(R.id.secondBtn);

    firstBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(NotificationTestActivity.this, MyOtherService.class);
        intent.putExtra("name", "testtttttt");
        startService(intent);

//        NotificationCompat.Builder mBuilder =
//            new NotificationCompat.Builder(NotificationTestActivity.this)
//                .setSmallIcon(R.drawable.ic_launcher)
////                .setContentText("Hello World!")
//                .setAutoCancel(true);
//        RemoteViews remoteViews = new RemoteViews("lyc.lesson2", R.layout.custome_notification);
//        remoteViews.setProgressBar(R.id.pppp, 100, 50, false);
//        mBuilder.setContent(remoteViews);
//
//        Intent intent = new Intent(NotificationTestActivity.this, MainActivity.class);
//        intent.putExtra("greeting", "hello");
//        PendingIntent
//            pendingIntent =
//            PendingIntent.getActivity(NotificationTestActivity.this, 1, intent,
//                                      PendingIntent.FLAG_UPDATE_CURRENT);
//        mBuilder.setContentIntent(pendingIntent);
//
//        NotificationManager
//            notificationManager =
//            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(1, mBuilder.build());
      }
    });

    secondBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(NotificationTestActivity.this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("My notification")
                .setContentText("Hello World!")
                .setAutoCancel(true);
        mBuilder.setProgress(100, 50, false);
        Intent intent = new Intent(NotificationTestActivity.this, MainActivity.class);
        intent.putExtra("greeting", "hello");
        PendingIntent
            pendingIntent =
            PendingIntent.getActivity(NotificationTestActivity.this, 1, intent,
                                      PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setNumber(2);

        NotificationManager
            notificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, mBuilder.build());
      }
    });

  }
}

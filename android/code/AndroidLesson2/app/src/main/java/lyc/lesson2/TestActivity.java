package lyc.lesson2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ArrayAdapter;

import lyc.lesson2.fragment.DetailsFragment;

/**
 * Created by ivan on 14-10-20.
 */
public class TestActivity extends ActionBarActivity {


  private static final String TAG = "TestActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.test);
    ActionBar actionBar = getSupportActionBar();
    actionBar.setTitle("First");
    actionBar.setSubtitle("one");
//    actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.dac_logo));
//    actionBar.setCustomView(R.layout.aa);
//    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

    final ArrayAdapter<String>
        adapter =
        new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                                 android.R.id.text1,
                                 new String[]{"java", "c#", "php", "c", "python"});
    actionBar.setListNavigationCallbacks(adapter, new ActionBar.OnNavigationListener() {
      @Override
      public boolean onNavigationItemSelected(int i, long l) {
        Log.d(TAG, adapter.getItem(i));
        return false;
      }
    });

//    AlarmManager alarmManager =
//        (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//    int alarmType = AlarmManager.ELAPSED_REALTIME_WAKEUP;
//    long timeOrLengthOfWait = SystemClock.elapsedRealtime() + 10000;
//    String ALARM_ACTION = "ALARM_ACTION";
//    Intent intentToFire = new Intent(ALARM_ACTION);
//    PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0,
//                                                           intentToFire, 0);
//    alarmManager.setInexactRepeating(alarmType, 1000,
//                                     5 * 1000,
//                                     alarmIntent);

  }

  public void showDetail(String item) {
    DetailsFragment fragment = (DetailsFragment) getSupportFragmentManager()
        .findFragmentById(R.id.right_detail);

    if (fragment != null && fragment.isInLayout()) {
      //landspace
      fragment.showDetail(item);
    } else {
      //startActivity
      Intent intent = new Intent(this, DetailsActivity.class);
      intent.putExtra("desc", item);
      startActivity(intent);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.test, menu);
    return super.onCreateOptionsMenu(menu);
  }
}

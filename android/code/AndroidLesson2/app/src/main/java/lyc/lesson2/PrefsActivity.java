package lyc.lesson2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;


/**
 * Created by ivan on 14-10-11.
 */
public class PrefsActivity extends PreferenceActivity implements
                                                      SharedPreferences.OnSharedPreferenceChangeListener {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.my);
    Preference connectionPref = findPreference("keyVibrate");

    boolean
        result =
        getPreferenceManager().getSharedPreferences()
            .getBoolean("keyVibrate", false);
    connectionPref.setSummary(result ? "震动打开" : "震动关闭");
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    if (key.equals("keyVibrate")) {
      Preference connectionPref = findPreference(key);
      boolean result = sharedPreferences.getBoolean("keyVibrate", false);
      if (result) {
        Toast.makeText(this, "震动打开", Toast.LENGTH_SHORT).show();
        connectionPref.setSummary("震动打开");
      } else {
        Toast.makeText(this, "震动关闭", Toast.LENGTH_SHORT).show();
        connectionPref.setSummary("震动关闭");
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    getPreferenceScreen().getSharedPreferences()
        .registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  protected void onPause() {
    super.onPause();
    getPreferenceScreen().getSharedPreferences()
        .unregisterOnSharedPreferenceChangeListener(this);
  }
}

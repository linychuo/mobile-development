package lyc.lesson2.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import lyc.lesson2.R;

/**
 * Created by ivan on 14-10-20.
 */
public class DetailsFragment extends Fragment {

  private TextView textView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.b_fragment, container, false);
    textView = (TextView) view.findViewById(R.id.descTxtView);
    if (getArguments() != null) {
      textView.setText(getArguments().getString("desc"));
    } else {
      textView.setText("java");
    }

    return view;
  }

  public void showDetail(String desc) {
    textView.setText(desc);
  }
}

package lyc.lesson2.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import lyc.lesson2.R;
import lyc.lesson2.TestActivity;

/**
 * Created by ivan on 14-10-20.
 */
public class MyListFragment extends Fragment {

  private static final String TAG = "MyListFragment";
  private TestActivity activity;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    Log.d(TAG, "-------onAttach-------");
    this.activity = (TestActivity) activity;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d(TAG, "------onCreate-------");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    Log.d(TAG, "--------onCreateView---------");
    ListView listView = (ListView) inflater.inflate(R.layout.a_fragment, container, false);
    final ArrayAdapter<String>
        adapter =
        new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                                 android.R.id.text1,
                                 new String[]{"java", "c#", "php", "c", "python"});
    listView.setAdapter(adapter);
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //
        activity.showDetail(adapter.getItem(position));
      }
    });
    return listView;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    Log.d(TAG, "-----------onActivityCreated-------------");
    super.onActivityCreated(savedInstanceState);
  }

  @Override
  public void onStart() {
    super.onStart();
    Log.d(TAG, "----------------onStart----------------");
  }

  @Override
  public void onResume() {
    super.onResume();
    Log.d(TAG, "-------------------onResume-------------------");
  }

  @Override
  public void onPause() {
    super.onPause();
    Log.d(TAG, "------------------------onPause----------------------");
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Log.d(TAG, "------------------------onSaveInstanceState----------------------");
  }

  @Override
  public void onStop() {
    super.onStop();
    Log.d(TAG, "------------------------onStop----------------------");
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    Log.d(TAG, "--------------onDestroyView-----------------");
  }


  @Override
  public void onDetach() {
    super.onDetach();
    Log.d(TAG, "--------------onDetach------------------");
  }
}

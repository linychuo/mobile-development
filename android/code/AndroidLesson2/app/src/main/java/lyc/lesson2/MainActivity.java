package lyc.lesson2;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import lyc.lesson2.db.TodoDao;
import lyc.lesson2.model.Todo;

/**
 * Created by ivan on 14-9-24.
 */
public class MainActivity extends Activity {

  private static final String TAG = "MainActivity";
  private TodoDao dao;
  private MyService serviceRef;
  private ProgressBar progressBar2;
  private TextView numTxtView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Intent intent = getIntent();
    if (intent != null) {
      Log.d(TAG, ">>>>> " + intent.getStringExtra("greeting"));
      NotificationManager
          notificationManager =
          (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      notificationManager.cancel(1);
    }

//    final SharedPreferences myprefs = getSharedPreferences(", MODE_PRIVATE);
    final SharedPreferences myprefs = PreferenceManager.getDefaultSharedPreferences(this);
    dao = TodoDao.getInstance(this);
    Button btnSendBrd = (Button) findViewById(R.id.btnSendBrd);
    btnSendBrd.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d("ddd", ">>>>>>>>>>>>>");
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(MainActivity.this);
        Intent intent = new Intent("ACTION_TEST_BROADCAST");
        intent.putExtra("message", "hello");
        lbm.registerReceiver(new TestReceiver(),
                             new IntentFilter("ACTION_TEST_BROADCAST"));
        lbm.sendBroadcastSync(intent);
      }
    });

    Button btnSaveTodo = (Button) findViewById(R.id.btnSaveTodo);
    btnSaveTodo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Todo todo = new Todo();
        todo.setTask("hello todo");
        todo.setStatus(Todo.TodoStatus.UNFINISHED);
        todo.setCreated(System.currentTimeMillis());
        todo.setModified(System.currentTimeMillis());
        int id = dao.saveTodo(todo);
        Log.d(TAG, "ID = " + id);
        List<Todo> todoList = dao.getAllTodoByStatus(Todo.TodoStatus.UNFINISHED);
        Log.d(TAG, "todoList size = " + todoList.size());
        for (int i = 0; i < todoList.size(); i++) {
          Log.d(TAG,
                "todoList[" + i + "] = " + todoList.get(i).toString());
        }
      }
    });

    Button btnSongList = (Button) findViewById(R.id.btnSongList);
    btnSongList.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Cursor cursor = getContentResolver()
            .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
//        for (String column : cursor.getColumnNames()) {
//          Log.d(TAG, column);
//        }
        while (cursor.moveToNext()) {
          Log.d(TAG,
                cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID)) + ", " +
                cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE))
                + ", "
                + cursor
                    .getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST)));
        }
      }
    });

    Button btnContact = (Button) findViewById(R.id.btnContact);
    btnContact.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Cursor
            cursor =
            getContentResolver()
                .query(ContactsContract.Contacts.CONTENT_URI, null,
                       null, null, null);
//        for (String column : cursor.getColumnNames()) {
//          Log.d(TAG, column);
//        }

        while (cursor.moveToNext()) {
          String name = cursor
              .getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
          Cursor cursor1 = getContentResolver()
              .query(ContactsContract.Data.CONTENT_URI, null,
                     ContactsContract.Data.CONTACT_ID + " = " + cursor.getString(
                         cursor.getColumnIndex(ContactsContract.Contacts._ID)) + " AND "
                     + ContactsContract.Data.MIMETYPE + "='"
                     + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'",
                     null,
                     null);
          while (cursor1.moveToNext()) {
            Log.d("Test",
                  name + ", " + cursor1
                      .getString(cursor1.getColumnIndex(ContactsContract.Data.DATA1)));
          }
          cursor1.close();
        }
      }
    });

    Button btnPrefs = (Button) findViewById(R.id.btnPrefs);
    btnPrefs.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Log.d(TAG, "keyAge = " + myprefs.getString("keyAge", ""));
        Log.d(TAG, "keyName = " + myprefs.getString("keyName", ""));
      }
    });

    Resources res = getResources();
    InputStream is = res.openRawResource(R.raw.a);
    Scanner scanner = new Scanner(is);
    while (scanner.hasNext()) {
      Log.d(TAG, scanner.nextLine());
    }

    Button btnCreateFile = (Button) findViewById(R.id.btnCreateFile);
    btnCreateFile.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        File f = new File(getDir("dd", 0), "aa.txt");
        try {
          FileOutputStream ff = new FileOutputStream(f);
          PrintStream pp = new PrintStream(ff);
          pp.println("a");
          pp.println("b");
          pp.println("c");
          pp.println("d");
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }

        String fileName = "bb.txt";
        try {
          FileOutputStream os = openFileOutput(fileName, MODE_PRIVATE);
          PrintStream ps = new PrintStream(os);
          for (int i = 0; i < 10; i++) {
            ps.println("id: " + i);
          }
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }

        String[] files = fileList();
        for (String file : files) {
          Log.d(TAG, file);
        }

//    Environment.getExternalStorageState()

        File f1 = new File(Environment.getExternalStorageDirectory(), "cc.txt");
        try {
          FileOutputStream ff1 = new FileOutputStream(f1);
          PrintStream pp1 = new PrintStream(ff1);
          pp1.println("aa");
          pp1.println("bb");
          pp1.println("cc");
          pp1.println("dd");
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }

        File f2 = new File(getExternalFilesDir(""), "dd.txt");
        try {
          FileOutputStream ff2 = new FileOutputStream(f2);
          PrintStream pp2 = new PrintStream(ff2);
          pp2.println("aaa");
          pp2.println("bbb");
          pp2.println("ccc");
          pp2.println("ddd");
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }
      }
    });

    numTxtView = (TextView) findViewById(R.id.numTxtView);
    final Handler handler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
        progressBar2.setProgress(msg.arg1);
//        numTxtView.setText(String.valueOf(msg.arg1));
      }
    };

    Button btnHandler = (Button) findViewById(R.id.btnHandler);
    btnHandler.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            for (int i = 1; i <= 100; i++) {
              Message msg = new Message();
              msg.arg1 = i;
              handler.sendMessage(msg);
              try {
                Thread.sleep(100);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
        }).start();
//        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(MainActivity.this);
//        lbm.registerReceiver(new TestReceiver(),
//                             new IntentFilter("MY_BROADCAST_RECEIVER"));
//
//        Intent intent = new Intent(MainActivity.this, MyOtherService.class);
//        intent.putExtra("name", "testtttttt");
//        Intent intent = new Intent("MY_SERVICE");
//        intent.putExtra("name", "testtttttt");
//        startService(intent);

//        bindService(intent, new ServiceConnection() {
//          @Override
//          public void onServiceConnected(ComponentName name, IBinder service) {
//            serviceRef = ((MyService.MyBinder) service).getService();
//            serviceRef.hello();
//          }
//
//          @Override
//          public void onServiceDisconnected(ComponentName name) {
//            serviceRef = null;
//          }
//        }, BIND_AUTO_CREATE);
      }
    });

    progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
    Button btnAsyncTask = (Button) findViewById(R.id.btnAsyncTask);
    btnAsyncTask.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        new MyAsyncTask().execute(100);
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menuSettings:
        Intent intent = new Intent(MainActivity.this, PrefsActivity.class);
        startActivity(intent);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }


  public class MyAsyncTask extends AsyncTask<Integer, Integer, Integer> {

    @Override
    protected Integer doInBackground(Integer... parameter) {
      // Moved to a background thread.
      int result = 0;
      // Perform background processing task, update myProgress]
      for (int i = 1; i <= parameter[0]; i++) {
        result += i;
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        publishProgress(i);
      }
      // Return the value to be passed to onPostExecute
      return result;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
      // Synchronized to UI thread.
      // Update progress bar, Notification, or other UI elements
      progressBar2.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Integer result) {
      // Synchronized to UI thread.
      // Report results via UI update, Dialog, or notifications
      numTxtView.setText(String.valueOf(result));
    }
  }
}

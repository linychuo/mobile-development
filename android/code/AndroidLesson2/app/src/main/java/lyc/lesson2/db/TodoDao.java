package lyc.lesson2.db;

import android.content.ContentValues;
import android.content.Context;

import java.util.List;

import lyc.lesson2.model.Todo;

/**
 * Created by ivan on 3/6/14.
 */
public class TodoDao {

  private static final String GET_ALL_UNFINISHED_TODO = "select * from todo where "
                                                        + TodoDBMetaData.TodoColumns.STATUS
                                                        + " = ? order by "
                                                        + TodoDBMetaData.TodoColumns.DEFAULT_ORDER;

  private static TodoDao instance = null;
  private final TodoDBHelper helper;

  private TodoDao(Context context) {
    helper = TodoDBHelper.getInstance(context);
  }

  public static TodoDao getInstance(Context context) {
    if (instance == null) {
      instance = new TodoDao(context);
    }

    return instance;
  }

  public int saveTodo(Todo todo) {
    return helper.insert(TodoDBMetaData.Tables.TODO, convertTo(todo));
  }

  public int updateTodo(Todo todo) {
    return helper.update(TodoDBMetaData.Tables.TODO, convertTo(todo),
                         TodoDBMetaData.TodoColumns.ID + " = " + todo.getId());
  }

  public List<Todo> getAllTodoByStatus(Todo.TodoStatus status) {
    return helper.queryAll(GET_ALL_UNFINISHED_TODO, new String[]{String.valueOf(status.ordinal())},
                           new RowProcess<Todo>() {
                             @Override
                             public Todo handler(CursorNewWrapper cursor) {
                               Todo todo = new Todo();
                               todo.setId(cursor.getInt(TodoDBMetaData.TodoColumns.ID));
                               todo.setTask(cursor.getString(TodoDBMetaData.TodoColumns.TASK));
                               todo.setStatus(
                                   cursor.getInt(TodoDBMetaData.TodoColumns.TASK) == 1
                                   ? Todo.TodoStatus.FINISHED
                                   : Todo.TodoStatus.UNFINISHED);
                               todo.setCreated(cursor.getLong(TodoDBMetaData.TodoColumns.CREATED));
                               todo.setModified(
                                   cursor.getLong(TodoDBMetaData.TodoColumns.MODIFIED));
                               return todo;
                             }
                           });
  }

  public void release() {
    helper.close();
  }

  public ContentValues convertTo(Todo todo) {
    ContentValues values = new ContentValues();
    values.put(TodoDBMetaData.TodoColumns.TASK, todo.getTask());
    values.put(TodoDBMetaData.TodoColumns.STATUS, todo.getStatus().ordinal());
    values.put(TodoDBMetaData.TodoColumns.CREATED, todo.getCreated());
    values.put(TodoDBMetaData.TodoColumns.MODIFIED, todo.getModified());
    return values;
  }

}

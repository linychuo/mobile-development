package lyc.lesson2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by ivan on 14-9-24.
 */
public class TestReceiver extends BroadcastReceiver {

  private static final String TAG = "TestReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.d(TAG, ">>>>>>" + intent.getStringExtra("message"));
    Toast.makeText(context, intent.getStringExtra("message"), Toast.LENGTH_LONG).show();
//    setResultCode(110);

  }
}

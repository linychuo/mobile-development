package lyc.lesson2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by ivan on 14-9-24.
 */
public class BootReceiver extends BroadcastReceiver {

  private static final String TAG = "BootReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.d(TAG, ">>>>>>" + intent);
    Toast.makeText(context, "device boot completed", Toast.LENGTH_SHORT).show();
    Log.d(TAG, "<<<<<" +getResultCode());
  }
}

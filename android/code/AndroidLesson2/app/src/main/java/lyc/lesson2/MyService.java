package lyc.lesson2;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by ivan on 14-10-14.
 */
public class MyService extends Service {

  private static final String TAG = "MyService";
  private final IBinder binder = new MyBinder();

  @Override
  public void onCreate() {
    super.onCreate();
    Log.d(TAG, "============");
  }

  @Override
  public int onStartCommand(final Intent intent, int flags, int startId) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        String name = intent.getStringExtra("name");
        try {
          Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
//        Toast.makeText(getApplicationContext(), "name = " + name, Toast.LENGTH_SHORT).show();
        //download file
        Log.d(TAG, ">>>>>>> name = " + name);
        NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(MyService.this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("My notification")
                .setContentText("Hello World!")
                .setAutoCancel(true);
        mBuilder.setProgress(100, 50, false);
        Intent newIntent1 = new Intent(MyService.this, MainActivity.class);
        intent.putExtra("greeting", "hello");
        PendingIntent
            pendingIntent =
            PendingIntent.getActivity(MyService.this, 1, newIntent1,
                                      PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager
            notificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, mBuilder.build());
      }
    }).start();

    return START_REDELIVER_INTENT;
  }

  @Override
  public IBinder onBind(Intent intent) {
    return binder;
  }


  public class MyBinder extends Binder {

    MyService getService() {
      return MyService.this;
    }
  }

  public void hello() {
    Log.d(TAG, "hello");
  }
}

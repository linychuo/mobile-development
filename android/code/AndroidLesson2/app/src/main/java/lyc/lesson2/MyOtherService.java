package lyc.lesson2;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by ivan on 14-10-14.
 */
public class MyOtherService extends IntentService {

  private static final String TAG = "MyOtherService";

  public MyOtherService() {
    super("MyOtherService");
    setIntentRedelivery(true);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    String name = intent.getStringExtra("name");
    try {
      Thread.sleep(10 * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    //download file
    Log.d(TAG, ">>>>>>> name = " + name);
    Intent newIntent = new Intent("lyc.app2.MY_BROADCAST_RECEIVER");
    newIntent.putExtra("message", "ok");
    LocalBroadcastManager.getInstance(this).sendBroadcast(newIntent);

    NotificationCompat.Builder mBuilder =
        new NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentTitle("My notification")
            .setContentText("Hello World!")
            .setAutoCancel(true);
    mBuilder.setProgress(100, 50, false);
    Intent newIntent1 = new Intent(this, MainActivity.class);
    intent.putExtra("greeting", "hello");
    PendingIntent
        pendingIntent =
        PendingIntent.getActivity(this, 1, newIntent1,
                                  PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(pendingIntent);

    NotificationManager
        notificationManager =
        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify(1, mBuilder.build());
//    startForeground(1, mBuilder.build());
  }
}

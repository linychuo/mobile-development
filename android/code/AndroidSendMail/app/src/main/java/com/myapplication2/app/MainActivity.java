package com.myapplication2.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    final Handler handler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
        if (msg.what == 1) {
          Toast.makeText(MainActivity.this, "Email was sent successfully.", Toast.LENGTH_LONG)
              .show();
        }
        if (msg.what == 2) {
          Toast.makeText(MainActivity.this, "Email was not sent.", Toast.LENGTH_LONG).show();
        }

        if (msg.what == 3) {
          Toast.makeText(MainActivity.this, "There was a problem sending the email.",
                         Toast.LENGTH_LONG)
              .show();
        }
      }
    };

    Button addImage = (Button) findViewById(R.id.send_email);
    addImage.setOnClickListener(new View.OnClickListener() {
      public void onClick(View view) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            Mail m = new Mail("linychuo@163.com", "rl850827lyc");

            String[] toArr = {"182183531@qq.com", "linychuo@sohu.com"};
            m.setTo(toArr);
            m.setFrom("linychuo@163.com");
            m.setSubject(
                "This is an email sent using my Mail JavaMail wrapper from an Android device.");
            m.setBody("Email body.");

            try {
              // m.addAttachment("/sdcard/filelocation");
              if (m.send()) {
                handler.sendEmptyMessage(1);
              } else {
                handler.sendEmptyMessage(2);
              }
            } catch (Exception e) {
              handler.sendEmptyMessage(3);
              Log.e("MailApp", "Could not send email", e);
            }
          }
        }).start();
      }
    });
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.action_settings) {
      return true;
    }
    return super.onOptionsItemSelected(item);
  }
}

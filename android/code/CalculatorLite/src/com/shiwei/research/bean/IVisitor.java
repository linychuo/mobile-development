package com.shiwei.research.bean;

import com.shiwei.research.bean.impl.BinaryOp;
import com.shiwei.research.bean.impl.Number;
import com.shiwei.research.bean.impl.UnaryOp;

public interface IVisitor {

	double visit(BinaryOp op);

	double visit(Number decimal);

	double visit(UnaryOp op);
}

package com.shiwei.research.bean.impl;

import com.shiwei.research.bean.Expr;
import com.shiwei.research.bean.IVisitor;

public class VisitorImpl implements IVisitor {

	private double exponentiate(double base, double exponent) {
		if (exponent == 0) {
			return 1;
		} else {
			return base * exponentiate(base, exponent - 1);
		}
	}

	private double divide(double left, double right) {
		if (right == 0) {
			throw new ArithmeticException();
		} else if (left == right) {
			return 1;
		} else {
			return left / right;
		}
	}

	private double prepare(Expr expr) {
		if (expr instanceof Number) {
			return visit((Number) expr);
		} else if (expr instanceof UnaryOp) {
			return visit((UnaryOp) expr);
		} else if (expr instanceof BinaryOp) {
			return visit((BinaryOp) expr);
		} else {
			return 0;
		}
	}

	@Override
	public double visit(BinaryOp op) {
		double left = prepare(op.getLeft());
		double right = prepare(op.getRight());

		if (op.getOperator().equals("+")) {
			return left + right;
		} else if (op.getOperator().equals("-")) {
			return left - right;
		} else if (op.getOperator().equals("*")) {
			return left * right;
		} else if (op.getOperator().equals("/")) {
			return divide(left, right);
		} else if (op.getOperator().equals("^")) {
			return exponentiate(left, right);
		} else {
			throw new RuntimeException("no such operator");
		}
	}

	@Override
	public double visit(Number decimal) {
		return decimal.getValue();
	}

	@Override
	public double visit(UnaryOp op) {
		double value = prepare(op.getArg());
		if (op.getOperator().equals("+")) {
			return value;
		} else if (op.getOperator().equals("-")) {
			return -value;
		} else {
			throw new RuntimeException("no such operator");
		}
	}

}

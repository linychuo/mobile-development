package com.shiwei.research.bean;


public interface Expr {
	double accept(IVisitor visitor);
}

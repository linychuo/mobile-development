package com.shiwei.research.bean.impl;

import com.shiwei.research.bean.Expr;
import com.shiwei.research.bean.IVisitor;

public class UnaryOp implements Expr {

	private final String operator;
	private final Expr arg;

	public UnaryOp(String operator, Expr arg) {
		this.operator = operator;
		this.arg = arg;
	}

	public String getOperator() {
		return operator;
	}

	public Expr getArg() {
		return arg;
	}

	@Override
	public double accept(IVisitor visitor) {
		return visitor.visit(this);
	}

}

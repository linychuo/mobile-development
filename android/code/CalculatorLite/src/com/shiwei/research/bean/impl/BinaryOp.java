package com.shiwei.research.bean.impl;

import com.shiwei.research.bean.Expr;
import com.shiwei.research.bean.IVisitor;

public class BinaryOp implements Expr {
	private final String operator;
	private final Expr left;
	private final Expr right;

	public BinaryOp(String operator, Expr left, Expr right) {
		this.operator = operator != null ? operator.trim() : "+";
		this.left = left;
		this.right = right;
	}

	public String getOperator() {
		return operator;
	}

	public Expr getLeft() {
		return left;
	}

	public Expr getRight() {
		return right;
	}

	@Override
	public double accept(IVisitor visitor) {
		return visitor.visit(this);
	}

}

package com.shiwei.research.bean.impl;

import com.shiwei.research.bean.Expr;
import com.shiwei.research.bean.IVisitor;

public class Number implements Expr {

	private final double value;

	public Number(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public double accept(IVisitor visitor) {
		return visitor.visit(this);
	}
}

package com.shiwei.research;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.CalculatorLite.R;

public class MainActivity extends Activity {

  private static final String TAG = MainActivity.class.getSimpleName();

  private EditText resultText;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    resultText = (EditText) findViewById(R.id.et_result);
    resultText.setKeyListener(new CustomKeyListener());
    resultText.addTextChangedListener(new TextWatcher() {

      @Override
      public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                int arg3) {

        Log.d(TAG, ">>>>  " + arg0);
      }

      @Override
      public void beforeTextChanged(CharSequence arg0, int arg1,
                                    int arg2, int arg3) {
        // TODO Auto-generated method stub

      }

      @Override
      public void afterTextChanged(Editable arg0) {
        // TODO Auto-generated method stub

      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  public void clickIt(View view) {
    CharSequence btnText = ((Button) view).getText();
    resultText.getText().append(btnText);
  }

  public void clearResult(View view) {
    resultText.setText("");
  }

  private static class CustomKeyListener extends NumberKeyListener {

    @Override
    public int getInputType() {
      return InputType.TYPE_NULL;
    }

    @Override
    protected char[] getAcceptedChars() {
      return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8',
                        '9', '+', '-', '*', '/'};
    }
  }
}

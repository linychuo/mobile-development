package lyc.actionbar.loader;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import lyc.actionbar.db.Todo;
import lyc.actionbar.db.TodoDao;

import static lyc.actionbar.db.Todo.TodoStatus;

/**
 * Created by ivan on 3/19/14.
 */
public class TodoLoader extends AsyncTaskLoader<List<Todo>> {

  private final TodoStatus status;
  private final TodoDao dao;
  private final String orderBy;

  public TodoLoader(Context context, TodoStatus status, String orderBy) {
    super(context);
    dao = TodoDao.getInstance(context);
    this.status = status;
    this.orderBy = orderBy;

  }

  @Override
  public List<Todo> loadInBackground() {
    return dao.getAllTodoByStatus(status, orderBy);
  }

  @Override
  protected void onStartLoading() {
    super.onStartLoading();
    forceLoad();
  }

  @Override
  protected void onStopLoading() {
    cancelLoad();
  }

  @Override
  protected void onReset() {
    super.onReset();
    onStopLoading();
  }
}

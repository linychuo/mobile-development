package lyc.actionbar.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import lyc.actionbar.R;
import lyc.actionbar.ui.fragment.BaseFragment;

/**
 * Created by ivan on 3/19/14.
 */
public class NavTabListener<T extends BaseFragment> implements ActionBar.TabListener {

  private static final String TAG = "NavTabListener";
  private final Activity mActivity;
  private final Class<T> mClass;
  private BaseFragment mFragment;

  public NavTabListener(Activity mActivity, Class<T> mClass) {
    this.mActivity = mActivity;
    this.mClass = mClass;
  }

  @Override
  public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
    if (mFragment == null) {
      mFragment = (BaseFragment) Fragment.instantiate(mActivity, mClass.getName());
      ft.replace(R.id.main_content, mFragment, mClass.getName());
    } else {
      ft.attach(mFragment);
      mFragment.refreshData();
    }
  }

  @Override
  public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
    if (mFragment != null) {
      mFragment.refreshActionMode();
      ft.detach(mFragment);
    }
  }

  @Override
  public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

  }
}

package lyc.actionbar.ui.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import lyc.actionbar.R;
import lyc.actionbar.adapter.TodoAdapter;
import lyc.actionbar.db.Todo;
import lyc.actionbar.db.TodoDao;
import lyc.actionbar.loader.TodoLoader;
import lyc.actionbar.ui.MyActivity;

import static lyc.actionbar.db.Todo.TodoStatus;
import static lyc.actionbar.db.TodoDBMetaData.TodoColumns;

/**
 * Created by ivan on 3/19/14.
 */
public class UnFinishedFragment extends BaseFragment {

  private static final String TAG = UnFinishedFragment.class.getName();
  private TodoAdapter adapter;
  private MyActivity myActivity;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    myActivity = (MyActivity) activity;
    adapter = new TodoAdapter(myActivity, TodoStatus.UNFINISHED);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    myActivity.getSupportLoaderManager().initLoader(0, null, this);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    return inflater.inflate(R.layout.unfinished_fragment, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    ListView todoListView = (ListView) getView().findViewById(R.id.unfinished_todo_lv);
    todoListView.setAdapter(adapter);
    registerForContextMenu(todoListView);

    myActivity.getSupportActionBar().setSubtitle(getString(R.string.view_unfinished));
  }


  @Override
  public Loader<List<Todo>> onCreateLoader(int i, Bundle bundle) {
    return new TodoLoader(myActivity, TodoStatus.UNFINISHED, TodoColumns.DEFAULT_ORDER);
  }

  @Override
  public void onLoadFinished(Loader<List<Todo>> listLoader, List<Todo> todos) {
    adapter.clear();
    for (Todo todo : todos) {
      adapter.insert(todo, 0);
    }
  }

  @Override
  public void onLoaderReset(Loader<List<Todo>> listLoader) {
    if (adapter != null) {
      adapter.clear();
    }
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    MenuInflater inflater = myActivity.getMenuInflater();
    inflater.inflate(R.menu.single_operation_menu, menu);
    menu.setHeaderTitle(R.string.finished_todo_alert_msg);
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_yes:
        AdapterView.AdapterContextMenuInfo
            info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (info != null) {
          Todo todo = adapter.getItem(info.position);
          todo.setStatus(TodoStatus.FINISHED);
          todo.setModified(System.currentTimeMillis());
          TodoDao.getInstance(myActivity).updateTodo(todo);
        }
        myActivity.getSupportLoaderManager().restartLoader(0, null, UnFinishedFragment.this);
        return true;
      case R.id.menu_item_no:
        return true;
      default:
        return super.onContextItemSelected(item);
    }
  }

  @Override
  public void onDetach() {
    Log.d(TAG, "--------------");
    super.onDetach();
    myActivity.getSupportLoaderManager().destroyLoader(0);
    adapter = null;
    myActivity = null;
  }

  @Override
  public void refreshData() {
    if (myActivity != null) {
      myActivity.getSupportLoaderManager().restartLoader(0, null, this);
    }
  }

}

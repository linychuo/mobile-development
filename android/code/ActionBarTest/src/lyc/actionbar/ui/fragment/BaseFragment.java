package lyc.actionbar.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;

import java.util.List;

import lyc.actionbar.db.Todo;

/**
 * Created by ivan on 3/30/14.
 */
public abstract class BaseFragment extends Fragment
    implements LoaderManager.LoaderCallbacks<List<Todo>> {

  public abstract void refreshData();

  public void refreshActionMode() {
  }
}

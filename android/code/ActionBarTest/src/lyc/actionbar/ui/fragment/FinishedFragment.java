package lyc.actionbar.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import lyc.actionbar.R;
import lyc.actionbar.adapter.TodoAdapter;
import lyc.actionbar.db.Todo;
import lyc.actionbar.db.TodoDao;
import lyc.actionbar.loader.TodoLoader;
import lyc.actionbar.ui.MyActivity;

import static lyc.actionbar.db.Todo.TodoStatus;
import static lyc.actionbar.db.TodoDBMetaData.TodoColumns;

/**
 * Created by ivan on 3/19/14.
 */
public class FinishedFragment extends BaseFragment
    implements RemoveItemDialog.OnItemRemoveListener, ActionMode.Callback {

  private static final String TAG = FinishedFragment.class.getName();
  private MyActivity myActivity;
  private TodoAdapter adapter;
  private ActionMode mActionMode;
  private ListView todoListView;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    myActivity = (MyActivity) activity;
    adapter = new TodoAdapter(myActivity, TodoStatus.FINISHED);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    myActivity.getSupportLoaderManager().initLoader(1, null, this);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    return inflater.inflate(R.layout.finished_fragment, container, false);
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    todoListView = (ListView) getView().findViewById(R.id.finished_todo_lv);
    todoListView.setAdapter(adapter);
    todoListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
    todoListView.setItemsCanFocus(true);

    todoListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        boolean result = false;
        if (mActionMode == null) {
          refreshActionMode();
          mActionMode = myActivity.startSupportActionMode(FinishedFragment.this);
          result = true;
        }

        todoListView.setItemChecked(i, true);
        setSelectedCnt();
        return result;
      }
    });

    todoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (mActionMode != null) {
          setSelectedCnt();
        }
      }
    });

    myActivity.getSupportActionBar().setSubtitle(getString(R.string.view_finished));
  }

  private void setSelectedCnt() {
    MenuItem item = mActionMode.getMenu().findItem(R.id.menu_item_select_cnt);
    if (item != null) {
      item.setTitle(String.valueOf(todoListView.getCheckedItemIds().length));
    }

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < todoListView.getCheckedItemPositions().size(); i++) {
      int position = todoListView.getCheckedItemPositions().keyAt(i);
      boolean isSelected = todoListView.getCheckedItemPositions().valueAt(i);

      sb.append("[" + position + ":" + isSelected + "]");
      if (i < todoListView.getCheckedItemPositions().size() - 1) {
        sb.append(",");
      }
    }
    Log.d(TAG, sb.toString());
  }

  @Override
  public Loader<List<Todo>> onCreateLoader(int i, Bundle bundle) {
    return new TodoLoader(myActivity, TodoStatus.FINISHED,
                          TodoColumns.ORDER_BY_MODIFIED);
  }

  @Override
  public void onLoadFinished(Loader<List<Todo>> listLoader,
                             List<Todo> todos) {
    adapter.clear();
    for (Todo todo : todos) {
      adapter.insert(todo, 0);
    }
  }

  @Override
  public void onLoaderReset(android.support.v4.content.Loader<List<Todo>> listLoader) {
    if (adapter != null) {
      adapter.clear();
    }
  }

  @Override
  public void onDetach() {
    Log.d(TAG, "++++++++++++++++");
    super.onDetach();
    myActivity.getSupportLoaderManager().destroyLoader(1);
    mActionMode = null;
    adapter = null;
    todoListView = null;
    myActivity = null;
  }

  @Override
  public void refreshData() {
    if (myActivity != null) {
      myActivity.getSupportLoaderManager().restartLoader(1, null, this);
    }
  }

  @Override
  public void refreshActionMode() {
    if (mActionMode != null) {
      mActionMode.finish();
    }
    if (todoListView != null) {
      todoListView.clearChoices();
    }
  }

  @Override
  public void process() {
    TodoDao.getInstance(myActivity).deleteTodo(todoListView.getCheckedItemIds());
    myActivity.getSupportLoaderManager().restartLoader(1, null, FinishedFragment.this);
  }

  @Override
  public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
    MenuInflater inflater = actionMode.getMenuInflater();
    inflater.inflate(R.menu.batch_operation_menu, menu);
    return true;
  }

  @Override
  public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
    return false;
  }

  @Override
  public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
    switch (menuItem.getItemId()) {
      case R.id.menu_item_delete:
        if (todoListView.getCheckedItemIds().length > 0) {
          RemoveItemDialog
              dialog =
              RemoveItemDialog.newInstance(todoListView.getCheckedItemIds().length);
          dialog.show(getFragmentManager(), null);
        }
        actionMode.finish();
        return true;
      default:
        return false;
    }
  }

  @Override
  public void onDestroyActionMode(ActionMode actionMode) {
    mActionMode = null;
  }
}

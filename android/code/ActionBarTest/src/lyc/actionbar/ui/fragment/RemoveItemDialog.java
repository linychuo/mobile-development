package lyc.actionbar.ui.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import lyc.actionbar.R;

/**
 * Created by ivan on 3/30/14.
 */
public class RemoveItemDialog extends DialogFragment {


  public static RemoveItemDialog newInstance(int selectedCnt) {
    RemoveItemDialog dialog = new RemoveItemDialog();
    Bundle args = new Bundle();
    args.putInt("selected_cnt", selectedCnt);
    dialog.setArguments(args);
    return dialog;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    int selectedCnt = getArguments().getInt("selected_cnt");
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setNegativeButton(R.string.menu_item_no, null);
    builder.setTitle(R.string.menu_item_del);
    builder.setMessage(getResources().getString(R.string.remove_todo_alert_msg, selectedCnt));

    builder.setPositiveButton(R.string.menu_item_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        Fragment fragment = getActivity().getSupportFragmentManager()
            .findFragmentByTag(FinishedFragment.class.getName());
        if (fragment != null) {
          OnItemRemoveListener listener = (OnItemRemoveListener) fragment;
          listener.process();
        }
      }
    });

    return builder.create();
  }

  public interface OnItemRemoveListener {

    void process();
  }
}

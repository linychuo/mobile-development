package lyc.actionbar.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lyc.actionbar.R;
import lyc.actionbar.ui.fragment.FinishedFragment;
import lyc.actionbar.ui.fragment.UnFinishedFragment;

public class MyActivity extends ActionBarActivity {

  private static final String TAG = "MyActivity";
  private ActionBar actionBar;

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    actionBar = getSupportActionBar();
    actionBar.setTitle("Todo");
    actionBar.setHomeButtonEnabled(false);
    setNavUsingTabs();
//    setNavUsingList();
  }

  private void setNavUsingTabs() {
    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

    ActionBar.Tab tabFirst = actionBar.newTab();
    tabFirst.setText(getResources().getString(R.string.view_unfinished))
        .setTabListener(new NavTabListener<>(this, UnFinishedFragment.class));
    actionBar.addTab(tabFirst);

    ActionBar.Tab tabSecond = actionBar.newTab();
    tabSecond.setText(getResources().getString(R.string.view_finished))
        .setTabListener(new NavTabListener<>(this, FinishedFragment.class));
    actionBar.addTab(tabSecond);
  }


  private void setNavUsingList() {
    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
    List<String> dropDownList = new ArrayList<>();
    dropDownList.add(getResources().getString(R.string.view_unfinished));
    dropDownList.add(getResources().getString(R.string.view_finished));
    ArrayAdapter<String>
        dropDownAdapter =
        new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, dropDownList);
    actionBar.setListNavigationCallbacks(dropDownAdapter, new ActionBar.OnNavigationListener() {
      @Override
      public boolean onNavigationItemSelected(int i, long l) {
        return false;
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.add_menu:
        Intent intent = new Intent(this, AddTaskActivity.class);
        startActivity(intent);
        return true;
      case R.id.abount_menu:
        intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
        return true;
      case R.id.setting_menu:
        Toast.makeText(this, getResources().getString(R.string.dialog_tips), 1000).show();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

}

package lyc.actionbar.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import lyc.actionbar.R;
import lyc.actionbar.db.Todo;
import lyc.actionbar.db.TodoDao;

/**
 * Created by ivan on 3/19/14.
 */
public class AddTaskActivity extends ActionBarActivity {

  private EditText editText;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.add_task);

    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setTitle(R.string.add_menu);

    editText = (EditText) findViewById(R.id.task_content_txt);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.add_task_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.save_task_menu:
        if (TextUtils.isEmpty(editText.getText())) {
          Toast.makeText(this, getResources().getString(R.string.task_content_empty_tips), 2000)
              .show();
        } else {
          Todo todo = new Todo();
          todo.setTask(editText.getText().toString());
          todo.setCreated(System.currentTimeMillis());
          todo.setModified(System.currentTimeMillis());
          todo.setStatus(Todo.TodoStatus.UNFINISHED);
          int id = TodoDao.getInstance(this).saveTodo(todo);
          if (id > 0) {
            finish();
          }
        }
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }

  }
}

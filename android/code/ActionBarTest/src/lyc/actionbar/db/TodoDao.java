package lyc.actionbar.db;

import android.content.ContentValues;
import android.content.Context;

import java.util.List;

import static lyc.actionbar.db.TodoDBMetaData.Tables;
import static lyc.actionbar.db.TodoDBMetaData.TodoColumns;

/**
 * Created by ivan on 3/6/14.
 */
public class TodoDao {

  private static final String GET_ALL_TODO = "select * from todo where "
                                             + TodoColumns.STATUS
                                             + " = ? order by ? ";

  private static TodoDao instance = null;
  private final TodoDBHelper helper;

  private TodoDao(Context context) {
    helper = TodoDBHelper.getInstance(context);
  }

  public static TodoDao getInstance(Context context) {
    if (instance == null) {
      instance = new TodoDao(context);
    }

    return instance;
  }

  public int saveTodo(Todo todo) {
    return helper.insert(Tables.TODO, convertTo(todo));
  }

  public int updateTodo(Todo todo) {
    return helper.update(Tables.TODO, convertTo(todo),
                         TodoColumns.ID + " = " + todo.getId());
  }

  public int deleteTodo(Todo todo) {
    return helper.delete(Tables.TODO, TodoColumns.ID + " = ?",
                         new String[]{String.valueOf(todo.getId())});
  }

  public int deleteTodo(long[] ids) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < ids.length; i++) {
      sb.append(ids[i]);
      if (i < ids.length - 1) {
        sb.append(",");
      }
    }
    return helper.delete(Tables.TODO, TodoColumns.ID + " in (" + sb.toString() + ")", null);
  }

  public List<Todo> getAllTodoByStatus(Todo.TodoStatus status, String orderBy) {
    return helper
        .queryAll(GET_ALL_TODO, new String[]{String.valueOf(status.ordinal()), orderBy},
                  new RowProcess<Todo>() {
                    @Override
                    public Todo handler(CursorNewWrapper cursor) {
                      Todo todo = new Todo();
                      todo.setId(cursor.getInt(TodoColumns.ID));
                      todo.setTask(cursor.getString(TodoColumns.TASK));
                      todo.setStatus(
                          cursor.getInt(TodoColumns.TASK) == 1
                          ? Todo.TodoStatus.FINISHED
                          : Todo.TodoStatus.UNFINISHED);
                      todo.setCreated(cursor.getLong(TodoColumns.CREATED));
                      todo.setModified(
                          cursor.getLong(TodoColumns.MODIFIED));
                      return todo;
                    }
                  });
  }

  public void release() {
    helper.close();
  }

  public ContentValues convertTo(Todo todo) {
    ContentValues values = new ContentValues();
    values.put(TodoColumns.TASK, todo.getTask());
    values.put(TodoColumns.STATUS, todo.getStatus().ordinal());
    values.put(TodoColumns.CREATED, todo.getCreated());
    values.put(TodoColumns.MODIFIED, todo.getModified());
    return values;
  }

}

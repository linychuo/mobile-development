package lyc.actionbar.db;

/**
 * Created by ivan on 3/6/14.
 */
public class Todo {

  public enum TodoStatus {
    UNFINISHED, FINISHED
  }

  private int id;
  private String task;
  private TodoStatus status;
  private long created;
  private long modified;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTask() {
    return task;
  }

  public void setTask(String task) {
    this.task = task;
  }

  public TodoStatus getStatus() {
    return status;
  }

  public void setStatus(TodoStatus status) {
    this.status = status;
  }

  public long getCreated() {
    return created;
  }

  public void setCreated(long created) {
    this.created = created;
  }

  public long getModified() {
    return modified;
  }

  public void setModified(long modified) {
    this.modified = modified;
  }

  @Override
  public String toString() {
    return "Todo{" +
           "id=" + id +
           ", task='" + task + '\'' +
           ", status=" + status +
           ", created=" + created +
           ", modified=" + modified +
           '}';
  }
}

package lyc.actionbar.db;

import android.provider.BaseColumns;

/**
 * Created by ivan on 3/6/14.
 */
public interface TodoDBMetaData {

  public interface Tables {

    String TODO = "todo";
  }

  public interface TodoColumns {

    //integer type, auto increment
    String ID = BaseColumns._ID;
    //string type
    String TASK = "task";
    //integer type, task status: 0,unfinished, 1,finished
    String STATUS = "status";
    //integer type
    String CREATED = "created";
    //default order by created desc
    String DEFAULT_ORDER = CREATED + " DESC";
    //integer type
    String MODIFIED = "modified";
    String ORDER_BY_MODIFIED = MODIFIED + " DESC";
  }
}

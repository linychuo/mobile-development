package lyc.actionbar.adapter;

import android.app.Activity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Date;

import lyc.actionbar.R;
import lyc.actionbar.db.Todo;

import static lyc.actionbar.db.Todo.TodoStatus;

/**
 * Created by ivan on 3/19/14.
 */
public class TodoAdapter extends ArrayAdapter<Todo> {

  private final Activity ctx;
  private final TodoStatus status;

  public TodoAdapter(Activity ctx, TodoStatus status) {
    super(ctx, R.layout.todo_list_item);
    this.ctx = ctx;
    this.status = status;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    if (rowView == null) {
      LayoutInflater inflater = ctx.getLayoutInflater();
      rowView = inflater.inflate(R.layout.todo_list_item, null);
      ViewHolder holder = new ViewHolder();
      holder.taskTxtView = (TextView) rowView.findViewById(R.id.task_tv);
      holder.timeStampView = (TextView) rowView.findViewById(R.id.timestamp_tv);
      rowView.setTag(holder);
    }
    ViewHolder holder = (ViewHolder) rowView.getTag();
    Todo item = getItem(position);
    holder.taskTxtView.setText(item.getTask());
    holder.timeStampView
        .setText(getDate(status == TodoStatus.UNFINISHED ? item.getCreated() : item.getModified()));
    return rowView;
  }

  private String getDate(long datetime) {
    return DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date(datetime)).toString();
  }

  @Override
  public long getItemId(int position) {
    return getItem(position).getId();
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  private static class ViewHolder {

    TextView taskTxtView;
    TextView timeStampView;
  }
}

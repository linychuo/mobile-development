package com.example.TextLinkify;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyActivity extends Activity {

  private static final String TAG = "MyActivity";

  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    TextView textViewResource = (TextView) findViewById(R.id.text_html_resource);
    textViewResource.setText(Html.fromHtml(getResources().getString(R.string.link_text_manual)));
    textViewResource.setMovementMethod(LinkMovementMethod.getInstance());

    TextView textViewHtml = (TextView) findViewById(R.id.text_html_program);
    textViewHtml
        .setText(Html.fromHtml("<b>text_html_program: Constructed from HTML programmatically.</b>"
                               + "  Text with a <a href=\"http://www.google.com\">link</a> "
                               + "created in the Java source code using HTML."));
    textViewHtml.setMovementMethod(LinkMovementMethod.getInstance());

    SpannableString
        ss =
        new SpannableString(
            "text_spannable: Manually created spans. Click here to dial the phone.");
    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, 39, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
    ss.setSpan(new URLSpan("tel:432432432423"), 40 + 6, 40 + 10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    TextView textViewSpan = (TextView) findViewById(R.id.text_spannable);
    textViewSpan.setText(ss);

    textViewSpan.setMovementMethod(LinkMovementMethod.getInstance());

    fun2();
  }


  private void fun1() {
    Button btnBack = (Button) findViewById(R.id.back);
    btnBack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setContentView(R.layout.main);
        fun2();
      }
    });
  }

  private void fun2() {
    Button btnGo = (Button) findViewById(R.id.go);
    btnGo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        setContentView(R.layout.b);
        fun1();
      }
    });
  }
}

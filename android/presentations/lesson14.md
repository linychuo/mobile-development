title: Notifications
author: ivan
---
# Notifications
##### by ivan 2014.3
---
# Overview
![iconic_notification](./iconic_notification.png)
![normal_notification](./normal_notification.png)

---
# Notification display elements
Notifications in the notification drawer can appear in one of two visual styles, depending on the version and the state of the drawer

--
## Normal view
![normal_notification_callouts](./normal_notification_callouts.png)


--
## Big View
![bigpicture_notification_callouts](./bigpicture_notification_callouts.png)

---
# Creating a Notification
You specify the UI information and actions for a notification in a [NotificationCompat.Builder](http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html) object. To create the notification itself, you call [NotificationCompat.Builder.build()](http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#build()), which returns a [Notification](http://developer.android.com/reference/android/app/Notification.html) object containing your specifications. To issue the notification, you pass the Notification object to the system by calling [NotificationManager.notify()](http://developer.android.com/reference/java/lang/Object.html#notify())

--
## Required notification contents
A [Notification](http://developer.android.com/reference/android/app/Notification.html) object *must* contain the following:

- A small icon, set by [setSmallIcon](http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#setSmallIcon(int))
- A title, set by [setContentTitlte](http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#setContentTitle(java.lang.CharSequence))
- Detail text, set by [setContentText](http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#setContentText(java.lang.CharSequence))


--
## Notification actions
- Althouth they're optional, you should add at least one action to your notification. An action allows users to go directly from the notification to an Activity in your application, where they can look at one or more events or do further work
- Inside a Notification, the itself is defined by a [PendingIntent](http://developer.android.com/reference/android/app/PendingIntent.html) containing an Intent that starts an Activity in your application

--
## Creating a simple notification

    NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.notification_icon)
            .setContentTitle("My notification")
            .setContentText("Hello World!");
    // Creates an explicit intent for an Activity in your app
    Intent resultIntent = new Intent(this, ResultActivity.class);
    
    // The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
    // Adds the back stack for the Intent (but not the Intent itself)
    stackBuilder.addParentStack(ResultActivity.class);
    // Adds the Intent that starts the Activity to the top of the stack
    stackBuilder.addNextIntent(resultIntent);
    PendingIntent resultPendingIntent =
            stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
            );
    mBuilder.setContentIntent(resultPendingIntent);
    NotificationManager mNotificationManager =
        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    mNotificationManager.notify(mId, mBuilder.build());


--
## Applying a big view style to a notification

    NotificationCompat.InboxStyle inboxStyle =
            new NotificationCompat.InboxStyle();
    String[] events = new String[6];
    // Sets a title for the Inbox style big view
    inboxStyle.setBigContentTitle("Event tracker details:");
    ...
    // Moves events into the big view
    for (int i=0; i < events.length; i++) {
    
        inboxStyle.addLine(events[i]);
    }
    // Moves the big view style object into the notification object.
    mBuilder.setStyle(inBoxStyle);


--
## Handling compatibility
To ensure the best compatibility, create notifications with NotificationCompat and its subclasses, particularly NotificationCompat.Builder. In addition, follow this process when you implement a notification

--

1. Provide all of the notification's functionality to all users, regardless of the verison they're using. To do this, verify that all of the functionality is available from an Activity in your app. You may want to add a new Activity to do this
2. Ensure that all users can ge to the functionality in the Activity. by having it start when users click the notification. To do this, create a PendingIntent for the Activity. Call setContentIntent() to add the PendingIntent to the notification

3. Now add the expanded notification features you want to use to the notification. Remember that any functionality you add also has to be available in the Activity that starts when users click the notification


---
# Manageing Notifications
When you need to issue a notification multiple times for the same type of event, you should avoid making a completely new notification. Instead, you should consider updating a previous notification, either by changing some of its values or by adding to it, or both

--
## Updating notifications

    mNotificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    // Sets an ID for the notification, so it can be updated
    int notifyID = 1;
    mNotifyBuilder = new NotificationCompat.Builder(this)
        .setContentTitle("New Message")
        .setContentText("You've received new messages.")
        .setSmallIcon(R.drawable.ic_notify_status)
    numMessages = 0;
    // Start of a loop that processes data and then notifies the user
    ...
        mNotifyBuilder.setContentText(currentText)
            .setNumber(++numMessages);
        // Because the ID remains unchanged, the existing notification is
        // updated.
        mNotificationManager.notify(
                notifyID,
                mNotifyBuilder.build());
                
                
--
## Removing notifications
Notifications remain visible until one of the following happens:

- The user dismisses the notification either individually or by using "Clear All" (if the notification can be cleared)
- The user clicks the notification, and you called setAutoCancel() when you created the notification
- You call cancel() for a specific notification ID. This method also deletes ongoing notifications
- You call cancelAll(), which removes all of the notifications you previously issued


---
# Preserving Navigation when starting an activity

--
## Setting up a regular activity PendingIntent
1. Define your application's Activity hierarchy in the manifest
2. Create a back stack based on the Intent that starts the Activity

---
# Displaying Progress in a Notification
To use a progress indicator on platforms starting with Android 4.0, call setProgress(). For previous versions, you must create your own custom notification layout that includes a ProgressBar view

--
## Displaying a fixed-duration progress indicator
    mNotifyManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    mBuilder = new NotificationCompat.Builder(this);
    mBuilder.setContentTitle("Picture Download")
        .setContentText("Download in progress")
        .setSmallIcon(R.drawable.ic_notification);
    // Start a lengthy operation in a background thread
    new Thread(
        new Runnable() {
            @Override
            public void run() {
                int incr;
                // Do the "lengthy" operation 20 times
                for (incr = 0; incr <= 100; incr+=5) {
                        // Sets the progress indicator to a max value, the
                        // current completion percentage, and "determinate"
                        // state
                        mBuilder.setProgress(100, incr, false);
                        // Displays the progress bar for the first time.
                        mNotifyManager.notify(0, mBuilder.build());
                            // Sleeps the thread, simulating an operation
                            // that takes time
                            try {
                                // Sleep for 5 seconds
                                Thread.sleep(5*1000);
                            } catch (InterruptedException e) {
                                Log.d(TAG, "sleep failure");
                            }
                }
                // When the loop is finished, updates the notification
                mBuilder.setContentText("Download complete")
                // Removes the progress bar
                        .setProgress(0,0,false);
                mNotifyManager.notify(ID, mBuilder.build());
            }
        }
    // Starts the thread by calling the run() method in its Runnable
    ).start();
    
    
--
## Displaying a continuing activity indicator

    // Sets an activity indicator for an operation of indeterminate length
    mBuilder.setProgress(0, 0, true);
    // Issues the notification
    mNotifyManager.notify(0, mBuilder.build());
    
    
---
# Custom notification layouts
1. Create an XML layout for the notification in a separate file. You can use any file name you wish, but you must use the extension .xml
2. In your app, use RemoteViews methods to define your notification's icons and text. Put this RemoteViews object into your NotificationCompat.Builder by calling setContent(). Avoid setting a background Drawable on your RemoteViews object, because your text color may become unreadable

--

The RemoteViews class also includes methods that you can use to easily add a Chronometer or ProgressBar to your notification's layout. For more information about creating custom layouts for your notification, refer to the RemoteViews reference documentation.

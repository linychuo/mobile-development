title: Maps, Geocoding and Location-Based Services
author: ivan
---
# Maps, Geocoding and Location-Based Services
##### by ivan 2014.3
---
# Using location-based services
"Location-based services" is an umbrella term that describes the different technologies you can use to find a device's current location. The two main LBS elements are:

- Location Manager
- Location Providers

--

Using the location manager, you can do the following:

- Obtain your current location
- Follow movement
- Set proximity alerts for detecting movement into and out of a specified area
- Find avaiable Location Providers
- Monitor the status of the GPS receiver

--
## Code snippet

    LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
        public void onLocationChanged(Location location) {}
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        }
    );

    <uses-permission android:name=”android.permission.ACCESS_FINE_LOCATION”/>
    <uses-permission android:name=”android.permission.ACCESS_COARSE_LOCATION”/>
    
    
---
# Selecting a location provider
Depending on the device, you can use several technologies to determine the current location. Each technology, available as a Location Provider, offers different capabilities(including differences in power consumption, accuracy, and the ability to determine altitude, speed, or heading information)

--
## Finding Location Providers
- LocationManager.GPS_PROVIDER
- LocationManager.NETWORK_PROVIDER
- LocationManager.PASSIVE_PROVIDER
    
        boolean enabledOnly = true;
        List<String> providers = locationManager.getProviders(enabledOnly);
        
        
--
## Finding location providers by specifying criteria
In most scenarios it's unlikely that you want to explicitly choose a Location Provider to use. It's better practice to specify your requirements and let Android determine the best technology to use

Use the *Criteria* class to dictate the requirements of a provider in terms of accuracy, power use(low, medium, high), financial cost, and the ability to return values for altitude, speed, and heading

--
## Code snippet

    Criteria criteria = new Criteria();
    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
    criteria.setPowerRequirement(Criteria.POWER_LOW);
    criteria.setAltitudeRequired(false);
    criteria.setBearingRequired(false);
    criteria.setSpeedRequired(false);
    criteria.setCostAllowed(true);
    
    criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
    criteria.setVerticalAccuracy(Criteria.ACCURACY_MEDIUM);
    criteria.setBearingAccuracy(Criteria.ACCURACY_LOW);
    criteria.setSpeedAccuracy(Criteria.ACCURACY_LOW);
    
    String bestProvider = locationManager.getBestProvider(criteria, true);
    
    
--

If more than one Location Provider matches your Criteria, the one with the greatest accuracy is returned. If no Location Providers meet your requirements, the Criteria are loosened, in the following order, until a provider is found:

- Power user
- Accuracy of returned location
- Accuracy of bearing, speed, ant altitude
- Availablility of bearing, speed, and altitude


--

To get a list of names for all the providers matching your Criteria, use *getProviders*. It accepts a Criteria object and returns a String list of all Location Providers that match it, As with the *getBestProviders* call, if no matching providers are found, this method returns *null* or an empty list

    List<String> matchingProviders = locationManager.getProviders(criteria, false);

    
    
---
# Determining Location Provider Capabilities

    String providerName = LocationManager.GPS_PROVIDER;
    LocationProvider gpsProvider = locaitonManager.getProvider(providerName);
    
    
---
# Finding your current location
One of the most powerful uses of location-based services is to find the physical location of the device. The accuracy of the returned location is dependent on the hardware available and the permissions requested by your application

--
## Location privacy
Privacy is an important consideration when your application uses the the user's location, particularly when it is regularly updating their current position. Ensure that your application uses the device location data in a way that respects the user's privacy by:

- Only using and updating location when necessary for your application
- Notifiying users of when you track their locaitons, and if and how that location information is used, transmitted, and stored
- Allowing users to disable location updates, and respecting the system settings for LBS preferences

--
## Finding the last known location

    String provider = LocationManager.GPS_PROVIDER;
    Location location = locationManager.getLastKnownLocation(provider);
    
>getLastKnownLocation does not ask the Location Provider to update the current position. If the device has not recently updated the current position, this value may not exist or be out of date

--
## Refreshing the current location

    String provider = LocationManager.GPS_PROVIDER;
    int t = 5000; // milliseconds
    int distance = 5; //meters
    
    LocationListener myLocationListener = new LocationListener(){
        
        public void onLocationChanged(Location location){
            // update application based on new locaiton
        }
    
        public void onProviderDisabled(String provider){
            // update application if provider diabled
        }
        
        public void onProviderEnabled(String provider){
            // update application if provider enable
        }
        
        public void onStatusChanged(String provider, int status, Bundle extras){
            // update application if provider hardware status changed
        }
    };
    
    locationManager.requestLocationUpdates(provider, t, distance, myLocationListener);
    
    locationManager.removeUpdates(myLocationListener);
    
> You can request multiple location updates pointing to the same or different Location Listeners using different minimum time and distance thresholds or Location Providers

--

Android 3.0(API level 11) introduced an alternative technique for receiving location change. Rather than creating a Location Listener, you can specify a Pending Intent that will be broadcast whenever the location changes or the location provider status or availability changes. The new location is stored as an extra with the key **KEY_LOCATION_CHANGED**


--
## Code Snippet

    import android.content.BroadcastReceiver;
    import android.content.Context;
    import android.content.Intent;
    import android.location.Location;
    import android.location.LocationManager;
    
    public class MyLocationUpdateReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context ctx, Intent intent){
            String key = LocationManager.KEY_LOCATION_CHANGED;
            Location location = (Location)intent.getExtras().get(key);
            // TODO: do something with the new location
        }
    }
    
    String provider = LocationManager.GPS_PROVIDER;
    int t = 5000;
    int distance = 5;
    
    final int locationUpdateRC = 0;
    int flags = PendingIntent.FLAG_UPDATE_CURRENT;
    
    Intent intent = new Intent(this, MyLocationUpdateReceiver.class);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(this, locationUpdateRC, intent, flags);
    
    locaitonManager.requestLocationUpdates(provider, t, distance, pendingIntent);
    
    locationManager.removeUpdates(pendingIntent);
 
 

---
# Requesting a single location update

---
# Best practice for location updates
When using location within your application, consider the following factors:

- Battery life versus accuracy
- Startup time
- Update rate
- Provider availability

---
# Monitoring location provider status and availability

---
## Using proximity alerts
Proximity alerts let your app set Pending Intents that are fired when the device moves within or beyond a set distance from a fixed location


    private static final String TREASURE_PROXIMITY_ALERT = “com.paad.treasurealert”;
    
    private void setProximityAlert(){
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        
        double lat = 73.147536;
        double lng = 0.510638;
        float radius = 100f;
        long expirations = -1;
        
        Intent intent = new Intent(TREASURE_PROXIMITY_ALERT);
        PendingIntent proximityIntent = PendingIntent.getBroadcast(this, -1, intent, 0);
        locationManager.addProximityAlert(lat, lng, radius, expiration, proximityIntent);   
    }
    
    public class ProximityIntentReceiver extends BroadcastReceiver{
    
        @Override
        public void onReceive(Context context, Intent intent){
            String key = LocationManager.KEY_PROXIMITY_ENTERING;
            Boolean entering = intent.getBooleanExtra(key, false);
            // TODO: perform proximity alert actions
        }
    }
    
    IntentFilter filter = new IntentFilter(TREASURE_PROXIMITY_ALERT);
    registerReceiver(new ProximityIntentReceiver(), filter);
title: Working in the Background
author: ivan
---
# Working in the Background
##### by ivan 2014.3
---
# Introducing services
- Android offers the **Service** class to create application components that handle long-lived operations and include functionality that doesn't require a user interface
- Unlike Activities, which display graphical interfaces, Services run invisibly doing Internet lookups, processing data, updating your Content Providers, firing Intents, and triggering Notifications
- Services are started, stopped, and controlled from other application components, including Activities, Broadcast Receivers, and other Services. If your application provides functionality that doesn't depend directly on user input, or includes time-consuming operations, Services may be the answer

---
# Creating and Controlling Services

    public class MyService extends Service{
        
        @Override
        public void onCreate(){
            super.onCreate();
        }
        
        @Override
        publc IBinder onBind(Intent intent){
            return null;
        }
    }
    
    <service android:enabled="true" android.name=".MyService"
        android:permission="com.shiwei.ssr.MY_SERVICE_PERMISSION"/>

---
# Executing a Service and Controlling Its Restart Behavior
- Override the **onStartCommand** event handler to execute the task encapsulated by your Service
- The **onStartCommand** method is called whenever the Service is started using **startService**, so it may be executed several times within a Service's lifetime

--
- Services are launched on the main Application Thread, meaning that any processing done in the **onStartCommand** handler will happen on the main GUI Thread
- The standard pattern for implementing a Service is to create and run a new Thread from **onStartCommand** to perform the processing in the background, and then stop the service when it's been completed

--
## Code snippet

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
       doBackground(intent, startId);
       return Service.START_STICKY;
    }
    

---
# Starting and Stopping Services

    private void explicitStart(){
        Intent intent = new Intent(this, MyService.class);
        startService(intent);
    }
    
    private void implicitStart(){
        Intent intent = new Intent(MyMusicService.PLAY_ALBUM);
        intent.putExtra(MyMusicService.ALBUM_NAME_EXTRA, "United");
        intent.putExtra(MyMusicService.ARTIST_NAME_EXTRA, "Pheonix");
        startService(intent);
    }
    
    
    // stop a service explicitly
    stopService(new Intent(this, MyService.class));
    
    // stop a service implicitly
    Intent intent = new Intent(MyMusicService.PLAY_ALBUM);
    stopService(intent);

--
Calls to **startService** do not nest, so a single call to **stopService ** will terminate the running Service it matches, no matter how many times **startService** has been called

---
# Self-Terminating Services
---
# Binding Services to Activities
Services can be bound to Activities, with the latter maintaining a reference to an instance of the former, enabling you to make method calls on the running Service as you would on any other instantiated class

--
## Code snippet

    @Override
    public IBinder onBind(Intent intent){
        return binder;
    }
    
    public class MyBinder extends Binder{
        MyService getService(){
            return MyService.this;
        }
    }
    
    private final IBinder binder = new MyBinder();

--
## ServiceConnection
- It is the connection between the Service and another component
- To bind a Service to another application component, you need to implement a new **ServiceConneciton**, overriding the *onServiceConnected* and *onServiceDisConnected* methods to get a reference to the Service instance after a connection has be established

--
## Code snippet

    // Reference to the service
    private MyService serviceRef;
    
    private ServiceConnection conn = new ServiceConneciton(){
        public void onServiceConnected(ComponentName className, IBinder service){
            serviceRef = ((MyService.MyBinder)service).getService();
        }
        
        public void onServiceDisconnected(ComponentName className){
            serviceRef = null;
        }
    };

To perform the binding, call **bindService** within your Activity, passing in an Intent(either explicit or implicit) that selects the Service to bind to, and an instance of a *ServiceConneciton* implementation

--
## Code snippet
    
    // Bind to the service
    Intent bindIntent = new Intent(MyActivity.this, MyService.class);
    bindService(bindIntent, conn, Context.BIND_AUTO_CREATE);
    
When the Service has been bound, all its public methods and properties are available through the *serviceBinder* object obtained from the *onServiceConnected* handler

---
# Using background threads
- Responsiveness is one of the most critical attributes of a good Android application
- To ensure that your application responds quickly to any user intercation or system event, it's vital that you move all processing and I/O operations off the main application Thread and into a child Thread

--
- Android offers a number of alternatives for moving your processing to the background. You can implement your own Threads and use the Handler class to synchronize with GUI Thread before updating the UI. Alternatively, the **AsyncTask** class lets you define an operation to be performed in the background and provides event handlers that enable you to monitor progress and post the results on the GUI Thread

--
## Using AsyncTask to run asynchronous tasks
- AsyncTask handles all the Thread creation, management, and synchronization, enabling you to create an asynchronous task consisting of processing to be done in the background and GUI updates to be performed both during the processing, and once it's complete
- Similarly, Cursor Loaders are better optimized for the use-case of Content Provider or database results

--
## Creating New Asynchronous Tasks
- Each AsyncTask implemention can specify parameter types that will be used for input parameters, the progress-reporting values, and result values
- If you don't need or want to take input parameters, update progress, or report a final result, simply specify *Void* for any or all the types required

--
## Code snippet
    private class MyAsyncTask extends AsyncTask<String, Integer, String> {

    @Override
    protected String doInBackground(String... parameter) {
      // Moved to a background thread
      String result = "";
      int myProgress = 0;

      int inputLength = parameter[0].length();

      // Perform background processing task, update myProgress
      for (int i = 1; i <= inputLength; i++) {
        myProgress = i;
        result = result + parameter[0].charAt(inputLength - i);

        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {

        }

        publishProgress(myProgress);
      }
      return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
      // Synchronized to UI thread
      // Update progress bar, Notification, or the UI elements
      super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
      // Synchronized to UI thread
      // Report results via UI update, Dialog, or notifications
      super.onPostExecute(s);
    }
  }
  
  
--
## Notes
- doInBackground
- onProgressUpdate
- onPostExecute

--
## Running Asynchronous Tasks

    String input = "redrum ... redrum";
    new MySyncTask().execute(input);
    
    
---
# Introducing the Intent Service
- The Intent Service is a convenient wrapper class that implements the best practice pattern for background Services that perform set tasks on demand, such as recurring Internet updates or data processing
- Other application components request an Intent Service complete a task by starting the Service and passing in an Intent containing the parameters necessary to complete the task

--
- The Intent Service queues request Intents as they are received and process them consecutively on an asynchronous background Thread. After every received Intent has been processed, the Intent Service will terminate itself
- The Intent Service handles all the complexities around queuing multiple requests, background Thread creation, and UI Thread asynchronization


--
## Code snippet

    public class MyIntenteService extends IntentService {

      public MyIntenteService(String name){
        super(name);
        // TODO: complete any required constructor tasks
      }
    
      @Override
      public void onCreate() {
        super.onCreate();
        // TODO: actions to perform when service is created
      }
    
      @Override
      protected void onHandleIntent(Intent intent) {
        // This handler occurs on a background thread
        // TODO the time consuming task should be implemented here
        // Each Intent supplied to this IntentService will be
        // processed consecutively here. When all incoming Intents
        // have been processed the Service will terminate itself
      }
    }
    
The **onHandleIntent** handler will be executed on a worker Thread, once for each Intent received. Then Intent Service is the best-practice approach to creating Services that perform set tasks either on-demand or at regular intervals


---
# Introducing Loaders
When creating your own Loader implementation, it is typically best practice to extend the **AsyncTaskLoader** class rather than the Loader class directly. in general your custom Loaders should:
- Asynchronously load data
- Monitor the source of the loaded data and automatically provide updated results

    
---
# Manual thread creation

    private void backgroundExecution(){
        Thread thread = new Thread(null, doBackgroundThreadProcessing, "Background");
        thread.start();
    }
    
    private Runnable doBackgroundThreadProcessing = new Runnable(){
        public void run(){
            backgroundThreadProcessing();
        }
    };
    
    private void backgroundThreadProcessing(){
        handler.post(doUpdateGUI);
    }
    
    private Handler handler = new Handler();
    
    private Runnable doUpdateGUI = new Runnable(){
        public void run(){
            updateGUI();
        }
    };
    
    private void updateGUI(){
    
    }
    
    
---
# Using Alarms
- Alarms are a means of firing Intents at predetermined times or intervals. Unlike Timers, Alarm operate outside the scope of your application, so you can use them to trigger application events after your application has been closed
- Alarms are particularly powerful when used in combination with Broadcast Receivers, enabling you to set Alarms that fire broadcast Intents, start Services, or even open Activities, without your application needing to be open or running
- You can use Alarms to schedule regular updates based on network lookups, to schedule time-consuming or cost-bound operations at "off-peak" times, or to schedule retries for failed operations

    
    
title: Fragment
author: ivan
---
# Fragment
##### by ivan 2014.3
---
# Fragment
- A **Fragment** represents a behavior or portion of user interface in an **Activity**. You can combine multiple fragments in a single activity to build a multi-pane UI and reuse a fragment in multiple activities. You can think of a fragment as a modular section of an activity, which has its own lifecycle, receives its own input events, and which you can add or remove while the activity is running(sort of like a "sub activity" that you can reuse in different activities)
- A fragment must always be embedded in an activity and the fragment's lifecycle is directly affected by the host activity's lifecycle

--
## When to Use Fragments
- One of the primary reasons to use a fragment is so you can reuse a chunk of user interface and functionality across devices and screen size
- We would want the separate activities for the list and for the details to be able to share the logic you've built into these portions for a large screen

--
## Design Philosophy
![fragments](./fragments.png)

--
## The structure of a Fragment
- A fragment is like a sub-activity: it has a fairly specific purpose and almost always displays a user interface. But where an activity is subclassed below *Context*, a fragment is extended from *Object* in package *android.app*
- A fragment can have a view hierarchy to engage with a user
- Similar to an activity, a fragment can be saved and later restored automatically by the system
- An activity can have multiple fragments in play at one time

--
## A Fragment's lifecycle
![fragment lifecycle](fragment-lifecycle.png)

--
## Important callbacks
- onAttach
- onCreate
- onCreateView
- onActivityCreated
- onDetanch


--
## Code sample

---
# The FragmentManager
The **FragmentManager** is a component that takes care of the fragments belonging to an activity, This includes fragments on the back stack and fragments that may just be hanging around

---
# Communications with fragments
    FragmentOther other = (FragmentOther)getFragmentManager().findFragmentByTag("other");
    other.callCustomMethod(arg1, arg2);
    
---
# Using startActivity and setTargetFragment

    OtherFragment other = new OtherFragment();
    other.setTargetFragment(this, 0);
    fm.beginTransaction().add(other, "work").commit();
    
---
# Custom Animations with ObjectAnimator
objectAnimator
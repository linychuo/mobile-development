author: ivan
title: Databases and Content Providers
---
# Databases and Content Providers
##### by ivan 2014.3
---
# Introducing Android Databases
- Android provides structured data persistence through a combination of SQLite database and Content Providers
- Every application can create its own databases over which it has complete control
- Having created your underlying data store, Content Providers off a generic, well-defined interface for using and sharing data that provides a consistent absraction from the underlying data source

---
# SQLite Databases
Android databases are stored in the /data/data/<package_name>/databases folder on your device (or emulator). All databases are private, accessible only by the application that created them

---
# Content Values and Cursors
- Content Values are used to insert new rows into tables. Each *ContentValues* object represents a single table row as a map of column names to values
- Cursors are pointers to the result set within the underlying data. Cursors provide a managed way of controlling your position(row) in the result set of a database query
- Android provides a convenient mechanism to ensure queries are performed asynchronously

---
# Working with SQLite databases
When working with databases, it's good form to encapsulate the underlying database and expose only the public methods and constants required to interact with that database

--
## Introducing the SQLiteOpenHelper
*SQLiteOpenHelper* is an abstract class used to implement the best practice pattern for creating, opening, and upgrading database

--
## Implementing an SQLite Open Helper
    public class BookDBOpenHelper extends SQLiteOpenHelper {

        private static final String TAG = "BookDBOpenHelper";
        
        public static final String DB_NAME = "bookmgr.db";
        public static final int DB_VERSION = 1;
        
        
        public interface Tables {
            String BOOK = "book";
        }
        
        public interface BookColumns extends BaseColumns {
        
            String BOOK_ID = Tables.BOOK + "." + _ID;
            String BOOK_NAME = Tables.BOOK + "." + "name";
        }
        
        public BookDBOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }
        
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table " + Tables.BOOK + " ("
                       + BookColumns.BOOK_ID + " INTEGER PRIMARY KEY,"
                       + BookColumns.BOOK_NAME + " TEXT"
                       + ");");
        }
        
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "upgrading from version " 
                        + oldVersion + " to  " + newVersion);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.BOOK);
            onCreate(db);
        }
        
        public SQLiteDatabase getDB(boolean writable) {
            return writable ? getWritableDatabase() : getReadableDatabase();
        }
}
    
--
## Note
The *onUpgrade* method simply drops the existing table and replaces it with the new definition. This is often the simplest and most practical solution. However, for important data that is not synchronized with an online service or is hard to recapture, a better approach may be to migrate existing data into the new table

--
## Using SQLite Open Helper
- To access a database using the SQLite Open Helper, call *getWritableDatabase* or *getReadableDatabase* to open and obtain a writable or read-only instance of the underlying dtabase, respectively
- If the database doesn't exist, the helper executes its *onCreate* handler. If the database version has changed, the *onUpgrade* handler will fire. In either case, the *get<read/writ>ableDatabase* call will return the cached, newly opened, newly created, or upgraded database, as appropriate

--
## Opening or Creating db without the SQLite Open Helper

    SQLiteDatabase db = context.openOrCreateDatabase(DATABASE_NAME,
                                                    Context.MODE_PRIVATE,
                                                    null);
                                                    
--
## Android DB Design considerations
- Files(such as bitmaps or audio files) are not usually stored within database tables, Use a string to store a path to the file, preferably a fully qualified URI
- Although not strictly a requiredment, it's strongly recommended that all tables include an auto-increment key field as a unique index field for each row. If you plan to share your table using a Content Provider, a unique ID field is required

--
## Query a Database
    BookDBOpenHelper d = new BookDBOpenHelper(this);
    SQLiteDatabase sd = d.getDB(false);

    String[] result_columns = {BookDBOpenHelper.BookColumns.BOOK_ID,
                               BookDBOpenHelper.BookColumns.BOOK_NAME};

    String where = null;
    String[] whereArgs = null;
    String groupBy = null;
    String having = null;
    String order = null;

    Cursor cursor = sd.query(BookDBOpenHelper.Tables.BOOK, result_columns,
                             where, whereArgs, groupBy, having, order);
    // Cursor cursor = sd.rawQuery("select book._id, book.name from book", null);
    int ID_COLUMN_IDX = cursor.getColumnIndex(BookDBOpenHelper.BookColumns.BOOK_ID);
    int BOOK_NAME_COLUMN_IDX = cursor.getColumnIndex(BookDBOpenHelper.BookColumns.BOOK_NAME);

    while (cursor.moveToNext()) {
      Log.d(TAG, "_ID = " + cursor.getInt(ID_COLUMN_IDX) + ", BOOK_NAME = " + cursor
          .getString(BOOK_NAME_COLUMN_IDX));
    }
    cursor.close();
    
--
## Adding, Updating and Removing Rows
---
# Content Providers
- This idea of content providers makes data sources look like [REST](http://zh.wikipedia.org/wiki/REST)-enabled data providers
- Content Providers provide an interface for publishing and consuming data, based around a simple URI address model using the *content://* schema
- Content Providers can be shared between applications, queried for results, have their existing records updated or deleted, and have new records added
- Any application with the appropriate permissions can add, remove, or update data from any other application, including the native Android Content Providers
- Several native Content Providers have been made accessible for access by third-party applications, including the contact manage, media store, and calendar

---
# Creating Content Providers
1. Plan your database, URIs, column names, and so on, and create a metadata class that defines constants for all all of these metadata elements
2. Extend the abstract class *ContentProvider*
3. Implement these methods: query, insert, update, delete, and getType
4. Register the provider in the manifest file

--
## Code Example
    public class BookContentProvider extends ContentProvider {

      @Override
      public boolean onCreate() {
        return false;
      }
    
      @Override
      public Cursor query(Uri uri, String[] strings, String s, String[] strings2, String s2) {
        return null;
      }
    
      @Override
      public String getType(Uri uri) {
        return null;
      }
    
      @Override
      public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
      }
    
      @Override
      public int delete(Uri uri, String s, String[] strings) {
        return 0;
      }
    
      @Override
      public int update(Uri uri, ContentValues contentValues, String s, 
                        String[] strings) {
        return 0;
      }
    }
    
--
## Registering
Each Content Provider authority must be unique, so it's good practice to base to URI path on your package name

    com.<companyname>.provider.<applicationname>
    
    <provider android:name=".BookContentProvider" 
                android:authorities="com.shiwei.provider.booksys"/>
                
--
## Publishing your Content Provider's URI Address
Each Content Provider should expose its authority using a public static **CONTENT_URI** property to make it more easily disconverable

    public static final Uri CONTENT_URI = 
        Uri.parse("content://com.shiwei.provider.booksys/book");
        
These content URIs will be used whn accessing your Content Provider using a Content Resolver
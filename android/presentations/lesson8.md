author: ivan
title: Shared Preference and Files
---
# Shared Preference and Files
##### by ivan 2014.3
---
# Saving simple applicaiton Data
- Saving and loading data is essential for most applications, At a minimum, an Activity should save its user interface state before it becomes inactive to ensure the same UI is presented when it restarts
- Shared Preferences are a simple, lightweight name/value pair (NVP) mechanism for saving primitive application data, most commonly a user’s application preferences. 
- Android also offers a mechanism for recording application state within the Activity lifecycle handlers, as well as for providing access to the local filesystem, through both specialized methods and the java.io classes

---
# Shared Preferences
When storing UI state, user preferences, or application settings, you want a lightweight mechanism to store a know set of values. Shared Preferences let you save groups of name/value pairs of primitive data as named preferences

--
## Creating and saving Shared Preferences
    SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREFS, Activity.MODE_PRIVATE);
    
    SharedPreferences.Editor editor = mySharedPreferences.edit();
    editor.putBoolean("isTrue", true);
    editor.putFloat("lastFloat", 1f);
    editor.putInt("wohleNumber", 2);
    editor.putLong("aNumber", 31);
    editor.putString("textEntryValue", "Not empty");
    
    //save edits, commit the changes
    editor.apply();
    
--
## Retrieving Shared Preferences
    // Retrieve the saved values.
    boolean isTrue = mySharedPreferences.getBoolean(“isTrue”, false);
    float lastFloat = mySharedPreferences.getFloat(“lastFloat”, 0f);
    int wholeNumber = mySharedPreferences.getInt(“wholeNumber”, 1);
    long aNumber = mySharedPreferences.getLong(“aNumber”, 0);
    String stringPreference = mySharedPreferences.getString(“textEntryValue”, “”);
    
    
    // retrieving all available shared preferences
    Map<String, ?> allPreferences = mySharedPreferences.getAll();
    boolean containsLastFloat = mySharedPreferences.contains(“lastFloat”);
    
---
# Including static file as resources
If you applicatioin requires external file resources, you can include them in your distribution package by placing them in **res/raw** folder of your project hierarchy

    Resources myRes = getResources();
    InputStream f = myRes.openRawResource(R.raw.myfilename);
    
---
# Working with the file system
- Android offers two corresponding methods via the application Context, **getDir** and **getExternalFilesDir**, both of which return a File object that contains the path to the internal and external application file storage directory, respectively
- All files stored in thess directories or the subfolders will be erased when your application is uninstalled
- Android offers the **openFileInput** and **openFileOutput** methods to simplify reading and writing streams from and to files stored in the application's sandbox


title: ActionBar
author: ivan
---
# ActionBar
##### by ivan 2014.3
---
# ActionBar
ActionBar was introduced in the Android 3.0 SDK for tablets and is now available in the Support Library for compatibility with Android 2.1 (API level 7) and above

![actionbar](./actionbar@2x.png)

--
## Caution
- If supporting API levels *lower* than 11:

        import android.support.v7.app.ActionBar
    
- If supporting *only* API level 11 and higher:

        import android.app.ActionBar

--
## What does it do?
- Provides a dedicated space for giving your app an identity and indicating the user's location in the app
- Makes important actions prominent and accessible in predictable way(such search)
- Support consistent navigation and view switching within apps(with tabs or drop-down lists)

---
# Adding the Action Bar
1. Create your activity by extending [ActionBarActivity](http://developer.android.com/reference/android/support/v7/app/ActionBarActivity.html)
2. Use(or extend) one of the [Theme.AppCompat](http://developer.android.com/reference/android/support/v7/appcompat/R.style.html#Theme_AppCompat) themes for your activity
    
--
## Removing the action bar
You can hide the action bar at runtime by calling hide()

    ActionBar actionBar = getSupportActionBar();
    actionBar.hide();
    
--
## Using a logo instead of an icon
By default, the system uses your application icon in the action bar, as specified by the icon attribute in the <application> or <activity> element. However, if you also specify the logo attribute, then the action bar uses the logo image instead of the icon

--
# Adding Action Items
--
## Handling clicks on action items
--
## Using split aciton bar
Split action bar provides a separate bar at the bottom of the screen to display all action items when the activity is running on a narrow screen (such as a portrait-oriented handset)

--

![actionbar-splitaction@2x](./actionbar-splitaction@2x.png)

--

1. Add uiOptions="splitActionBarWhenNarrow" to each <activity> element or to the <application> element. This attribute is understood only by API level 14 and higher (it is ignored by older versions)
2. To support older versions, add a <meta-data> element as a child of each <activity> element that declares the same value for "android.support.UI_OPTIONS"

---
# Navigating Up with the App Icon

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        ...
    }
    
    
    <application ... >
    ...
        <activity
            android:name="com.example.myfirstapp.MainActivity" ...>
            ...
        </activity>
        <activity
            android:name="com.example.myfirstapp.DisplayMessageActivity"
            android:label="@string/title_activity_display_message"
            android:parentActivityName="com.example.myfirstapp.MainActivity" >
            <meta-data
                android:name="android.support.PARENT_ACTIVITY"
                android:value="com.example.myfirstapp.MainActivity" />
        </activity>
    </application>

--
## You can
override *getSupportParentActivityIntent()* and *onCreateSupportNavigateUpTaskStack()* in your activity

---
# Adding an Action View
- An *action view* is a widget that appears in the action bar as a substitute for an action button
- An *action view* provides fast access to rich actions without changing activities or fragments, and without replacing the action bar

--
## Xml snippet
    
    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android"
          xmlns:yourapp="http://schemas.android.com/apk/res-auto" >
        <item android:id="@+id/action_search"
              android:title="@string/action_search"
              android:icon="@drawable/ic_action_search"
              yourapp:showAsAction="ifRoom|collapseActionView"
              yourapp:actionViewClass="android.support.v7.widget.SearchView" />
    </menu>
    
Notice that the *showAsAction* attribute also includes the *"collapseActionView"* value. This is optional and declares that the action view should be collapsed into a button

--

If you need to configure the action view(such as to add event listeners), you can do so during the *onCreateOptionsMenu()* callback. You can acquire the action view object by calling the static method *MenuItemCompat.getActionView()* and passing it the corresponding *MenuItem*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        // Configure the search info and add any event listeners
        ...
        return super.onCreateOptionsMenu(menu);
    }

--
# Handling collapsible action views
To preserve the action bar space, you can collapse your action view into an action button. When collapsed, the system might place the action into the action overflow, but the action view still appears in the action bar when the user selects it, You can make your action view collapsible by adding *"collapseActionView"* to the *showAction* attribute

--
## Code snippet

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        MenuItem menuItem = menu.findItem(R.id.actionItem);
        ...
    
        // When using the support library, the setOnActionExpandListener() method is
        // static and accepts the MenuItem object as an argument
        MenuItemCompat.setOnActionExpandListener(menuItem, new OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }
    
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
    }
    
    
---
# Adding an Action Provider
- Similar to an action view, an *action provider* replaces an action button with a customized layout. However, unlike an action view, an action provider takes control of all the action's behaviors and an action provider can display a submenu when pressed
- You can build your own action provider by extending the [ActionProvider](http://developer.android.com/reference/android/support/v4/view/ActionProvider.html) class


--
## Using the ShareActionProvider

    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android"
          xmlns:yourapp="http://schemas.android.com/apk/res-auto" >
        <item android:id="@+id/action_share"
              android:title="@string/share"
              yourapp:showAsAction="ifRoom"
              yourapp:actionProviderClass="android.support.v7.widget.ShareActionProvider"
              />
        ...
    </menu>

    private ShareActionProvider mShareActionProvider;
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
    
        // Set up ShareActionProvider's default share intent
        MenuItem shareItem = menu.findItem(R.id.action_share);
        mShareActionProvider = (ShareActionProvider)
                MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultIntent());
    
        return super.onCreateOptionsMenu(menu);
    }
    
    /** Defines a default (dummy) share intent to initialize the action provider.
      * However, as soon as the actual content to be used in the intent
      * is known or changes, you must update the share intent by again calling
      * mShareActionProvider.setShareIntent()
      */
    private Intent getDefaultIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        return intent;
    }
    
--
## Creating a custom action provider
1. Extend the **ActionProvider** class and implement its callback methods as appropriate
2. onCreateActionView(MenuItem)
3. onPerformDefaultAction()

---
# Adding Navigation Tabs
1. Implement the ActionBar.TabListener interface. This interface provides callbacks for tab events, such as when the user presses one so you can swap the tabs
2. For each tab you want to add, instantiate an ActionBar.Tab and set the ActionBar.TabListener by calling setTabListener(). Also set the tab's title and with setText() (and optionally, an icon with setIcon())
3. Then add each tab to the action bar by calling addTab()

---
# Adding Drop-down Navigation
1. Create a [SpinnerAdapter](http://developer.android.com/reference/android/widget/SpinnerAdapter.html) that provides the list of selectable items for the drop-down and the layout to use when drawing each item in the list
2. Implement [ActionBar.OnNavigationListener](http://developer.android.com/reference/android/support/v7/app/ActionBar.OnNavigationListener.html) to define the behavior that occurs when the user selects an item from the list
3. During your activity's onCreate() method, enable the action bar's drop-down list by calling *setNavigationMode(NAVIGATION_MODE_LIST)*
4. Set the callback for the drop-down list with *setListNavigationCallbacks()*

---
# [Styling the Action Bar](http://developer.android.com/guide/topics/ui/actionbar.html#style)
author: ivan
title: User Interface 1
---
# User Interface 1
##### by ivan 2014.2
---
# UI Overview
- All user interface elements in an Android app are built using **View** and **ViewGroup** objects
- A **View** is an object that draws something on the screen that user can interact with
- A **ViewGroup** is an object that holds other **View**(and **ViewGroup**) objects in order to define that layout of the interface

---
# User Interface Layout
![view hierarchy](./viewgroup.png)

---
# Layouts
- Declare UI elements in XML
- Instantiate layout elements at runtime

--
## Layout Parameters
- *wrap_content* tells your view to size itself to the dimensions required by its content
- *fill_parent*(renamed *match_parent* in API Level 8) tells your view to become as big as its parent view group will allow

--
## Layout Position
- getLeft(): returns the left, or X
- getTop(): returns the top, or Y
- getRigth(): getLeft() + getWidth()
- getBottom()

--
## Size,Padding and Margins
- getMeasuredWidth vs getWidth
- getMeasuredHeight vs getHeight
- setPadding getPadding*
- Even though a view can define a padding, it does not provide any support for margins. However, view groups provide such a support. Refer to **ViewGroup** and **ViewGroup.MarginLayoutParams** for further information

--
## Common layouts
- Liner layout
- ![linearlayout](./linearlayout-small.png)
- Relative layout
- ![relativelayout](./relativelayout-small.png)
--
## Liner layout
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="fill_parent"
        android:layout_height="fill_parent"
        android:paddingLeft="16dp"
        android:paddingRight="16dp"
        android:orientation="vertical" >
        <EditText
            android:layout_width="fill_parent"
            android:layout_height="wrap_content"
            android:hint="@string/to" />
        <EditText
            android:layout_width="fill_parent"
            android:layout_height="wrap_content"
            android:hint="@string/subject" />
        <EditText
            android:layout_width="fill_parent"
            android:layout_height="0dp"
            android:layout_weight="1"
            android:gravity="top"
            android:hint="@string/message" />
        <Button
            android:layout_width="100dp"
            android:layout_height="wrap_content"
            android:layout_gravity="right"
            android:text="@string/send" />
    </LinearLayout>
    
    
--
## Relative layout
    <?xml version="1.0" encoding="utf-8"?>
    <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="fill_parent"
        android:layout_height="fill_parent"
        android:paddingLeft="16dp"
        android:paddingRight="16dp" >
        <EditText
            android:id="@+id/name"
            android:layout_width="fill_parent"
            android:layout_height="wrap_content"
            android:hint="@string/reminder" />
        <Spinner
            android:id="@+id/dates"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_below="@id/name"
            android:layout_alignParentLeft="true"
            android:layout_toLeftOf="@+id/times" />
        <Spinner
            android:id="@id/times"
            android:layout_width="96dp"
            android:layout_height="wrap_content"
            android:layout_below="@id/name"
            android:layout_alignParentRight="true" />
        <Button
            android:layout_width="96dp"
            android:layout_height="wrap_content"
            android:layout_below="@id/times"
            android:layout_alignParentRight="true"
            android:text="@string/done" />
    </RelativeLayout>

--
## List View
![listview](./listview.png)

--
## List View Sample
    public class ListViewLoader extends ListActivity
            implements LoaderManager.LoaderCallbacks<Cursor> {
    
        // This is the Adapter being used to display the list's data
        SimpleCursorAdapter mAdapter;
    
        // These are the Contacts rows that we will retrieve
        static final String[] PROJECTION = new String[] {ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME};
    
        // This is the select criteria
        static final String SELECTION = "((" + 
                ContactsContract.Data.DISPLAY_NAME + " NOTNULL) AND (" +
                ContactsContract.Data.DISPLAY_NAME + " != '' ))";
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
    
            // Create a progress bar to display while the list loads
            ProgressBar progressBar = new ProgressBar(this);
            progressBar.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT, Gravity.CENTER));
            progressBar.setIndeterminate(true);
            getListView().setEmptyView(progressBar);
    
            // Must add the progress bar to the root of the layout
            ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
            root.addView(progressBar);
    
            // For the cursor adapter, specify which columns go into which views
            String[] fromColumns = {ContactsContract.Data.DISPLAY_NAME};
            int[] toViews = {android.R.id.text1}; // The TextView in simple_list_item_1
    
            // Create an empty adapter we will use to display the loaded data.
            // We pass null for the cursor, then update it in onLoadFinished()
            mAdapter = new SimpleCursorAdapter(this, 
                    android.R.layout.simple_list_item_1, null,
                    fromColumns, toViews, 0);
            setListAdapter(mAdapter);
    
            // Prepare the loader.  Either re-connect with an existing one,
            // or start a new one.
            getLoaderManager().initLoader(0, null, this);
        }
    
        // Called when a new Loader needs to be created
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            // Now create and return a CursorLoader that will take care of
            // creating a Cursor for the data being displayed.
            return new CursorLoader(this, ContactsContract.Data.CONTENT_URI,
                    PROJECTION, SELECTION, null, null);
        }
    
        // Called when a previously created loader has finished loading
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            // Swap the new cursor in.  (The framework will take care of closing the
            // old cursor once we return.)
            mAdapter.swapCursor(data);
        }
    
        // Called when a previously created loader is reset, making the data unavailable
        public void onLoaderReset(Loader<Cursor> loader) {
            // This is called when the last Cursor provided to onLoadFinished()
            // above is about to be closed.  We need to make sure we are no
            // longer using it.
            mAdapter.swapCursor(null);
        }
    
        @Override 
        public void onListItemClick(ListView l, View v, int position, long id) {
            // Do something when a list item is clicked
        }
    }
    
--
## Grid View
![gridview](./gridview.png)

--
## Grid View Example
    <?xml version="1.0" encoding="utf-8"?>
    <GridView xmlns:android="http://schemas.android.com/apk/res/android" 
        android:id="@+id/gridview"
        android:layout_width="fill_parent" 
        android:layout_height="fill_parent"
        android:columnWidth="90dp"
        android:numColumns="auto_fit"
        android:verticalSpacing="10dp"
        android:horizontalSpacing="10dp"
        android:stretchMode="columnWidth"
        android:gravity="center"
    />
    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
    
        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(HelloGridView.this, "" + position, Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    
--
## GridView Adapter
    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
    
        public ImageAdapter(Context c) {
            mContext = c;
        }
    
        public int getCount() {
            return mThumbIds.length;
        }
    
        public Object getItem(int position) {
            return null;
        }
    
        public long getItemId(int position) {
            return 0;
        }
    
        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {  // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }
    
            imageView.setImageResource(mThumbIds[position]);
            return imageView;
        }
    
        // references to our images
        private Integer[] mThumbIds = {
                R.drawable.sample_2, R.drawable.sample_3,
                R.drawable.sample_4, R.drawable.sample_5,
                R.drawable.sample_6, R.drawable.sample_7,
                R.drawable.sample_0, R.drawable.sample_1,
                R.drawable.sample_2, R.drawable.sample_3,
                R.drawable.sample_4, R.drawable.sample_5,
                R.drawable.sample_6, R.drawable.sample_7,
                R.drawable.sample_0, R.drawable.sample_1,
                R.drawable.sample_2, R.drawable.sample_3,
                R.drawable.sample_4, R.drawable.sample_5,
                R.drawable.sample_6, R.drawable.sample_7
        };
    }


--
## TableLayout
The TableLayout layout manager is an extension of **LinearLayout**. This layout manager structures its child controls into rows and columns

    <?xml version="1.0" encoding="utf-8"?>
    <TableLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent" android:layout_height="fill_parent">
        <TableRow>
            <TextView android:text="First Name:"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"/>
            <EditText android:text="Edgar"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"/>
        </TableRow>
        <TableRow>
            <TextView android:text="Last Name:"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content" />
            <EditText android:text="Poe"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content" />
        </TableRow>
    </TableLayout>
    
    
--
## FrameLayout
It was mainly used to display a single item, You mainly use this utility layout class to dynamically display a single view, but you can populate it with many items, setting one to visible while the other are invisible


    <?xml version="1.0" encoding="utf-8"?>
        <FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
        android:id="@+id/frmLayout"
        android:layout_width="fill_parent" android:layout_height="fill_parent">
    <ImageView
        android:id="@+id/oneImgView" android:src="@drawable/one"
        android:scaleType="fitCenter"
        android:layout_width="fill_parent" android:layout_height="fill_parent"/>
    <ImageView
        android:id="@+id/twoImgView" android:src="@drawable/two"
        android:scaleType="fitCenter"
        android:layout_width="fill_parent" android:layout_height="fill_parent"
        android:visibility="gone" />
    </FrameLayout>

---
# Customizing the layout for various Device configurations
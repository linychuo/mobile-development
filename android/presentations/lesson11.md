title: Menu
author: ivan
---
# Menu
##### by ivan 2014.3
---
# Menu
Begining with Android3.0(API 11), Android-powered devices are no longer required to provide a dedicated *Menu* button. With this change, Android apps should migrate away from a dependence on the traditional 6-item menu panel and instead provide an action bar to present common user actions

- Options menu and action bar
- Context menu and contextual action mode
- Popup menu

---
# Defining a menu in xml
For all menu types, Android provides a standard XML format to define menu items. Instead of building a menu in your activity's code, you should define a menu and all its items in an XML menu resource, You can then inflate the menu resource(laod it as a **Menu** object) in your activity or fragment

--

Using a menu resource is a good practice for a few reasons:

- It's easier to visualize the menu structure in XML
- It separates the content for the menu from your application's behavioral code
- It allows you to create alternative menu configurations for different platform versions, screen sizes, and other configurations by leveraging the [app resources](http://developer.android.com/guide/topics/resources/index.html) framework

--
## res/menu/
- &lt;menu&gt; 

    Defines a [Menu](http://developer.android.com/reference/android/view/Menu.html), which is a container for menu items. A &lt;menu&gt; element be the root node for the file and can hold one or more &lt;item&gt; and &lt;group&gt; elements

- &lt;item&gt; 
    
    Creates a [MenuItem](http://developer.android.com/reference/android/view/MenuItem.html), which represents a single item in a menu. This element may contain a nested &lt;menu&gt; element in order to create a submenu

- &lt;group&gt; 

    An optional , invisible container for &lt;item&gt; elements. It allows you to categorize menu items so they share properties such as active state and visibility

--
## Xml snippet

    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android">
        <item android:id="@+id/new_game"
              android:icon="@drawable/ic_new_game"
              android:title="@string/new_game"
              android:showAsAction="ifRoom"/>
        <item android:id="@+id/help"
              android:icon="@drawable/ic_help"
              android:title="@string/help" />
    </menu>
    
    
--
## Attributes of &lt;item&gt;
- android:id

    A resource ID that's unique to the item, which allows the application can recognize the item when the user selects it
    
- android:icon 
    
    A reference to a drawable to use as the item's icon
    
- android:title 
    
    A reference to a string use as the item's title
    
- android:showAsAction 

    Specifies when and how this item should appear as an action item in the [action bar](http://developer.android.com/guide/topics/ui/actionbar.html)

--

These are the most important attributes you should use, For information about all the supported attributes, see the [Menu Resource](http://developer.android.com/guide/topics/resources/menu-resource.html) document
    
---
# Creating an options menu
The options menu is where you should include actions and other options that are relevant to the current activity context, such as "Search", "Compose email", and "Settings"


--

- If you've developed your application for **Android 2.3.x(API 10) or lower**, the contents of your options menu appear at the bottom of the screen when the user presses the *Menu* button, If your menu includes more than six items, Android places sixth item and the rest into the overflow menu, which the user can open by selecting *More*
- If you've developed your application for **Android3.0(API 11) and higher**, items from the options menu are available in the action bar. By default, the system places all items in the action overflow, which the user can reveal with action overflow icon on the right side of the action bar. To enable quick the action bar by adding **android:showAsAction="ifRoom"** to the corresponding &lt;item&gt; elements

--
## Code snippet
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.new_game:
                newGame();
                return true;
            case R.id.help:
                showHelp();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
---
# Changing menu items at runtime
- On Android 2.3.x and lower, the system calls onPrepareOptionsMenu() each time the user opens the options menu (presses the Menu button).

- On Android 3.0 and higher, the options menu is considered to always be open when menu items are presented in the action bar. When an event occurs and you want to perform a menu update, you must call invalidateOptionsMenu() to request that the system call onPrepareOptionsMenu().


---
# Creating contextual Menus
A contextual menu offers actions that affect a specific item or context frame in the UI, You can provide a context menu for any view, but they are most often used for items in a [ListView](http://developer.android.com/reference/android/widget/ListView.html), [GridView](http://developer.android.com/reference/android/widget/GridView.html), or other view collections in which the user can perform direct actions on each item

--
## When you use it?
![menu-context](./menu-context.png)

--

- In a floating context menu. A menu appears as a floating list of menu items (similar to a dialog) when the user performs a long-click (press and hold) on a view that declares support for a context menu. Users can perform a contextual action on one item at a time
- In the contextual action mode. This mode is a system implementation of [ActionMode](http://developer.android.com/reference/android/view/ActionMode.html) that displays a contextual action bar at the top of the screen with action items that affect the selected item(s). When this mode is active, users can perform an action on multiple items at once (if your app allows it)


---
# Creating a floating context menu
1. Register the View to which the context menu should be associated by calling *registerForContextMenu()* and pass it the View
2. Implement the *onCreateContextMenu* method in your *Activity* or *Fragment*
3. Implement *onContextItemSelected()*

--

- If your activity uses a *ListView* or *GridView* and you want each item to provide the same context menu, register all items for a context menu by passing the *ListView* or *GridView* to *registerForContextMenu()*
- When the registered view receivea long-click event, the system calls your *onCreateContextMenu()* method
- When the user selects a menu item, the system calls this method so you can perform the appropriate action

--
## Code snippet

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit:
                editNote(info.id);
                return true;
            case R.id.delete:
                deleteNote(info.id);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    

---
# Using the contextual action mode
The contextual action mode is a system implementation of ActionMode that focuses user interaction toward performing contextual actions. When a user enables this mode by selecting an item, a contextual action bar appears at the top of the screen to present actions the user can perform on the currently selected item(s). 

--

While this mode is enabled, the user can select multiple items (if you allow it), deselect items, and continue to navigate within the activity (as much as you're willing to allow). The action mode is disabled and the contextual action bar disappears when the user deselects all items, presses the BACK button, or selects the Done action on the left side of the bar


---
# Enabling the contextual action mode for individual views
1. Implement the [ActionMode.Callback](http://developer.android.com/reference/android/view/ActionMode.Callback.html) interface. In its callback methods, you can specify the actions for the contextual action bar, respond to click events on action items, and handle other lifecycle events for the action mode
2. Call *startActionMode()* when you want to show the bar(such as when user long-clicks the view)

--
## Code snippet

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
    
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }
    
        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }
    
        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_share:
                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }
    
        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
    
    
    someView.setOnLongClickListener(new View.OnLongClickListener() {
        // Called when the user long-clicks on someView
        public boolean onLongClick(View view) {
            if (mActionMode != null) {
                return false;
            }
    
            // Start the CAB using the ActionMode.Callback defined above
            mActionMode = getActivity().startActionMode(mActionModeCallback);
            view.setSelected(true);
            return true;
        }
    });
    
    
---
# Enabling batch contextual actions in a ListView and GridView
- Implement the [AbsListView.MultiChoiceModeListener](http://developer.android.com/reference/android/widget/AbsListView.MultiChoiceModeListener.html) interface and set it for the view group with *setMultiChoiceModeListener()*. In the listener's callback methods, you can specify the actions for the contextual action bar, respond to click events on action items, and handle other callbacks inherited from the ActionMode.Callback interface
- Call *setChoiceMode()* with the **CHOICE_MODE_MULTIPLE_MODAL** argument

    
--
## Code snippet
    ListView listView = getListView();
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {
    
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position,
                                              long id, boolean checked) {
            // Here you can do something when items are selected/de-selected,
            // such as update the title in the CAB
        }
    
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            // Respond to clicks on the actions in the CAB
            switch (item.getItemId()) {
                case R.id.menu_delete:
                    deleteSelectedItems();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }
    
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate the menu for the CAB
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context, menu);
            return true;
        }
    
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // Here you can make any necessary updates to the activity when
            // the CAB is removed. By default, selected items are deselected/unchecked.
        }
    
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // Here you can perform updates to the CAB due to
            // an invalidate() request
            return false;
        }
    });    
  

---
# Creating a popup menu
A [PopupMenu](http://developer.android.com/reference/android/widget/PopupMenu.html) is a modal menu anchored to a View, It appears below the anchor view if these is room, or above the view otherwise, It's useful for:

- Providing an overflow-style menu for actions that relate to specific content
- Providing a second part of a command sentence(such as button marked "Add" the produces a popup menu with different "Add" options)
- Providing a drop-down similar to [Spinner](http://developer.android.com/reference/android/widget/Spinner.html) that does not retain a persistent selection
  
  
--

If you define your menu in XML, here's how you can show the popup menu:

1. Instantate a PopupMenu with its constructor, which takes the current application Context and the View to which the menu should be anchored
2. Use MenuInflater to inflate your menu resource into the Menu object returned by PopupMenu.getMenu(). On API level 14 and above, you can use PopupMenu.inflate() instead
3. Call PopupMenu.show()


--
## Code snippet

    <ImageButton
        android:layout_width="wrap_content" 
        android:layout_height="wrap_content" 
        android:src="@drawable/ic_overflow_holo_dark"
        android:contentDescription="@string/descr_overflow_button"
        android:onClick="showPopup" />
        
        
    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions, popup.getMenu());
        popup.show();
    }
    
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.archive:
                archive(item);
                return true;
            case R.id.delete:
                delete(item);
                return true;
            default:
                return false;
        }
    }
    
    
---
# Creating Menu Groups
A menu group is a collection of menu items that share certain traits. With a group, you can:

- Show or hide all items with *setGroupVisible()*
- Enable or disable all items with *setGroupEnabled()*
- Specify whether all items are checkable with *setGroupCheckable()*

--
## Xml snippet
    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android">
        <item android:id="@+id/menu_save"
              android:icon="@drawable/menu_save"
              android:title="@string/menu_save" />

        <group android:id="@+id/group_delete">
            <item android:id="@+id/menu_archive"
                  android:title="@string/menu_archive" />
            <item android:id="@+id/menu_delete"
                  android:title="@string/menu_delete" />
        </group>
    </menu>


--
## Using checkable menu items

    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android">
        <group android:checkableBehavior="single">
            <item android:id="@+id/red"
                  android:title="@string/red" />
            <item android:id="@+id/blue"
                  android:title="@string/blue" />
        </group>
    </menu>


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.vibrate:
            case R.id.dont_vibrate:
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    
---
# Adding Menu Items Based on an Intent
To add menu items based on available activities that accept an intent:

1. Define an intent with the category CATEGORY_ALTERNATIVE and/or CATEGORY_SELECTED_ALTERNATIVE, plus any other requirements
2. Call Menu.addIntentOptions(). Android then searches for any applications that can perform the intent and adds them to your menu

>If there are no applications installed that satisfy the intent, then no menu items are added

>Note: CATEGORY_SELECTED_ALTERNATIVE is used to handle the currently selected element on the screen. So, it should only be used when creating a Menu in onCreateContextMenu()

--
## Code snippet

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
    
        // Create an Intent that describes the requirements to fulfill, to be included
        // in our menu. The offering app must include a category value of Intent.CATEGORY_ALTERNATIVE.
        Intent intent = new Intent(null, dataUri);
        intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
    
        // Search and populate the menu with acceptable offering applications.
        menu.addIntentOptions(
             R.id.intent_group,  // Menu group to which new items will be added
             0,      // Unique item ID (none)
             0,      // Order for the items (none)
             this.getComponentName(),   // The current activity name
             null,   // Specific items to place first (none)
             intent, // Intent created above that describes our requirements
             0,      // Additional flags to control items (none)
             null);  // Array of MenuItems that correlate to specific items (none)
    
        return true;
    }


--
## Allowing your activity to be added to other menus
- You can also offer the services of your activity to other applications, so your application can be included in the menu of others (reverse the roles described above).
- To be included in other application menus, you need to define an intent filter as usual, but be sure to include the CATEGORY_ALTERNATIVE and/or CATEGORY_SELECTED_ALTERNATIVE values for the intent filter category


    <intent-filter label="@string/resize_image">
        ...
        <category android:name="android.intent.category.ALTERNATIVE" />
        <category android:name="android.intent.category.SELECTED_ALTERNATIVE" />
        ...
    </intent-filter>

author: ivan
title: User Interface 2
---
# User Interface 2
##### by ivan 2014.2
---
# Input Controls
- Button
- Text Fields
- Checkboxes
- Radio Buttons
- Toggle Buttons
- Spinners
- Pickers

--
## Button
- ImageButton
- android:drableLeft = "@drable/button_icon"

--
## Custom background
- Create three bitmaps for the button background that represent that *default*, *pressed*, and *focused* button states
- Place the bitmaps into the *res/drawable/* directory of your project. Be sure each bitmap is named properly to reflect the button state that they each represnet, such as *button_default.9.png*
- Create a new XML file in the *res/drawable/* directory(name it something like *button_custom.xml*)

--
## button_custom.xml
    <?xml version="1.0" encoding="utf-8"?>
    <selector xmlns:android="http://schemas.android.com/apk/res/android">
        <item android:drawable="@drawable/button_pressed"
              android:state_pressed="true" />
        <item android:drawable="@drawable/button_focused"
              android:state_focused="true" />
        <item android:drawable="@drawable/button_default" />
    </selector>
    
--

    <Button android:id="@+id/button_send"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/button_send"
        android:onClick="sendMessage"
        android:background="@drawable/button_custom"  />
        
---
# TextFields
android:inputType

- text
- textEmailAddress
- textUri
- number
- phone
- textCapsentences
- textCapWords
- textAutoCorrect
- textPassword
- textMultiLine

--
## Specifying Keyboard Actions
- android:imeOptions
- EditorInfo.IME_ACTION_SEND
- android:imeActionLabel

--
## Providing Auto-complete Suggestions
AutoCompleteTextView
---
# Input Events
- onClick: View.OnClickListener
- onLongClick: View.OnLongClickListener
- onFocusChange: View.OnFocusChangeListener
- onKey: View.OnKeyListener
- onTouch: View.OnTouchListener
- onCreateContextMenu: View.OnCreateContextMenu

--
## Note
- **onClick** callback in *View.OnClickListener* has no return value
- **onLongClick**: This returns a boolean to indicate whether you have consumed that event and it should not be carried further. That is, return *true* to indicate that you have handle the event and it should stop here; return *false* if you have not handled it and/or the event should continue to any other on-click listeners
- **onKey**
- **onTouch**: This event can have multiple actions that follow each other. So, if you return *false* when the down action event is received, you indeicate that you have not consumed the event and are also not interested in subsequent actions from this event. Thus, you will not be called for any other actions within the event, such as a finger gesture, or the eventual up action event
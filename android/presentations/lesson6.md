author: ivan
title: Broadcast Receivers
---
# Broadcast Receivers
##### by ivan 2014.3
---
# What is Broadcast Receivers?
- A broadcast receiver is a component that can respond to a broadcast message by a client
- The message itself is an Android broadcast intent
- A broadcast intent(message) can invoke(or be respond to by) more than one receiver

---
# Sending a Broadcast

    private void testSendBroadcast(Activity activity){
        //Create an intent with an action
        String uniqueActionString = "com.androidbook.intents.testbc";
        Intent broadcastIntent = new Intent(uniqueActionString);
        broadcastIntent.putExtra("message", "hello world");
        
        Log.d(TAG, "before");
        activity.sendBroadcast(broadcastIntent);
        Log.d(TAG, "after");
    }
    
    
---
# Coding a simple receiver

    public class TestReceiver extends BroadcastReceiver{
        private static final String TAG = "TestReceiver";
        
        @Override
        public void onReceive(Context context, Intent intent){
            Log.d(TAG, "inten = " + intent);
            String message = intent.getStringExtra("message");
            Log.d(TAG, message);
        }
    }
    
---
# Registering a receiver in the Manifest file

    <manifest>
        <application>
            ...
            <activity ..>
            </activity>
            ...
            <receiver android:name=".TestReceiver">
                <intent-filter>
                    <action android:name="com.androidbook.intents.testbc"/>
                </intent-filter>
            </receiver>
        </application>
    </manifest>
    
    
---
# Accommodating multiple receivers

    public class TestReceiver2 extends BroadcastReceiver{
        private static final String TAG = "TestReceiver2";
        
        @Override
        public void onReceive(Context context, Intent intent){
            Log.d(TAG, "inten = " + intent);
            String message = intent.getStringExtra("message");
            Log.d(TAG, message);
        }
    }
    
    <receiver android:name=".TestReceiver2">
        <intent-filter>
            <action android:name="com.androidbook.intents.testbc"/>
        </intent-filter>
    </receiver>
    
---
# Conclusion
- The proves that the main thread is going around in a round-robin fashion and finally attending to the broadcast receivers at a later time from the message queue
- So, the *sendBroadcast()* is clearly an asynchronous message that lets the main thread get back to its queue

---
# TimeDelayReceiver

    public class TestTimeDelayReceiver extends BroadcastReceiver{
        private static final String TAG = "TestTimeDelayReceiver";
        
        @Override
        public void onReceive(Context context, Intent intent){
            Log.d(TAG, "inten = " + intent);
            Log.d(TAG, "going to sleep for 2 secs");
            Thread.sleep(2);
            Log.d(TAG, "wake up");
            String message = intent.getStringExtra("message");
            Log.d(TAG, message);
        }
    }

---
# Out-of-Process Receivers
---
# Broadcasting Ordered Intents
When the order in which the Broadcast Receivers receive the Intent is important, you can use *sendOrderedBroadcast*, as follows:

    String requiredPermission = "com.paad.MY_BORADCAT_PERMISSION";
    sendOrderedBroadcast(intent, requiredPermission);
    
    
    <receiver android:name=”.MyOrderedReceiver”
        android:permission=”com.paad.MY_BROADCAST_PERMISSION”>
        <intent-filter android:priority=”100”>
            <action android:name=”com.paad.action.ORDERED_BROADCAST” />
        </intent-filter>
    </receiver>

---
# Broadcasting Sticky Intents
Sticky Intents are useful variations of Broadcast Intents that persist the values associated with their last broadcast, returning them as an Intent when a new Receiver is registered to receive the broadcast

---
# Local Broadcast Manager
- The Local Broadcast Manager was introduced to the Anroid Support Library to simplify the process of registering for, and sending, Broadcast Intents between components within your application
- Because of the reduced broadcast scope, using the Local Broadcast Manager is more efficient than sending a global broadcat. It also ensures that the Intent you broadcast cannot be received by any components outside your application, ensuring that there is no risk of leaking private or sensitive data, such as location information

--
- Similarly, other applications can't transmit broadcasts to your Receivers, negating the risk of these Receivers becoming vectors for security exploits

--
## Using it
You must first include the Android Support Library in your appliation

    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
    lbm.registerReceiver(new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent){
            //
        }
    }, new IntentFilter(LOCAL_ACTION));


The Local Broadcast Manager also includes a *sendBroadcastSync* method that operates synchronously, blocking until each registered Receiver has been dispatched

---
# Listening for Native Broadcast Intents
- ACTION_BOOT_COMPLETED
- ACTION_CAMERA_BUTTON
- ACTION_DATE_CHANGED  and ACTION_TIME_CHANGED
- ACTION_MEDIA_EJECT
- ACTION_MEDIA_MOUNTED and ACTION_MEDIA_UNMOUNTED
- ACTION_NEW_OUTGOING_CALL
- ACTION_SCREEN_OFF and ACTION_SCREEN_ON
- ACTION_TIMEZONE_CHANGED

1. Insert into below lines into the AndroidManifest.xml

        <uses-permission android:name="android.permission.INTERNET"/>
        <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
        <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
        <activity android:name="com.tencent.tauth.AuthActivity"
                    android:noHistory="true"
                    android:launchMode="singleTask">
            <intent-filter>
                <action android:name="android.intent.action.VIEW"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <category android:name="android.intent.category.BROWSABLE"/>
                <data android:scheme="$APP_ID$"/>
            </intent-filter>
        </activity>
        <activity android:name="com.tencent.connect.common.AssistActivity"
                      android:theme="@android:style/Theme.Translucent.NoTitleBar"
                      android:screenOrientation="portrait"/>

2. Placing below two jar files into libs
    - mta-sdk-1.0.0.jar
    - open_sdk.jar
                  
                  
3. Creating a new Activity and doing according by below steps

    - define a instance variable: Tencent tencent;
    - initial the variable: tecent = Tencent.createInstance(APP_ID, this.getApplicationContext());
    - set onclick listener with button
        
            tencent.login(MainActivity.this, "all", new IUiListener() {
                  @Override
                  public void onComplete(Object o) {
                    JSONObject response = (JSONObject) o;
                    Log.d(TAG, "response = " + response);
                  }
        
                  @Override
                  public void onError(UiError uiError) { }
        
                  @Override
                  public void onCancel() {}
            });

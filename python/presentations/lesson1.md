title: Introduction Python
author: Ivan
---
# Introduction Python
##### by ivan 2014.4

---
# The History of Python
Python was created by **Guido van Rossum** in the Netherlands in 1990 and was named after the popular British comedy troupe *Monty Python's Flying Cirus*,Van Rossum developed Python as a hobby, and Python has become a popular programming language widely used in industry and academia due to its simple, concise, and intuitive syntax and extensive library

--
## Features
- Python is a *general-purpose programming language*
- Python is *interpreted*
- Python is an *object-oriented programming (OOP)* language

---
# Getting started with Python
- Launching python
- Creating python source code files

--
## Welcome.py

    # Displaying two message
    print("Welcome to python")
    print "Python is fun"
    

--
## Special characters
- **()**        Used with functions
- **\#**        Precedes a comment line
- **" "**       Encloses a string (i.e., sequence of characters)
- **''' '''**   Encloses a paragraph comment 

--
## Proper spacing

    print(3+4*4) 
    print(3 + 4 * 4)
    
---
# Writing a simple program
Let’s first consider the simple problem of computing the area of a circle. How do we write a program for solving this problem?

--
## ComputeArea.py

    radius = 20
    area = radius * radius * 3.14159
    
    print("The area for the circle of radius", radius, "is", area)
    
--
## Reading input from the console

    variable = input("Enter a value:")

The value entered is a string. You can use the function **eval** to evaluate and convert it to a numberic value

--
## ComputeAreaWithConsoleInput.py

    radius = eval(input("Enter a value for radius: "))
    area = radius * radius * 3.14159
    print("The area for the circle of radius", radius, "is", area)
    
    
---
# Identifiers
- An identifier is a sequence of characters that consists of letters, digits, and underscores(_)
- An identifier must start with a letter or an underscore. It cannot start with a digit
- An identifier cannot be a keyword. *Keywords*, also called *reserved words*, have special meanings in Python
- An identifier can be of any length

---
## Variables, Assignment statements, and Expressions
variable = expression

- An *expression* represents a computation involving values, variables, and operators that, taken together, evaluate to a value
- If a value is assigned to multiple variables, you can use a syntax like this:
    
    i = j = k = 1
    
    which is equivalent to 
    
    k = 1
    
    j = k
    
    i = j


--
## Caution
A variable must be assigned a value before it can be used in an expression

---
# Simultaneous Assignments
Python also supports *simultaneous assignment* in syntax like this:

    var1, var2, ..., varn = exp1, exp2, ..., expn

--
## Code snippet
    
    number1, number2, number3 = eval(input("Enter three numbers separated by commas: "))
    
    average = (number1 + number2 + number3) / 3
    print("The average of", number1, number2, number3, "is", average)
    
---
## Numberic data types and operators
Python has two types of numberic data: integers and floating-point numbers

![NumericOperators](./NumericOperators.png)

---
# Type conversions and rounding

    value1 = 5.6
    value1 = int(5.6)
    print(value1)
    
    value2 = 6.5
    print(round(value2))
    print(value2)
    
 
---
# Common python functions
![built-inFunctions](./built-inFunctions.png)

---
# Strings and characters
String values must be enclosed in matching single quotes(') or double quotes("). Python does not have a data type for characters. A single-character string represents a character

    letter = 'A'
    numChar = '4'
    message = "Good morning"
    
--
## The *str* function
The **str** function can be used to convert a number into a string

    s = str(3.4)
    print(s)
    s = str(3)
    pirnt(s)
    
--
## The String concatenation operator
You can use the + operator to add two numbers. The + operator can be used to concatenate two strings

    message = "Welcome " + "to " + "python"
    print(message)
    chapterNo = 1
    s = "Chapter " + str(chapterNo)
    print(s)
    message = "Welcome to python"
    message += " and python is fun"
    print(message)
    
    
---
# Introduction to Objects and Methods
In python, all data (including numbers and strings) are actually objects
    
    s = "Welcome"
    print(s.lower())
    print(s.upper())
    s = "\t Welcome \n"
    print(s.strip())
    
    
---
# Selections
--
## Boolean types, values and expressions
![comparisonoperators](./comparisonoperators.png)

--
# if statements
    if boolean-expression:
        statement(s)
    elif boolean-expression1:
        statement1(s1)
    else:
        statement2(s2)

---
# Loops

--
# The while loop

    while loop-continuation-condition:
        statement(s)
        
--
# The for loop
    
    for i in range(initialValue, endValue):
        ...
        
    for var in sequence:
        ...

--
# break and continue

---
# Functions
--
## Defining a function

    def max(num1, num2):
        if num1 > num2:
            result = num1
        else
            result = num2
            
        return result
        
--
## Calling a function

    large = max(3, 2)
    
---
# Modularizing code
- Functions can be used to reduce redundant code and enable code reuse
- Functions can also be used to modularize code and improve a program's quality
- In Python, you can place the function definition into a file called module with the file-name extension .py. The module can be later imported into a program for reuse. The module file should be placed in the same directory with your other programs. A module can contain more than one function. Each function is a module must have a different name

---
# The scope of variables
- A variable created inside a function is referred to as a *local variable*, Local variables can only be accessed within a function. The scope of a local variable starts from its creation and continues to the end of the function that contains the variable
- In Python, you can also use *global variables*. They are created outside all functions and are accessible to all functions in their scope

--
## Example

    globalVar = 1
    
    def f1():
        localVar = 2
        print(globalVar)
        print(localVar)
        
    f1()
    print(globalVar)
    print(localVar)
    
    x = 1
    def increase():
        global x
        x = x + 1
        print(x)
        
    increase()
    print(x)
    
---
# Default arguments
Python allows you to define functions with default argument values. The default values are passed to the parameters when a function is invoked without the arguments

--
## DefaultArgsDemo.py

    def printArea(width = 1, height = 2):
        area = width * height
        print("width:", width, "\theight:", height, "\tarea:", area)
        
    printArea()
    printArea(4, 2.5)
    printArea(height = 5, width = 3)
    printArea(width = 1.2)
    printArea(height = 6.2)
    
---
# Returning multiple values
The Python *return* statement can return multiple values

--
## MultipleReturnValuesDemo.py
    
    def sort(number1, number2):
        if number1 < number2:
            return number1, number2
        else:
            return number2, number1
            
    n1, n2 = sort(3, 2)
    print("n1 is", n1)
    print("n2 is", n2)
    
---
# The str class
A **str** object is immutable; that is, its content cannot be changed once the string is created

--
## Creating Strings
    
    s1 = str()
    s2 = str("Welcome")
    
    s1 = ""
    s2 = "Welcome"

--
## Functions for Strings
- len
- max
- min

--
## Index Operator []
A string is a sequence of characters. A character in the string can be accessed through the index operator using the syntax:
    
    s[index]
    
The indexes are **0** based; that is, they range from **0** to **len(s) - 1**

--
## The Slicing Operator [start:end]

    s = "Welcome"
    print(s[1:4])
    print(s[:6]) #s[0:6]
    print(s[4:]) #s[4:7]
    print(s[1:-1]) #s[1:-1+len(s)]
    
--
## The concatenation(+) and repetition(*) Operators
    
    s1 = "Welcome"
    s2 = "Python"
    s3 = s1 + " to " + s2
    print(s3)
    s4 = 3 * s1
    print(s4)
    s5 = s1 * 3
    print(s5)
    
--
## The *in* and *not in* Operators

    s1 = "Welcome"
    print("come" in s1)
    print("come" not in s1)
    
--
## Comparing Strings

    print("green" == "glow")
    print("green" != "glow")
    print("green" > "glow")
    print("green" >= "glow")
    print("green" < "glow")
    print("green" <= "glow")
    print("ab" <= "abc")
    
--
## Iterating a String
A string is *iterable*. This means that you can use a for loop to traverse all characters in the string sequentially

    for ch in s:
        print(ch)
        
--
## Testing Strings
![stringmethod](./stringmethod.png)

--
## Searching for Substrings
![searchsubstrings.png](./searchsubstrings.png)

--
## Converting Strings
![convertingStrings](./convertingStrings.png)

--
## Stripping whitespace characters from a string
The characters ' ', \t, \f, \r, and \n are called the *whitespace characters*
![stripstring](./stripstring.png)

--
## Formatting Strings
![formattingstrings](./formattingstrings.png)

--
## Example
    
    s = "welcome to python"
    print(s.isalnum())
    print(s.islower())
    print(s.isupper())
    print(s.isspace())
    
    print("Welcome".isalpha())
    print("2012".isdigit())
    print("first number".isidentifier())
    
    print(s.endswith("thon"))
    print(s.startswith("good"))
    print(s.find("come"))
    print(s.find("become"))
    
    s1 = s.capitalize()
    print(s1)
    s2 = s.title()
    print(s2)
    
    s = "New England"
    s3 = s.lower()
    print(s3)
    s4 = s.upper()
    print(s4)
    s5 = s.swapcase()
    print(s5)
    s6 = s.replace("England", "Haven")
    print(s6)
    print(s)
    
    print(s.rfind("o"))
    print(s.count("o"))
    
    s = " Welcome to Python\t"
    s1 = s.lstrip()
    print(s1)
    s2 = s.rstrip()
    print(s2)
    s3 = s.strip()
    print(s3)
    
    s = "Welcome"
    s1 = s.center(11)
    print(s1)
    s2 = s.ljust(11)
    print(s2)
    s3 = s.rjust(11)
    print(s3)
    
---
# Lists
A list is a sequence defined by the *list* class, It contains the methods for creating, manipulating, and processing lists. Elements in a list can be accessed through an index

--
## Creating Lists

    list1 = [] # same as list()
    list2 = [2, 3, 4] # same as list([2, 3, 4])
    list3 = ["red", "green"] # same as list(["red", "green"])
    list4 = [2, "three", 4]
    
--
## List is a sequence type
Strings and lists are sequence types in Python. A string is a sequence of characters, while a list is a sequence of any elements
![commonoperationsforsequences](./commonoperationsforsequences.png)

--
## Functions for Lists

    list1 = [2, 3, 4, 1, 32]
    print(len(list1))
    print(max(list1))
    print(min(list1))
    print(sum(list1))
    
    import random
    random.shuffle(list1)
    print(list1)
    
--
## Index Operator []
--
## List Slicing [start:end]
--
## The +, *, and in/not in Operators

    list1 = [2, 3]
    list2 = [1, 9]
    list3 = list1 + list2
    print(list3)
    list4 = 3 * list1
    print(list4)
    
    print(2 in list1)
    print(2 not in list1)
    
--
## Traversing Elements in a for Loop

    for u in myList:
        print(u)
        
    for i in range(0, len(myList), 2):
        print(myList[i])
    
--
## Comparing Lists

--
## List Comprehensions

    list1 = [x for x in range(5)]
    print(list1)
    list2 = [0.5 * x for x in list1]
    print(list2)
    list3 = [x for x in list2 if x < 1.5]
    print(list3)
    
--
## List Methods
![listmethods](./listmethods.png)

--
## Example

    list1 = [2, 3, 4, 1, 4]
    list1.append(5)
    print(list1)
    print(list1.count(4))
    
    list2 = [99, 52]
    list1.extend(list2)
    print(list1)
    print(list1.index(4))
    list1.insert(1, 25)
    print(list1)
    
    print(list1.pop(2))
    print(list1.pop())
    list1.remove(1)
    print(list1)
    list1.reverse()
    print(list1)
    list1.sort()
    print(list1)
    print(list1)

---
# Tuples
Tuples are like lists, but their elements are fixed; that is, once a tuple is created, you cannot add new elements, delete elements, replace elements, or reorder the elements in the tuple

---
# Sets
Sets are like lists in that you use them for storing a collection of elements. Unlike lists, however, the elements in a set are nonduplicates and are not placed in any particular order

---
# Dictionaries
A dictionary is a container object that stores a collection of key/value pairs. It enables fast retrieval, deletion, and updating of the value by using the key
    
    
---
# Files

--
## Opening a File

    fileVariable = open(filename, mode)
    
![filemodes](./filemodes.png)

--
## Writing Data
The **open** function creates a file object, which is an instance of the **_io.TextIOWrapper** class. This class contains the methods for reading and writing data for closing the file

![textiowrapper](./textiowrapper.png)

--
## WriteDemo.py

    def main():
        # open file for output
        outfile = open("test.txt", "w")
        outfile.write("hello")
        outfile.write("world\n")
        outfile.write("python")
        outfile.close()
        
    main()
    
--
## Testing a File's Existence

    import os.path
    if os.path.isfile("test.txt"):
        print("test.txt exists")

--
## Reading Data
After a file is opened for reading data, you can use the **read** method to read a specified number of characters or all characters from the file and return them as a string, the **readline()** method to read the next line, and the **readlines()** method to read all the lines into a list of strings

--
## ReadDemo.py
    
    def main():
        infile = open("test.txt", "r")
        print(infile.read())
        infile.close()
        
        infile = open("test.txt", "r")
        s1 = infile.read(5)
        print(s1)
        infile.close()
        
        infile = open("test.txt", "r")
        line1 = infile.readline()
        line2 = infile.readline()
        print(repr(line1))
        print(repr(line2))
        infile.close()
        
        infile = open("test.txt", "r")
        print(infile.readlines())
        infile.close()
        
    main()
  
--
## Reading all data from a large file

    line = infile.readline()
    while line != '':
        # process the line here...
        # read next line
        line = infile.readline()
        
    for line in infile:
        # process the line here...
        
--
## CopyFile.py

    import os.path
    import sys
    
    def main():
        f1 = input("Enter a source file: ").strip()
        f2 = input("Enter a target file: ").strip()
        
        if os.path.isfile(f2):
            print(f2 + " already exists")
            sys.exit()
        
        infile = open(f1, "r")
        outfile = open(f2, "w")
        
        #copy from input file to output file
        countLines = countChars = 0
        for line in infile:
            countLines += 1
            countChars += len(line)
            outfile.write(line)
        print(countLines, "lines and", countChars, "chars copied")
        
        infile.close()
        outfile.close()
        
    main()
    
--
## Appending data
You can use the **a** mode to open a file for appending data to the end of an existing file

--
## AppendDemo.py

    def main():
        outfile = open("info.txt", "a")
        outfile.write("append data")
        outfile.close()
        
    main()
    
---
# Binary IO using pickle

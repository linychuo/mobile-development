import os.path

aa = {'xml': 0, 'clj': 0, "other": 0, 'java':0}

def computeLineCnt(filePath):
	try:
		f = open(filePath, 'r')
		cnt = 0
		for line in f:
			cnt += 1

		print filePath + " = " + str(cnt)
		return cnt
	except:
		return 0

def visit(arg, dirname, names):
	for item in names:
		path = os.path.join(dirname, item)
		if os.path.isfile(path):
			if len(item.split(".")) > 1:
				ext = item.split(".")
				if ext == "xml":
					aa['xml'] += computeLineCnt(path)
				elif ext == "clj":				
					aa['clj'] += computeLineCnt(path)
				elif ext == "java":
					aa['java'] += computeLineCnt(path)
				else:
					aa['other'] += computeLineCnt(path)
					#print "-----"
			
os.path.walk("/home/ivan", visit, None)
print aa

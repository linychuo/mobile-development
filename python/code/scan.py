import argparse
from socket import *
import sys

def connScan(host, port):
	try:
		skt = socket(AF_INET, SOCK_STREAM)
		skt.connect((host, port))
		skt.send("hello\r\n")
		results = skt.recv(100)
		print "[+]%d/tcp open" % port
		print "[+] " + str(results)
		skt.close()
	except:
		print "[-]%d/tcp closed" % port


def portScan(host, ports):
	try:
		ip = gethostbyname(host)
	except:
		print "[-] Cannot resolve '%s' : Unknown host" % host
		return 

	try:
		name = gethostbyaddr(ip)
		print "\n[+] Scan Results for: " + name[0]
	except:
		print "\n[+] Scan Results for: " + ip

	setdefaulttimeout(1)

	for port in ports:
		print "Scanning port " + port
		connScan(host, int(port))


def main():
	parser = argparse.ArgumentParser(description="scan specify port by specify host")
	parser.add_argument("-H", dest = "host", type = str, \
		help = "specify target host")
	parser.add_argument("-p", dest = "port", type = str,\
		help = "specify target port[s] separated by comma")

	args = parser.parse_args()
	host = args.host
	ports = str(args.port).split(",")
	if host is None or ports[0] is None:
		parser.print_help()
		sys.exit()

	portScan(host, ports)


if __name__ == '__main__':
	main()
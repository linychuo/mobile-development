import web
        
urls = (
    '/(.*)', 'hello'
    '/hello/(.*)', 'helloOther'
)
app = web.application(urls, globals())

class hello:        
    def GET(self, name):
        if not name: 
            name = 'World'
        return 'Hello, ' + name + '!'

    def POST():
    	pass

class helloOther:        
    def GET(self, name):
        if not name: 
            name = 'World'
        return 'Hello, ' + name + '!'

    def POST():
    	pass

if __name__ == "__main__":
    app.run()
a = [1,2,3,5,6,7,8,10,2,2,2,2,2,5,10,1,1,1,1,1,1,1,5,10,7,1,7,1,7,17]

def filter5Or7(original):
	if not isinstance(original, list):
		raise ValueError("The parameter 'original' must be list")
	newer = []
	for i, item in enumerate(original):
		if i % 5 == 0 or i % 7 == 0:
			if item not in newer:
				newer.append(item)

	return newer

print filter5Or7(a)
print isinstance(a, list)

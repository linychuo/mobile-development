def sort(lst, beg, end):
    if beg < end:
        first = beg
        last = end
        k = lst[first]

        while first < last:
            while first < last and lst[last] >= k:
                last = last - 1
            lst[first] = lst[last]
            while first < last and lst[first] <= k:
                first = first + 1
            lst[last] = lst[first]

        lst[first] = k
        sort(lst, beg, first - 1)
        sort(lst, last + 1, end)


def sort1(lst, beg, end):
    if beg < end:
        mid = (beg + end)/2
            sort1(lst, beg, mid)
            sort1(lst, mid, end)

        i = 0
        j = 0
        for l in range(beg, end):
            if j > end - mid or (i <= mid - beg and lst[beg + i] < lst[mid + j]):
                lst[l], lst[beg+i] = lst[beg + i], lst[l]
                i = i + 1
            else:
                lst[l], lst[mid+j] = lst[mid + j], lst[l]
                j = j + 1


def qsort(arr):
    if len(arr) <= 1:
        return arr
    else:
        pivot = arr[0]
        return qsort([x for x in arr[1:] if x < pivot]) + \
               [pivot] + \
               qsort([x for x in arr[1:] if x >= pivot])

a = [11,213,134,44,77,78,23,43]
sort1(a, 0, len(a)-1)
print a



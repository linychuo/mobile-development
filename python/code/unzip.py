import sys
import os.path
import zipfile
from threading import Thread
import argparse

def extractFile(zFile, password):
	try:
		zFile.extractall(pwd=password)
		print "[+] Password = " + password + "\n"
	except:
		pass

def main():
	parser = argparse.ArgumentParser(description="unzip encrypt compress file")
	parser.add_argument("-f", dest = "zname", type = str, \
		help = "specify zip file")
	parser.add_argument("-d", dest = "dname", type = str,\
		help = "specify dictionary file")

	args = parser.parse_args()
	zname = args.zname
	dname = args.dname
	if zname is None or dname is None:
		parser.print_help()
		sys.exit()
		
	if zipfile.is_zipfile(zname) and os.path.isfile(dname):
		zFile = zipfile.ZipFile(zname)
		passFile = open(dname)
		for line in passFile:
			password = line.strip()
			t = Thread(target = extractFile, args = (zFile, password))
			t.start()
	else:
		parser.print_help()

if __name__ == '__main__':
	main()
	
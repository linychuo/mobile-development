import crypt

def testPass(cryptPass):
	salt = cryptPass[:2]
	dictFile = open("dict.txt", 'r')
	for word in dictFile:
		word = word.strip()
		cryptWord = crypt.crypt(word, salt)
		if(cryptWord == cryptPass):
			print "[+] Found password: " + word + "\n"
			return

	print "[-] Password Not Found.\n"
	dictFile.close()
	return

def main():
	passFile = open('password.txt')
	for line in passFile:
		if ":" in line:
			user = line.split(":")[0]
			cryptPass = line.split(":")[1].strip()
			print "[*] Cracking password For: " + user
			testPass(cryptPass)

	passFile.close()

if __name__ == '__main__':
	main()
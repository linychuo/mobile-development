def fib_non_recursive(stop=1000):
	if not isinstance(stop, int):
		raise ValueError("The parameter of 'stop' must be integer")
	first = 0
	second = 1

	result = [first, second]
	while second < stop:
		s = first + second
		result.append(s)
		first = second
		second = s

	return result


def fib_recursive(result=[], stop=1000):
	if not isinstance(result, list):
		raise ValueError("The parameter of 'result' must be list")
	if len(result) < 2:
		raise ValueError("The parameter of 'result' has at least two elemnts")
	if not isinstance(stop, int):
		raise ValueError("The parameter of 'stop' must be integer")
		
	s = result[len(result)-2] + result[len(result)-1]
	result.append(s)

	if result[len(result)-1] < stop:
		fib_recursive(result)


result=[0,1]
fib_recursive(result)
print result

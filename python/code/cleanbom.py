# -*- coding: utf-8 -*-
import os
import codecs
import sys

def clean(fpath):
	with open(fpath, 'r') as f:
		f.seek(3)
		with open(fpath + '.new', 'w') as nf:
			nf.write(f.read())
			os.rename(nf.name, f.name)

def isBomFile(fpath):
	with open(fpath, 'rb') as f:
		return f.read(3) == codecs.BOM_UTF8 

def visit(arg, dirname, names):
	for item in names:
		path = os.path.join(dirname, item)
		if os.path.splitext(item)[1] == '.java':
			if isBomFile(path):
				print path
				clean(path)

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print "Usage: python cleanbom.py /xxxx/xxx or c:\d"
	else:
		if not os.path.isdir(sys.argv[1]):
			print "please enter an absolute path directory"
		else:
			os.path.walk(sys.argv[1], visit, None)
	
